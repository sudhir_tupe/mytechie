<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Gurdeep singh osahan">
      <meta name="author" content="Gurdeep singh osahan">
      <title>AgroMart</title>
      <!-- Favicon Icon -->
      <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
   </head>
   <body>
      <div class="bg-light">
         <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +1-123-456-7890</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold">$499</span></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav shadow-sm">
         <jsp:include page="navbar.jsp"/>
         </div>
      </div>
      <!-- About -->
      <section class="py-5 about-us btn-primary">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 mx-auto text-center">
                  <img class="rounded img-fluid" src="img/about.png" alt="Card image cap">
                  <h2 class="mt-4 mb-1 text-white">What is <b>"shpoee"</b>?</h2>
                  <p class="mb-3 mt-3 h5 text-white">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more.
                  </p>
                  <p class="text-white">When looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, Lorem Ipsum has been the industry's standard dummy text ever since, as opposed to using 'Content here, Lorem Ipsum has been the industry's standard dummy text ever since.
                  </p>
                  <div class="row mt-5">
                     <div class="col-lg-6">
                        <div class="text-left bg-white p-4 shadow-sm rounded">
                           <h5 class="text-dark mt-0">Our Vision</h5>
                           <p class="text-dark mb-0">When looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution</p>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="text-left bg-white p-4 shadow-sm rounded">
                           <h5 class="text-dark mt-0">Our Mission</h5>
                           <p class="text-dark mb-0">It is a long established fact that a of using Lorem Ipsum is that it has a more-or-less normal distribution</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- End About -->
      <!-- What We Provide -->
      <section class="pt-5 pb-4 pbc-5  box-provide bg-white text-center">
         <div class="section-title text-center mb-4">
            <h2 class="text-dark">What We Provide?</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-lg-4 col-md-4 px-5">
                  <div class="mt-4 mb-4"><i class="icofont icofont-cart icofont-5x text-danger"></i></div>
                  <h5 class="mt-3 mb-3">Best Prices &amp; Offers</h5>
                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
               </div>
               <div class="col-lg-4 col-md-4 px-5">
                  <div class="mt-4 mb-4"><i class="icofont icofont-earth icofont-5x text-danger"></i></div>
                  <h5 class="mb-3">Wide Assortment</h5>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text eve.</p>
               </div>
               <div class="col-lg-4 col-md-4 px-5">
                  <div class="mt-4 mb-4"><i class="icofont icofont-refresh icofont-5x text-danger"></i></div>
                  <h5 class="mt-3 mb-3">Easy Returns</h5>
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using.</p>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-4 col-md-4 px-5">
                  <div class="mt-4 mb-4"><i class="icofont icofont-truck icofont-5x text-danger"></i></div>
                  <h5 class="mb-3">Free &amp; Next Day Delivery</h5>
                  <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>
               </div>
               <div class="col-lg-4 col-md-4 px-5">
                  <div class="mt-4 mb-4"><i class="icofont icofont-basket icofont-5x text-danger"></i></div>
                  <h5 class="mt-3 mb-3">100% Satisfaction Guarantee</h5>
                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
               </div>
               <div class="col-lg-4 col-md-4 px-5">
                  <div class="mt-4 mb-4"><i class="icofont icofont-tag icofont-5x text-danger"></i></div>
                  <h5 class="mt-3 mb-3">Great Daily Deals Discount</h5>
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using.</p>
               </div>
            </div>
         </div>
      </section>
      <!-- End What We Provide -->
      <!-- Our Team -->
      <section class="py-5 bg-warning">
         <div class="section-title text-center mb-5">
            <h2>Our Team</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-lg-4 col-md-4">
                  <div class="team-card text-center bg-white p-5 shadow-sm rounded">
                     <img class="img-fluid mb-4" src="img/user/1.jpg" alt="">
                     <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</p>
                     <h6 class="mb-0 text-info">- Stave Martin</h6>
                     <small>Manager</small>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="team-card text-center bg-white p-5 shadow-sm rounded">
                     <img class="img-fluid mb-4" src="img/user/2.jpg" alt="">
                     <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</p>
                     <h6 class="mb-0 text-info">- Mark Smith</h6>
                     <small>Designer</small>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="team-card text-center bg-white p-5 shadow-sm rounded">
                     <img class="img-fluid mb-4" src="img/user/3.jpg" alt="">
                     <p class="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</p>
                     <h6 class="mb-0 text-info">- Ryan Printz</h6>
                     <small>Marketing</small>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- End Our Team -->
      <!-- Footer -->
     <jsp:include page="footer.jsp" />
      <div class="cart-sidebar">
         <div class="cart-sidebar-header">
            <h5>
               My Cart <span class="text-info">(5 item)</span> <a data-toggle="offcanvas" class="float-right" href="#"><i class="icofont icofont-close-line"></i>
               </a>
            </h5>
         </div>
         <div class="cart-sidebar-body">
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/1.jpg" alt="">
               <span class="badge badge-success">50% OFF</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$135.00 <del class="small text-secondary">$ 500.00 </del></p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/2.jpg" alt="">
               <span class="badge badge-danger">55% OFF</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$250.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-info rounded-sm pl-1 ml-1 pr-1 text-white small">NEW</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>           
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/3.jpg" alt="">
               <span class="badge badge-info">NEW</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$900.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small"> 50% OFF</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/4.jpg" alt="">
               <span class="badge badge-danger">NEW</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$135.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small"> 50% OFF</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/5.jpg" alt="">
               <span class="badge badge-info">NEW</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$135.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small"> 50% OFF</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
         </div>
         <div class="cart-sidebar-footer">
            <div class="cart-store-details">
               <p>Sub Total <strong class="float-right">$900.69</strong></p>
               <p>Delivery Charges <strong class="float-right text-danger">+ $29.69</strong></p>
               <h6>Your total savings <strong class="float-right text-danger">$55 (42.31%)</strong></h6>
            </div>
            <a href="checkout.html"><button class="btn btn-primary btn-lg btn-block text-left" type="button"><span class="float-left"><i class="icofont icofont-cart"></i> Proceed to Checkout </span><span class="float-right"><strong>$1200.69</strong> <span class="icofont icofont-bubble-right"></span></span></button></a>
         </div>
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Owl Carousel -->
      <script src="vendor/owl-carousel/owl.carousel.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
   </body>
</html>