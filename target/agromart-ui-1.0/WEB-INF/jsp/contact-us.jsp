<!DOCTYPE html>
<html lang="en">
   <head>
  <meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="AGRODHAN">
	  <meta name="Keywords" content="Online Agro Shopping in India,online Agro Shopping store,Online Agro Shopping Site, Buy Online,Shop Online,Online Shopping,Agrodhan"/> 
	  <meta name="Description" content="India&#x27;s biggest online agro store for Vegetable Seeds,Field Crops Seeds,Fertilisers,Water Soluble fertilisers,Organic Products,Pesticides,Agri Equipments,Micronutrients,Insecticides,Fungicides,Herbicides,Bio Pesticides,Flower Seeds,Onion Seeds from all brands at the lowest prices in India. Payment options - COD, EMI, Credit card, Debit card &amp;amp; more."/>
	<meta name="google-site-verification" content="3RnLdERPVodJ4P2YohoQWVROxQqbmN8xzF-KRmOpc2k"/>
   <title>Online Agro Shopping Site for Vegetable Seeds, Agro Chemicals &amp; More. Best Offers!</title>
     <!-- Favicon Icon -->
      <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
   </head>
   <body>
    <input type="hidden" value="${isUser}" id="isUser">
      <div class="bg-light">
         <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +91-770962647</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold"><i
							class="icofont-rupee"></i>10,000</span></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav shadow-sm">
            <jsp:include page="navbar.jsp"/>
         </div>
      </div>
      <section class="py-4 bg-light inner-header">
         <div class="container">
            <div class="row d-flex align-items-center">
               <div class="col-lg-6 col-md-6">
                  <h4 class="mt-0 mb-0 text-dark">
                  Contact Us       
               </div>
               <div class="col-lg-6 col-md-6 text-right">
                  <div class="breadcrumbs">
                     <p class="mb-0"><a href="#"><i class="icofont-ui-home"></i> Home</a>  /  <span>Contact Us</span></p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Contact Us -->
      <section class="py-5 bg-light border-top">
         <div class="container">
            <div class="row">
               <div class="col-lg-4 col-md-4">
                  <div class="p-4 h-100 bg-white rounded overflow-hidden position-relative shadow-sm">
                     <h4 class="mt-0 mb-4 text-dark">Get In Touch</h4>
                     <h6 class="text-dark"><i class="icofont-location-pin pr-1"></i> Address :</h6>
                     <p class="pl-4">A wing 106,Mega Center,
Solapur Road,Magarpatta ,Hadapsar, Pune, Maharashtra 411013</p>
                     <h6 class="text-dark"><i class="icofont-smart-phone pr-1"></i> Phone :</h6>
                     <p class="pl-4">+91 7709462647</p>
                     <h6 class="text-dark"><i class="icofont-email pr-1"></i> Email :</h6>
                     <p class="pl-4">agrodhan2020@gmail.com</p>
                     <h6 class="text-dark"><i class="icofont-link pr-1"></i> Website :</h6>
                     <p class="pl-4"><a href="/">http://agrodhan.com/</a></p>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="p-4 bg-white rounded overflow-hidden position-relative shadow-sm">
                     <h4 class="mt-0 mb-4 text-dark">Feedback</h4>
                     <form name="sentMessage" id="contactForm" novalidate>
                        <div class="control-group form-group">
                           <div class="controls">
                              <label>Full Name <span class="text-danger">*</span></label>
                              <input type="text" placeholder="Full Name" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                              <p class="help-block"></p>
                           </div>
                        </div>
                        <div class="row">
                           <div class="control-group form-group col-md-6">
                              <label>Phone Number <span class="text-danger">*</span></label>
                              <div class="controls">
                                 <input type="tel" placeholder="Phone Number" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
                              </div>
                           </div>
                           <div class="control-group form-group col-md-6">
                              <div class="controls">
                                 <label>Email Address <span class="text-danger">*</span></label>
                                 <input type="email" placeholder="Email Address"  class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
                              </div>
                           </div>
                        </div>
                        <div class="control-group form-group">
                           <div class="controls">
                              <label>Message <span class="text-danger">*</span></label>
                              <textarea rows="4" cols="100" placeholder="Message"  class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                           </div>
                        </div>
                        <div id="success"></div>
                        <!-- For success/fail messages -->
                        <button type="submit" class="btn btn-success btn-sm float-right" disabled="disabled">Send Message</button>
                     </form>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4">
                  <div class="h-100 p-4 bg-white rounded overflow-hidden position-relative shadow-sm">
                     <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d109552.19658195564!2d75.78663251672796!3d30.900473637371658!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391a837462345a7d%3A0x681102348ec60610!2sLudhiana%2C+Punjab!5e0!3m2!1sen!2sin!4v1530462134939" width="100%" height="370" frameborder="0" style="border:0" allowfullscreen ></iframe> -->
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3783.550909325801!2d73.92368981489251!3d18.50399058741867!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c1f04b884c4f%3A0x1abdd9a03c7dbd28!2sMega%20Center%2C%20Solapur%20Rd%2C%20North%20Hadapsar%2C%20Hadapsar%2C%20Pune%2C%20Maharashtra%20411028!5e0!3m2!1sen!2sin!4v1608825190398!5m2!1sen!2sin" width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
                     
                     
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- End Contact Us -->
      <!-- Footer -->
   <jsp:include page="footer.jsp" />
      <div class="cart-sidebar">
         <div class="cart-sidebar-header">
            <h5>
               My Cart <span class="text-info">(0)</span> <a data-toggle="offcanvas" class="float-right" href="#"><i class="icofont icofont-close-line"></i>
               </a>
            </h5>
         </div>
         <div class="cart-sidebar-body">
            <div class="cart-list-product">
            </div>
         </div>
         <div class="cart-sidebar-footer">
            <div class="cart-store-details">
               <p>Sub Total <strong class="float-right">0.0</strong></p>
            </div>
            <a href="#"><button class="btn btn-success btn-lg btn-block text-left" type="button"><span class="float-left"><i class="icofont icofont-cart"></i> Proceed to Checkout </span><span class="float-right"><strong>0.0</strong> <span class="icofont icofont-bubble-right"></span></span></button></a>
         </div>
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Owl Carousel -->
      <script src="vendor/owl-carousel/owl.carousel.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
      <!-- Contact form JavaScript -->
      <!-- Do not edit these files! In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
      <script src="js/jqBootstrapValidation.js"></script>
      <script src="js/contact_me.js"></script>
      
       <script>
        $(document).ready(function() {
     	  var isUser=$("#isUser").val();
	  	if(isUser){
	  		$('#profileId li:contains(Login)').hide();
	  		$('#profileId li:contains(My Account)').show();
	  	}else{
	  		$('#profileId li:contains(My Account)').hide();
	  	}
      });

      </script>
      
   </body>
</html>