<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="AGROMART">
<meta name="author" content="AGROMART">
<title>AGROMART</title>
<!-- Favicon Icon -->
<link rel="icon" type="image/png" href="img/fav-icon.png">



<!-- Bootstrap core CSS -->
<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>

<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Slider CSS -->
<link rel="stylesheet" href="vendor/slider/slider.css">
<!-- Select2 CSS -->
<link href="vendor/select2/css/select2-bootstrap.css" />
<link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
<!-- Font Awesome-->
<link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
<link href="vendor/icofont/icofont.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
</head>
<body>

<button id="getProductDataBtn">GetData</button>
	<section class="product-list pbc-5 pb-4 pt-5 bg-white">
		<div class="container">
		<div id="product-container" class="row">
		</div>
	</div>
	</section>
	
	<script id="bestProductsTemplate">
		{{#each this}}
		<div class="col-6 col-md-3">
		<div class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
		<span class="like-icon"><a href="#"><i
		class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
	<span class="badge badge-danger">NEW</span> <img
	src="img/item/bhendi.jpg" class="card-img-top" alt="...">
</a>
		<div class="card-body">
			<h6 class="card-title mb-1">{{this.productName}}</h6>
			<div class="stars-rating">
				<i class="icofont icofont-star active"></i><i
					class="icofont icofont-star active"></i><i
					class="icofont icofont-star active"></i><i
					class="icofont icofont-star active"></i><i
					class="icofont icofont-star"></i> <span>613</span>
			</div>
			<p class="mb-0 text-dark">
				<i class="icofont-rupee"></i>{{this.price}}<span class="text-black-50"><del>
						<i class="icofont-rupee"></i>3000.00
					</del></span>
			</p>
		</div>
		</div>
		</div>
	 {{/each}}
	</script>
	<!-- End Of Cart -->
	
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- select2 Js -->
	<script src="vendor/select2/js/select2.min.js"></script>
	<!-- Owl Carousel -->
	<script src="vendor/owl-carousel/owl.carousel.js"></script>
	<!-- Slider Js -->
	<script src="vendor/slider/slider.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="js/custom.js"></script>
	<script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
	<link rel="stylesheet" href="js/demo.css?ver=3.4.0">
	<link rel="stylesheet"
		href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<script>
      (function($) {

			$('#getProductDataBtn').click(function(event) {
				$.ajax({
	  				url : "http://localhost:8080/api/products/best-selling"
	  			}).then(function(data) {
	  				console.log(data);
	  				createHTML(data);
	  			});
			});

			function createHTML(productData){
				var rawTemplate=document.getElementById("bestProductsTemplate").innerHTML;
				var compiledTemplate=Handlebars.compile(rawTemplate);
				var ourGeneratedHTML =	compiledTemplate(productData);

				console.log(ourGeneratedHTML);

				var productContainer=document.getElementById("product-container");
				productContainer.innerHTML=ourGeneratedHTML;
				
				}
		})(jQuery);
	</script>
</body>
</html>