<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Gurdeep singh osahan">
<meta name="author" content="TECHIES">
<title>AGROMART</title>
<!-- Favicon Icon -->
<link rel="icon" type="image/png" href="img/fav-icon.png">

<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Select2 CSS -->
<link href="vendor/select2/css/select2-bootstrap.css" />
<link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
<!-- Font Awesome-->
<link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
<link href="vendor/icofont/icofont.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
<link href="vendor/datatables/datatables.min.css" rel="stylesheet" />
<script src="vendor/datatables/datatables.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
<link rel="stylesheet" href="js/demo.css?ver=3.4.0">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="edit-profile-modal" tabindex="-1"
		role="dialog" aria-labelledby="edit-profile" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="edit-profile">Edit profile</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-row">
							<div class="form-group col-md-12">
								<label>Phone number </label> <input type="text"
									value="+91 9766088848" class="form-control"
									placeholder="Enter Phone number">
							</div>
							<div class="form-group col-md-12">
								<label>Email id </label> <input type="text"
									value="agroadmin@gmail.com" class="form-control"
									placeholder="Enter Email id
                              ">
							</div>
							<div class="form-group col-md-12 mb-0">
								<label>Password </label> <input type="password"
									value="**********" class="form-control"
									placeholder="Enter password">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-outline-primary"
						data-dismiss="modal">CANCEL</button>
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-primary">UPDATE</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="delete-address-modal" tabindex="-1"
		role="dialog" aria-labelledby="delete-address" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="delete-address">Add New Company</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mb-0 text-black">Are you sure you want to add this
						company?</p>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-outline-primary"
						data-dismiss="modal">CANCEL</button>
					<button type="button" id="submitBtn"
						class="btn d-flex w-50 text-center justify-content-center btn-primary">SUBMIT</button>
				</div>
				<div id="successDiv" class="alert alert-success">Company
					Registered successfully</div>
				<div id="errorDiv" class="hide"></div>

			</div>
		</div>
	</div>
	<div class="bg-light shadow-sm">
		<div class="header-top border-bottom bg-white">
			<div class="container">
				<div class="row">
					<div class="col-lg-12"></div>
				</div>
			</div>
		</div>
		<div class="main-nav shadow-sm">
			<jsp:include page="navbar.jsp" />
		</div>
	</div>
	<section class="py-5 account-page bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div
						class="osahan-account-page-left overflow-hidden shadow-sm rounded bg-white h-100">
						<div class="p-4">
							<div class="osahan-user text-center">
								<div class="osahan-user-media">
									<img class="mb-3 rounded-pill shadow-sm mt-1"
										src="img/user/admin.png" alt="gurdeep singh osahan">
									<div class="osahan-user-media-body">
										<h6 class="mb-2 font-weight-bold">AgroMart Admin</h6>
										<p class="mb-1">+91 9766088848</p>
										<p>admin@agromart.com</p>
										<p class="mb-0 font-weight-bold">
											<a class="btn btn-outline-info btn-sm" data-toggle="modal"
												data-target="#edit-profile-modal" href="#"><i
												class="icofont-ui-edit"></i> EDIT</a>
										</p>
									</div>
								</div>
							</div>
						</div>
						<ul class="nav nav-tabs flex-column border-0" id="myTab"
							role="tablist">
							<li class="nav-item"><a class="nav-link" id="my-address-tab"
								data-toggle="tab" href="#my-profile" role="tab"
								aria-controls="my-address" aria-selected="false"><i
									class="icofont-company"></i> Add New Company</a></li>
							<li class="nav-item"><a class="nav-link" id="my-address-tab"
								data-toggle="tab" href="#my-address" role="tab"
								aria-controls="my-address" aria-selected="false"><i
									class="icofont-business-man"></i> Add New Dealer</a></li>
							<li class="nav-item"><a class="nav-link active"
								id="order-list-tab" data-toggle="tab" href="#order-list"
								role="tab" aria-controls="order-list" aria-selected="false"><i
									class="icofont-list"></i> Company List</a></li>


							<li class="nav-item"><a class="nav-link"
								id="order-status-tab" data-toggle="tab" href="#order-status"
								role="tab" aria-controls="order-status" aria-selected="false"><i
									class="icofont-listine-dots"></i> Dealer List</a></li>


							<li class="nav-item"><a class="nav-link"
								id="order-status-tab" data-toggle="tab" href="#assignDealer"
								role="tab" aria-controls="#map-dealer" aria-selected="false"><i
									class="icofont-users"></i>Assign Dealer</a></li>

							<li class="nav-item"><a class="nav-link"
								id="order-status-tab" data-toggle="tab" href="#adminDashboard"
								role="tab" aria-controls="#map-dealer" aria-selected="false"><i
									class="icofont-bars"></i>Dashboard</a></li>

							<li class="nav-item"><a class="nav-link" href="/"><i
									class="icofont-logout"></i> Logout</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div
						class="osahan-account-page-right rounded shadow-sm bg-white p-4 h-100">
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade" id="my-profile" role="tabpanel"
								aria-labelledby="my-profile-tab">
								<h4 class="text-dark mt-0 mb-4">Add New Company</h4>
								<p></p>
								<form:form modelAttribute="companyDTO" id="frm"
									action="/newcompany" enctype="multipart/form-data">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company Name <span
													class="required">*</span></label>

												<form:input type="text" path="companyName"
													class="form-control border-form-control" id="companyName"
													placeholder="Company Name" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company Email <span
													class="required">*</span></label>

												<form:input type="email" path="companyEmailId"
													name="companyEmailId"
													class="form-control border-form-control"
													id="companyEmailId" placeholder="Company Email" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">GST Number <span
													class="required">*</span></label>
												<!-- <input
													class="form-control border-form-control" value=""
													placeholder="123 456 7890" type="number"> -->

												<form:input type="text" path="companyGSTNumber"
													class="form-control border-form-control"
													id="companyGSTNumber" placeholder="GST Number" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">PAN Number <span
													class="required">*</span></label>
												<form:input type="email" path="companyPanNumber"
													class="form-control  border-form-control"
													id="companyPanNumber" placeholder="PAN Number" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company License Number
													<span class="required">*</span>
												</label>
												<form:input type="email" path="companyLicenseNo"
													class="form-control border-form-control"
													id="companyLicenseNo" placeholder="License Number" />
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company License Valid
													Upto<span class="required">*</span>
												</label>
												<form:input type="text" id="companyLicenseExpiryDate"
													value="License Validity" path="companyLicenseExpiryDate"
													class="form-control border-form-control" />

											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company Auth Person
													Name <span class="required">*</span>
												</label>
												<form:input type="email" path="companyAuthPersonName"
													class="form-control border-form-control"
													id="companyAuthPersonName" placeholder="Auth Person Name" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Authorised Person No<span
													class="required">*</span>
												</label>
												<form:input type="text" path="contactNumber"
													class="form-control border-form-control" id="contactNumber"
													placeholder="Authorised Person No" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Company Registered
													Address <span class="required">*</span>
												</label>
												<form:textarea path="companyRegAddress"
													class="form-control border-form-control"
													id="companyRegAddress"
													placeholder="Company Registered Address"></form:textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Company Storage Address
													<span class="required">*</span>
												</label>

												<form:textarea path="companyStorageAddress"
													class="form-control border-form-control"
													id="companyStorageAddress"
													placeholder="Company Storage Address"></form:textarea>
											</div>
										</div>
									</div>
									<form:hidden path="principalCertificateFileName"
										id="principalCertificateFileName" />
								</form:form>

								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label">Upload Principal
												Certificate <span class="required">*</span>
											</label>
											<form method="POST" enctype="multipart/form-data"
												id="uploadForm">

												<input type="file" name="file" />
												<button type="submit" class="btn" id="uploadBtn">Upload</button>


											</form>
										</div>
									</div>
								</div>

								<div id="uploadSuccessDiv" class="alert alert-success"></div>
								<div id="uploadErrorDiv" class="hide"></div>

								<div class="row">
									<div class="col-sm-12 text-right">
										<button type="reset" class="btn btn-secondary">
											Cancel</button>
										<button type="button" class="btn btn-primary"
											data-toggle="modal" data-target="#delete-address-modal">
											Save Changes</button>
									</div>
								</div>
							</div>

							<!--  Dealer Registration -->
							<div class="tab-pane fade" id="my-address" role="tabpanel"
								aria-labelledby="my-profile-tab">
								<h4 class="text-dark mt-0 mb-4">Add New Dealer</h4>
								<p></p>
								<form:form modelAttribute="dealerDTO" id="dealerForm"
									action="/newdealer" enctype="multipart/form-data">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer Name <span
													class="required">*</span></label>

												<form:input type="text" path="dealerName"
													class="form-control border-form-control" id="dealerName"
													placeholder="Dealer Name" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer Email <span
													class="required">*</span></label>

												<form:input type="email" path="dealerEmail"
													name="dealerEmail" class="form-control border-form-control"
													id="dealerEmail" placeholder="Dealer Email" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">GST Number <span
													class="required">*</span></label>
												<form:input type="text" path="dealerGSTNumber"
													class="form-control border-form-control"
													id="dealerGSTNumber" placeholder="GST Number" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">PAN Number <span
													class="required">*</span></label>
												<form:input type="email" path="dealerPanNumber"
													class="form-control  border-form-control"
													id="dealerPanNumber" placeholder="PAN Number" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer License Number <span
													class="required">*</span>
												</label>
												<form:input type="email" path="dealerLicenseNumber"
													class="form-control border-form-control"
													id="dealerLicenseNumber"
													placeholder="Dealer License Number" />
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer License Valid
													Upto<span class="required">*</span>
												</label>
												<form:input type="text" id="dealerLicenseExpiryDate"
													value="License Validity" path="dealerLicenseExpiryDate"
													class="form-control border-form-control" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Proprietor Name <span
													class="required">*</span>
												</label>
												<form:input type="email" path="proprietorName"
													class="form-control border-form-control"
													id="proprietorName" placeholder="Proprietor Name" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Contact No<span
													class="required">*</span>
												</label>
												<form:input type="text" path="dealerContactNumber"
													class="form-control border-form-control"
													id="dealerContactNumber"
													placeholder="Delaer Contact Number" />

											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Dealer Registered
													Address <span class="required">*</span>
												</label>
												<form:textarea path="registerOfficeAddress"
													class="form-control border-form-control"
													id="registerOfficeAddress"
													placeholder="Dealer Registered Address"></form:textarea>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Dealer Storage Address
													<span class="required">*</span>
												</label>

												<form:textarea path="storageAddress"
													class="form-control border-form-control"
													id="storageAddress" placeholder="Dealer Storage Address"></form:textarea>
											</div>
										</div>
									</div>
								</form:form>
								<div class="row">
									<div class="col-sm-12 text-right">
										<button type="button" class="btn btn-secondary">
											Cancel</button>
										<button type="button" id="dealerSubmitBtn"
											class="btn btn-primary">Save Changes</button>
										<div id="dealerSuccessDiv" class="alert alert-success">Dealer
											Registered successfully</div>
										<div id="dealerErrorDiv" class="hide"></div>
									</div>
								</div>
							</div>

							<!-- End of Dealer Registration -->

							<!--  Assign Dealer -->


							<div class="tab-pane fade" id="assignDealer" role="tabpanel"
								aria-labelledby="my-profile-tab">
								<h4 class="text-dark mt-0 mb-4">Assign Dealer</h4>
								<p></p>

								<form:form modelAttribute="companyDealerDTO"
									id="companyDealerForm" action="/addCompanyDealer">
									<table>

										<tr>
											<td><label class="control-label">Company Name <span
													class="required">*</span></label></td>
											<td><form:select path="companyId" class="form-control"
													id="companySelect">
													<option value="">Select Company</option>
													<c:forEach items="${companyDTOs}" var="companyDTO">
														<form:option value="${companyDTO.companyId}">
															<c:out value="${companyDTO.companyName}" />
														</form:option>
													</c:forEach>
												</form:select></td>
										</tr>
										<tr>
											<td><br> <label class="control-label">Dealer
													Name <span class="required">*</span>
											</label></td>

											<td><br> <form:select path="dealerId"
													class="form-control" id="dealerSelect">
													<option value="">Select Dealer</option>
													<c:forEach items="${dealerDTOs}" var="dealerDTO">
														<form:option value="${dealerDTO.dealerId}">
															<c:out value="${dealerDTO.dealerName}" />
														</form:option>
													</c:forEach>
												</form:select></td>
										</tr>

										<tr>
											<td><br>
												<button type="button" class="btn btn-outline-secondary">
													Cancel</button></td>

											<td><br>
												<button type="button" id="assignDealerSubmitBtn"
													class="btn btn-primary">Assign</button></td>
										</tr>

									</table>
								</form:form>
								<div id="assignDealerSuccessDiv" class="alert alert-success">Dealer
									Assigned successfully</div>
								<div id="assignDealerErrorDiv" class="hide"></div>
								<br> <br>
								<table class="table table-striped" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th>Company Name</th>
											<th>Dealer Name</th>
											<th>Assignment Date</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${companyDealerDTOs}" var="cddto">
											<tr>
												<td><c:out value="${cddto.companyName}"></c:out></td>
												<td><c:out value="${cddto.dealerName}"></c:out></td>
												<td><c:out value="${cddto.creationDate}"></c:out></td>
												<td><c:out value="${cddto.dealerStatus}"></c:out></td>
												<td><a data-toggle="tooltip" data-placement="top"
													title="" href="#" data-original-title="View Detail"
													class="btn btn-info btn-sm"><i class="icofont-eye-alt"></i></a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>


							<!-- End of Assing Dealer -->



							<div class="tab-pane fade" id="adminDashboard" role="tabpanel"
								aria-labelledby="my-profile-tab">

								<h4 class="text-dark mt-0 mb-4">Dashboard</h4>
								<div class="order-list-tabel-main table-responsive">
									
								</div>
							</div>

							<div class="tab-pane fade show active" id="order-list"
								role="tabpanel" aria-labelledby="order-list-tab">
								<h4 class="text-dark mt-0 mb-4">Company List</h4>
								<div class="order-list-tabel-main table-responsive">
									<table class="datatabel table table-striped order-list-tabel"
										width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>Company Name</th>
												<th>Company Email</th>
												<th>Company Auth Person</th>
												<th>Contact Number</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${companyDTOs}" var="cdto">
												<tr>
													<td><c:out value="${cdto.companyName}"></c:out></td>
													<td><c:out value="${cdto.companyEmailId}"></c:out></td>
													<td><c:out value="${cdto.companyAuthPersonName}"></c:out>
													</td>
													<td><c:out value="${cdto.contactNumber}"></c:out></td>
													<td><a data-toggle="tooltip" data-placement="top"
														title="" href="#" data-original-title="View Detail"
														class="btn btn-info btn-sm"><i class="icofont-eye-alt"></i></a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>




							<div class="tab-pane fade" id="order-status" role="tabpanel"
								aria-labelledby="order-status-tab">
								
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer -->
	<footer class="bg-white border-bottom border-top">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-md-4">
					<div class="border-right py-5 pr-5">
						<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">TOP
							CATEGORIES</h6>
						<div class="row no-gutters">
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">Vegetable Seeds</a></li>
									<li><a href="#"> Field Crops Seeds </a></li>
									<li><a href="#">Water Soluble fertilisers</a></li>
									<li><a href="#">Organic Products </a></li>
									<li><a href="#">Pesticides</a></li>
								</ul>
							</div>
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#"> Agri Equipments</a></li>
									<li><a href="#">Micronutrients</a></li>
									<li><a href="#">Insecticides</a></li>
									<li><a href="#">Herbicides</a></li>
									<li><a href="#"> Bio Pesticides</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="border-right py-5 px-5">
						<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">ABOUT
							US</h6>
						<div class="row no-gutters">
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">History</a></li>
									<li><a href="#">Band of Trust</a></li>
									<li><a href="#">Brand Guidelines</a></li>
									<li><a href="#">TV Commercials</a></li>
									<li><a href="#">In the News </a></li>
								</ul>
							</div>
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">Awards</a></li>
									<li><a href="#">Terms & Conditions</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Careers</a></li>
									<li><a href="#">Offers</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="py-5 pl-5">
						<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">DOWNLOAD
							APP</h6>
						<div class="app">
							<a href="#"> <img class="img-fluid" src="img/google.png">
							</a> <a href="#"> <img class="img-fluid" src="img/apple.png">
							</a>
						</div>
						<h6 class="mt-4 mb-4 f-14 text-dark font-weight-bold">KEEP IN
							TOUCH</h6>
						<div class="footer-social">
							<a class="btn-facebook" href="#"><i class="icofont-facebook"></i></a>
							<a class="btn-twitter" href="#"><i class="icofont-twitter"></i></a>
							<a class="btn-instagram" href="#"><i
								class="icofont-instagram"></i></a> <a class="btn-whatsapp" href="#"><i
								class="icofont-whatsapp"></i></a> <a class="btn-messenger" href="#"><i
								class="icofont-facebook-messenger"></i></a> <a class="btn-google"
								href="#"><i class="icofont-google-plus"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</footer>
	<div class="popular-tag py-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">

					<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">POPULAR
						SEARCHES</h6>
					<p class="mb-0">
						<a href="#">Seeds </a> &nbsp; | &nbsp; <a href="#">Flower
							Seeds </a> &nbsp; | &nbsp; <a href="#">Vegetable Seeds </a> &nbsp; |
						&nbsp; <a href="#">Field Crops Seeds </a> &nbsp; | &nbsp; <a
							href="#">Fertilisers </a> &nbsp; | &nbsp; <a href="#">Water
							Soluble fertilisers</a> &nbsp; | &nbsp; <a href="#">Organic
							Products </a> &nbsp; | &nbsp; <a href="#">Pesticides </a> &nbsp; |
						&nbsp; <a href="#">Agri Equipments </a> &nbsp; | &nbsp; <a
							href="#">Micronutrients </a> &nbsp; | &nbsp; <a href="#">Insecticides
						</a> &nbsp; | &nbsp; <a href="#">Fungicides </a> &nbsp; | &nbsp; <a
							href="#">Herbicides </a> &nbsp; | &nbsp; <a href="#">Bio
							Pesticides </a> &nbsp; | &nbsp; <a href="#">Surfactants </a> &nbsp; |
						&nbsp; <a href="#">Organic Premium PGR </a> &nbsp; | <a href="#">Organic
							Coated Granules </a> &nbsp; | &nbsp; <a href="#">Organic PGR
							Technicals </a> &nbsp; | &nbsp; <a href="#">Micronutrients Spray
						</a> &nbsp; | &nbsp; <a href="#">Individual Micronutrient </a> &nbsp;
						| &nbsp; <a href="#">Micronutrient Mixture </a> &nbsp; | &nbsp; <a
							href="#">Organic Neem Products </a> &nbsp; | &nbsp; <a href="#">Organic
							Stimulant </a> &nbsp; | &nbsp; <a href="#">Starters </a> &nbsp; |
						&nbsp; <a href="#">Pumps </a> &nbsp; | &nbsp; <a href="#">Agro
							Sprayers </a> &nbsp; | &nbsp; <a href="#">Agro Shed Nets </a> &nbsp;
						| &nbsp; <a href="#">House Hold Insecticides </a> &nbsp; | &nbsp;
						<a href="#">Drip Irrigation </a> &nbsp; | &nbsp; <a href="#">Pulses
							& Beans </a> &nbsp; | &nbsp; <a href="#">Home / kitchen Garden
							Seeds </a> &nbsp; | &nbsp; <a href="#">Mulching Film </a> &nbsp; |
						&nbsp; <a href="#">Cotton Seeds</a> &nbsp;
					</p>

				</div>
			</div>
		</div>
	</div>
	<div class="copyright bg-light py-3">
		<div class="container">
			<div class="row">
				<div class="col-md-6 d-flex align-items-center">
					<p class="mb-0">
						� Copyright 2020 <a href="#">TECHIES</a> . All Rights Reserved
					</p>
				</div>
				<div class="col-md-6 text-right">
					<img class="img-fluid" src="img/payment_methods.png">
				</div>
			</div>
		</div>
	</div>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- select2 Js -->
	<script src="vendor/select2/js/select2.min.js"></script>
	<!-- Owl Carousel -->
	<script src="vendor/owl-carousel/owl.carousel.js"></script>

	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Data Tables -->
	<link href="vendor/datatables/datatables.min.css" rel="stylesheet" />
	<script src="vendor/datatables/datatables.min.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
	<link rel="stylesheet" href="js/demo.css?ver=3.4.0" />
	<link rel="stylesheet"
		href="https://fonts.googleapis.com/icon?family=Material+Icons" />

	<script>

(function($) {
		$("#uploadSuccessDiv").hide();
		$("#uploadErrorDiv").hide();

		$("#assignDealerSuccessDiv").hide();
		$("#assignDealerErrorDiv").hide();
		
		
		
		$("#dealerSuccessDiv").hide();
		$("#dealerErrorDiv").hide();
		  
		
		$("#successDiv").hide();
		$("#errorDiv").hide();


		$("#successDiv").hide();
		$("#errorDiv").hide();

		$("#companyLicenseExpiryDate").datepicker({
			  dateFormat: "yy-mm-dd"
		});


		$("#dealerLicenseExpiryDate").datepicker({
			  dateFormat: "yy-mm-dd"
		});
	      
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultOptions = {
      disableAt: false,
      customToggle: $toggle,
      levelSpacing: 40,
      navTitle: 'All Categories',
      levelTitles: true,
      levelTitleAsBack: true,
      pushContent: '#container',
      insertClose: 2
    };

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultOptions);

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
      var $this = $(this);
      var $li = $this.parent();
      var items = eval('(' + $this.attr('data-add') + ')');

      $li.before('<li class="new"><a href="#">'+items[0]+'</a></li>');

      items.shift();

      if (!items.length) {
        $li.remove();
      }
      else {
        $this.attr('data-add', JSON.stringify(items));
      }

      Nav.update(true);
    });

    // demo settings update

    const update = (settings) => {
      if (Nav.isOpen()) {
        Nav.on('close.once', function() {
          Nav.update(settings);
          Nav.open();
        });

        Nav.close();
      }
      else {
        Nav.update(settings);
      }
    };

    $('.actions').find('a').on('click', function(e) {
      e.preventDefault();

      var $this = $(this).addClass('active');
      var $siblings = $this.parent().siblings().children('a').removeClass('active');
      var settings = eval('(' + $this.data('demo') + ')');

      update(settings);
    });

    $('.actions').find('input').on('change', function() {
      var $this = $(this);
      var settings = eval('(' + $this.data('demo') + ')');

      if ($this.is(':checked')) {
        update(settings);
      }
      else {
        var removeData = {};
        $.each(settings, function(index, value) {
          removeData[index] = false;
        });

        update(removeData);
      }
    });

    $('#uploadBtn').click(function(event) {
    	event.preventDefault();
		uploadForm();
	});

    $('#submitBtn').click(function(event) {
		event.preventDefault();
		companySubmitForm();
	});

    $('#dealerSubmitBtn').click(function(event) {
    	event.preventDefault();
    	dealerSubmitForm();
	});

    $('#assignDealerSubmitBtn').click(function(event) {
    	event.preventDefault();
    	companyDealerSubmitForm();
	});


    $("#Logout").click(function(){
        alert("Inside logout");
        $form=$("<form>").attr({"action":"${pageContext.request.contextPath}"+"/logout","method":"post"})
        .append($("<input>").attr({"type":"hidden","name":"${_csrf.parameterName}","value":"${_csrf.token}"}))
        $("#Logout").append($form);
        $form.submit();
    });

function companyDealerSubmitForm(){
	var companyDealerDTO = {};
	companyDealerDTO["companyId"] =$("#companySelect option:selected").val()
	companyDealerDTO["dealerId"] = $("#dealerSelect option:selected").val()
	companyDealerDTO["companyName"] = $( "#companySelect option:selected" ).text();
	companyDealerDTO["dealerName"] = $( "#dealerSelect option:selected" ).text();
	
	$.ajax({
		type : "POST",
		url : "/addCompanyDealer",
		contentType : "application/json",
		processData : false,
		data : JSON.stringify(companyDealerDTO),
		success : function(data) {
			console.log(data.actionPassed)
			if (data.actionPassed == false) {
				$("#assignDealerSubmitBtn").prop("disabled", false);
				return;
			} else {
				$("#assignDealerSubmitBtn").prop("disabled", false);
				$("#assignDealerSuccessDiv").fadeTo(2000, 500).slideUp(500,
						function() {
							$("#dealerSuccessDiv").slideUp(500);
				});
				window.location.href="/admin";
			}
		}
	});
	
}

    
    function dealerSubmitForm() {
		var dealerDTO = {}
		dealerDTO["dealerName"] = $("#dealerName").val();
		dealerDTO["dealerEmail"] = $("#dealerEmail").val();

		dealerDTO["dealerLicenseNumber"] = $("#dealerLicenseNumber").val();
		dealerDTO["dealerLicenseExpiryDate"] = $("#dealerLicenseExpiryDate").val();

		dealerDTO["dealerPanNumber"] = $("#dealerPanNumber").val();
		dealerDTO["dealerGSTNumber"] = $('#dealerGSTNumber').val();

		dealerDTO["storageAddress"] = $("#storageAddress").val();
		dealerDTO["registerOfficeAddress"] = $("#registerOfficeAddress")
				.val();

		dealerDTO["proprietorName"] = $("#proprietorName")
				.val();

		dealerDTO["dealerContactNumber"] = $("#dealerContactNumber").val();

		$("#dealerSubmitBtn").prop("disabled", true);
		
		$.ajax({
			type : "POST",
			url : "/newdealer",
			contentType : "application/json",
			processData : false,
			data : JSON.stringify(dealerDTO),
			success : function(data) {
				console.log(data.actionPassed)
				if (data.actionPassed == false) {
					$("#dealerSubmitBtn").prop("disabled", false);
					return;
				} else {
					$("#dealerSubmitBtn").prop("disabled", false);
					$("#dealerSuccessDiv").fadeTo(2000, 500).slideUp(500,
							function() {
								$("#dealerSuccessDiv").slideUp(500);
					});
					window.location.href="/admin";
				}
			}
		});
	}
    
    function uploadForm() {
			var form = $('#uploadForm')[0];

			var data = new FormData(form);
			
			$("#uploadBtn").prop("disabled", true);

			$.ajax({
				type : "POST",
				enctype : 'multipart/form-data',
				url : "/upload",
				contentType : false,
				processData : false,
				data : data,
				cache : false,
				timeout : 600000,
				success : function(data) {
					if (data.actionPassed == false) {
						$("#uploadBtn").prop("disabled", false);
						$("#uploadErrorDiv").show();
						return;
					} else {
						$('#principalCertificateFileName').val(data.message);
						$('#uploadSuccessDiv').html("File "+data.message+" upload success.");
						
						$("#uploadSuccessDiv").show();
						$("#uploadBtn").prop("disabled", false);
					}
				}
			});
		}

    function companySubmitForm() {

		var companyDTO = {}
		companyDTO["companyName"] = $("#companyName").val();
		companyDTO["companyEmailId"] = $("#companyEmailId").val();

		companyDTO["companyGSTNumber"] = $("#companyGSTNumber").val();
		companyDTO["companyPanNumber"] = $("#companyPanNumber").val();

		companyDTO["companyLicenseNo"] = $("#companyLicenseNo").val();
		companyDTO["companyLicenseExpiryDate"] = $('#companyLicenseExpiryDate').val();

		companyDTO["companyAuthPersonName"] = $("#companyAuthPersonName")
				.val();

		companyDTO["companyRegAddress"] = $("#companyRegAddress").val();
		companyDTO["companyStorageAddress"] = $("#companyStorageAddress")
				.val();

		companyDTO["companyAuthPersonName"] = $("#companyAuthPersonName")
				.val();

		companyDTO["contactNumber"] = $("#contactNumber").val();

		$("#submitBtn").prop("disabled", true);
		$.ajax({
			type : "POST",
			url : "/newcompany",
			contentType : "application/json",
			processData : false,
			data : JSON.stringify(companyDTO),
			success : function(data) {
				console.log(data.actionPassed)
				if (data.actionPassed == false) {
					$("#submitBtn").prop("disabled", false);
					return;
				} else {
					$("#submitBtn").prop("disabled", false);
					$("#successDiv").fadeTo(2000, 500).slideUp(500,
							function() {
								$("#successDiv").slideUp(500);
					});
					window.location.href="/admin";
				}
			}
		});
	}

})(jQuery);

	  



    </script>
</body>
</html>