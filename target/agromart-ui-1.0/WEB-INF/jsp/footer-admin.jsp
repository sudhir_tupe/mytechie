<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<!-- Footer -->
   <footer class="bg-white border-bottom border-top">
      <div class="container">
         <div class="row no-gutters">
            <div class="col-md-4">
               <div class="border-right py-5 pr-5">
                  <h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">TOP CATEGORIES</h6>
						<div class="row no-gutters">
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">Vegetable Seeds</a></li>
									<li><a href="#"> Field Crops Seeds </a></li>
									<li><a href="#">Water Soluble fertilisers</a></li>
									<li><a href="#">Organic Products </a></li>
									<li><a href="#">Pesticides</a></li>
								</ul>
							</div>
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#"> Agri Equipments</a></li>
									<li><a href="#">Micronutrients</a></li>
									<li><a href="#">Insecticides</a></li>
									<li><a href="#">Herbicides</a></li>
									<li><a href="#"> Bio Pesticides</a></li>
								</ul>
							</div>
						</div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="border-right py-5 px-5">
                  <h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">ABOUT US</h6>
                  <div class="row no-gutters">
                     <div class="col-6">
                        <ul class="list-unstyled mb-0">
                           <li><a href="#">History</a></li>
                           <li><a href="#">Band of Trust</a></li>
                           <li><a href="#">Brand Guidelines</a></li>
                           <li><a href="#">TV Commercials</a></li>
                           <li><a href="#">In the News
                              </a>
                           </li>
                        </ul>
                     </div>
                     <div class="col-6">
                        <ul class="list-unstyled mb-0">
                           <li><a href="#">Awards</a></li>
                           <li><a href="#">Terms & Conditions</a></li>
                           <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">Careers</a></li>
                           <li><a href="#">Offers</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="py-5 pl-5">
                  <h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">DOWNLOAD APP</h6>
                  <div class="app">
                     <a href="#">
                        <img class="img-fluid" src="img/google.png">
                     </a>
                     <a href="#">
                        <img class="img-fluid" src="img/apple.png">
                     </a>
                  </div>
                  <h6 class="mt-4 mb-4 f-14 text-dark font-weight-bold">KEEP IN TOUCH</h6>
                  <div class="footer-social">
                     <a class="btn-facebook" href="#"><i class="icofont-facebook"></i></a>
                     <a class="btn-twitter" href="#"><i class="icofont-twitter"></i></a>
                     <a class="btn-instagram" href="#"><i class="icofont-instagram"></i></a>
                     <a class="btn-whatsapp" href="#"><i class="icofont-whatsapp"></i></a>
                     <a class="btn-messenger" href="#"><i class="icofont-facebook-messenger"></i></a>
                     <a class="btn-google" href="#"><i class="icofont-google-plus"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /.container -->
   </footer>
   <div class="copyright bg-light py-3">
      <div class="container">
         <div class="row">
            <div class="col-md-6 d-flex align-items-center">
               <p class="mb-0">� Copyright 2020 <a href="#">TECHIES</a> . All Rights Reserved
               </p>
            </div>
            <div class="col-md-6 text-right">
               <img class="img-fluid" src="img/payment_methods.png">
            </div>
         </div>
      </div>
   </div>


</body>
</html>