<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="author" content="AGRODHAN">
	  <meta name="Keywords" content="Online Agro Shopping in India,online Agro Shopping store,Online Agro Shopping Site, Buy Online,Shop Online,Online Shopping,Agrodhan"/> 
	  <meta name="Description" content="India&#x27;s biggest online agro store for Vegetable Seeds,Field Crops Seeds,Fertilisers,Water Soluble fertilisers,Organic Products,Pesticides,Agri Equipments,Micronutrients,Insecticides,Fungicides,Herbicides,Bio Pesticides,Flower Seeds,Onion Seeds from all brands at the lowest prices in India. Payment options - COD, EMI, Credit card, Debit card &amp;amp; more."/>
      <title>Online Agro Shopping Site for Vegetable Seeds, Agro Chemicals &amp; More. Best Offers!</title>
      <!-- Favicon Icon -->
      <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Select2 CSS -->
      <link href="vendor/select2/css/select2-bootstrap.css" />
      <link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
   </head>
   <body>
   <input type="hidden" value='${isUser}' id="isUser">
      <div class="bg-light shadow-sm">
         <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +91-7709462647</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold"><i class="icofont-rupee"></i>5000</span></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav shadow-sm">
            <jsp:include page="navbar.jsp"/>
         </div>
      </div>
      <section class="py-5 products-listing bg-light">
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <div class="filters mobile-filters shadow-sm rounded bg-white mb-4 d-none d-block d-md-none">
                     <div class="border-bottom">
                        <a class="h6 font-weight-bold text-dark d-block m-0 p-3" data-toggle="collapse" href="#mobile-filters" role="button" aria-expanded="false" aria-controls="mobile-filters">Filter By <i class="icofont-arrow-down float-right mt-1"></i></a>
                     </div>
                     <div id="mobile-filters" class="filters-body collapse multi-collapse">
                        <div id="accordion">
                           <div class="filters-card border-bottom p-3">
                              <div class="filters-card-header" id="headingOffer">
                                 <h6 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseSort" aria-expanded="true" aria-controls="collapseSort">
                                    Sort by Products <i class="icofont-arrow-down float-right"></i>
                                    </a>
                                 </h6>
                              </div>
                              <div id="collapseSort" class="collapse" aria-labelledby="headingOffer" data-parent="#accordion">
                                 <div class="filters-card-body card-shop-filters">
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan111">
                                       <label class="custom-control-label" for="osahan111">Relevance </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan112">
                                       <label class="custom-control-label" for="osahan112">Price (Low to High)
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan113">
                                       <label class="custom-control-label" for="osahan113">Price (High to Low)
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan114">
                                       <label class="custom-control-label" for="osahan114">Discount (High to Low)
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan115">
                                       <label class="custom-control-label" for="osahan115">Name (A to Z)
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="filters-card border-bottom p-3">
                              <div class="filters-card-header" id="headingTwo">
                                 <h6 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                                    All Category
                                    <i class="icofont-arrow-down float-right"></i>
                                    </a>
                                 </h6>
                              </div>
                              <div id="collapsetwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                 <div class="filters-card-body card-shop-filters">
                                    <form class="filters-search mb-3">
                                       <div class="form-group">
                                          <i class="icofont-search"></i>                                 
                                          <input type="text" class="form-control" placeholder="Start typing to search...">
                                       </div>
                                    </form>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan116">
                                       <label class="custom-control-label" for="osahan116">Onion <small class="text-black-50">156</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan117">
                                       <label class="custom-control-label" for="osahan117">Tomato <small class="text-black-50">120</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan118">
                                       <label class="custom-control-label" for="osahan118">Potato <small class="text-black-50">130</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan119">
                                       <label class="custom-control-label" for="osahan119">Brinjal <small class="text-black-50">120</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan110">
                                       <label class="custom-control-label" for="osahan110"> CHILLI<small class="text-black-50">111</small></label>
                                    </div>
                                    <div class="mt-2"><a href="#" class="link">See all</a></div>
                                 </div>
                              </div>
                           </div>
                           <div class="filters-card border-bottom p-3">
                              <div class="filters-card-header" id="headingOne">
                                 <h6 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Brand <i class="icofont-arrow-down float-right"></i>
                                    </a>
                                 </h6>
                              </div>
                              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                 <div class="filters-card-body card-shop-filters">
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan1155">
                                       <label class="custom-control-label" for="osahan1155">Fusion Crop <small class="text-black-50">230</small>
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan1166">
                                       <label class="custom-control-label" for="osahan1166">Ajeet Seeds <small class="text-black-50">95</small>
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan1177">
                                       <label class="custom-control-label" for="osahan1177">A.G.Sunseeds<small class="text-black-50">35</small>
                                       </label>
                                    </div>
                                    <div class="mt-2"><a href="#" class="link">See all</a></div>
                                 </div>
                              </div>
                           </div>
                           <div class="filters-card border-bottom p-3">
                              <div class="filters-card-header" id="headingOffer">
                                 <h6 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseOffer" aria-expanded="true" aria-controls="collapseOffer">
                                    Price <i class="icofont-arrow-down float-right"></i>
                                    </a>
                                 </h6>
                              </div>
                              <div id="collapseOffer" class="collapse" aria-labelledby="headingOffer" data-parent="#accordion">
                                 <div class="filters-card-body card-shop-filters">
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan1100">
                                       <label class="custom-control-label" for="osahan1100">Any Price </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11111">
                                       <label class="custom-control-label" for="osahan11111"><i class="icofont-rupee"></i>50 - 100
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11222">
                                       <label class="custom-control-label" for="osahan11222"><i class="icofont-rupee"></i>100 - 150
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11333">
                                       <label class="custom-control-label" for="osahan11333"><i class="icofont-rupee"></i>150 - 200
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11444">
                                       <label class="custom-control-label" for="osahan11444"><i class="icofont-rupee"></i>200 - 1000
                                       </label>
                                    </div>
                                    <div class="mt-2"><a href="#" class="link">See all</a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="filters shadow-sm rounded bg-white mb-3 d-none d-sm-none d-md-block">
                     <div class="filters-header border-bottom pl-4 pr-4 pt-3 pb-3">
                        <h5 class="m-0 text-dark">Filter By</h5>
                     </div>
                     <div class="filters-body">
                        <div id="accordion">
                           <div class="filters-card border-bottom p-4">
                              <div class="filters-card-header" id="headingTwo">
                                 <h6 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                                    All Category
                                    <i class="icofont-arrow-down float-right"></i>
                                    </a>
                                 </h6>
                              </div>
                              <div id="collapsetwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                                 <div class="filters-card-body card-shop-filters">
                                    <form class="filters-search mb-3">
                                       <div class="form-group">
                                          <i class="icofont-search"></i>                                 
                                          <input type="text" class="form-control" placeholder="Start typing to search...">
                                       </div>
                                    </form>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11q">
                                       <label class="custom-control-label" for="osahan11q">Onion <small class="text-black-50">156</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11w">
                                       <label class="custom-control-label" for="osahan11w">Tomato<small class="text-black-50">120</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11e">
                                       <label class="custom-control-label" for="osahan11e">Bhendi <small class="text-black-50">130</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11r">
                                       <label class="custom-control-label" for="osahan11r">Brinjal <small class="text-black-50">120</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11y">
                                       <label class="custom-control-label" for="osahan11y">Chilli <small class="text-black-50">111</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11u">
                                       <label class="custom-control-label" for="osahan11u"> Cucumber <small class="text-black-50">95</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11i">
                                       <label class="custom-control-label" for="osahan11i"> Cabbage <small class="text-black-50">50</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11p">
                                       <label class="custom-control-label" for="osahan11p"> Cauliflower <small class="text-black-50">32</small></label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11po">
                                       <label class="custom-control-label" for="osahan11po"> Radish <small class="text-black-50">156</small></label>
                                    </div>
                                    <div class="mt-2"><a href="#" class="link">See all</a></div>
                                 </div>
                              </div>
                           </div>
                           <div class="filters-card border-bottom p-4">
                              <div class="filters-card-header" id="headingOne">
                                 <h6 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Company Brand <i class="icofont-arrow-down float-right"></i>
                                    </a>
                                 </h6>
                              </div>
                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                 <div class="filters-card-body card-shop-filters">
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11z">
                                       <label class="custom-control-label" for="osahan11z">Fusion Crop <small class="text-black-50">230</small>
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11zz">
                                       <label class="custom-control-label" for="osahan11zz">Ajeet Seeds <small class="text-black-50">95</small>
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11zzz">
                                       <label class="custom-control-label" for="osahan11zzz">Fusion Genetics <small class="text-black-50">35</small>
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11x">
                                       <label class="custom-control-label" for="osahan11x">A.G.Sunseeds <small class="text-black-50">46</small>
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11xx">
                                       <label class="custom-control-label" for="osahan11xx">Agro Biotech <small class="text-black-50">20</small></label>
                                    </div>
                                    <div class="mt-2"><a href="#" class="link">See all</a></div>
                                 </div>
                              </div>
                           </div>
                           <div class="filters-card border-bottom p-4">
                              <div class="filters-card-header" id="headingOffer">
                                 <h6 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseOffer" aria-expanded="true" aria-controls="collapseOffer">
                                    Price <i class="icofont-arrow-down float-right"></i>
                                    </a>
                                 </h6>
                              </div>
                              <div id="collapseOffer" class="collapse" aria-labelledby="headingOffer" data-parent="#accordion">
                                 <div class="filters-card-body card-shop-filters">
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11cc">
                                       <label class="custom-control-label" for="osahan11cc">Any Price </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11c">
                                       <label class="custom-control-label" for="osahan11c"><i class="icofont-rupee"></i>500 - 1000
                                       </label>
                                       
                                       
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11v">
                                       <label class="custom-control-label" for="osahan11v"><i class="icofont-rupee"></i>1000 - 2000
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11vv">
                                       <label class="custom-control-label" for="osahan11vv"><i class="icofont-rupee"></i>2000 - 3000
                                       </label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="osahan11gg">
                                       <label class="custom-control-label" for="osahan11gg"><i class="icofont-rupee"></i>3000 - 4000
                                       </label>
                                    </div>
                                    <div class="mt-2"><a href="#" class="link">See all</a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
				  <img src="img/offer-2.png" class="w-100 bg-white rounded overflow-hidden position-relative shadow-sm d-none d-sm-none d-md-block" alt="...">
               </div>
               <div class="col-md-9">
                  <div class="shop-head mb-3">
                     <div class="btn-group float-right mt-2 d-none d-sm-none d-md-block">
                        <button type="button" class="btn btn-dark btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icofont icofont-filter"></span> Sort by Products &nbsp;&nbsp;
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                           <a class="dropdown-item" href="#">Relevance</a>
                           <a class="dropdown-item" href="#">Price (Low to High)</a>
                           <a class="dropdown-item" href="#">Price (High to Low)</a>
                           <a class="dropdown-item" href="#">Discount (High to Low)</a>
                           <a class="dropdown-item" href="#">Name (A to Z)</a>
                        </div>
                     </div>
                     <h5 class="mb-1 text-dark">VEGGIE CROPS</h5>
                     <a href="#"><span class="icofont icofont-ui-home"></span> Home</a> <span class="icofont icofont-thin-right"></span> <a href="#">VEGGIE</a> <span class="icofont icofont-thin-right"></span> <span>Vegetables</span>
                  </div>
                  <div class="row">
                  <c:forEach items="${bestSellingProducts}" var="product">
                  <div class="col-6 col-md-4">
                        <div class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
                           <span class="like-icon"><a href="#"> <i class="icofont icofont-heart"></i></a></span>
                           <a href='/product-detail?id=${product.productId}'>
                           <span class="badge badge-danger">NEW</span>
                           <img src="${product.productImgPath}" class="card-img-top" alt="..."></a>
                           <div class="card-body">
                              <h6 class="card-title mb-1">${product.productName}</h6>
                              <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
                              <p class="mb-0 text-dark"><i class="icofont-rupee"></i>${product.price} <span class="text-black-50"><del><i class="icofont-rupee"></i>${product.price + 500}</del></span></p>
                           </div>
                        </div>
                     </div>
                  </c:forEach>
                  
                     <div class="col-md-12 text-center load-more">
                        <button class="btn btn-primary btn-sm" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        Loading...
                        </button>  
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Footer -->
      <jsp:include page="footer.jsp" />
      <div class="cart-sidebar">
         <div class="cart-sidebar-header">
            <h5>
               My Cart <span class="text-info">(5 item)</span> <a data-toggle="offcanvas" class="float-right" href="#"><i class="icofont icofont-close-line"></i>
               </a>
            </h5>
         </div>
         <div class="cart-sidebar-body">
         </div>
       		<div class="cart-sidebar-footer">
			<div class="cart-store-details">
				<p>
					Sub Total <span id="subTotal" class="float-right"><strong class="float-right text-danger"><i
						class="icofont-rupee"></i>0</strong></span>
				</p>
				<h6>
					Your total savings <span id="totalSaving" class="float-right"> <strong class="float-right text-danger"><i
						class="icofont-rupee"></i>0</strong></span>
				</h6>
			</div>
			<a href="/checkout"><button id="checkoutBtn"
					class="btn btn-success btn-lg btn-block text-left" type="button">
					<span class="float-left"><i class="fa fa-shopping-cart"></i>
						Checkout </span><span id="newTotal" class="float-right"><strong><i
							class="icofont-rupee"></i>0</strong> <span
						class="icofont icofont-bubble-right"></span></span>
				</button></a>
		</div>
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- select2 Js -->
      <script src="vendor/select2/js/select2.min.js"></script>
      <!-- Owl Carousel -->
      <script src="vendor/owl-carousel/owl.carousel.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
      <script>
      var isUser=$("#isUser").val();

		if(isUser){
			$('#profileId li:contains(Login)').hide();
			$('#profileId li:contains(My Account)').show();
		}else{
			$('#profileId li:contains(My Account)').hide();
			$('#profileId li:contains(Login)').show();
		}


      </script>
      
      
   </body>
</html>