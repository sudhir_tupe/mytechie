<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="AGRODHAN">
	  <meta name="Keywords" content="Online Agro Shopping in India,online Agro Shopping store,Online Agro Shopping Site, Buy Online,Shop Online,Online Shopping,Agrodhan"/> 
	  <meta name="Description" content="India&#x27;s biggest online agro store for Vegetable Seeds,Field Crops Seeds,Fertilisers,Water Soluble fertilisers,Organic Products,Pesticides,Agri Equipments,Micronutrients,Insecticides,Fungicides,Herbicides,Bio Pesticides,Flower Seeds,Onion Seeds from all brands at the lowest prices in India. Payment options - COD, EMI, Credit card, Debit card &amp;amp; more."/>
	<meta name="google-site-verification" content="3RnLdERPVodJ4P2YohoQWVROxQqbmN8xzF-KRmOpc2k"/>
   <title>Online Agro Shopping Site for Vegetable Seeds, Agro Chemicals &amp; More. Best Offers!</title>
   <link rel="icon" type="image/png" href="img/fav-icon.png">
   <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" href="vendor/slider/slider.css">
   <link href="vendor/select2/css/select2-bootstrap.css" />
   <link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
   <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
   <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
   <link href="css/style.css" rel="stylesheet">
   <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
   <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="js/demo.css?ver=3.4.0">
   <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
	  <div class="modal fade login-modal-main" id="login">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-body">
               <div class="login-modal">
                  <div class="row">
                     <div class="col-lg-6 d-flex align-items-center">
                        <div class="login-modal-left p-4 text-center pl-5">
                           <img src="img/login_log.png" alt=""> 
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <button type="button" class="close close-top-right position-absolute" data-dismiss="modal"
                           aria-label="Close">
                           <span aria-hidden="true"><i class="icofont-close-line"></i></span>
                           <span class="sr-only">Close</span>
                        </button>
                        <form class="position-relative">
                           <ul class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute" role="tablist">
                              <li>
                                 <a class="nav-link-login active" data-toggle="tab" href="#login-form" role="tab"><i
                                       class="icofont-ui-lock"></i> LOGIN</a>
                              </li>
                              <li>
                                 <a class="nav-link-login" data-toggle="tab" href="#register" role="tab"><i
                                       class="icofont icofont-pencil"></i> SIGN UP</a>
                              </li>
                               <li>
                                 <a class="nav-link-login" data-toggle="tab" href="#forgot-form" id="forgot-form-tab" role="tab"><i class="icofont-key"></i> RESET</a>
                              </li>
                           </ul>
                           <div class="login-modal-right p-4">
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <!--  LOGIN -->
                                 <div class="tab-pane active" id="login-form" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">Login</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="number" class="form-control" id="username" placeholder="Mobile number"/>
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label>Enter Password</label>
                                       <input type="password" id="password" class="form-control" placeholder="Password"/>
                                    </fieldset>
                                    <fieldset class="form-group">
										<div id="userLoginSuccessDiv" class="alert alert-success hide"></div>
										<div id="userLoginFailureDiv" class="alert alert-danger hide"></div>
									</fieldset>
                                    <fieldset class="form-group">
                                       <button type="submit" id="enterloginBtn" class="btn btn-lg btn-success btn-block">Enter to your
                                          account</button>
                                    </fieldset>
                                    <div class="form-group">
                                       <a data-toggle="tab" href="#forgot-form" role="tab" class="btn btn-link" id="btnForgotPassword">Forgot Password ?</a>
                                    </div>
                                 </div>
                                 
                                 <!--  END OF LOGIN -->
                                 <input type="hidden" id="textUserId">
                                 
                                  <div class="tab-pane" id="forgot-form" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">Reset</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="number" class="form-control" id="mobileNumber" placeholder="Mobile number"/>
                                    </fieldset>
                                    
                                    <fieldset class="form-group">
										<div id="requestOTPSuccessDiv" class="alert alert-success hide"></div>
										<div id="requestOTPFailureDiv" class="alert alert-danger hide"></div>
									</fieldset>
									<div id="otpDiv">
                                     <fieldset class="form-group">
                                       <label>Enter OTP</label>
                                       <input type="number" id="textOtp" class="form-control" placeholder="Enter OTP"/>
                                    </fieldset>
                                    
                                     <fieldset class="form-group">
                                       <button type="submit"  id="btnVerifyOTP" class="btn btn-lg btn-success btn-block">Verify</button>
                                    </fieldset>
                                    </div>
												<div id="newPasswordDiv">
												<fieldset class="form-group">
															<label>Set New Password</label>
															<input type="password" class="form-control"
																id="newPassword">
														</fieldset>
														<fieldset class="form-group">
														<button type="button" class="btn btn-lg btn-success btn-block" id="btnSubmitNewPassword">Submit</button>
														</fieldset>
														
												</div>
												<div id="verifyOTPSuccessDiv" class="alert alert-success"></div>
												<div id="verifyOTPFailureDiv" class="alert alert-danger"></div>
												
												<div id="passwordUpdateSuccessDiv" class="alert alert-success"></div>
												<div id="passwordUpdateFailureDiv" class="alert alert-success"></div>
												
												
									<div id="requestOTPBtnDiv">
                                    <fieldset class="form-group">
                                       <button type="submit"  id="btnRequestOTP" class="btn btn-lg btn-success btn-block">Request OTP</button>
                                    </fieldset>
                                    </div>
                                 </div>
                                 
                                 
                                 <!-- REGISTER -->
                                 <form:form action="/registerNewUser" class="position-relative" modelAttribute="userAccount">
                                 <div class="tab-pane" id="register" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">Sign Up</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="number" id="regUsername" class="form-control" placeholder="Mobile number">
                                    </fieldset>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Email</label>
                                       <input type="text" id="regEmailId" class="form-control" placeholder="Email Id">
                                    </fieldset>
                                    
                                    <fieldset class="form-group">
                                       <label>Enter Password</label>
                                       <input type="password" id="regPassword" class="form-control" placeholder="Password">
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label>Confirm Password </label>
                                       <input type="password" id="confirmPassword" class="form-control" placeholder="Confirm Password">
                                    </fieldset>
                                    
                                    <fieldset class="form-group">
												<div id="userRegSuccessDiv" class="alert alert-success hide"></div>
												<div id="userRegFailureDiv" class="alert alert-danger hide"></div>
												</fieldset>
												
											<fieldset class="form-group">
												<div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="customCheck2">
                                       <label class="custom-control-label" for="customCheck2">I Agree with <a
                                             href="/terms">Term and Conditions</a></label>
                                    </div>
											</fieldset>		
                                    <fieldset class="form-group">
                                       <button type="submit" id="createAccountBtn" class="btn btn-lg btn-success btn-block">Create Your
                                          Account</button>
                                    </fieldset>
                                 </div>
                                 </form:form>
                                   <!--  END OF REGISTER -->
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
	
	
	<div class="bg-white">
		<div class="main-nav shadow-sm">
			<!--  Navbar-->
			<jsp:include page="navbar.jsp" />
			<!-- End of NavBar -->
		</div>
		<div class="py-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<header>
							<div id="owl-carousel-one" class="owl-carousel">
							<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm img-center"
										src="img/banner/adv7.png">
								</div>
							
							<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm img-center"
										src="img/banner/a.png">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm img-center"
										src="img/banner/b.png">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm img-center"
										src="img/banner/c.png">
								</div>
								
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm img-center"
										src="img/banner/d.png">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm img-center"
										src="img/banner/e.png">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded img-center"
										src="img/banner/f.png">
								</div>
							</div>
						</header>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="product-list pbc-5 pb-4 pt-5 bg-white">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark font-weight-normel">Best Selling
				Items</h4>
			<div class="row">
			
			 <c:forEach items="${bestSellingProducts}" var="product">
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail?id=${product.productId}">
							<span class="badge badge-danger">${product.packagingGram}</span><img
							src="<c:out value="${product.productImgPath}"></c:out>" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1"><c:out value="${product.productName}"></c:out></h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
																		<!-- &nbsp;&nbsp;<span class="badge-primary">500gm</span> -->
									
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i><c:out value="${product.price}"></c:out><span class="text-black-50"><del>
										<i class="icofont-rupee"></i>${product.price + 500}
									</del></span>
									
								<button class="btn btn-success like-icon" data-toggle="modal" title="ADD TO CART"  data-target="#exampleModal" style="border-radius: 6px;" onclick="myFunction('${product.productId}','${product.productImgPath}','${product.price}','${product.productName}')"><i class="fa fa-shopping-cart" style="margin-right: 3px;"></i></button>
							</p>
						</div>
					</div>
				</div>
				</c:forEach>
			</div>
		</div>
	</section>
	
	<section class="product-list pbc-5 pb-4 pt-5 bg-white">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark">Top Savers Today</h4>
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-carousel-category owl-theme">
						
						 <c:forEach items="${agroChemicalList}" var="product">
						
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"><i
										class="icofont-shopping-cart"></i></a></span><a href='/product-detail?id=${product.productId}'>
									<span class="badge badge-danger">${product.packagingGram}</span> <img
									src="<c:out value="${product.productImgPath}"></c:out>" class="card-img-top" alt="...">
								</a>
								<div class="card-body">
									<h6 class="card-title mb-1"><c:out value="${product.productName}"></c:out></h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										<i class="icofont-rupee"></i> <c:out value="${product.price}"></c:out>  <span
											class="text-black-50"><del>
												<i class="icofont-rupee"></i><c:out value="${product.price + 500}"></c:out> 
											</del></span> <span
											class="bg-success  rounded-sm pl-1 ml-1 pr-1 text-white small">
											<i class="icofont-rupee"></i>500 OFF</span>
											<button class="btn btn-success like-icon" data-toggle="modal" title="ADD TO CART" data-target="#exampleModal" style="border-radius: 6px;" onclick="myFunction('${product.productId}','${product.productImgPath}','${product.price}','${product.productName}')"><i class="fa fa-shopping-cart" style="margin-right: 3px;"></i></button>
									</p>
								</div>
							</div>
						</div>
				 </c:forEach>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer -->
	<jsp:include page="footer.jsp" />
	
	<!-- Shopping Cart -->
	<form:form action="/checkout">
	<div class="cart-sidebar">
		<div class="cart-sidebar-header">
			<h5>
            My Cart <font class="text-info">(</font><span id="numOfItemsId" class="text-info">0</span><font class="text-info"> item)</font><a data-toggle="offcanvas" class="float-right" href="#"><i
                  class="icofont icofont-close-line"></i>
            </a>
         </h5>
					
		</div>
		<div class="cart-sidebar-body" id="cartBody">
		<input id="cartCounter" type="hidden" value="0">
		
		</div>
		<div class="cart-sidebar-footer">
			<div class="cart-store-details">
				<p>
					Sub Total <span id="subTotal" class="float-right"><strong class="float-right text-danger"><i
						class="icofont-rupee"></i>0</strong></span>
				</p>
				<h6>
					Your total savings <span id="totalSaving" class="float-right"> <strong class="float-right text-danger"><i
						class="icofont-rupee"></i>0</strong></span>
				</h6>
			</div>
			<a href="/checkout"><button id="checkoutBtn"
					class="btn btn-success btn-lg btn-block text-left" type="button">
					<span class="float-left"><i class="fa fa-shopping-cart"></i>
						Checkout </span><span id="newTotal" class="float-right"><strong><i
							class="icofont-rupee"></i>0</strong> <span
						class="icofont icofont-bubble-right"></span></span>
				</button></a>
		</div>
	</div>
	</form:form>
	
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-shopping-cart" style="margin-right: 3px;"></i> Shopping Cart</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">CLOSE</button>
        <button type="button" class="btn btn-success" data-toggle="offcanvas" class="nav-link" data-dismiss="modal">VIEW CART</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" value='${isUser}' id="isUser">
<div class="modal fade" id="removeItemModal" tabindex="-1" role="dialog" aria-labelledby="removeItemModal" aria-hidden="true">
  <div class="modal-dialog  modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Remove Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       Are you sure you want to remove this item?
       	<input id="removeCounter" type="hidden" value="0">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">CANCEL</button>
        <button type="button" class="btn btn-success" data-toggle="offcanvas" id="removeItemBtn" class="nav-link" data-dismiss="modal">REMOVE</button>
      </div>
    </div>
  </div>
</div>
<!-- <div id="preloader"></div> -->
	<!-- End Of Cart -->
	 
	   <!-- Bootstrap core JavaScript -->
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!-- select2 Js -->
   <script src="vendor/select2/js/select2.min.js"></script>
   <!-- Owl Carousel -->
   <script src="vendor/owl-carousel/owl.carousel.js"></script>
   <!-- Slider Js -->
   <script src="vendor/slider/slider.js"></script>
   <!-- Custom scripts for all pages-->
   <script src="js/custom.js"></script>
   <script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
	 <script src="assets/js/main.js"></script>
	
	
	<script>

	//window.onpaint = hideAccount();
	
	$(document).ready(function() {

		hideAccount();
		$("#userRegSuccessDiv").hide();
		$("#userRegFailureDiv").hide();
		$("#userLoginFailureDiv").hide();
		$("#userLoginSuccessDiv").hide();

		$("#requestOTPSuccessDiv").hide();
		$("#requestOTPFailureDiv").hide();

		$("#verifyOTPSuccessDiv").hide();
		$("#verifyOTPFailureDiv").hide();


		$("#passwordUpdateSuccessDiv").hide();
		$("#passwordUpdateFailureDiv").hide();
		
		 $("#otpDiv").hide();

		 $("#newPasswordDiv").hide();
		
		$('#removeItemBtn').click(function(event) {
			var removeCounter=$("#removeCounter").val();
			$("div").remove("#"+removeCounter+"");
		});

		$('#createAccountBtn').click(function(event) {
			event.preventDefault();
		  var user = {};
		  user["username"] = $("#regUsername").val();
		  user["phoneNumber"] = $("#regUsername").val();
		  user["emailId"] = $("#regEmailId").val();
		  user["password"] = $("#regPassword").val();
		  var confirmPassword = $("#confirmPassword").val();

			if(user.username == ""){
				$("#userRegFailureDiv").html("Enter Mobile number");
				$("#userRegFailureDiv").show();
				return;
			}

			if(user.emailId == ""){
				$("#userRegFailureDiv").html("Enter Email Id");
				$("#userRegFailureDiv").show();
				return;
			}

			if(user.password == ""){
				$("#userRegFailureDiv").html("Enter Password");
				$("#userRegFailureDiv").show();
				return;
			}

			if(confirmPassword==""){
				$("#userRegFailureDiv").html("Enter Confirm Password");
				$("#userRegFailureDiv").show();
				return;
			}

			
			if(user.password != confirmPassword){
				$("#userRegFailureDiv").html("Confirm Password does not match.");
				$("#userRegFailureDiv").show();
				return;
			}

			
	         if($("#customCheck2").is(":not(:checked)")){

	        	 $("#userRegFailureDiv").html("You must agree to terms and conditions.");
					$("#userRegFailureDiv").show();
	                return;
	           }
		  
			$.ajax({
				type : "POST",
				url : "/registerNewUser",
				contentType : "application/json",
				processData : false,
				data : JSON.stringify(user),
				success : function(data) {
					if (data.actionPassed == false) {
						$("#userRegFailureDiv").html('<strong>'+data.message+'</strong>');
						$("#userRegFailureDiv").show();
						return;
					} else {
						$("#userRegFailureDiv").hide();
						$("#userRegSuccessDiv").html("User Account Created.");
						$("#userRegSuccessDiv").show();
						
						window.location.href = "/user";
						
					}
				}
			});
		});

		$('#btnForgotPassword').click(function(event) {
			$('.nav-link-login').removeClass('active');
			$('#forgot-form-tab').addClass('active');
			$(".tab-pane").find(".active").removeClass("active");
		});


		$('#btnRequestOTP').click(function(event) {
			event.preventDefault();
			  var user = {};
			  user["phoneNumber"] = $("#mobileNumber").val();

				if(user.phoneNumber =="" || user.emailId==""){
					$("#requestOTPFailureDiv").html("Enter Phone Number OR Email Id");
					$("#requestOTPFailureDiv").show();
					$("#requestOTPFailureDiv").fadeTo(2000, 500).slideUp(500,
							function() {
								$("#requestOTPFailureDiv").slideUp(500);
							});
						return;
					}
  			  
			  $.ajax({
					type : "POST",
					url : "/requestOTP",
					contentType : "application/json",
					processData : false,
					data : JSON.stringify(user),
					success : function(data) {
						if (data.actionPassed == false) {
							$("#requestOTPFailureDiv").html(data.message);
							$("#requestOTPFailureDiv").show();

							$("#requestOTPFailureDiv").fadeTo(2000, 500).slideUp(500,
									function() {
										$("#requestOTPFailureDiv").slideUp(500);
									});
							
							 $("#textOtp").attr("disabled", "disabled"); 
				        	 $("#btnVerifyOTP").prop("disabled", true );
							
							return;
						} else {
								$("#requestOTPSuccessDiv").html(data.message);
								$("#requestOTPSuccessDiv").show();	

								$("#requestOTPSuccessDiv").fadeTo(5000, 500).slideUp(500,
    									function() {
    										$("#requestOTPSuccessDiv").slideUp(500);
    									});

								$("#textUserId").val(data.responseId);
								$("#requestOTPBtnDiv").hide();
								$("#otpDiv").show();
								  $("#textOtp").removeAttr("disabled");
								  $("#btnVerifyOTP").prop("disabled",false);
						}
					}
				});
	  	});

		$('#btnSubmitNewPassword').click(function(event) {
			event.preventDefault();
			var user = {};
			 user["phoneNumber"] = $("#mobileNumber").val();
			 user["password"] = $("#newPassword").val();
			 user["userId"] = $("#textUserId").val();
			 
			  $.ajax({
					type : "POST",
					url : "/updateNewPassword",
					contentType : "application/json",
					processData : false,
					data : JSON.stringify(user),
					success : function(data) {
						if (data.actionPassed == false) {
						alert("Error");
							return;
						} else {
					    $("#passwordUpdateSuccessDiv").html(data.message);
						$("#passwordUpdateSuccessDiv").show();
						$("#passwordUpdateSuccessDiv").fadeTo(4000, 500).slideUp(500,
								function() {
									$("#passwordUpdateSuccessDiv").slideUp(500);
								});
						 $("#newPasswordDiv").hide();
						 $("#requestOTPBtnDiv").show();
					 }
					}
				});
			

        });

		$('#btnVerifyOTP').click(function(event) {
			event.preventDefault();
			var user = {};
			 user["phoneNumber"] = $("#mobileNumber").val();
			 user["oneTimePassword"] = $("#textOtp").val();
			 
			  $.ajax({
					type : "POST",
					url : "/verifyOTP",
					contentType : "application/json",
					processData : false,
					data : JSON.stringify(user),
					success : function(data) {
						if (data.actionPassed == false) {
							$("#verifyOTPFailureDiv").html(data.message);
							$("#verifyOTPFailureDiv").show();

							$("#verifyOTPFailureDiv").fadeTo(4000, 500).slideUp(500,
									function() {
										$("#verifyOTPFailureDiv").slideUp(500);
									});
							
							return;
						} else {
							$("#verifyOTPSuccessDiv").html(data.message);
							$("#verifyOTPSuccessDiv").show();
							$("#verifyOTPSuccessDiv").fadeTo(4000, 500).slideUp(500,
									function() {
										$("#verifyOTPSuccessDiv").slideUp(500);
									});

							$("#otpDiv").hide();
							$("#newPasswordDiv").show();
						}
					}
				});
	  	});

		
		
		$('#checkoutBtn').click(function(event) {
			event.preventDefault();
			
			var items = [];
			$(".cart-list-product").each(function(){
				var display=$(this).css("display");	
				if(display !="none"){
				  	var imgPath=$(this).find("img").attr('src');
					var pNameval=$(this).find("h5").text();
					var price=$(this).find("p").text();
					var qnty=1;
					var item= { productName: pNameval,quantity: qnty,price:price,imgPath: imgPath};
					items.push(item);
				}
			});

			var cart ={'items': items};
				$.ajax({
					url : "/checkout",
					contentType: 'application/json; charset=utf-8',
				    dataType: 'json',
				    type: 'POST',
				    data: JSON.stringify(cart),
					success : function(data) {
						if (data.actionPassed == false) {
							alert("ERROR");
							return;
						} else {
							window.location.href = "/viewcart?id="+data.responseId+"";
						}
					}
				});
			});



		
    	  $('#enterloginBtn').click(function(event) {
    		  event.preventDefault();
    		  var user = {}
    		  user["username"] = $("#username").val();
    		  user["password"] = $("#password").val();

    		  if(user.username == ""){
  				$("#userLoginFailureDiv").html("Enter Mobile number");
  				$("#userLoginFailureDiv").show();
  				return;
  			}

  			if(user.password == ""){
  				$("#userLoginFailureDiv").html("Enter Password");
  				$("#userLoginFailureDiv").show();
  				return;
  			}

  			$.ajax({
				type : "POST",
				url : "/validateUser",
				contentType : "application/json",
				processData : false,
				data : JSON.stringify(user),
				success : function(data) {
					if (data.actionPassed == false) {
						$("#userLoginFailureDiv").html(data.message);
						$("#userLoginFailureDiv").show();
						return;
					} else {
						$("#userLoginFailureDiv").hide();
						$("#userLoginSuccessDiv").html(data.message);
						$("#userLoginSuccessDiv").show();
						window.location.href = "/home-account";
					}
				}
			});
		});
	});


	function hideAccount(){
		var isUser=$("#isUser").val();

		if(isUser){
			$('#profileId li:contains(Login)').hide();
			$('#profileId li:contains(My Account)').show();
		}else{
			$('#profileId li:contains(My Account)').hide();
		}
	}

	function removeItemFromCart(counter,price){
		$('#removeCounter').val(counter);
		$("#"+counter+"").hide();
		var newTotal = parseFloat($("#newTotal").text()) - parseFloat(price);
		$("#subTotal").html('<strong class="float-right text-danger"><i class="icofont-rupee"></i>'+newTotal+'</strong>');
		$("#newTotal").html('<i class="icofont-rupee"></i>'+newTotal+'<span class="icofont icofont-bubble-right"></span>');
		var totalSaving =parseFloat($("#totalSaving").text());
		totalSaving=totalSaving-500;
		$("#totalSaving").html('<strong><i class="icofont-rupee"></i>'+totalSaving+'</strong>');

		 var itemCount=parseInt($("#numOfItemsId").text());	
		$("#numOfItemsId").text(itemCount - 1);
		//$('#removeItemModal').show();
	}
	
	 function myFunction(id,img,price,name) {
			var mymodal = $('#exampleModal');
			 mymodal.find('.modal-body').html('Product <strong>'+name+' </strong> added to CART successfully.');
				
			var totalSaving =parseFloat($("#totalSaving").text());
			
			var currentTotatPrice  = parseFloat($("#newTotal").text());

			var subTotal=  parseFloat(currentTotatPrice) + parseFloat(price);
			$("#subTotal").html('<strong class="float-right text-danger"><i class="icofont-rupee"></i>'+subTotal+'</strong>');
			

			var newTotal = parseFloat(currentTotatPrice) + parseFloat(price);
			$("#newTotal").html('<i class="icofont-rupee"></i>'+newTotal+'<span class="icofont icofont-bubble-right"></span>');

			
			totalSaving=totalSaving+500;
			$("#totalSaving").html('<strong><i class="icofont-rupee"></i>'+totalSaving+'</strong>');
			var cartCounter = parseInt($("#cartCounter").val()) + parseInt(1); 

			$("#cartCounter").val(cartCounter);
			var delPrice= parseInt(price) + 500;
			 $('#cartBody').append('<div class="cart-list-product" id='+cartCounter+'><a onclick="removeItemFromCart('+cartCounter+','+price+');" class="float-right remove-cart" title="REMOVE"><i class="icofont-close-squared-alt" style=""></i></a>'+
					 '<img class="img-fluid" src='+img+'><span class="badge badge-success"><i class="icofont-rupee"></i>500 OFF</span><br><h5><a class="productName" href="#">'+name+'</a></h5>'
			 +'<div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i><span>613</span></div>'
			 
			 +'<p class="f-14 mb-0 text-dark float-right"><i class="icofont-rupee"></i>'+price+'</p>'
+'<span class="count-number float-left"><button class="btn btn-outline-secondary btn-sm left dec" disabled><i class="icofont-minus"></i></button><input class="count-number-input" type="text" value="1" disabled><button class="btn btn-outline-secondary btn-sm right inc" disabled><i class="icofont-plus"></i></button></span></div>');

			 var itemCount=parseInt($("#numOfItemsId").text());	
			$("#numOfItemsId").text(itemCount + 1);
			}
    </script>
</body>
</html>