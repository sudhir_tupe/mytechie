<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Gurdeep singh osahan">
      <meta name="author" content="Gurdeep singh osahan">
      <title>AgroMart</title>
      <!-- Favicon Icon -->
      <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
   </head>
   <body>
      <div class="bg-light">
         <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +1-123-456-7890</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold">$499</span></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav shadow-sm">
             <jsp:include page="navbar.jsp"/>
         </div>
      </div>
      <section class="py-4 bg-light inner-header">
         <div class="container">
            <div class="row d-flex align-items-center">
               <div class="col-lg-6 col-md-6">
                  <h4 class="mt-0 mb-0 text-dark">
                  FAQ       
               </div>
               <div class="col-lg-6 col-md-6 text-right">
                  <div class="breadcrumbs">
                     <p class="mb-0"><a href="#"><i class="icofont-ui-home"></i> Home</a>  /  <span>FAQ</span></p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="faq-page py-5 bg-light border-top">
         <div class="container">
            <div class="row">
               <div class="col-md-12 mx-auto">
                  <div class="row">
                     <div class="col-lg-8 col-md-8">
                        <div class="bg-white p-4 shadow-sm rounded h-100">
                           <div class="accordion" id="accordionExample">
                              <div class="card mb-0">
                                 <div class="card-header" id="headingOne">
                                    <h6 class="mb-0">
                                       <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                       <i class="icofont icofont-question-square"></i>   Is my order confirmed? 
                                       </a>
                                    </h6>
                                 </div>
                                 <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                       <strong>wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. </strong>
                                       Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch etorigin coffee nulla assumenda shoreditch et. Nihil helvetica, craf.
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="headingTwo">
                                    <h6 class="mb-0">
                                       <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                       <i class="icofont icofont-question-square"></i>   When will you ship my order? 
                                       </a>
                                    </h6>
                                 </div>
                                 <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                       Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil helvetica, craf.
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="headingTwo">
                                    <h6 class="mb-0">
                                       <a href="#" data-toggle="collapse" data-target="#collapsefour" aria-expanded="true" aria-controls="collapsefour">
                                       <i class="icofont icofont-question-square"></i>   When will my order get delivered? 
                                       </a>
                                    </h6>
                                 </div>
                                 <div id="collapsefour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                       Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil helvetica, craf.
                                    </div>
                                 </div>
                              </div>
                              <div class="card">
                                 <div class="card-header" id="headingThree">
                                    <h6 class="mb-0">
                                       <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                       <i class="icofont icofont-question-square"></i>   How much do you charge for delivery?
                                       </a>
                                    </h6>
                                 </div>
                                 <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                       Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil helvetica, craf.
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4">
                        <div class="bg-white p-4 shadow-sm rounded">
                           <div class="section-header">
                              <h5 class="heading-design-h5">
                                 Ask us question
                              </h5>
                           </div>
                           <form>
                              <div class="row">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                       <label class="control-label">Your Name <span class="required">*</span></label>
                                       <input class="form-control border-form-control" value="" placeholder="Enter Name" type="text">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                       <label class="control-label">Email Address <span class="required">*</span></label>
                                       <input class="form-control border-form-control " value="" placeholder="ex@gmail.com" type="email">
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                       <label class="control-label">Phone <span class="required">*</span></label>
                                       <input class="form-control border-form-control" value="" placeholder="Enter Phone" type="number">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                       <label class="control-label">Your Message <span class="required">*</span></label>
                                       <textarea class="form-control border-form-control"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-12 text-right">
                                    <button type="button" class="btn btn-outline-primary btn-sm"> Cencel </button>
                                    <button type="button" class="btn btn-primary btn-sm"> Send Message </button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Footer -->
    <jsp:include page="footer.jsp" />
      <div class="cart-sidebar">
         <div class="cart-sidebar-header">
            <h5>
               My Cart <span class="text-info">(5 item)</span> <a data-toggle="offcanvas" class="float-right" href="#"><i class="icofont icofont-close-line"></i>
               </a>
            </h5>
         </div>
         <div class="cart-sidebar-body">
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/1.jpg" alt="">
               <span class="badge badge-success">50% OFF</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$135.00 <del class="small text-secondary">$ 500.00 </del></p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/2.jpg" alt="">
               <span class="badge badge-danger">55% OFF</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$250.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-info rounded-sm pl-1 ml-1 pr-1 text-white small">NEW</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>           
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/3.jpg" alt="">
               <span class="badge badge-info">NEW</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$900.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small"> 50% OFF</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/4.jpg" alt="">
               <span class="badge badge-danger">NEW</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$135.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small"> 50% OFF</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
            <div class="cart-list-product">
               <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
               <img class="img-fluid" src="img/item/5.jpg" alt="">
               <span class="badge badge-info">NEW</span>
               <h5><a href="#">Floret Printed Ivory Skater Dress</a></h5>
               <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
               <p class="f-14 mb-0 text-dark float-right">$135.00 <del class="small text-secondary">$ 500.00 </del> <span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small"> 50% OFF</span> </p>
               <span class="count-number float-left">
               <button class="btn btn-outline-secondary  btn-sm left dec"> <i class="icofont-minus"></i> </button>
               <input class="count-number-input" type="text" value="1" readonly="">
               <button class="btn btn-outline-secondary btn-sm right inc"> <i class="icofont-plus"></i> </button>
               </span>
            </div>
         </div>
         <div class="cart-sidebar-footer">
            <div class="cart-store-details">
               <p>Sub Total <strong class="float-right">$900.69</strong></p>
               <p>Delivery Charges <strong class="float-right text-danger">+ $29.69</strong></p>
               <h6>Your total savings <strong class="float-right text-danger">$55 (42.31%)</strong></h6>
            </div>
            <a href="checkout.html"><button class="btn btn-primary btn-lg btn-block text-left" type="button"><span class="float-left"><i class="icofont icofont-cart"></i> Proceed to Checkout </span><span class="float-right"><strong>$1200.69</strong> <span class="icofont icofont-bubble-right"></span></span></button></a>
         </div>
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Owl Carousel -->
      <script src="vendor/owl-carousel/owl.carousel.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
   </body>
</html>