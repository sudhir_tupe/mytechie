<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="AGRODHAN">
      <meta name="author" content="AGRODHAN">
      <title>Online Agro Shopping Site for Vegetable Seeds, Agro Chemicals &amp; More. Best Offers!</title>
      <!-- Favicon Icon -->
     <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Select2 CSS -->
      <link href="vendor/select2/css/select2-bootstrap.css" />
      <link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
      
      
   </head>
   <body>
    <input type="hidden" value="${isUserLoggedIn}" id="isUserLoggedIn">
    <input type="hidden" value='${userId}' id="userId">
      <!-- Modal -->
      <div class="modal fade" id="delete-address-modal" tabindex="-1" role="dialog" aria-labelledby="delete-address" aria-hidden="true">
         <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="delete-address">Delete</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <p class="mb-0 text-black">Are you sure you want to delete Address?</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">CANCEL
                  </button><button type="button" class="btn d-flex w-50 text-center justify-content-center btn-primary">DELETE</button>
               </div>
            </div>
         </div>
      </div>
      
      
      
      
         <div class="modal fade login-modal-main" id="login">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-body">
               <div class="login-modal">
                  <div class="row">
                     <div class="col-lg-6 d-flex align-items-center">
                        <div class="login-modal-left p-4 text-center pl-5">
                           <img src="img/login_log.png" alt="AGRODHAN">
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <button type="button" class="close close-top-right position-absolute" data-dismiss="modal"
                           aria-label="Close">
                           <span aria-hidden="true"><i class="icofont-close-line"></i></span>
                           <span class="sr-only">Close</span>
                        </button>
                        <form class="position-relative">
                           <ul class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute" role="tablist">
                              <li>
                                 <a class="nav-link-login active" data-toggle="tab" href="#login-form" role="tab"><i
                                       class="icofont-ui-lock"></i> LOGIN</a>
                              </li>
                              <li>
                                 <a class="nav-link-login" data-toggle="tab" href="#register" role="tab"><i
                                       class="icofont icofont-pencil"></i> SIGN UP</a>
                              </li>
                               <li>
                                 <a class="nav-link-login" data-toggle="tab" href="#forgot-form" id="forgot-form-tab" role="tab"><i class="icofont-key"></i> RESET</a>
                              </li>
                              
                           </ul>
                           <div class="login-modal-right p-4">
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <!--  LOGIN -->
                                 <div class="tab-pane active" id="login-form" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">Login</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="number" class="form-control" id="username" placeholder="Mobile number"/>
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label>Enter Password</label>
                                       <input type="password" id="password" class="form-control" placeholder="Password"/>
                                    </fieldset>
                                    <fieldset class="form-group">
										<div id="userLoginSuccessDiv" class="alert alert-success hide"></div>
										<div id="userLoginFailureDiv" class="alert alert-danger hide"></div>
									</fieldset>
                                    <fieldset class="form-group">
                                       <button type="submit" id="enterloginBtn" class="btn btn-lg btn-success btn-block">Enter to your
                                          account</button>
                                    </fieldset>
                                    <div class="form-group">
                                       <a data-toggle="tab" href="#forgot-form" role="tab" class="btn btn-link" id="btnForgotPassword">Forgot Password ?</a>
                                    </div>
                                 </div>
                                 
                                 <!--  END OF LOGIN -->
                                 <input type="hidden" id="textUserId">
                                 
                                  <div class="tab-pane" id="forgot-form" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">Reset</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="number" class="form-control" id="mobileNumber" placeholder="Mobile number"/>
                                    </fieldset>
                                    
                                    <fieldset class="form-group">
										<div id="requestOTPSuccessDiv" class="alert alert-success hide"></div>
										<div id="requestOTPFailureDiv" class="alert alert-danger hide"></div>
									</fieldset>
									<div id="otpDiv">
                                     <fieldset class="form-group">
                                       <label>Enter OTP</label>
                                       <input type="number" id="textOtp" class="form-control" placeholder="Enter OTP"/>
                                    </fieldset>
                                    
                                     <fieldset class="form-group">
                                       <button type="submit"  id="btnVerifyOTP" class="btn btn-lg btn-success btn-block">Verify</button>
                                    </fieldset>
                                    </div>
												<div id="newPasswordDiv">
												<fieldset class="form-group">
															<label>Set New Password</label>
															<input type="password" class="form-control"
																id="newPassword">
														</fieldset>
														<fieldset class="form-group">
														<button type="button" class="btn btn-lg btn-success btn-block" id="btnSubmitNewPassword">Submit</button>
														</fieldset>
														
												</div>
												<div id="verifyOTPSuccessDiv" class="alert alert-success"></div>
												<div id="verifyOTPFailureDiv" class="alert alert-danger"></div>
												
												<div id="passwordUpdateSuccessDiv" class="alert alert-success"></div>
												<div id="passwordUpdateFailureDiv" class="alert alert-success"></div>
												
												
									<div id="requestOTPBtnDiv">
                                    <fieldset class="form-group">
                                       <button type="submit"  id="btnRequestOTP" class="btn btn-lg btn-success btn-block">Request OTP</button>
                                    </fieldset>
                                    </div>
                                 </div>
                                 
                                 
                                 <!-- REGISTER -->
                                 <form:form action="/registerNewUser" class="position-relative" modelAttribute="userAccount">
                                 <div class="tab-pane" id="register" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">Sign Up</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="number" id="regUsername" class="form-control" placeholder="Mobile number">
                                    </fieldset>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Email</label>
                                       <input type="text" id="regEmailId" class="form-control" placeholder="Email Id">
                                    </fieldset>
                                    
                                    <fieldset class="form-group">
                                       <label>Enter Password</label>
                                       <input type="password" id="regPassword" class="form-control" placeholder="Password">
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label>Confirm Password </label>
                                       <input type="password" id="confirmPassword" class="form-control" placeholder="Confirm Password">
                                    </fieldset>
                                    
                                    <fieldset class="form-group">
												<div id="userRegSuccessDiv" class="alert alert-success hide"></div>
												<div id="userRegFailureDiv" class="alert alert-danger hide"></div>
												</fieldset>
												
											<fieldset class="form-group">
												<div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="customCheck2">
                                       <label class="custom-control-label" for="customCheck2">I Agree with <a
                                             href="/terms">Term and Conditions</a></label>
                                    </div>
											</fieldset>		
                                    <fieldset class="form-group">
                                       <button type="submit" id="createAccountBtn" class="btn btn-lg btn-success btn-block">Create Your
                                          Account</button>
                                    </fieldset>
                                 </div>
                                 </form:form>
                                   <!--  END OF REGISTER -->
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
      <!--  End of LOGIN -->
      
      
      
      
<%--         <div class="modal fade login-modal-main" id="login">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
         <div class="modal-content">
            <div class="modal-body">
               <div class="login-modal">
                  <div class="row">
                     <div class="col-lg-6 d-flex align-items-center">
                        <div class="login-modal-left p-4 text-center pl-5">
                           <img src="img/logo6.png" alt="Gurdeep singh osahan">
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <button type="button" class="close close-top-right position-absolute" data-dismiss="modal"
                           aria-label="Close">
                           <span aria-hidden="true"><i class="icofont-close-line"></i></span>
                           <span class="sr-only">Close</span>
                        </button>
                        <form class="position-relative">
                           <ul class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute" role="tablist">
                              <li>
                                 <a class="nav-link-login active" data-toggle="tab" href="#login-form" role="tab"><i
                                       class="icofont-ui-lock"></i> LOGIN</a>
                              </li>
                              <li>
                                 <a class="nav-link-login" data-toggle="tab" href="#register" role="tab"><i
                                       class="icofont icofont-pencil"></i> REGISTER</a>
                              </li>
                           </ul>
                           <div class="login-modal-right p-4">
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <!--  LOGIN -->
                                 <div class="tab-pane active" id="login-form" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">LOGIN</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="text" class="form-control" id="username" placeholder="Mobile number"/>
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label>Enter Password</label>
                                       <input type="password" id="password" class="form-control" placeholder="Password"/>
                                    </fieldset>
                                    <fieldset class="form-group">
										<div id="userLoginSuccessDiv" class="alert alert-success hide"></div>
										<div id="userLoginFailureDiv" class="alert alert-danger hide"></div>
									</fieldset>
                                    <fieldset class="form-group">
                                       <button type="submit"  id="enterloginBtn" class="btn btn-lg btn-success btn-block">Enter to your
                                          account</button>
                                    </fieldset>
                                   <div class="form-group">
                                       <button type="button" class="btn btn-link">Forgot Password ?</button>
                                    </div>
                                    <div class="login-with-sites mt-4">
                                       <p class="mb-2">or Login with your social profile:</p>
                                       <div class="row text-center">
                                          <div class="col-6 pr-1">
                                             <a class="btn-facebook btn-block login-icons btn-lg" href="#"><i
                                                   class="icofont icofont-facebook"></i> Facebook</a>
                                          </div>
                                          <div class="col-6 pl-1">
                                             <a class="btn-google btn-block login-icons btn-lg" href="#"><i
                                                   class="icofont icofont-google-plus"></i> Google</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <!--  END OF LOGIN -->
                                 <!-- REGISTER -->
                                 <form:form action="/registerNewUser" class="position-relative" modelAttribute="userAccount">
                                 <div class="tab-pane" id="register" role="tabpanel">
                                    <h5 class="heading-design-h5 text-dark">REGISTER</h5>
                                    <fieldset class="form-group mt-4">
                                       <label>Enter Mobile number</label>
                                       <input type="text" id="regUsername" class="form-control" placeholder="Mobile number">
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label>Enter Password</label>
                                       <input type="password" id="regPassword" class="form-control" placeholder="Password">
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label>Confirm Password </label>
                                       <input type="password" id="confirmPassword" class="form-control" placeholder="Password">
                                    </fieldset>
                                    		<fieldset class="form-group">
												<div id="userRegSuccessDiv" class="alert alert-success hide"></div>
												<div id="userRegFailureDiv" class="alert alert-danger hide"></div>
												</fieldset>
												
												<fieldset class="form-group">
												<div class="custom-control custom-checkbox">
                                       <input type="checkbox" class="custom-control-input" id="customCheck2">
                                       <label class="custom-control-label" for="customCheck2">I Agree with <a
                                             href="#">Term and Conditions</a></label>
                                    </div>
											</fieldset>	
												
                                    <fieldset class="form-group">
                                       <button type="submit" id="createAccountBtn" class="btn btn-lg btn-success btn-block">Create Your
                                          Account</button>
                                    </fieldset>
                                    <div class="login-with-sites mt-4">
                                       <p class="mb-2">or Login with your social profile:</p>
                                       <div class="row text-center">
                                          <div class="col-6 pr-1">
                                             <a class="btn-facebook btn-block login-icons btn-lg"  href="#"><i
                                                   class="icofont icofont-facebook"></i> Facebook</a>
                                          </div>
                                          <div class="col-6 pl-1">
                                             <a class="btn-google btn-block login-icons btn-lg" href="#"><i
                                                   class="icofont icofont-google-plus"></i> Google</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </form:form>
                                   <!--  END OF REGISTER -->
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
	 --%>
      
      <!-- Add Address Modal -->
	<div class="modal fade" id="add-address-modal" tabindex="-1"
		role="dialog" aria-labelledby="add-address" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="add-address">Add Delivery Address</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-row">
						<div class="form-group col-md-12">
								<label for="inputPassword4">Enter Name </label> <input
									type="text" class="form-control" id="delAddressName"
									placeholder="Enter your Name">
							</div>
						
							<div class="form-group col-md-12">
								<label for="inputPassword4">Complete Address </label> <input
									type="text" class="form-control" id="addressLine1"
									placeholder="Complete Address e.g. house number, street name, landmark">
							</div>
							
							<div class="form-group col-md-6">
								<label class="control-label">Zip Code <span
									class="required">*</span></label> 
									 <div class="input-group">
									 
                              <input type="text" class="form-control" id="pincodeTextBox" placeholder="Pin Code">
                              <div class="input-group-append">
                                 <button class="btn btn-outline-secondary" type="button" id="zipCodeBtn"><i class="icofont-ui-pointer"></i></button>
                              </div>
                           </div>
							</div>

							<div class="form-group col-md-6">
								<label>Name<span class="required">*</span></label>
								<select id= "selectVillage" class="select2 form-control border-form-control">
									<option value="">Select Name</option>
									<option value="">NA</option>
								</select>
							</div>

							<div class="form-group col-md-6">
								<label>Block/Taluka <span class="required">*</span></label> <select
									class="select2 form-control border-form-control" id="selectBlock">
									<option value="">Select Block </option>
								</select>
							</div>

							<div class="form-group col-md-6">
								<label>District <span class="required">*</span></label> <select
									class="select2 form-control border-form-control" id="selectDistrict">
									<option value="">Select District</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label>State <span class="required">*</span></label> <select id="selectState"
									class="select2 form-control border-form-control">
									<option value="">Select State</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label>Country <span class="required">*</span></label> <select
									class="select2 form-control border-form-control">
									<option value="IN">INDIA</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-outline-success"
						data-dismiss="modal">CANCEL</button>
					<button type="button" id="addAddressBtn"
						class="btn d-flex w-50 text-center justify-content-center btn-success">Add</button>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Add Address -->
      <div class="bg-light shadow-sm">
         <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +91-770962647</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold"><i class="icofont-rupee"></i>10,000</span></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav shadow-sm">
             <jsp:include page="navbar.jsp"/>
         </div>
      </div>
      <section class="checkout-body py-5 bg-light">
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="checkout-body-left">
                     <div class="accordion checkout-step" id="accordionExample">
                        <div class="bg-white rounded shadow-sm mb-3 overflow-hidden">
                           <div class="card-header bg-white" id="headingOne">
										<c:choose>
											<c:when test="${fn:length(isUserLoggedIn) == 5}">
											<h2 class="mb-0">
												<button id="loginOrSignUpBtn" class="btn btn-link collapsed" type="button"
													data-toggle="collapse" data-target="#collapseOne"
													aria-expanded="true" aria-controls="collapseOne">
													<i class="icofont-simple-down float-right mt-1"></i>
													1. LOGIN OR SIGNUP
												</button>
											</h2>
											</c:when>
											<c:otherwise>
											<h2 class="mb-0">
												<button class="btn btn-link collapsed" type="button"
													data-toggle="collapse" data-target="#collapseOne"
													aria-expanded="true" aria-controls="collapseOne">
													<i class="icofont-simple-down float-right mt-1"></i> 1. Phone Number Verification
												</button>
											</h2>
											</c:otherwise>
										</c:choose>
                           </div>
                           <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                              <div class="card-body">
                                 <p>We need your phone number so that we can update you about your order.</p>
                                 <form>
                                    <div class="form-row align-items-center">
                                       
                                       <div class="col-auto" id="delPhoneNumberId">
                                          <label class="sr-only">phone number</label>
                                          <div class="input-group mb-2">
                                             <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="icofont-smart-phone"></i></div>
                                             </div>
                                             <input type="text" value="${userDetails.phoneNumber}" class="form-control" id="delPhoneId">
                                          </div>
                                       </div>
                                       
                                       <div class="col-auto" id="forSignUp">
                                       <button type="button" data-target="#login" data-toggle="modal" class="btn btn-success">LOGIN</button>
                                       </div>
                                       <div class="col-auto">
                                          <button type="button" id="btnNextId"data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="btn btn-success mb-2 collapsed">NEXT</button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                           
                        </div>
                        <div class="bg-white rounded shadow-sm mb-3 overflow-hidden">
                           <div class="card-header bg-white" id="headingTwo">
                              <h2 class="mb-0">
                                 <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                 <i class="icofont-simple-down float-right mt-1"></i>
                                 2. Delivery Address
                                 </button>
                              </h2>
                           </div>
                           <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                              <div class="card-body">
                                 <div class="row">
                                 
                                       <div class="col-md-4" id="defaultAddressCardId">
                                       <div class="bg-white card addresses-item mb-0  shadow-sm">
                                          <div class="gold-members p-3">
                                             <div class="media">
                                                <div class="media-body">
                                                   <span class="badge badge-danger">Default - Delivery Address</span>
                                                   <h6 class="mb-3 mt-3 text-dark" id="customerName">${userDetails.userFName} ${userDetails.userLName}</h6>
                                                   
													<p id="delAddress"><c:if test="${not empty defaultAddress}">${defaultAddress}</c:if><p>

                                                   <p class="text-secondary">Phone: <span id="phoneNumberId" class="text-dark">${userDetails.phoneNumber}</span></p>
                                                   <button data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" type="button" class="btn btn-success btn-block">DELIVER HERE</button>
                                                   <hr>
                                                   <p class="mb-0 text-black"><a class="text-success mr-3" data-toggle="modal" data-target="#add-address-modal" href="#"><i class="icofont-ui-edit"></i> EDIT</a> <a class="text-danger" data-toggle="modal" data-target="#delete-address-modal" href="#"><i class="icofont-ui-delete"></i> DELETE</a></p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                       <a data-toggle="modal" data-target="#add-address-modal" href="#">
                                          <div class="bg-light p-4 border rounded  mb-0 shadow-sm text-center h-100 d-flex align-items-center">
                                             <h6 class="text-center m-0 w-100"><i class="icofont-plus-circle icofont-3x mb-5"></i><br><br>Add New Address</h6>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="bg-white rounded shadow-sm mb-3 overflow-hidden">
                           <div class="card-header bg-white" id="headingThree">
                              <h2 class="mb-0">
                                 <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                 <i class="icofont-simple-down float-right mt-1"></i>                                 
                                 3. Review Order
                                 </button>
                              </h2>
                           </div>
                           <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                              <div class="card-body p-0">
                                 <div class="cart-table">
                                    <div class="table-responsive">
                                       <table class="table cart_summary">
                                          <thead>
                                             <tr>
                                                <th>Product</th>
                                                <th>Description</th>
                                                <th>Delivery Options</th>
                                                <th>Quantity</th>
                                                <th>Subtotal</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                          
                                          <c:set var="rTotal" value="${0}"/>
										 <c:set var="rTotalDiscount" value="${0}"/>
                                          <c:forEach items="${items}" var="item">
                                             <tr>
                                                <td class="cart_product"><a href="#"><img class="img-fluid" src="${item.imgPath}" alt=""></a></td>
                                                <td class="cart_description">
                                                   <h6 class="product-name"><a href="#" class="text-dark">${item.productName}</a></h6>
                                                   <hr>
                                                  <a class="text-danger" href="#"><i class="icofont-trash"></i> REMOVE</a>
                                                </td>
                                                <td>
                                                   <p class="text-secondary"><i class="icofont-check-circled"></i> 14 Nov to 25 Nov <span class="text-dark"></span></p>
                                                </td>
                                                <td class="qty">
                                                   <select class="custom-select custom-select-sm" disabled>
                                                      <option selected>1</option>
                                                      <option value="1">2</option>
                                                      <option value="2">3</option>
                                                      <option value="3">4</option>
                                                   </select>
                                                </td>
                                                <td class="price">
                                                   <p class="f-14 mb-0 text-dark"><i class="icofont-rupee"></i>${item.price}<br> <del class="small text-secondary"><i class="icofont-rupee"></i>
                                                   <c:out value="${item.price + 500}"></c:out>
                                                    </del></p>
                                                </td>
                                             </tr>
                                             <c:set var="rTotal" value="${rTotal + item.price}" />
   							<c:set var="rtotalDiscount" value="${rtotalDiscount+500}"/>
                                          </c:forEach>
                                          </tbody>
                                          <tfoot>
                                             <tr>
                                             	<td class="text-right" colspan="3">Discount</td>
                                                <td colspan="2"><i class="icofont-rupee"></i><c:out value="${rtotalDiscount}"></c:out></td>
                                             </tr>
                                             <tr>
                                                <td class="text-right" colspan="3">Total Amount</td>
                                                <td colspan="2"><i class="icofont-rupee"></i><c:out value="${rTotal}"></c:out></td>
                                             </tr>
                                             <tr>
                                                <td class="text-right" colspan="3">Delivery Fee</td>
                                                <td colspan="2"><strong class="text-success">FREE 
					</strong></td>
                                             </tr>
                                             <tr>
                                                <td class="text-right" colspan="3"><strong>You Pay</strong></td>
                                                <td class="text-danger" colspan="2"><strong><i class="icofont-rupee"></i>${rTotal} </strong></td>
                                             </tr>
                                          </tfoot>
                                       </table>
                                    </div>
                                    <button data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour" class="btn btn-success btn-lg btn-block text-left" type="button"><span class="float-left"><i class="icofont icofont-cart"></i> PROCEED TO PAY</span><span class="float-right"><strong><i class="icofont-rupee"></i>${rTotal}</strong> <span class="icofont icofont-bubble-right"></span></span></button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="bg-white rounded shadow-sm overflow-hidden">
                           <div class="card-header bg-white" id="headingfour">
                              <h2 class="mb-0">
                                 <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapsefour" aria-expanded="true" aria-controls="collapsefour">
                                 <i class="icofont-simple-down float-right mt-1"></i> 4. Make Payment
                                 </button>
                              </h2>
                           </div>
                           <div id="collapsefour" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                              <div class="card-body osahan-payment">
                                 <div class="row">
                                    <div class="col-sm-4 pr-0">
                                       <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                          <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="icofont-credit-card"></i> Credit/Debit Cards</a>
                                          <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="icofont-id-card"></i> UPI(PhonePe/Google Pay)</a>
                                          <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="icofont-bank-alt"></i> Netbanking</a>
                                          <a class="nav-link active" id="v-pills-cash-tab" data-toggle="pill" href="#v-pills-cash" role="tab" aria-controls="v-pills-cash" aria-selected="false"><i class="icofont-money"></i> Cash on Delivery</a>
                                       </div>
                                    </div>
                                    <div class="col-sm-8 pl-0">
                                       <div class="tab-content h-100" id="v-pills-tabContent">
                                          <div class="tab-pane fade" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                             <h6 class="mb-3 mt-0 mb-3">Add new card</h6>
                                             <p>WE ACCEPT <span class="osahan-card">
                                                <i class="icofont-visa-alt"></i> <i class="icofont-mastercard-alt"></i> <i class="icofont-american-express-alt"></i> <i class="icofont-payoneer-alt"></i> <i class="icofont-apple-pay-alt"></i> <i class="icofont-bank-transfer-alt"></i> <i class="icofont-discover-alt"></i> <i class="icofont-jcb-alt"></i>
                                                </span>
                                             </p>
                                             <form>
                                                <div class="form-row">
                                                   <div class="form-group col-md-12">
                                                      <label for="inputPassword4">Card number</label>
                                                      <div class="input-group">
                                                         <input type="number" class="form-control" placeholder="Card number">
                                                         <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="icofont-card"></i></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="form-group col-md-8">
                                                      <label>Valid through(MM/YY)
                                                      </label>
                                                      <input type="number" class="form-control" placeholder="Enter Valid through(MM/YY)">
                                                   </div>
                                                   <div class="form-group col-md-4">
                                                      <label>CVV
                                                      </label>
                                                      <input type="number" class="form-control" placeholder="Enter CVV Number">
                                                   </div>
                                                   <div class="form-group col-md-12">
                                                      <label>Name on card
                                                      </label>
                                                      <input type="text" class="form-control" placeholder="Enter Card number">
                                                   </div>
                                                   <div class="form-group col-md-12">
                                                      <div class="custom-control custom-checkbox">
                                                         <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                         <label class="custom-control-label" for="customCheck1">Securely save this card for a faster checkout next time.</label>
                                                      </div>
                                                   </div>
                                                   <div class="form-group col-md-12 mb-0">
                                                      <a href="/thanks" class="btn btn-success btn-block btn-lg">PAY <i class="icofont-rupee"></i>${rTotal} 
                                                      <i class="icofont-long-arrow-right"></i></a>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                          <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                             <h6 class="mb-3 mt-0 mb-3">Add new shop Card</h6>
                                             <p>WE ACCEPT  <span class="osahan-card">
                                                <i class="icofont-sage-alt"></i> <i class="icofont-stripe-alt"></i> <i class="icofont-google-wallet-alt-1"></i>
                                                </span>
                                             </p>
                                             <form>
                                                <div class="form-row">
                                                   <div class="form-group col-md-12">
                                                      <label for="inputPassword4">Card number</label>
                                                      <div class="input-group">
                                                         <input type="number" class="form-control" placeholder="Card number">
                                                         <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="icofont-card"></i></button>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="form-group col-md-8">
                                                      <label>Valid through(MM/YY)
                                                      </label>
                                                      <input type="number" class="form-control" placeholder="Enter Valid through(MM/YY)">
                                                   </div>
                                                   <div class="form-group col-md-4">
                                                      <label>CVV
                                                      </label>
                                                      <input type="number" class="form-control" placeholder="Enter CVV Number">
                                                   </div>
                                                   <div class="form-group col-md-12">
                                                      <label>Name on card
                                                      </label>
                                                      <input type="text" class="form-control" placeholder="Enter Card number">
                                                   </div>
                                                   <div class="form-group col-md-12">
                                                      <div class="custom-control custom-checkbox">
                                                         <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                         <label class="custom-control-label" for="customCheck1">Securely save this card for a faster checkout next time.</label>
                                                      </div>
                                                   </div>
                                                   <div class="form-group col-md-12 mb-0">
                                                      <a href="/thanks" class="btn btn-success btn-block btn-lg">PAY <i class="icofont-rupee"></i>${rTotal}
                                                      <i class="icofont-long-arrow-right"></i></a>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                          <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                             <h6 class="mb-3 mt-0 mb-3">Netbanking</h6>
                                             <form>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                   <label class="btn btn-outline-success active">
                                                   <input type="radio" name="options" id="option1" autocomplete="off" checked> HDFC <i class="icofont-check-circled"></i>
                                                   </label>
                                                   <label class="btn btn-outline-success">
                                                   <input type="radio" name="options" id="option2" autocomplete="off"> ICICI <i class="icofont-check-circled"></i>
                                                   </label>
                                                   <label class="btn btn-outline-success">
                                                   <input type="radio" name="options" id="option3" autocomplete="off"> AXIS <i class="icofont-check-circled"></i>
                                                   </label>
                                                </div>
                                                <hr>
                                                <div class="form-row">
                                                   <div class="form-group col-md-12">
                                                      <label>Select Bank
                                                      </label>
                                                      <br> <select class="custom-select form-control">
																		<option value="SELECT_BANK">---Select Bank---</option>
																		<option value="AIRTELMONEY">Airtel Payments
																			Bank</option>
																		<option value="ALLAHABAD">Allahabad Bank</option>
																		<option value="ANDHRA">Andhra Bank</option>
																		<option value="AUSMALLFINBANK">AU Small
																			Finance Bank</option>
																		<option value="BANDHAN">Bandhan Bank</option>
																		<option value="BASSIENCATHOLICCOOPB">Bassien
																			Catholic Co-Operative Bank</option>
																		<option value="BNPPARIBAS">BNP Paribas</option>
																		<option value="BOBAHRAIN">Bank of Bahrain and
																			Kuwait</option>
																		<option value="BOBARODA">Bank of Baroda</option>
																		<option value="BOBARODAC">Bank of Baroda
																			Corporate</option>
																		<option value="BOBARODAR">Bank of Baroda
																			Retail</option>
																		<option value="BOI">Bank of India</option>
																		<option value="BOM">Bank of Maharashtra</option>
																		<option value="CANARA">Canara Bank</option>
																		<option value="CATHOLICSYRIAN">Catholic
																			Syrian Bank</option>
																		<option value="CBI">Central Bank</option>
																		<option value="CITYUNION">City Union Bank</option>
																		<option value="CORPORATION">Corporation Bank</option>
																		<option value="COSMOS">Cosmos Co-op Bank</option>
																		<option value="DBS">digibank by DBS</option>
																		<option value="DCB">DCB BANK LTD</option>
																		<option value="DENA">Dena Bank</option>
																		<option value="DEUTSCHE">Deutsche Bank</option>
																		<option value="DHANBANK">Dhanalakshmi Bank</option>
																		<option value="FEDERALBANK">Federal Bank</option>
																		<option value="IDBI">IDBI Bank</option>
																		<option value="IDFC">IDFC FIRST Bank</option>
																		<option value="INDIANBANK">Indian Bank</option>
																		<option value="INDUSIND">IndusInd Bank</option>
																		<option value="IOB">Indian Overseas Bank</option>
																		<option value="JANATABANKPUNE">JANATA
																			SAHAKARI BANK LTD PUNE</option>
																		<option value="JKBANK">J&amp;K Bank</option>
																		<option value="KARNATAKA">Karnataka Bank</option>
																		<option value="KARURVYSYA">Karur Vysya Bank</option>
																		<option value="LAKSHMIVILAS">Lakshmi Vilas
																			Bank - Retail</option>
																		<option value="LAKSHMIVILASC">Lakshmi Vilas
																			Bank - Corporate</option>
																		<option value="OBC">Oriental Bank of Commerce</option>
																		<option value="PNB">Punjab National Bank</option>
																		<option value="PNBC">Punjab National Bank
																			Corporate</option>
																		<option value="PNSB">Punjab &amp; Sind Bank</option>
																		<option value="PUNJABMAHA">Punjab &amp;
																			Maharashtra Co-op Bank</option>
																		<option value="RBS">RBS</option>
																		<option value="SARASWAT">Saraswat Co-op Bank</option>
																		<option value="SHAMRAOVITHAL">Shamrao Vithal
																			Co-op Bank</option>
																		<option value="SHIVAMERCOOP">Shivalik
																			Mercantile Co-op Bank</option>
																		<option value="SOUTHINDIAN">The South Indian
																			Bank</option>
																		<option value="STANC">Standard Chartered Bank</option>
																		<option value="SYNDICATE">Syndicate Bank</option>
																		<option value="TMBANK">Tamilnad Mercantile
																			Bank Limited</option>
																		<option value="TNMERCANTILE">Tamil Nadu
																			Merchantile Bank</option>
																		<option value="TNSC">TNSC Bank</option>
																		<option value="UCO">UCO Bank</option>
																		<option value="UNIONBANK">Union Bank of India</option>
																		<option value="UNITEDBANK">United Bank of
																			India</option>
																		<option value="VIJAYABANK">Vijaya Bank</option>
																		<option value="YESBANK">Yes Bank</option>
																		<option value="ZOROACOPBANK">The Zoroastrian
																			Co-Operative Bank</option>
																	</select>
																</div>
                                                   <div class="form-group col-md-12 mb-0 ">
                                                      <a href="/thanks" class="btn btn-success btn-block btn-lg">PAY <i class="icofont-rupee"></i> ${rTotal}
                                                      <i class="icofont-long-arrow-right"></i></a>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                          <div class="tab-pane fade show active" id="v-pills-cash" role="tabpanel" aria-labelledby="v-pills-cash-tab">
                                             <h6 class="mb-3 mt-0 mb-3">Cash On Delivery</h6>
                                             <p>Please keep exact change handy to help us serve you better</p>
                                             <input type="hidden" id="cartId" value="${cart.cartId}">
                                             <hr>
                                             <form>
                                                <a href="/thanks" id="codBtn" class="btn btn-success btn-block btn-lg">PAY <i class="icofont-rupee"></i>${rTotal}
                                                <i class="icofont-long-arrow-right"></i></a>
                                                <br>
                                                
                                                <div id="btnLoading" class="col-md-12 text-center load-more">
                        <button class="btn btn-warning btn-sm" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        Loading...
                        </button> 
                     </div>
                                                
                                          </div>
                                          <div id="paymentErrorDiv" class="alert alert-danger"></div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="osahan-cart-item">
                     <h5 class="mb-3 mt-0 text-dark">Summary <span class="small text-success"></span></h5>
<c:set var="total" value="${0}"/>
<c:set var="totalDiscount" value="${0}"/>
                     <div class="bg-white rounded shadow-sm mb-3">
                          <c:forEach items="${items}" var="item">
                          <div class="cart-list-product">
                           <a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>
                           <img class="img-fluid" src=${item.imgPath}>
                           <span class="badge badge-success"><i class="icofont-rupee"></i>500 OFF</span>
                           <h5><a href="#">${item.productName}</a></h5>
                           <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
                           <p class="f-14 mb-0 text-dark float-right"><i class="icofont-rupee"></i>${item.price}<del class="small text-secondary"><i class="icofont-rupee"></i>${item.price +500}</del></p>
                           <p class="f-12 text-secondary float-left quantity-text">Quantity: 1</p>
   							<c:set var="total" value="${total + item.price}" />
   							<c:set var="totalDiscount" value="${totalDiscount+500}"/>
                        </div>
                          </c:forEach>
                     </div>
                     <div class="mb-3 bg-white rounded shadow-sm p-3 clearfix">
                        <p class="mb-1">Total Amount <span class="float-right text-dark"><i class="icofont-rupee"></i><c:out value="${total}"></c:out></span></p>
                        <p class="mb-1">Delivery Fee <span class="text-info" data-toggle="tooltip" data-placement="top" title="Delivery Fee">
                           <i class="icofont-info-circle"></i>
                           </span> <span class="float-right text-success"><strong>FREE 
					</strong></span>
                        </p>
                        <p class="mb-1 text-info">Total Discount 
                           <span class="float-right text-info"><i class="icofont-rupee"></i><c:out value="${totalDiscount}"></c:out></span>
                        </p>
                        <hr />
                        <h6 class="font-weight-bold text-danger mb-0">PAY  <span class="float-right"><i class="icofont-rupee"></i><c:out value="${total}"></c:out></span></h6>
                     	<input type="hidden" value="${total}"  id="totalAmount">
                     </div>
                  </div>
                  <div class="text-info">
                  You will save <strong><i class="icofont-rupee"></i><c:out value="${totalDiscount}"></c:out></strong> on this order.
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Footer -->
	<jsp:include page="footer.jsp" />

      <div class="cart-sidebar">
         <div class="cart-sidebar-header">
            <h5>
               My Cart <span class="text-info"></span> <a data-toggle="offcanvas" class="float-right" href="#"><i class="icofont icofont-close-line"></i>
               </a>
            </h5>
         </div>
         <div class="cart-sidebar-body">
            
         </div>
         <div class="cart-sidebar-footer">
            <div class="cart-store-details">
               <p>Sub Total <strong class="float-right"></strong></p>
               <p>Delivery Charges <strong class="float-right text-danger"></strong></p>
               <h6>Your total savings <strong class="float-right text-danger"></strong></h6>
            </div>
            <a href="#"><button class="btn btn-primary btn-lg btn-block text-left" type="button"><span class="float-left"><i class="icofont icofont-cart"></i> Proceed to Checkout </span><span class="float-right"><strong><i class="icofont-rupee"></i>0</strong> <span class="icofont icofont-bubble-right"></span></span></button></a>
         </div>
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- select2 Js -->
      <script src="vendor/select2/js/select2.min.js"></script>
      <!-- Owl Carousel -->
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
      
      
      
	<script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
	<link rel="stylesheet" href="js/demo.css?ver=3.4.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      
      <script>
      $(document).ready(function() {
    	$("#btnLoading").hide();
    	$("#paymentErrorDiv").hide();
  		$("#userRegSuccessDiv").hide();
		$("#userRegFailureDiv").hide();
		$("#userLoginFailureDiv").hide();
		$("#userLoginSuccessDiv").hide();

		$("#requestOTPSuccessDiv").hide();
		$("#requestOTPFailureDiv").hide();

		$("#verifyOTPSuccessDiv").hide();
		$("#verifyOTPFailureDiv").hide();


		$("#passwordUpdateSuccessDiv").hide();
		$("#passwordUpdateFailureDiv").hide();
		
		 $("#otpDiv").hide();

		 $("#newPasswordDiv").hide();
  		

  		

    	//$('#profileId li:contains(Login)').hide();
    	  var isUserLoggedIn=$("#isUserLoggedIn").val();

    		if(isUserLoggedIn == "true"){
    			$('#profileId li:contains(Login)').hide();
    			$("#forSignUp").hide();
    			$("#delPhoneNumberId").show();
    			$("#btnNextId").show();
    			$('#profileId li:contains(My Account)').show();
    		}else{
    			$('#profileId li:contains(My Account)').hide();
    			$("#delPhoneNumberId").hide();
    			$("#btnNextId").hide();
    			$("#forSignUp").show();
        	}
    	  
    	  $('#codBtn').click(function(event) {
  			event.preventDefault();
  			var order={};
  			order["orderStatus"] = "PENDING";
  			order["dealerId"] =$("#userId").val();
  			order["cartId"] = $("#cartId").val(); 
  			var phoneNumber=$("#phoneNumberId").text();
			var customerName=$("#customerName").text();
			order["newAddress"] = customerName+", "+$("#delAddress").text()+", Mobile Number:"+phoneNumber;
  			
  			order["totalAmount"] = parseInt($("#totalAmount").val());
  			$("#btnLoading").show();
   			$.ajax({
  					url : "/placeOrder",
  					contentType: 'application/json; charset=utf-8',
  				    dataType: 'json',
  				    type: 'POST',
  				    data: JSON.stringify(order),
  					success : function(data) {
  						if (data.actionPassed == false) {
  							$("#btnLoading").hide();
  							$("#paymentErrorDiv").html(data.message);
							$("#paymentErrorDiv").show();
  							return;
  						} else {
  							window.location.href = "/thanks?id="+data.responseId+"";
  						}
  					}
  				});
  			});

          
    	  $('#zipCodeBtn').click(function(event) {
  			event.preventDefault();
  			searchByPinCode();
  		});


    		$('#createAccountBtn').click(function(event) {
    			event.preventDefault();
    		  var user = {};
    		  user["username"] = $("#regUsername").val();
    		  user["password"] = $("#regPassword").val();
    		  user["phoneNumber"] = $("#regUsername").val();
    		  user["emailId"] = $("#regEmailId").val();
    		  var confirmPassword = $("#confirmPassword").val();

    			if(user.username == ""){
    				$("#userRegFailureDiv").html("Enter Mobile number or email id");
    				$("#userRegFailureDiv").show();
    				return;
    			}
    			
    			if(user.emailId == ""){
    				$("#userRegFailureDiv").html("Enter Email Id");
    				$("#userRegFailureDiv").show();
    				return;
    			}

    			if(user.password == ""){
    				$("#userRegFailureDiv").html("Enter Password");
    				$("#userRegFailureDiv").show();
    				return;
    			}

    			if(confirmPassword==""){
    				$("#userRegFailureDiv").html("Enter Confirm Password");
    				$("#userRegFailureDiv").show();
    				return;
    			}

    			
    			if(user.password != confirmPassword){
    				$("#userRegFailureDiv").html("Confirm Password does not match.");
    				$("#userRegFailureDiv").show();
    				return;
    			}

    			if($("#customCheck2").is(":not(:checked)")){
   	        	 $("#userRegFailureDiv").html("You must agree to terms and conditions.");
   					$("#userRegFailureDiv").show();
   	                return;
   	           }

    		  
    			$.ajax({
    				type : "POST",
    				url : "/registerNewUser",
    				contentType : "application/json",
    				processData : false,
    				data : JSON.stringify(user),
    				success : function(data) {
    					if (data.actionPassed == false) {
    						$("#userRegFailureDiv").html('<strong>'+data.message+'</strong>');
    						$("#userRegFailureDiv").show();
    						return;
    					} else {
    						$("#userRegFailureDiv").hide();
    						$("#userRegSuccessDiv").html("User Account Created.");
    						$("#userRegSuccessDiv").show();
    						$('#login').modal('hide');
    						refreshPage();
        				}
    				}
    			});
    		});


    		$('#btnRequestOTP').click(function(event) {
    			event.preventDefault();
    			  var user = {};
    			  user["phoneNumber"] = $("#mobileNumber").val();

    				if(user.phoneNumber =="" || user.emailId==""){
    					$("#requestOTPFailureDiv").html("Enter Phone Number OR Email Id");
    					$("#requestOTPFailureDiv").show();
    					$("#requestOTPFailureDiv").fadeTo(2000, 500).slideUp(500,
    							function() {
    								$("#requestOTPFailureDiv").slideUp(500);
    							});
    						return;
    					}
      			  
    			  $.ajax({
    					type : "POST",
    					url : "/requestOTP",
    					contentType : "application/json",
    					processData : false,
    					data : JSON.stringify(user),
    					success : function(data) {
    						if (data.actionPassed == false) {
    							$("#requestOTPFailureDiv").html(data.message);
    							$("#requestOTPFailureDiv").show();

    							$("#requestOTPFailureDiv").fadeTo(2000, 500).slideUp(500,
    									function() {
    										$("#requestOTPFailureDiv").slideUp(500);
    									});
    							
    							 $("#textOtp").attr("disabled", "disabled"); 
    				        	 $("#btnVerifyOTP").prop("disabled", true );
    							
    							return;
    						} else {
    								$("#requestOTPSuccessDiv").html(data.message);
    								$("#requestOTPSuccessDiv").show();	

    								$("#requestOTPSuccessDiv").fadeTo(5000, 500).slideUp(500,
        									function() {
        										$("#requestOTPSuccessDiv").slideUp(500);
        									});

    								$("#textUserId").val(data.responseId);
    								$("#requestOTPBtnDiv").hide();
    								$("#otpDiv").show();
    								  $("#textOtp").removeAttr("disabled");
    								  $("#btnVerifyOTP").prop("disabled",false);
    						}
    					}
    				});
    	  	});

    		$('#btnForgotPassword').click(function(event) {
    			$('.nav-link-login').removeClass('active');
    			$('#forgot-form-tab').addClass('active');
    			$(".tab-pane").find(".active").removeClass("active");
    		});



    		$('#btnSubmitNewPassword').click(function(event) {
    			event.preventDefault();
    			var user = {};
    			 user["phoneNumber"] = $("#mobileNumber").val();
    			 user["password"] = $("#newPassword").val();
    			 user["userId"] = $("#textUserId").val();
    			 
    			  $.ajax({
    					type : "POST",
    					url : "/updateNewPassword",
    					contentType : "application/json",
    					processData : false,
    					data : JSON.stringify(user),
    					success : function(data) {
    						if (data.actionPassed == false) {
    							$("#paymentError").html(data.message);
    							$("#paymentError").show();
    							
    							return;
    						} else {
    					    $("#passwordUpdateSuccessDiv").html(data.message);
    						$("#passwordUpdateSuccessDiv").show();
    						$("#passwordUpdateSuccessDiv").fadeTo(4000, 500).slideUp(500,
    								function() {
    									$("#passwordUpdateSuccessDiv").slideUp(500);
    								});
    						 $("#newPasswordDiv").hide();
    						 $("#requestOTPBtnDiv").show();
    					 }
    					}
    				});
    			

            });

    		$('#btnVerifyOTP').click(function(event) {
    			event.preventDefault();
    			var user = {};
    			 user["phoneNumber"] = $("#mobileNumber").val();
    			 user["oneTimePassword"] = $("#textOtp").val();
    			 
    			  $.ajax({
    					type : "POST",
    					url : "/verifyOTP",
    					contentType : "application/json",
    					processData : false,
    					data : JSON.stringify(user),
    					success : function(data) {
    						if (data.actionPassed == false) {
    							$("#verifyOTPFailureDiv").html(data.message);
    							$("#verifyOTPFailureDiv").show();

    							$("#verifyOTPFailureDiv").fadeTo(4000, 500).slideUp(500,
    									function() {
    										$("#verifyOTPFailureDiv").slideUp(500);
    									});
    							
    							return;
    						} else {
    							$("#verifyOTPSuccessDiv").html(data.message);
    							$("#verifyOTPSuccessDiv").show();
    							$("#verifyOTPSuccessDiv").fadeTo(4000, 500).slideUp(500,
    									function() {
    										$("#verifyOTPSuccessDiv").slideUp(500);
    									});

    							$("#otpDiv").hide();
    							$("#newPasswordDiv").show();
    						}
    					}
    				});
    	  	});
    		
    		$('#enterloginBtn').click(function(event) {
      		  event.preventDefault();
      		  var user = {}
      		  user["username"] = $("#username").val();
      		  user["password"] = $("#password").val();
      		  if(user.username == ""){
    				$("#userLoginFailureDiv").html("Enter Mobile number or email id");
    				$("#userLoginFailureDiv").show();
    				return;
    			}

    			if(user.password == ""){
    				$("#userLoginFailureDiv").html("Enter Password");
    				$("#userLoginFailureDiv").show();
    				return;
    			}

    			$.ajax({
  				type : "POST",
  				url : "/validateUser",
  				contentType : "application/json",
  				processData : false,
  				data : JSON.stringify(user),
  				success : function(data) {
  					if (data.actionPassed == false) {
  						$("#userLoginFailureDiv").html(data.message);
  						$("#userLoginFailureDiv").show();
  						return;
  					} else {
  						$("#userLoginFailureDiv").hide();
  						$("#userLoginSuccessDiv").html(data.message);
  						$("#userLoginSuccessDiv").show();
  						$('#login').modal('hide');
  						refreshPage();
  					}
  				}
  			});
  		});

      	  
    	  $('#addAddressBtn').click(function(event) {
				var pinCode = $("#pincodeTextBox").val();
				var addressLine1 = $("#addressLine1").val();
				var Taluka = $("#selectBlock option:selected").text().split(":");
				
				var address={};

				address["AddressLine"] =addressLine1;
				address["Post"]=$("#selectVillage option:selected").text();
				address["Taluka"]=Taluka[1];
				address["District"]=$("#selectDistrict option:selected").text();
				address["State"]=$("#selectState option:selected").text();
				address["Country"] = "INDIA"
				address["Pincode"] = $("#pincodeTextBox").val();
				address["PhoneNumber"] = $("#delPhoneId").val();
				address["delAddressName"] = $("#delAddressName").val();
				
				var deliveryAddress=address;
				
				$("#delAddress").text(deliveryAddress.AddressLine+", "+deliveryAddress.Post+", "+deliveryAddress.Taluka+", "+deliveryAddress.District+", "+deliveryAddress.State+", "+deliveryAddress.Country+", "+deliveryAddress.Pincode);
				$("#phoneNumberId").text(address.PhoneNumber);
				$("#customerName").text(address.delAddressName);
				$("#defaultAddressCardId").show();
				
				$('#add-address-modal').modal('hide');
				
			});

 	    });

      function refreshPage() {
		    location.reload(true);
		}
      
		function searchByPinCode() {
			$("#selectVillage").html("");
			$("#selectBlock").html("");
			$("#selectDistrict").html("");
			$("#selectState").html("");
			var pinCode = $("#pincodeTextBox").val();
			var divDistrict;
			var divState;
			$.ajax({url : "https://api.postalpincode.in/pincode/"+ pinCode}).then(function(data) {
								if (data[0].Status == "Error") {
									alert("Enter Data Manually");
								} else {
									var postOfcName = {};
									$.each(data[0].PostOffice,function(index, value) {
											$.each(value, function(ind,val) {
																			postOfcName[ind] = val;
																		});
														var divName = "<option value="+postOfcName.Name+">"
																+ postOfcName.Name
																+ "</option>";
														var divBlock = "<option value="+postOfcName.Block+">"
																+ postOfcName.Name
																+ ":"
																+ postOfcName.Block
																+ "</option>";
														divDistrict = "<option value="+postOfcName.District+">"
																+ postOfcName.District
																+ "</option>";
														divState = "<option value="+postOfcName.State+">"
																+ postOfcName.State
																+ "</option>";
														$(divName)
																.appendTo(
																		'#selectVillage');
														$(divBlock)
																.appendTo(
																		'#selectBlock');
													});
									$(divDistrict).appendTo(
											'#selectDistrict');
									$(divState).appendTo('#selectState');
								}
							});
		}

		
      </script>
      
   </body>
</html>