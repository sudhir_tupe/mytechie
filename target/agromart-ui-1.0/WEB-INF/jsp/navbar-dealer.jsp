<nav class="navbar navbar-expand-lg navbar-dark pt-0 pb-0"  style="background-color:#51aa1b">
				<div class="container">
					<a class="navbar-brand" href="/">  <img
						src="img/agro-logo_new.png" alt="AGROMAART">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto main-nav-left">
							<!-- <li class="nav-item"><a class="nav-link" href="/"><font color="#ffffff"><i
									class="icofont-ui-home"></i></font></a></li> -->
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> 
								<font color="#ffffff">VEGGIE CROPS </font>
								
								
								</a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
													<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Vegetables</a>
												<a href="/product-grid">ONION</a> <a
													href="/product-grid">BHENDI</a> <a
													href="/product-grid">TOMATO</a> <a
													href="/product-grid">BITTER GOURD</a> <a
													href="/product-grid">BOTTLE GOURD</a> <a
													href="/product-grid">BRINJAL</a> <a
													href="/product-grid">CHILLI</a> <a
													href="/product-grid">CUCUMBER</a> <a
													href="/product-grid">CAULIFLOWER</a> <a
													href="/product-grid">CABBAGE</a>

											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Root
													Vegetables</a> <a href="/product-grid"> POTATO</a> <a
													href="/product-grid"> CARROT</a> <a
													href="/product-grid"> RADISH </a> <a
													href="/product-grid"> BEET ROOT</a> <a
													href="/product-grid">GINGER</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Leafy
													Vegetables</a> <a href="/product-grid">CORIANDER</a> <a
													href="product-grid.ht/product-gridl">PALAK</a> <a
													href="/product-grid">FENUGREEK</a> <a
													href="/product-grid">MINT PUDINA</a> <a
													href="/product-grid">COLOCASIA LEAF</a> <a
													href="/product-grid">CAPSICUM</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">FIBRE AND
													OILS</a> <a href="/product-grid">COTTON</a> <a
													href="/product-grid">SUNFLOWER</a> <a
													href="/product-grid">SOYABEAN</a> <a
													href="/product-grid">MUSTARD</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">CEREALS
													AND PULSES</a> <a href="/product-grid"> BAJRA</a> <a
													href="/product-grid"> JOWAR</a> <a
													href="/product-grid">WHEAT</a> <a
													href="/product-grid">PADDY</a> <a
													href="/product-grid">MAIZE</a> <a
													href="/product-grid">GRAM</a> <a
													href="/product-grid">PIGEON-PEA (TUR)</a> <a
													href="/product-grid">GREEN GRAM</a> <a
													href="/product-grid">BLACK GRAM</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">GARDENING</a>
												<a href="/product-grid"> LOTUS FLOWER</a> <a
													href="/product-grid"> FRENCH MARIGOLD</a> <a
													href="/product-grid">AFRICON MARIGOLD</a> <a
													href="/product-grid">ICE FLOWER</a> <a
													href="/product-grid">CHERRY TOMATO</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
											</div>
										</div>
									</div>
								</div></li>
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <font color="#ffffff">AGRO CHEMICALS</font></a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fungicides</a> 
												<a href="/product-grid">AMISTAR</a> <a
													href="/product-grid">CAPTAN</a> <a
													href="/product-grid">BAVISTIN</a>
												<a href="/product-grid">KAVACH</a> <a
													href="/product-grid">ACROBAT</a> <a
													href="/product-grid">FIVE STAR</a> <a
													href="/product-grid">ANTRACOL</a> <a
													href="/product-grid">AMISTAR TOP</a> <a
													href="/product-grid">AVTAR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Insecticides</a> 
												<a href="/product-grid">Actara</a> <a
													href="/product-grid">Ampligo</a> <a
													href="/product-grid">Chess</a> <a
													href="/product-grid">Pegasus</a> <a
													href="/product-grid">Alika</a> <a
													href="/product-grid">Matador</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Herbicides</a>
												<a href="/product-grid"> Rifit</a> <a
													href="/product-grid">Gramoxone</a> <a
													href="/product-grid">Axial</a> <a
													href="/product-grid">Rifit Plus</a> <a
													href="/product-grid">Fusiflex</a> <a
													href="/product-grid">Topik</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Plant Growth Regulator(PGR)</a>
												<a href="/product-grid">Moddus</a> <a
													href="/product-grid">Palisade EC</a> <a
													href="/product-grid">Prime+ EC</a>
											</div>
										</div>
									</div>
								</div></li>
							<li class="nav-item dropdown">
							<a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#ffffff"> PRODUCTS </font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="/product-grid">VEGETABLE CROPS</a> 
									<a class="dropdown-item" href="product-detail.html">FIELD
										CROPS</a> 
									<a class="dropdown-item" href="product-detail.html">AGRO
										CHEMICALS</a>
									<a class="dropdown-item" href="product-detail.html">Fertilizer</a> 
									<a class="dropdown-item" href="product-detail.html">PLANTS GROWTH REGULATORS</a>
										<a class="dropdown-item" href="checkout.html">Checkout</a>
									<a class="dropdown-item" href="profile.html">My Account</a> <a
										class="dropdown-item" href="about-us.html">About Us</a> <a
										class="dropdown-item" href="faq.html">FAQ</a> <a
										class="dropdown-item" href="contact-us.html">Contact Us</a>
								</div></li>
						</ul>
						<form class="form-inline my-2 my-lg-0 top-search">
							<button class="btn-link" type="submit">
								<i class="icofont-search"></i>
							</button>
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search for products, brands and more"
								aria-label="Search">
						</form>
						
						
						
						<ul class="navbar-nav ml-auto profile-nav-right">
                        <li class="nav-item dropdown">
                           <a class="nav-link ml-0 dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <img alt="Generic placeholder image" src="img/user/dealer2.jpg" class="nav-osahan-pic rounded-pill"> <font color="#ffffff"> My Account</font>
                           </a>
                           <div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
 									<a class="dropdown-item" href="#">Hello <strong>${userName}</strong></a>
 
                              <a class="dropdown-item" href="#"><i class="icofont-location-pin"></i>My Address</a>
                              <a class="dropdown-item" href="#"><i class="icofont-heart"></i> Wish List</a>
                              <a class="dropdown-item" href="#"><i class="icofont-list"></i> Order List</a>
                              <a class="dropdown-item" href="#"><i class="icofont-users"></i> Order Status</a>
                              <a class="dropdown-item Logout"><i class="icofont-logout"></i> Logout</a>
                           </div>
                        </li>
                     </ul>
					</div>
				</div>
			</nav>
