<!DOCTYPE html>
<html lang="en">
  <head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="AGRODHAN">
<meta name="Keywords"
	content="Online Agro Shopping in India,online Agro Shopping store,Online Agro Shopping Site, Buy Online,Shop Online,Online Shopping,Agrodhan" />
<meta name="Description"
	content="India&#x27;s biggest online agro store for Vegetable Seeds,Field Crops Seeds,Fertilisers,Water Soluble fertilisers,Organic Products,Pesticides,Agri Equipments,Micronutrients,Insecticides,Fungicides,Herbicides,Bio Pesticides,Flower Seeds,Onion Seeds from all brands at the lowest prices in India. Payment options - COD, EMI, Credit card, Debit card &amp;amp; more." />
<meta name="google-site-verification"
	content="3RnLdERPVodJ4P2YohoQWVROxQqbmN8xzF-KRmOpc2k" />
<title>Online Agro Shopping Site for Vegetable Seeds, Agro
	Chemicals &amp; More. Best Offers!</title>


<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
          <link rel="icon" type="image/png" href="img/fav-icon.png">
    
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
    <script src="lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>
  <body class=""> 
  <!--<![endif]-->
    
    
    
    

        
        
<div class="main-nav shadow-sm">
<nav class="navbar navbar-expand-lg navbar-dark pt-0 pb-0" style="background-color:#1f7a1f">
				<div class="container">
				
					<!-- <a class="navbar-brand" href="/">  <img
						src="img/green_bag_logo.png" alt="AGROMAART">
					 -->	
				
					<a class="navbar-brand" href="/">  <img
						src="img/nlogo.png" alt="AGRODHAN">
				
						
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto main-nav-left">
							<!-- <li class="nav-item"><a class="nav-link" href="/"><font color="#ffffff">
							 <i class="icofont-ui-home"></i> 
							</font></a></li> -->
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> 
								<font color="#ffffff">SEEDS</font>
								</a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
													<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Vegetables Seeds</a>
												<a href="/product-grid" title="ONION">ONION</a> <a
													href="/product-grid" title="BHENDI">BHENDI</a> <a
													href="/product-grid" title="TOMATO">TOMATO</a> <a
													href="/product-grid" title="BITTER GOURD">BITTER GOURD</a> <a
													href="/product-grid" title="BOTTLE GOURD">BOTTLE GOURD</a> <a
													href="/product-grid" title="BRINJAL">BRINJAL</a> <a
													href="/product-grid" title="CHILLI">CHILLI</a> <a
													href="/product-grid" title="CUCUMBER">CUCUMBER</a> <a
													href="/product-grid" title="CAULIFLOWER">CAULIFLOWER</a> <a
													href="/product-grid" title="CABBAGE">CABBAGE</a>

											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Root
													Vegetables Seeds</a> <a href="/product-grid" title="POTATO"> POTATO</a> <a
													href="/product-grid" title="CARROT"> CARROT</a> <a
													href="/product-grid" title="RADISH"> RADISH </a> <a
													href="/product-grid" title="BEET ROOT"> BEET ROOT</a> <a
													href="/product-grid" title="GINGER">GINGER</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Leafy
													Vegetables Seeds</a> <a href="/product-grid" title="CORIANDER">CORIANDER</a> <a
													href="product-grid.ht/product-gridl"  title="PALAK">PALAK</a> <a
													href="/product-grid"  title="FENUGREEK">FENUGREEK</a> <a
													href="/product-grid"  title="MINT PUDINA">MINT PUDINA</a> <a
													href="/product-grid"  title="COLOCASIA LEAF">COLOCASIA LEAF</a> <a
													href="/product-grid"  title="CAPSICUM">CAPSICUM</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fibre and
													Oil Seeds</a> <a href="/product-grid" title="COTTON">COTTON</a> <a
													href="/product-grid" title="SUNFLOWER">SUNFLOWER</a> <a
													href="/product-grid" title="SOYABEAN">SOYABEAN</a> <a
													href="/product-grid" title="MUSTARD">MUSTARD</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Cereals
													and Pulses</a> <a href="/product-grid" title="BAJRA"> BAJRA</a> <a
													href="/product-grid" title="JOWAR"> JOWAR</a> <a
													href="/product-grid" title="WHEAT">WHEAT</a> <a
													href="/product-grid" title="PADDY">PADDY</a> <a
													href="/product-grid" title="MAIZE">MAIZE</a> <a
													href="/product-grid" title="GRAM">GRAM</a> <a
													href="/product-grid" title="PIGEON-PEA (TUR)">PIGEON-PEA (TUR)</a> <a
													href="/product-grid" title="GREEN GRAM">GREEN GRAM</a> <a
													href="/product-grid" title="BLACK GRAM">BLACK GRAM</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Gardening Seeds</a>
												<a href="/product-grid" title="LOTUS FLOWER"> LOTUS FLOWER</a> <a
													href="/product-grid" title="FRENCH MARIGOLD"> FRENCH MARIGOLD</a> <a
													href="/product-grid" title="AFRICON MARIGOLD">AFRICON MARIGOLD</a> <a
													href="/product-grid" title="ICE FLOWER">ICE FLOWER</a> <a
													href="/product-grid" title="CHERRY TOMATO">CHERRY TOMATO</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
											</div>
										</div>
									</div>
								</div></li>
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <font color="#ffffff">AGRO CHEMICALS</font></a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fungicides</a> 
												<a href="/product-grid" title="AMISTAR">AMISTAR</a> <a
													href="/product-grid" title="CAPTAN">CAPTAN</a> <a
													href="/product-grid" title="BAVISTIN">BAVISTIN</a>
												<a href="/product-grid" title="KAVACH">KAVACH</a> <a
													href="/product-grid" title="ACROBAT">ACROBAT</a> <a
													href="/product-grid" title="FIVE STAR">FIVE STAR</a> <a
													href="/product-grid" title="ANTRACOL">ANTRACOL</a> <a
													href="/product-grid" title="AMISTAR TOP">AMISTAR TOP</a> <a
													href="/product-grid" title="AVTAR">AVTAR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Insecticides</a> 
												<a href="/product-grid" title="ACTARA">ACTARA</a> <a
													href="/product-grid" title="AMPLIGO">AMPLIGO</a> <a
													href="/product-grid" title="CHESS">CHESS</a> <a
													href="/product-grid" title="PEGASUS">PEGASUS</a> <a
													href="/product-grid" title="ALIKA">ALIKA</a> <a
													href="/product-grid" title="METADOR">METADOR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Herbicides</a>
												<a href="/product-grid" title="RIFIT"> RIFIT</a> <a
													href="/product-grid" title="GRAMOXONE">GRAMOXONE</a> <a
													href="/product-grid" title="AXIAL">AXIAL</a> <a
													href="/product-grid" title="RIFIT PLUS">RIFIT PLUS</a> <a
													href="/product-grid" title="FUSIFLEX">FUSIFLEX</a> <a
													href="/product-grid" title="TOPIK">TOPIK</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Plant Growth Regulator(PGR)</a>
												<a href="/product-grid" title="MODDUS">MODDUS</a> <a
													href="/product-grid" title="PALISADE EC">PALISADE EC</a> <a
													href="/product-grid" title="PRIME+ EC">PRIME+ EC</a>
											</div>
										</div>
									</div>
								</div></li>
								
										<li class="nav-item dropdown"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#ffffff">FERTILIZERS</font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="#">VERMI COMPOST</a>
								</div></li>
								
								<li class="nav-item dropdown"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#ffffff">HARDWARE</font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="#" title="WATER MOTOR PUMP">WATER MOTOR PUMP</a> 
									<a class="dropdown-item" href="#" title="STARTERS">STARTERS</a>
									<a class="dropdown-item" href="#" title="SPRAYER PUMP (BATTERY)">SPRAYER PUMP (BATTERY)</a>
									<a class="dropdown-item" href="#" title="SPRINKLER">SPRINKLER</a>  
									<a class="dropdown-item" href="#" title="DRIP IRRIGATION">DRIP IRRIGATION</a> 
									<a class="dropdown-item" href="#" title="PIPE">PIPE</a>
									<a class="dropdown-item" href="#" title="MILK CANS">MILK CANS</a>
									<a class="dropdown-item" href="#" title="LED TORCH">LED TORCH</a>
								</div></li>
						</ul>
						<form class="form-inline my-2 my-lg-0 top-search">
							<button class="btn-link" type="submit">
								<i class="icofont-search"></i>
							</button>
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search for products, brands.."
								aria-label="Search">
						</form>
						
                <ul class="navbar-nav ml-auto profile-nav-right" id="profileId">
                  </ul>
					</div>
				</div>
			</nav>


			</div>
            <div class="content">
        <div class="header">
        <br>
            <h3 class="text-center">Refund/Return Policy</h3>
        </div>
                <ul class="breadcrumb">
            <li>Welcome to <a href="/"> <strong>agrodhan.com</strong></a>.  Agrodhan.com is a platform, where agricultural product sellers and seed manufactures who has Government License can advertise to promote and sell their agriculture input authorized products, thus we (agrodhan.com) act as a bridge between sellers and buyers for agricultural input products. agrodhan.com's mission is to help farmers of India by providing reliable agriculture products at a reasonable rate.</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
<h5>Refund or Return Policy</h5>
A refund will be considered only if the request is made within 7 days of placing an order. (If the product is damaged, Duplicate or quantity varies).
<br><br>

<h6>The return will be processed only if:</h6>

<ul>
<li> It is determined that the product was not damaged while in your possession</li>
<li>The product is not different from what was shipped to you</li>
</ul>

<ul>
					<li>In case of receipt of damaged or defective items please report the same to our Customer Service team. The request will, however, be entertained once the merchant has checked and determined the same at his own end. This should be reported within 7 days of receipt of the products.
					</li>
					<li>In case you feel that the product received is not as shown on the site or as per your expectations, you must bring it to the notice of our customer service within 24 hours of receiving the product. The Customer Service Team after looking into your complaint will take an appropriate decision.
					</li>
					<li>NOTE: If you have done online payment and you are canceling the order without any proper reason,  the payment gateway charge 3% plus GST will be deducted from your refund amount.
					</li>
					<li>Its your responsibility to take out print of the manifest that agrodhan provides, while returning the product.
					</li>
					<li>To request a refund, simply mail us on <strong>agrodhan2020@gmail.com</strong>  with your purchase details within <strong>seven (7) days</strong> of your purchase. Please include your order number (sent to you via email/message after ordering) and optionally tell us why you’re requesting a refund – we take customer feedback very seriously and use it to constantly improve our products and quality of service. Refunds are being processed after customer support team validate and analyze issue, but it will only show in your bank account only when the product comes back to hub. Once the refund is processed we will intimate you through EMAIL and SMS with the refund transaction details.
					</li>
					<li>To complete your return, we require a receipt or proof of purchase.
					</li>
					<li>Please do not send your purchase back to the manufacturer.The refund is only possible if the product is unused, undamaged, and is with the original packaging and with the receipt and the invoice. Only when the product will reach premises we will refund you your amount as decided.
					</li>
					<li>NOTE :  For a refund, the name of the customer must be the same as registered(agrodhan) account name and bank account name, Customer must send a cheque photo or passbook photo for a refund with Adhaar Card or PAN card.
					</li>

				</ul>
For query mail us at <a href=""><strong>agrodhan2020@gmail.com</strong></a> 
<br><br>

<strong>
The seeds germination depends on the soil, atmosphere, fertilizers, watering etc. Agrodhan never claims about seeds germination and harvesting and so refund is not possible in seeds.
</strong>
<br><br>

<strong>
No Warranty on Live Plants as Growing, flowering and fruiting are subject to So many climatic condition like Air, Kind of soil, water, humidity and temperature beyond our control. 
</strong>
<br><br>
Kindly note that refund will be made only after proper investigation of the whole scenario by our special team.

<br><br>
Agrodhan reserve the rights to change or update the terms and conditions at any time with or without any prior notice. To make sure you are always updated of the terms and conditions, please review this website periodically. 
<br><br>
Disputes shall be subject to exclusive jurisdiction of the courts of  Maharashtra 
            </div>
        </div>
    </div>

<!-- Footer -->
   <footer class="bg-white border-bottom border-top">
      <div class="container">
      </div>
      <!-- /.container -->
   </footer>
   <div class="copyright bg-light py-3">
      <div class="container">
         <div class="row">
            <div class="col-md-6 d-flex align-items-center">
               <p class="mb-0"><i class="icofont icofont-copyright"></i> Copyright 2020 <a href="#">AGRODHAN.COM </a> All Rights Reserved.
               </p>
            </div>
            <div class="col-md-6 text-right">
               <img class="img-fluid" src="img/payment_methods.png">
            </div>
         </div>
      </div>
   </div>

    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>