<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>TECHIES</title>
<meta content="TECHIES" name="descriptison">
<meta content="techies" name="keywords">

<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
	<!-- ======= Header ======= -->
	<header id="header" class="fixed-top ">
		<div class="container-fluid">

			<div class="row justify-content-center">
				<div class="col-xl-9 d-flex align-items-center">
					<h1 class="logo mr-auto">
						<a>Techies</a>
					</h1>
					<!-- Uncomment below if you prefer to use an image logo -->
					<!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

					<nav class="nav-menu d-none d-lg-block">
						<ul>
							<li class="active"><a href="/home">Home</a></li>
							<li><a href="#about">About</a></li>
							<li><a href="#services">Services</a></li>
							<li><a href="#team">Team</a></li>
							<li class="drop-down"><a href="">Courses</a>
								<ul>
									<li><a href="#">Internship Programs</a></li>
									<li class="drop-down"><a href="#"> Software Training</a>
										<ul>
											<li><a href="#">C, C++</a></li>
											<li><a href="#">Java</a></li>
											<li><a href="#">Advance Java</a></li>
											<li><a href="#">AWS</a></li>
											<li><a href="#">DevOps</a></li>
											<li><a href="#">Placement Training</a></li>
										</ul></li>
									<li><a href="#">Placement Assistance</a></li>
									<li><a href="#">Live Projects</a></li>
								</ul></li>
							<li><a href="#contact">Contact</a></li>

						</ul>
					</nav>
					<!-- .nav-menu -->

					<button type="button" class="btn get-started-btn"
						data-toggle="modal" data-target="#exampleModal">Enroll
						Now</button>
				</div>
			</div>

		</div>
	</header>
	<!-- End Header -->

	<!-- ======= Hero Section ======= -->
	<section id="hero" class="d-flex align-items-center">

		<div class="container-fluid" data-aos="fade-up">
			<div class="row justify-content-center">
				<div
					class="col-xl-5 col-lg-6 pt-3 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
					<h1>Bettter Learning Experience With Techies</h1>
					<h2>We are team of talented experienced software engineers
						guiding CS and IT engineering Students</h2>
					<div>
						<a href="#about" class="btn-get-started scrollto">Get Started</a>
					</div>
				</div>
				<div class="col-xl-4 col-lg-6 order-1 order-lg-2 hero-img"
					data-aos="zoom-in" data-aos-delay="150">
					<img src="assets/img/hero-img.png" class="img-fluid animated"
						alt="">
				</div>
			</div>
		</div>

	</section>
	<!-- End Hero -->

	<main id="main">

		<!-- ======= About Section ======= -->
		<section id="about" class="about">
			<div class="container">

				<div class="row">
					<div class="col-lg-6 order-1 order-lg-2" data-aos="zoom-in"
						data-aos-delay="150">
						<img src="assets/img/about.jpg" class="img-fluid" alt="">
					</div>
					<div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content"
						data-aos="fade-right">
						<h3>Welcome to the Techies</h3>
						<p class="font-italic">Techies is the leading Technology
							Learning Institute in Pune</p>
						<ul>
							<li><i class="icofont-check-circled"></i> Software
								Development Training</li>
							<li><i class="icofont-check-circled"></i> IT Internship
								Program</li>
							<li><i class="icofont-check-circled"></i> Live Projects
								Training</li>
							<li><i class="icofont-check-circled"></i> Campus Placement
								Assistance</li>
						</ul>
						<a href="#" class="read-more">Read More <i
							class="icofont-long-arrow-right"></i></a>
					</div>
				</div>

			</div>
		</section>
		<!-- End About Section -->

		<!-- ======= Counts Section ======= -->
		<section id="counts" class="counts">
			<div class="container">

				<div class="row counters">

					<div class="col-lg-3 col-6 text-center">
						<span data-toggle="counter-up">120</span>
						<p>Clients</p>
					</div>

					<div class="col-lg-3 col-6 text-center">
						<span data-toggle="counter-up">555</span>
						<p>Projects</p>
					</div>

					<div class="col-lg-3 col-6 text-center">
						<span data-toggle="counter-up">1,463</span>
						<p>Hours Of Support</p>
					</div>

					<div class="col-lg-3 col-6 text-center">
						<span data-toggle="counter-up">15</span>
						<p>Expert Software Engineers</p>
					</div>

				</div>

			</div>
		</section>
		<!-- End Counts Section -->
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">TECHIES - Online
							Training Program</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form:form modelAttribute="enrollDTO" id="frm" method="POST"
							action="/enroll" class="php-email-form">

							<div class="form-row">
								<div class="col-md-6 form-group">
									<form:input type="text" path="enrollName" class="form-control"
										id="enrollName" placeholder="Your Name" />
								</div>

								<div class="col-md-6 form-group">
									<form:input type="text" path="enrollPhone" class="form-control"
										id="enrollPhone" placeholder="Your Mobile" />
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-6 form-group">
									<form:input path="enrollEmail" type="text" class="form-control"
										id="enrollEmail" placeholder="Your Email" />
								</div>
								<div class="col-md-6 form-group">
									<form:select path="enrollCourse" class="form-control">
										<form:option path="enrollCourse" value="NONE">Select Course</form:option>
										<form:option path="enrollCourse" value="C, C++">C, C++</form:option>
										<form:option path="enrollCourse" value="JAVA">JAVA</form:option>
										<form:option path="enrollCourse" value="DevOps">DevOps</form:option>
										<form:option path="enrollCourse" value="AWS">AWS</form:option>
										<form:option path="enrollCourse" value="IT Internship">IT Internship</form:option>
										<form:option path="enrollCourse" value="Live Projects Training">Live Projects Training</form:option>
									</form:select>
								</div>
							</div>
							<div id="successDivEnroll" class="alert alert-success">
								<strong>Thank you!</strong> Our counsellor will get back to you.
							</div>
							<div id="errorDivEnroll" class="hide"></div>
						</form:form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
						<button type="button" id="enrollBtn" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</div>


		<!-- ======= Services Section ======= -->
		<section id="services" class="services section-bg">
			<div class="container" data-aos="fade-up">

				<div class="section-title">
					<h2>Services</h2>
					<p>Techies is a leading software training institute provides
						Software Training, Project Guidance, IT Consulting and Technology
						workshops. We provide high-value corporate training services that
						enable our clients to enhance business performance, accelerate
						time-to-market, increase productivity and improve customer
						service.</p>
				</div>

				<div class="row">
					<div class="col-lg-4 col-md-6 d-flex align-items-stretch"
						data-aos="zoom-in" data-aos-delay="100">
						<div class="icon-box iconbox-blue">
							<div class="icon">
								<svg width="100" height="100" viewBox="0 0 600 600"
									xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5"
										d="M300,521.0016835830174C376.1290562159157,517.8887921683347,466.0731472004068,529.7835943286574,510.70327084640275,468.03025145048787C554.3714126377745,407.6079735673963,508.03601936045806,328.9844924480964,491.2728898941984,256.3432110539036C474.5976632858925,184.082847569629,479.9380746630129,96.60480741107993,416.23090153303,58.64404602377083C348.86323505073057,18.502131276798302,261.93793281208167,40.57373210992963,193.5410806939664,78.93577620505333C130.42746243093433,114.334589627462,98.30271207620316,179.96522072025542,76.75703585869454,249.04625023123273C51.97151888228291,328.5150500222984,13.704378332031375,421.85034740162234,66.52175969318436,486.19268352777647C119.04800174914682,550.1803526380478,217.28368757567262,524.383925680826,300,521.0016835830174"></path>
                </svg>
								<i class="bx bxl-dribbble"></i>
							</div>
							<h4>
								<a href="">IT Internship Programs</a>
							</h4>
							<p>The IT internship is designed for students who have a
								passion for technology. It offers a wide range of opportunities
								for graduate and postgraduate students to gain insight in the
								technology.</p>
						</div>
					</div>

					<div
						class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0"
						data-aos="zoom-in" data-aos-delay="200">
						<div class="icon-box iconbox-orange ">
							<div class="icon">
								<svg width="100" height="100" viewBox="0 0 600 600"
									xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5"
										d="M300,582.0697525312426C382.5290701553225,586.8405444964366,449.9789794690241,525.3245884688669,502.5850820975895,461.55621195738473C556.606425686781,396.0723002908107,615.8543463187945,314.28637112970534,586.6730223649479,234.56875336149918C558.9533121215079,158.8439757836574,454.9685369536778,164.00468322053177,381.49747125262974,130.76875717737553C312.15926192815925,99.40240125094834,248.97055460311594,18.661163978235184,179.8680185752513,50.54337015887873C110.5421016452524,82.52863877960104,119.82277516462835,180.83849132639028,109.12597500060166,256.43424936330496C100.08760227029461,320.3096726198365,92.17705696193138,384.0621239912766,124.79988738764834,439.7174275375508C164.83382741302287,508.01625554203684,220.96474134820875,577.5009287672846,300,582.0697525312426"></path>
                </svg>
								<i class="bx bx-file"></i>
							</div>
							<h4>
								<a href="">Campus Placement Assistance</a>
							</h4>
							<p>We provide a series of Online Assessment & Mock interview
								sessions. Company specific assessment. Mock GD & Interview
								Sessions Webinars and students Interactive programs.</p>
						</div>
					</div>

					<div
						class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0"
						data-aos="zoom-in" data-aos-delay="300">
						<div class="icon-box iconbox-pink">
							<div class="icon">
								<svg width="100" height="100" viewBox="0 0 600 600"
									xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5"
										d="M300,541.5067337569781C382.14930387511276,545.0595476570109,479.8736841581634,548.3450877840088,526.4010558755058,480.5488172755941C571.5218469581645,414.80211281144784,517.5187510058486,332.0715597781072,496.52539010469104,255.14436215662573C477.37192572678356,184.95920475031193,473.57363656557914,105.61284051026155,413.0603344069578,65.22779650032875C343.27470386102294,18.654635553484475,251.2091493199835,5.337323636656869,175.0934190732945,40.62881213300186C97.87086631185822,76.43348514350839,51.98124368387456,156.15599469081315,36.44837278890362,239.84606092416172C21.716077023791087,319.22268207091537,43.775223500013084,401.1760424656574,96.891909868211,461.97329694683043C147.22146801428983,519.5804099606455,223.5754009179313,538.201503339737,300,541.5067337569781"></path>
                </svg>
								<i class="bx bx-tachometer"></i>
							</div>
							<h4>
								<a href="">Software Development Training</a>
							</h4>
							<p>Techies is the Best Software Training Institute for C,
								C++, Java, C#.NET, ASP.NET, AWS, DevOps, Live Projects</p>
						</div>
					</div>

					<div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4"
						data-aos="zoom-in" data-aos-delay="100">
						<div class="icon-box iconbox-yellow">
							<div class="icon">
								<svg width="100" height="100" viewBox="0 0 600 600"
									xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5"
										d="M300,503.46388370962813C374.79870501325706,506.71871716319447,464.8034551963731,527.1746412648533,510.4981551193396,467.86667711651364C555.9287308511215,408.9015244558933,512.6030010748507,327.5744911775523,490.211057578863,256.5855673507754C471.097692560561,195.9906835881958,447.69079081568157,138.11976852964426,395.19560036434837,102.3242989838813C329.3053358748298,57.3949838291264,248.02791733380457,8.279543830951368,175.87071277845988,42.242879143198664C103.41431057327972,76.34704239035025,93.79494320519305,170.9812938413882,81.28167332365135,250.07896920659033C70.17666984294237,320.27484674793965,64.84698225790005,396.69656628748305,111.28512138212992,450.4950937839243C156.20124167950087,502.5303643271138,231.32542653798444,500.4755392045468,300,503.46388370962813"></path>
                </svg>
								<i class="bx bx-layer"></i>
							</div>
							<h4>
								<a href="">Expert Software Engineers</a>
							</h4>
							<p>We are team of expert and talented software developers.
								Working in MNCs</p>
						</div>
					</div>

					<div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4"
						data-aos="zoom-in" data-aos-delay="200">
						<div class="icon-box iconbox-red">
							<div class="icon">
								<svg width="100" height="100" viewBox="0 0 600 600"
									xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5"
										d="M300,532.3542879108572C369.38199826031484,532.3153073249985,429.10787420159085,491.63046689027357,474.5244479745417,439.17860296908856C522.8885846962883,383.3225815378663,569.1668002868075,314.3205725914397,550.7432151929288,242.7694973846089C532.6665558377875,172.5657663291529,456.2379748765914,142.6223662098291,390.3689995646985,112.34683881706744C326.66090330228417,83.06452184765237,258.84405631176094,53.51806209861945,193.32584062364296,78.48882559362697C121.61183558270385,105.82097193414197,62.805066853699245,167.19869350419734,48.57481801355237,242.6138429142374C34.843463184063346,315.3850353017275,76.69343916112496,383.4422959591041,125.22947124332185,439.3748458443577C170.7312796277747,491.8107796887764,230.57421082200815,532.3932930995766,300,532.3542879108572"></path>
                </svg>
								<i class="bx bx-slideshow"></i>
							</div>
							<h4>
								<a href="">Live Sessions</a>
							</h4>
							<p>We provide online and offline training to the Engineering
								Graduates</p>
						</div>
					</div>

					<div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4"
						data-aos="zoom-in" data-aos-delay="300">
						<div class="icon-box iconbox-teal">
							<div class="icon">
								<svg width="100" height="100" viewBox="0 0 600 600"
									xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5"
										d="M300,566.797414625762C385.7384707136149,576.1784315230908,478.7894351017131,552.8928747891023,531.9192734346935,484.94944893311C584.6109503024035,417.5663521118492,582.489472248146,322.67544863468447,553.9536738515405,242.03673114598146C529.1557734026468,171.96086150256528,465.24506316201064,127.66468636344209,395.9583748389544,100.7403814666027C334.2173773831606,76.7482773500951,269.4350130405921,84.62216499799875,207.1952322260088,107.2889140133804C132.92018162631612,134.33871894543012,41.79353780512637,160.00259165414826,22.644507872594943,236.69541883565114C3.319112789854554,314.0945973066697,72.72355303640163,379.243833228382,124.04198916343866,440.3218312028393C172.9286146004772,498.5055451809895,224.45579914871206,558.5317968840102,300,566.797414625762"></path>
                </svg>
								<i class="bx bx-arch"></i>
							</div>
							<h4>
								<a href="">Placement Opportunities</a>
							</h4>
							<p>On Campus / Off Campus Recruitments, Virtual Campus Drives</p>
						</div>
					</div>

				</div>

			</div>
		</section>
		<!-- End Services Section -->
		<!-- ======= Contact Section ======= -->
		<section id="contact" class="contact section-bg">
			<div class="container" data-aos="fade-up">

				<div class="section-title">
					<h2>Contact</h2>
					<p>
						Thank you for showing interest in Techies. 
						<p><button type="button" class="btn btn-outline-success"
						data-toggle="modal" data-target="#exampleModal">Enroll
						Now</button></p>
					</p>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="info-box mb-4">
							<i class="bx bx-map"></i>
							<h3>Our Address</h3>
							<p>Pancard club Road, Baner, Pune 411045 MH, India</p>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="info-box  mb-4">
							<i class="bx bx-envelope"></i>
							<h3>Email Us</h3>
							<p>csitcareers@gmail.com</p>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="info-box  mb-4">
							<i class="bx bx-phone-call"></i>
							<h3>Call Us</h3>
							<p>+91 7709462647</p>
						</div>
					</div>

				</div>
				<div class="row">

					<div class="col-lg-6 ">
						<iframe class="mb-4 mb-lg-0"
							src="https://maps.google.com/maps?q=Pancard%20club%20road&t=&z=11&ie=UTF8&iwloc=&output=embed"
							frameborder="0" style="border: 0; width: 100%; height: 384px;"
							allowfullscreen></iframe>
					</div>

					<div class="col-lg-6">

						<form:form modelAttribute="contactUsDTO" id="frm" method="POST"
							action="/contact" class="php-email-form">

							<div class="form-row">
								<div class="col-md-6 form-group">
									<form:input type="text" path="userName" class="form-control"
										id="userName" placeholder="Your Name" />
								</div>
								<div class="col-md-6 form-group">
									<form:input type="email" path="userEmail" class="form-control"
										id="userEmail" placeholder="Your Email" data-rule="email" />
								</div>
							</div>

							<div class="form-group">
								<form:input path="subject" type="text" class="form-control"
									id="subject" placeholder="Subject" />
							</div>
							<div class="form-group">
								<form:textarea class="form-control contactTextArea" id="message"
									rows="6" placeholder="Message" path="message" />
							</div>
							<div class="text-center">
								<button type="submit" class="contactButton" id="submitBtn">Send
									Message</button>
							</div>
							<div id="successDiv" class="alert alert-success">
								<strong>Success!</strong> Your message has been sent. Thank you!
							</div>
							<div id="errorDiv" class="hide"></div>
						</form:form>
					</div>

				</div>

			</div>
		</section>
		<!-- End Contact Section -->

	</main>
	<!-- End #main -->

	<!-- ======= Footer ======= -->
	<footer id="footer">

		<div class="footer-top">
			<div class="container">
				<div class="row">

					<div class="col-lg-3 col-md-6 footer-contact">
						<h3>Techies</h3>
						<p>
							Pancard club Road <br> Baner, Pune 411045<br> MH, India
							<br> <br> <strong>Phone:</strong> +1 7709462647<br>
							<strong>Email:</strong> csitcareers@gmail.com<br>
						</p>
					</div>

					<div class="col-lg-2 col-md-6 footer-links">
						<h4>Useful Links</h4>
						<ul>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">About
									us</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Terms
									of service</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Privacy
									policy</a></li>
						</ul>
					</div>

					<div class="col-lg-3 col-md-6 footer-links">
						<h4>Our Services</h4>
						<ul>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Web
									Design</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Web
									Development</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Product
									Management</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="#">Graphic
									Design</a></li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-6 footer-newsletter">
						<h4>Join Our Newsletter</h4>
						<p>Subscribe to get more update about our projects</p>
						<form action="" method="post">
							<input type="email" name="email"><input type="submit"
								value="Subscribe">
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="container">

			<div class="copyright-wrap d-md-flex py-4">
				<div class="mr-md-auto text-center text-md-left">
					<div class="copyright">
						&copy; Copyright <strong><span>Techies</span></strong>. All Rights
						Reserved
					</div>
					<div class="credits">Designed by CSIT-Careers</div>
				</div>
				<div class="social-links text-center text-md-right pt-3 pt-md-0">
					<a href="#" class="twitter"><i class="bx bxl-twitter"></i></a> <a
						href="#" class="facebook"><i class="bx bxl-facebook"></i></a> <a
						href="#" class="instagram"><i class="bx bxl-instagram"></i></a> <a
						href="#" class="google-plus"><i class="bx bxl-skype"></i></a> <a
						href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
				</div>
			</div>

		</div>
	</footer>
	<!-- End Footer -->

	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
	<div id="preloader"></div>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/counterup/counterup.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>

	<script src="assets/vendor/aos/aos.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>

	<script>
		$(document).ready(
				function() {
					$("#successDiv").hide();
					$("#errorDiv").hide();
					
					$("#successDivEnroll").hide();
					$("#errorDivEnroll").hide();

					
					$('#submitBtn').click(
							function(event) {
								var name = $("#userName").val();

								var message = $("#message").val();
								var email = $("#userEmail").val();
								var subject = $("#subject").val();

								if ((name.length <= 0) || (message.length <= 0)
										|| (email.length <= 0)
										|| (subject.length <= 0)) {

									showAlert("Please fill all form data.");
									$("#submitBtn").prop("disabled", false);
									return;
								}

								//stop submit the form, we will post it manually.
								event.preventDefault();

								fire_ajax_submit();

							});

					$('#enrollBtn').click(
							function(event) {
								var name = $("#enrollName").val();
								var email = $("#enrollEmail").val();
								var phone = $("#enrollPhone").val();
								

								if ((name.length <= 0) || (phone.length <= 0)
										|| (email.length <= 0)
									) {
									showEnrollAlert("Please fill all form data.")
									$("#enrollBtn").prop("disabled", false);
									return;
								}

								//stop submit the form, we will post it manually.
								event.preventDefault();

								fire_ajax_enroll_submit();

 					});


					

				});

		function fire_ajax_submit() {
			var contactUsDTO = {}
			contactUsDTO["userName"] = $("#userName").val();
			contactUsDTO["message"] = $("#message").val();
			contactUsDTO["userEmail"] = $("#userEmail").val();
			contactUsDTO["subject"] = $("#subject").val();

			$("#submitBtn").prop("disabled", true);

			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/contact",
				data : JSON.stringify(contactUsDTO),
				success : function(data) {

					if (data.successAction == 'false') {
						showAlert("Please enter the valid email id.");
						$("#submitBtn").prop("disabled", false);
						return;
					} else {
						$("#submitBtn").prop("disabled", false);
						$("#successDiv").fadeTo(2000, 500).slideUp(500,
								function() {
									$("#successDiv").slideUp(500);
								});

						$("#userName").val("");
						$("#message").val("");
						$("#userEmail").val("");
						$("#subject").val("");

					}

				}
			});
		}

		function showAlert(message) {
			var errorMsg = "<div id='errorDiv' class='alert alert-danger'>"
					+ message + "</div>"
			$('#errorDiv').html(errorMsg);

			$("#errorDiv").fadeTo(2000, 500).slideUp(500, function() {
				$("#errorDiv").slideUp(500);
			});
		}


		function showEnrollAlert(message) {
			var errorMsg = "<div id='errorDivEnroll' class='alert alert-danger'>"
					+ message + "</div>"
			$('#errorDivEnroll').html(errorMsg);

			$("#errorDivEnroll").fadeTo(2000, 500).slideUp(500, function() {
				$("#errorDivEnroll").slideUp(500);
			});
		}

		function fire_ajax_enroll_submit() {	
			var enrollDTO = {}		
			enrollDTO["enrollName"] = $("#enrollName").val();
			enrollDTO["enrollPhone"] = $("#enrollPhone").val();
			enrollDTO["enrollEmail"] = $("#enrollEmail").val();
			enrollDTO["enrollCourse"] = $("#enrollCourse").val();

			$("#enrollBtn").prop("disabled", true);

			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/enroll",
				data : JSON.stringify(enrollDTO),
				success : function(data) {

					if (data.successAction == 'false') {
						
						$("#enrollBtn").prop("disabled", false);
						return;
					} else {
						$("#successDivEnroll").fadeTo(2000, 500).slideUp(500,
								function() {
									$("#successDivEnroll").slideUp(500);
								});						
						$("#enrollBtn").prop("disabled", false);
						$("#enrollName").val("");
						$("#enrollPhone").val("");
						$("#enrollEmail").val("");
					}

				}
			});
		}


		
	</script>
</body>

</html>