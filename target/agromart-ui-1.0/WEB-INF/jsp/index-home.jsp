<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="AGROMAART">
<meta name="author" content="AGROMAART">
<title>AGROMAART</title>
<!-- Favicon Icon -->
<link rel="icon" type="image/png" href="img/fav-icon.png">

<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Slider CSS -->
<link rel="stylesheet" href="vendor/slider/slider.css">
<!-- Select2 CSS -->
<link href="vendor/select2/css/select2-bootstrap.css" />
<link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
<!-- Font Awesome-->
<link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
<link href="vendor/icofont/icofont.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">


</head>


<body>
	<div class="modal fade login-modal-main" id="login">
		<div class="modal-dialog modal-lg modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="login-modal">
						<div class="row">
							<div class="col-lg-6 d-flex align-items-center">
								<div class="login-modal-left p-4 text-center pl-5">
									<img src="img/login2.png" alt="TECHIES">
								</div>
							</div>
							<div class="col-lg-6">
								<button type="button"
									class="close close-top-right position-absolute"
									data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true"><i class="icofont-close-line"></i></span>
									<span class="sr-only">Close</span>
								</button>
								<form:form action="/validate" class="position-relative" modelAttribute="userAccount" id="frm">
									<ul
										class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute"
										role="tablist">
										<li><a class="nav-link-login active" data-toggle="tab"
											href="#login-form" role="tab"><i class="icofont-ui-lock"></i>
												LOGIN</a></li>
										<li><a class="nav-link-login" data-toggle="tab"
											href="#register" role="tab"><i
												class="icofont icofont-pencil"></i> REGISTER</a></li>
									</ul>
									<div class="login-modal-right p-4">
										<!-- Tab panes -->
										<div class="tab-content">
										
											<div class="tab-pane active" id="login-form" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">LOGIN</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label> <input type="text"
														name="username" id="username" class="form-control"
														placeholder="Email/Mob Number">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label> <input type="password"
														name="password" id="password" class="form-control"
														placeholder="********">
												</fieldset>

												<fieldset class="form-group">
													<input type="radio" id="admin" name="user" value="admin">
													<label for="Admin">ADMIN</label> &nbsp;
													<input type="radio" id="dealer" name="user" value="dealer">
													<label for="Dealer">DEALER</label>
												</fieldset>
												<fieldset class="form-group">
													<button id="enterloginBtn" class="btn btn-lg btn-primary btn-block">Enter to
														your account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck1"> <label
														class="custom-control-label" for="customCheck1">Remember
														me</label>
												</div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="register" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">REGISTER</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label> <input type="text"
														class="form-control" placeholder="+91 123 456 7890">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label> <input type="password"
														class="form-control" placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Confirm Password </label> <input
														type="password" class="form-control"
														placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<button type="submit"
														class="btn btn-lg btn-primary btn-block">Create
														Your Account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck2"> <label
														class="custom-control-label" for="customCheck2">I
														Agree with <a href="#">Term and Conditions</a>
													</label>
												</div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-white">
		<div class="main-nav shadow-sm">
			<!--  Navbar-->
			<jsp:include page="navbar.jsp" />
			<!-- End of NavBar -->
		</div>
		<div class="py-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<header>
							<div id="owl-carousel-one" class="owl-carousel">
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/banner1.jpg">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/banner2.jpg">
								</div>
								<!--                     <div class="item"><img class="img-fluid mx-auto rounded shadow-sm" src="img/banner/3.png"></div>
                        <div class="item"><img class="img-fluid mx-auto rounded shadow-sm" src="img/banner/4.png"></div> -->
							</div>
						</header>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="product-list pbc-5 pb-4 pt-5 bg-white">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark font-weight-normel">Best Selling
				Items</h4>
			<div class="row">
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
							<span class="badge badge-danger">NEW</span> <img
							src="img/item/bhendi.jpg" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1">BHENDI - EXPORT-16</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i>2400.00 <span class="text-black-50"><del>
										<i class="icofont-rupee"></i>3000.00
									</del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
							<span class="badge badge-success">50% OFF</span> <img
							src="img/item/bottle.jpg" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1">BOTTLE GOURD - BHARTI-26</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i> 3000.00 <span
									class="text-black-50"><del>
										<i class="icofont-rupee"></i>4000.00
									</del></span>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a class="active" href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="#"> <span
							class="badge badge-danger">NEW</span> <img
							src="img/item/flower.jpg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">CAULIFLOWER - BHUMIKA-80</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i> 28000.00 <span
									class="text-black-50"><del>
										<i class="icofont-rupee"></i>30000.00
									</del></span>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
							<span class="badge badge-success">50% OFF</span> <img
							src="img/item/cucumber.jpg" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1">CUCUMBER - MASTANI-18</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i>15000.00 <span
									class="text-black-50"><del>
										<i class="icofont-rupee"></i>18000.00
									</del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
							<span class="badge badge-danger">NEW</span> <img
							src="img/item/onion.png" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1">Onion - PHULE SAMARTH</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i>1000.00 <span class="text-black-50"><del>
										<i class="icofont-rupee"></i>1500.00
									</del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
							<span class="badge badge-success">50% OFF</span> <img
							src="img/item/tomato.png" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1">TOMATO - JYOTI 540</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i>160.00 <span class="text-black-50"><del>
										<i class="icofont-rupee"></i>400.00
									</del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
							<span class="badge badge-danger">NEW</span> <img
							src="img/item/madhura.png" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1">WATER MELON - MADHURA-10</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i>5000.00 <span class="text-black-50"><del>
										<i class="icofont-rupee"></i>500.00
									</del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"><i
								class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
							<span class="badge badge-success">50% OFF</span> <img
							src="img/item/pune-fursungi.png" class="card-img-top" alt="...">
						</a>
						<div class="card-body">
							<h6 class="card-title mb-1">ONION - PUNE FURSUNGI</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								<i class="icofont-rupee"></i>25000.00 <span
									class="text-black-50"><del>
										<i class="icofont-rupee"></i>26000
									</del></span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="product-list pbc-5 pb-4 pt-5 bg-white">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark">Top Savers Today</h4>
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-carousel-category owl-theme">
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"><i
										class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
									<span class="badge badge-danger">NEW</span> <img
									src="img/item/amruta.png" class="card-img-top" alt="...">
								</a>
								<div class="card-body">
									<h6 class="card-title mb-1">TOMATO - AMRUTA-47</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										<i class="icofont-rupee"></i> 40000.00 <span
											class="text-black-50"><del>
												<i class="icofont-rupee"></i>45000.00
											</del></span> <span
											class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
											50% OFF</span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"><i
										class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
									<span class="badge badge-success">50% OFF</span> <img
									src="img/item/kashi.png" class="card-img-top" alt="...">
								</a>
								<div class="card-body">
									<h6 class="card-title mb-1">PUMPKIN - KASHI</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										<i class="icofont-rupee"></i>5000.00 <span
											class="text-black-50"><del>
												<i class="icofont-rupee"></i>5500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"><i
										class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
									<span class="badge badge-danger">NEW</span> <img
									src="img/item/suny.png" class="card-img-top" alt="...">
								</a>
								<div class="card-body">
									<h6 class="card-title mb-1">CABBAGE - SUNNY</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										<i class="icofont-rupee"></i>16000.00 <span
											class="text-black-50"><del>
												<i class="icofont-rupee"></i>18000.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"><i
										class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
									<span class="badge badge-success">50% OFF</span> <img
									src="img/item/ash.png" class="card-img-top" alt="...">
								</a>
								<div class="card-body">
									<h6 class="card-title mb-1">BITTER GOURD - AISHWARYA-28</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										<i class="icofont-rupee"></i>6000.00 <span
											class="text-black-50"><del>
												<i class="icofont-rupee"></i>6500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"><i
										class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
									<span class="badge badge-danger">NEW</span> <img
									src="img/item/sonali.png" class="card-img-top" alt="...">
								</a>
								<div class="card-body">
									<h6 class="card-title mb-1">CUCUMBER - SONALI-19</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										<i class="icofont-rupee"></i>14000.00 <span
											class="text-black-50"><del>
												<i class="icofont-rupee"></i>15000.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"><i
										class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
									<span class="badge badge-success">50% OFF</span> <img
									src="img/item/supriya.png" class="card-img-top" alt="...">
								</a>
								<div class="card-body">
									<h6 class="card-title mb-1">RIDGE GOURD - SUPRIYA</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										<i class="icofont-rupee"></i>5000.00 <span
											class="text-black-50"><del>
												<i class="icofont-rupee"></i>5500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer -->
	<jsp:include page="footer.jsp" />
	
	<!-- Shopping Cart -->
	
	<div class="cart-sidebar">
		<div class="cart-sidebar-header">
			<h5>
					<!--  dynamic item count-->
				My Cart <span class="text-info">(5 item)</span> <a
					data-toggle="offcanvas" class="float-right" href="#"><i
					class="icofont icofont-close-line"></i> </a>
			</h5>
		</div>
		<div class="cart-sidebar-body">
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/1.jpg" alt=""> <span
					class="badge badge-success">50% OFF</span>
				<h5>
					<a href="/product-detail">Fusion Crop Onion</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					<i class="icofont-rupee"></i>135.00
					<del class="small text-secondary">
						<i class="icofont-rupee"></i> 500.00
					</del>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
		</div>
		<div class="cart-sidebar-footer">
			<div class="cart-store-details">
				<p>
					Sub Total <strong class="float-right"><i
						class="icofont-rupee"></i>900.69</strong>
				</p>
				<p>
					Delivery Charges <strong class="float-right text-danger">+
						<i class="icofont-rupee"></i>29.69
					</strong>
				</p>
				<h6>
					Your total savings <strong class="float-right text-danger"><i
						class="icofont-rupee"></i>55 (42.31%)</strong>
				</h6>
			</div>
			<a href="checkout.html"><button
					class="btn btn-primary btn-lg btn-block text-left" type="button">
					<span class="float-left"><i class="icofont icofont-cart"></i>
						Proceed to Checkout </span><span class="float-right"><strong><i
							class="icofont-rupee"></i>1200.69</strong> <span
						class="icofont icofont-bubble-right"></span></span>
				</button></a>
		</div>
	</div>
	<!-- End Of Cart -->
	
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- select2 Js -->
	<script src="vendor/select2/js/select2.min.js"></script>
	<!-- Owl Carousel -->
	<script src="vendor/owl-carousel/owl.carousel.js"></script>
	<!-- Slider Js -->
	<script src="vendor/slider/slider.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="js/custom.js"></script>
	<script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
	<link rel="stylesheet" href="js/demo.css?ver=3.4.0">
	<link rel="stylesheet"
		href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<script>
      (function($) {

    	  $('#enterloginBtn').click(function(event) {
    		  var userAccount = {}
    		  userAccount["username"] = $("#username").val();
    		  userAccount["password"] = $("#password").val();

  			$.ajax({
				type : "POST",
				url : "/validate",
				contentType : "application/json",
				processData : false,
				data : JSON.stringify(userAccount),
				success : function(data) {
					if (data.actionPassed == false) {
						alert("ERROR");
						return;
					} else {
						window.location.href = "/admin";
					}
				}
			});
		});
      })(jQuery);
    </script>
</body>

</html>