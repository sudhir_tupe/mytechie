<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="Keywords" content="Online Agro Shopping in India,online Agro Shopping store,Online Agro Shopping Site, Buy Online,Shop Online,Online Shopping,Agrodhan"/> 
	  <meta name="Description" content="India&#x27;s biggest online agro store for Vegetable Seeds,Field Crops Seeds,Fertilisers,Water Soluble fertilisers,Organic Products,Pesticides,Agri Equipments,Micronutrients,Insecticides,Fungicides,Herbicides,Bio Pesticides,Flower Seeds,Onion Seeds from all brands at the lowest prices in India. Payment options - COD, EMI, Credit card, Debit card &amp;amp; more."/>
      <title>Online Agro Shopping Site for Vegetable Seeds, Agro Chemicals &amp; More. Best Offers!</title>
  <!-- Favicon Icon -->
      <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
   </head>
   <body>
     <input type="hidden" value="${isUser}" id="isUser">
      <div class="bg-light">
         <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +91-7709462647</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold"><i class="icofont-rupee"></i>10,000</span></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav shadow-sm">
            <jsp:include page="navbar.jsp"/>
         </div>
      </div>
      <section class="section pt-5 pb-5 osahan-not-found-page">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center pt-5 pb-5">
                  <img class="img-fluid mb-5" src="img/thanks.png" alt="404">
                  <h1 class="mt-2 mb-2 text-success">Congratulations!</h1>
                  <p class="mb-5">You have successfully placed your order: <strong><c:out value="${order.orderId}"></c:out></strong></p>
                  <a id="thankyouBtn" class="btn btn-success btn-lg" href="/user">View Order :)</a>
                  
                  <div id="btnLoading" class="col-md-12 text-center load-more">
                       <br> <button class="btn btn-warning btn-sm" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        Loading...
                        </button>  
                     </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Footer -->
    	<jsp:include page="footer.jsp" />

      <div class="cart-sidebar">
         <div class="cart-sidebar-header">
            <h5>
               My Cart <span class="text-info"></span> <a data-toggle="offcanvas" class="float-right" href="#"><i class="icofont icofont-close-line"></i>
               </a>
            </h5>
         </div>
         <div class="cart-sidebar-body">
         </div>
         <div class="cart-sidebar-footer">
            <div class="cart-store-details">
               <p>Sub Total <strong class="float-right">0</strong></p>
               <p>Delivery Charges <strong class="float-right text-danger">0</strong></p>
               <h6>Your total savings <strong class="float-right text-danger">0</strong></h6>
            </div>
            <a href="checkout.html"><button class="btn btn-primary btn-lg btn-block text-left" type="button"><span class="float-left"><i class="icofont icofont-cart"></i> Proceed to Checkout </span><span class="float-right"><strong>0</strong> <span class="icofont icofont-bubble-right"></span></span></button></a>
         </div>
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Owl Carousel -->
      <script src="vendor/owl-carousel/owl.carousel.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
      <script>

      $(document).ready(function() {
      
    	  $("#btnLoading").hide();
      var isUser=$("#").val();

  	if(isUser){
  		$('#profileId li:contains(Login)').hide();
  		$('#profileId li:contains(My Account)').show();
  	}else{
  		$('#profileId li:contains(My Account)').hide();
  	}

  	$('#thankyouBtn').click(function(event) {
			  $("#btnLoading").show();
	})
  	

      });

      </script>
      
   </body>
</html>