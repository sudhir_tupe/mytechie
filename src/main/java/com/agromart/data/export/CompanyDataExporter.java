package com.agromart.data.export;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.agromart.model.CompanyDTO;

public class CompanyDataExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<CompanyDTO> companyDTOs;
	

	public CompanyDataExporter(List<CompanyDTO> companyDTOs) {
		this.companyDTOs = companyDTOs;
		workbook = new XSSFWorkbook();
	}

	private void writeHeaderLine() {
		sheet = workbook.createSheet("Companies");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "COMPANY_ID", style);
		createCell(row, 1, "COMPANY_NAME", style);
		createCell(row, 2, "COMPANY_EMAIL", style);
		createCell(row, 3, "COMPANY_LICENSE_NUMBER ", style);
		createCell(row, 4, "COMPANY_GST_NUMBER", style);
		createCell(row, 5, "COMPANY_PAN_NUMBER", style);
		createCell(row, 6, "COMPANY_STATUS", style);
		createCell(row, 7, "COMPANY_REG_ADDRESS", style);
		createCell(row, 8, "COMPANY_STORAGE_ADDRESS", style);
		createCell(row, 9, "COMPANY_AUTH_PERSON_NAME", style);
		createCell(row, 10, "COMPANY_AUTH_PERSON_NUMBER", style);
		createCell(row, 11, "COMPANY_LICENSE_EXPIRY", style);
	}

	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (CompanyDTO companyDTO : companyDTOs) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;

			createCell(row, columnCount++, companyDTO.getCompanyId(), style);
			createCell(row, columnCount++, companyDTO.getCompanyName(), style);
			createCell(row, columnCount++, companyDTO.getCompanyEmailId(), style);
			createCell(row, columnCount++, companyDTO.getCompanyLicenseNo(), style);
			createCell(row, columnCount++, companyDTO.getCompanyGSTNumber(), style);
			createCell(row, columnCount++, companyDTO.getCompanyPanNumber(), style);
			createCell(row, columnCount++, companyDTO.getCompanyStatus(), style);
			createCell(row, columnCount++, companyDTO.getCompanyRegAddress(), style);
			createCell(row, columnCount++, companyDTO.getCompanyStorageAddress(), style);
			createCell(row, columnCount++, companyDTO.getCompanyAuthPersonName(), style);
			createCell(row, columnCount++, companyDTO.getContactNumber(), style);
			createCell(row, columnCount++, companyDTO.getCompanyLicenseExpiryDate().toString(), style);
		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();
		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();
		outputStream.close();

	}
}