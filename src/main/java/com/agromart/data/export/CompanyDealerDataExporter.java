package com.agromart.data.export;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.agromart.model.CompanyDealerDTO;

public class CompanyDealerDataExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<CompanyDealerDTO> companyDealerDTOs;
	

	public CompanyDealerDataExporter(List<CompanyDealerDTO> companyDealerDTOs) {
		this.companyDealerDTOs = companyDealerDTOs;
		workbook = new XSSFWorkbook();
	}

	private void writeHeaderLine() {
		sheet = workbook.createSheet("CompanyDealers");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);
		
		createCell(row, 0, "COMPANY_ID", style);
		createCell(row, 1, "DEALER_ID", style);
		createCell(row, 2, "COMPANY_NAME", style);
		createCell(row, 3, "DEALER_NAME ", style);
		createCell(row, 4, "DEALER_STATUS", style);
		createCell(row, 5, "CREATION_DATE", style);

	}

	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (CompanyDealerDTO companyDealerDTO : companyDealerDTOs) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			
			createCell(row, columnCount++, companyDealerDTO.getCompanyId(), style);
			createCell(row, columnCount++, companyDealerDTO.getDealerId(), style);
			createCell(row, columnCount++, companyDealerDTO.getCompanyName().trim(), style);
			createCell(row, columnCount++, companyDealerDTO.getDealerName().trim(), style);
			createCell(row, columnCount++, companyDealerDTO.getDealerStatus(), style);
			createCell(row, columnCount++, companyDealerDTO.getCreationDate().toString(), style);
		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();
		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();
		outputStream.close();

	}
}