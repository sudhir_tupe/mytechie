package com.agromart.data.export;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.agromart.model.DealerDTO;

public class DealerDataExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<DealerDTO> dealerDTOs;
	
	public DealerDataExporter(List<DealerDTO> dealerDTOs) {
		this.dealerDTOs = dealerDTOs;
		workbook = new XSSFWorkbook();
	}

	private void writeHeaderLine() {
		sheet = workbook.createSheet("Dealers");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "DEALER_NAME", style);
		createCell(row, 1, "DEALER_EMAIL", style);
		createCell(row, 2, "DEALER_CONTACT_NUMBER", style);
		createCell(row, 3, "LICENSE_NUMBER ", style);
		createCell(row, 4, "LICENSE_EXPIRY_DATE ", style);
		createCell(row, 5, "GST_NUMBER", style);
		createCell(row, 6, "PAN_NUMBER", style);
		createCell(row, 7, "DEALER_STATUS", style);
		createCell(row, 8, "STORAGE_ADDRESS", style);
		createCell(row, 9, "REGISTER_OFFICE_ADDRESS", style);
	}

	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (DealerDTO dealerDTO : dealerDTOs) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			createCell(row, columnCount++, dealerDTO.getDealerName(), style);
			createCell(row, columnCount++, dealerDTO.getDealerEmail(), style);
			createCell(row, columnCount++, dealerDTO.getDealerContactNumber(), style);
			createCell(row, columnCount++, dealerDTO.getDealerLicenseNumber(), style);
			createCell(row, columnCount++, dealerDTO.getDealerLicenseExpiryDate().toString(), style);
			createCell(row, columnCount++, dealerDTO.getDealerGSTNumber(), style);
			createCell(row, columnCount++, dealerDTO.getDealerPanNumber(), style);
			createCell(row, columnCount++, dealerDTO.getDealerStatus(), style);
			createCell(row, columnCount++, dealerDTO.getStorageAddress(), style);
			createCell(row, columnCount++, dealerDTO.getRegisterOfficeAddress(), style);
		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();
		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();
		outputStream.close();
	}
}