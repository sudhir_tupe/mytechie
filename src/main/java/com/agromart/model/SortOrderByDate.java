package com.agromart.model;

import java.util.Comparator;

public class SortOrderByDate implements Comparator<Order> {

	@Override
	public int compare(Order order1, Order order2) {
		return order1.getOrderDate().compareTo(order2.getOrderDate());
	}

}
