package com.agromart.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class Item {
	
    @DynamoDBAttribute(attributeName = "PRODUCT_ID")
	private String productID;
    
    @DynamoDBAttribute(attributeName = "PRODUCT_NAME")
	private String productName;
    
    @DynamoDBAttribute(attributeName = "QUANITY")
	private int quantity;
    
    @DynamoDBAttribute(attributeName = "PRICE")
	private double price;
    
    @DynamoDBAttribute(attributeName = "RATING")
	private String rating;
    
    @DynamoDBAttribute(attributeName = "IMG_PATH")
	private String imgPath;

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productID) {
		this.productID = productID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}


	public boolean updateQuantity(int quantity) {
	return	false;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	@Override
	public String toString() {
		return "\n Item [Name=" + productName + ", quantity=" + quantity + ", price="
				+ price + "]";
	}
	
	
}
