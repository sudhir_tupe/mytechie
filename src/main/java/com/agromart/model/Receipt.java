package com.agromart.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.layout.element.Image;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class Receipt {
	public static ByteArrayInputStream create(Order order, ShoppingCart cart) {

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Document doc = new Document();
		doc.addCreator("agrodhan.com");
		doc.addCreationDate();
		
		Paragraph paragraph = new Paragraph("TAX INVOICE",new Font(FontFamily.COURIER, 16, Font.UNDERLINE));
		
		String para1 = "Ordered Through Agrodhan \n\n http://agrodhan.com/ \n ";
		
		
		paragraph.setAlignment(Element.TITLE);
		Font bfBold12 = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
		Font bf12 = new Font(FontFamily.TIMES_ROMAN, 12);

		PdfPTable orderDetailsTable = new PdfPTable(2);
		PdfPCell cel;

		try {
			PdfWriter.getInstance(doc, out);
			doc.open();
			doc.add(paragraph);
			doc.add(Chunk.NEWLINE);
			doc.add(Chunk.NEWLINE);
			
		    PdfPCell c1 = new PdfPCell(new Phrase(para1,new Font(FontFamily.TIMES_ROMAN, 16, Font.BOLD)));
		    
		   
		    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		    c1.setColspan(2);
			
			orderDetailsTable.addCell(c1);

			cel = new PdfPCell(new Phrase("\n Order Id : \n" + order.getOrderId()+"\n\n",
					new Font(FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
			
			cel.setBackgroundColor(BaseColor.LIGHT_GRAY);
			
			orderDetailsTable.addCell(cel);

			DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // Or whatever IST is supposed to be

			
			cel = new PdfPCell(new Phrase("\n Order Date : " + formatter.format(order.getOrderDate()),
					new Font(FontFamily.TIMES_ROMAN, 10, Font.BOLD)));
			cel.setBackgroundColor(BaseColor.LIGHT_GRAY);
			orderDetailsTable.addCell(cel);

			cel = new PdfPCell(new Phrase("Sold By : \n \n" + "FUSION CROP SCIENCE PVT LTD \n"
					+ "A wing 106,Mega Center, \n"
					+ "Solapur Road,Magarpatta ,Hadapsar, Pune, Maharashtra 411013 \n\n GST No : 27AACCf5833F1ZQ \n CIN No : U1400PN2015PTC155061 \n\n",
					bf12));
			orderDetailsTable.addCell(cel);

			cel = new PdfPCell(new Phrase("Shiping Address: \n \n" + order.getNewAddress(), bf12));

			orderDetailsTable.addCell(cel);

			doc.add(orderDetailsTable);

			doc.add(Chunk.NEWLINE);
			doc.add(Chunk.NEWLINE);

			PdfPTable mainTable = new PdfPTable(5);
			PdfPCell cell;

			cell = new PdfPCell(new Phrase("Product", bfBold12));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);

			
			mainTable.addCell(cell);

			cell = new PdfPCell(new Phrase("Qty", bfBold12));
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			mainTable.addCell(cell);

			cell = new PdfPCell(new Phrase("Price", bfBold12));
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			mainTable.addCell(cell);

			cell = new PdfPCell(new Phrase("GST", bfBold12));
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			mainTable.addCell(cell);

			cell = new PdfPCell(new Phrase("Total", bfBold12));
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			mainTable.addCell(cell);

			List<Item> items = cart.getItems();

			for (int i = 0; i < items.size(); i++) {
				Item item = items.get(i);
				insertRow(item, mainTable);
			}

			cell = new PdfPCell(new Phrase(String.valueOf("\n TOTAL AMOUNT : " + order.getTotalAmount())+"\n\n", bfBold12));
			cell.setColspan(5);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			mainTable.addCell(cell);
			doc.add(mainTable);
			doc.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

	private static void insertRow(Item item, PdfPTable mainTable) {
		Font bf10 = new Font(FontFamily.TIMES_ROMAN, 10);
		PdfPCell cell = new PdfPCell(new Phrase(item.getProductName(), bf10));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(cell);

		cell = new PdfPCell(new Phrase("1", bf10));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(cell);

		cell = new PdfPCell(new Phrase(String.valueOf(item.getPrice()), bf10));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(cell);

		cell = new PdfPCell(new Phrase("0", bf10));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(cell);

		cell = new PdfPCell(new Phrase(String.valueOf(item.getPrice()), bf10));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(cell);
	}
}
