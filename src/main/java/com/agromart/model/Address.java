package com.agromart.model;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@DynamoDBAttribute(attributeName = "STREET_ADDRESS")
	private String streetAddress;
	
	@DynamoDBAttribute(attributeName = "NAME")
	private String delAddressName;
	
	@DynamoDBAttribute(attributeName = "POST")
	private String postName;
	
	@DynamoDBAttribute(attributeName = "TALUKA")
	private String taluka;
	
	@DynamoDBAttribute(attributeName = "CITY")
	private String city;
	
	@DynamoDBAttribute(attributeName = "STATE")
	private String state;

	@DynamoDBAttribute(attributeName = "ZIP_CODE")
	private String zipCode;
	
	@DynamoDBAttribute(attributeName = "COUNTRY")
	private String country;
	
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getTaluka() {
		return taluka;
	}
	public void setTaluka(String taluka) {
		this.taluka = taluka;
	}
	
	public String getPostName() {
		return postName;
	}
	public void setPostName(String postName) {
		this.postName = postName;
	}
	
	public String getDelAddressName() {
		return delAddressName;
	}
	public void setDelAddressName(String delAddressName) {
		this.delAddressName = delAddressName;
	}
	@Override
	public String toString() {
		return "Address [streetAddress=" + streetAddress + ", delAddressName=" + delAddressName + ", postName="
				+ postName + ", taluka=" + taluka + ", city=" + city + ", state=" + state + ", zipCode=" + zipCode
				+ ", country=" + country + "]";
	}
	
	
}
