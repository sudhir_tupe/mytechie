package com.agromart.model;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "COMPANY_DEALERS")
public class CompanyDealerDTO {

	@DynamoDBHashKey(attributeName = "COMPANY_ID")
	private String companyId;

	@DynamoDBRangeKey(attributeName = "DEALER_ID")
	private String dealerId;
	
	@DynamoDBAttribute(attributeName = "COMPANY_NAME")
	private String companyName;
	
	@DynamoDBAttribute(attributeName = "DEALER_NAME")
	private String dealerName;


	@DynamoDBAttribute(attributeName = "DEALER_STATUS")
	private String dealerStatus;

	@DynamoDBAttribute(attributeName = "CREATION_DATE")
	private Date creationDate;

	@DynamoDBAttribute(attributeName = "LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getDealerId() {
		return dealerId;
	}

	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getDealerStatus() {
		return dealerStatus;
	}

	public void setDealerStatus(String dealerStatus) {
		this.dealerStatus = dealerStatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
