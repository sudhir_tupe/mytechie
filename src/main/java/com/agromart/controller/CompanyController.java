package com.agromart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.agromart.model.CompanyDTO;
import com.agromart.service.AmazonS3ClientService;
import com.agromart.service.CompanyService;

@Controller
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private AmazonS3ClientService amazonS3ClientService;

	@RequestMapping(method = RequestMethod.PUT, produces = { "application/json" }, value = "api/companies")
	public ResponseEntity updateCompany(@RequestBody CompanyDTO companyDTO) {
		CompanyDTO retrived = companyService.updateCompany(companyDTO);
		return ResponseEntity.status(HttpStatus.OK).body(retrived);
	}

	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" }, value = "api/companies")
	public ResponseEntity addNewCompany(@RequestBody CompanyDTO companyDTO) {
		// System.out.println(companyDTO.toString());
		System.out.println("Inside createNewCompany ");

		CompanyDTO retrivedCompanyDTO = companyService.addNewCompany(companyDTO);

		return ResponseEntity.status(HttpStatus.OK).body(retrivedCompanyDTO);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/companies/{companyId}")
	public ResponseEntity getCompanyById(@PathVariable String companyId) {
		CompanyDTO companyDTO = companyService.getCompanyById(companyId);
		return ResponseEntity.status(HttpStatus.OK).body(companyDTO);
	}
	/*
	 * @RequestMapping(method = RequestMethod.POST, value = "/register")
	 * public @ResponseBody UserResponseDTO register(@RequestBody CompanyDTO
	 * companyDTO) { // System.out.println(companyDTO.toString());
	 * System.out.println("Inside createNewCompany ");
	 * 
	 * companyService.addNewCompany(companyDTO);
	 * 
	 * UserResponseDTO userResponseDTO = new UserResponseDTO();
	 * userResponseDTO.setMessage("Company Registered successfully"); return
	 * userResponseDTO; }
	 */

	

}
