package com.agromart.controller;

public class UserResponseDTO {
	private Long id;
	private boolean actionPassed;
	private String message;
	private String actionPerformed;
	private String successAction;
	private String responseId;

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isActionPassed() {
		return actionPassed;
	}

	public void setActionPassed(boolean actionPassed) {
		this.actionPassed = actionPassed;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(String actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	public String getSuccessAction() {
		return successAction;
	}

	public void setSuccessAction(String successAction) {
		this.successAction = successAction;
	}

}
