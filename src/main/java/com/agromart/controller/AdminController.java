package com.agromart.controller;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.agromart.data.export.CompanyDataExporter;
import com.agromart.data.export.CompanyDealerDataExporter;
import com.agromart.data.export.DealerDataExporter;
import com.agromart.model.CompanyDTO;
import com.agromart.model.CompanyDealerDTO;
import com.agromart.model.DealerDTO;
import com.agromart.model.Product;
import com.agromart.model.ShoppingCart;
import com.agromart.model.User;
import com.agromart.service.AmazonS3ClientService;
import com.agromart.service.CompanyDealerService;
import com.agromart.service.CompanyService;
import com.agromart.service.DealerService;
import com.agromart.service.ProductService;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

@Controller
public class AdminController {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private DealerService dealerService;

	@Autowired
	private ProductService productService;

	@Autowired
	private CompanyDealerService companyDealerService;

	@Autowired
	private AmazonS3ClientService amazonS3ClientService;

	@RequestMapping(method = RequestMethod.GET, value = "/admin")
	public String admin(ModelMap map) {

		System.out.println("Inside UserController.agrohome");
		User user = new User();

		CompanyDTO companyDTO = new CompanyDTO();
		DealerDTO dealerDTO = new DealerDTO();
		CompanyDealerDTO companyDealerDTO = new CompanyDealerDTO();

		Product product = new Product();

		List<CompanyDTO> companyDTOs = companyService.getCompanyList();
		List<DealerDTO> dealerDTOs = dealerService.getDealerList();

		List<Product> productList = productService.getListOfProducts();

		List<CompanyDealerDTO> companyDealerDTOs = companyDealerService.getCompanyDealersList();

		map.addAttribute("user", user);
		map.addAttribute("companyDTO", companyDTO);
		map.addAttribute("dealerDTO", dealerDTO);
		map.addAttribute("companyDTOs", companyDTOs);

		map.addAttribute("productDTO", product);

		map.addAttribute("companyDealerDTOs", companyDealerDTOs);
		map.addAttribute("dealerDTOs", dealerDTOs);
		map.addAttribute("productList", productList);
		map.addAttribute("companyDealerDTO", companyDealerDTO);

		return "admin";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/admin2")
	public String admin2(ModelMap map) {
		System.out.println("Inside UserController.admin2");

		System.out.println("Inside UserController.agrohome");
		User user = new User();

		CompanyDTO companyDTO = new CompanyDTO();
		DealerDTO dealerDTO = new DealerDTO();
		CompanyDealerDTO companyDealerDTO = new CompanyDealerDTO();

		List<CompanyDTO> companyDTOs = companyService.getCompanyList();
		List<DealerDTO> dealerDTOs = dealerService.getDealerList();

		List<CompanyDealerDTO> companyDealerDTOs = companyDealerService.getCompanyDealersList();

		map.addAttribute("user", user);
		map.addAttribute("companyDTO", companyDTO);
		map.addAttribute("dealerDTO", dealerDTO);
		map.addAttribute("companyDTOs", companyDTOs);

		map.addAttribute("companyDealerDTOs", companyDealerDTOs);
		map.addAttribute("dealerDTOs", dealerDTOs);

		map.addAttribute("companyDealerDTO", companyDealerDTO);
		return "admin2";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/newcompany")
	public @ResponseBody UserResponseDTO addNewCompany(@RequestBody CompanyDTO companyDTO) {
		System.out.println("Inside createNewCompany ");

		companyService.addNewCompany(companyDTO);

		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		userResponseDTO.setMessage("Company Registered successfully");
		return userResponseDTO;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/newdealer")
	public @ResponseBody UserResponseDTO addNewDealer(@RequestBody DealerDTO dealerDTO) {
		System.out.println("Inside createNewCompany ");
		dealerService.addNewDealer(dealerDTO);
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		userResponseDTO.setMessage("Dealer Registered successfully");
		return userResponseDTO;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/addNewProduct")
	public @ResponseBody UserResponseDTO addNewDealer(@RequestBody Product productDTO) {
		System.out.println("Inside addNewProduct");
		productService.addProduct(productDTO);
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		userResponseDTO.setMessage("Product Added Successfully");
		return userResponseDTO;
	}
	

	@RequestMapping(method = RequestMethod.POST, value = "/upload")
	public @ResponseBody UserResponseDTO upload(@RequestParam(value = "file") MultipartFile uploadFile) {
		System.out.println("Inside AdminController.upload ");

		this.amazonS3ClientService.uploadFileToS3Bucket(uploadFile, true);
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		userResponseDTO.setMessage(uploadFile.getOriginalFilename());
		return userResponseDTO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/companyList")
	public @ResponseBody UserResponseDTO getCompanyList() {
		System.out.println("Inside AdminController.upload ");
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		return userResponseDTO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/dealerList")
	public @ResponseBody UserResponseDTO getDealerList() {
		System.out.println("Inside AdminController.upload ");
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		return userResponseDTO;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addCompanyDealer")
	public @ResponseBody UserResponseDTO addNewDealerInCompany(@RequestBody CompanyDealerDTO companyDealerDTO) {
		System.out.println("Inside addNewDealerInCompany");
		companyDealerService.addNewDealer(companyDealerDTO);
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		userResponseDTO.setMessage("Company new dealer Registered successfully");
		return userResponseDTO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/companyDealerList")
	public @ResponseBody UserResponseDTO companyDealerList() {
		System.out.println("Inside AdminController.upload ");
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setActionPassed(true);
		return userResponseDTO;
	}

	@GetMapping("/companies/export/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=companies_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<CompanyDTO> companyDTOs = companyService.getCompanyList();

		CompanyDataExporter excelExporter = new CompanyDataExporter(companyDTOs);

		excelExporter.export(response);
	}

	@GetMapping("/companyDealers/export/excel")
	public void compdealersToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=company-dealers_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<CompanyDealerDTO> companyDealerDTOs = companyDealerService.getCompanyDealersList();
		CompanyDealerDataExporter excelExporter = new CompanyDealerDataExporter(companyDealerDTOs);
		excelExporter.export(response);
	}

	@GetMapping("/dealers/export/excel")
	public void dealersToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=dealers_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<DealerDTO> dealerDTOs = dealerService.getDealerList();
		DealerDataExporter excelExporter = new DealerDataExporter(dealerDTOs);
		excelExporter.export(response);
	}
	
	@RequestMapping(value = "/addProducts", method = RequestMethod.POST)
	public @ResponseBody UserResponseDTO addProducts(@RequestParam(value = "file") MultipartFile inboundFile) {
		System.out.println("Inside AdminController.inbound ");
		this.amazonS3ClientService.uploadFileToS3Bucket(inboundFile, true);
		
		try {
			InputStreamReader inputStreamReader = new InputStreamReader(inboundFile.getInputStream());
			CSVReader csvReader = new CSVReaderBuilder(inputStreamReader)
					.withSkipLines(1)
					.build();
			List<String[]> allData = csvReader.readAll();
			
			for(int i=0;i<allData.size();i++) {
				String[] row = allData.get(i);
				
				Product product=new Product();
				product.setProductName(row[0]);
				product.setPackagingGram(row[1]);
				product.setPrice(Double.parseDouble(row[2]));
				product.setAvailableItemCount(Integer.parseInt(row[3]));
				product.setCategory(row[4]);
				product.setDescription(row[5]);
				product.setProductCompanyName(row[6]);
				product.setProductCompanyId(row[7]);
				productService.addProduct(product);
			}
			
			UserResponseDTO userResponseDTO = new UserResponseDTO();
			userResponseDTO.setActionPassed(true);
			userResponseDTO.setMessage(inboundFile.getOriginalFilename());
			return userResponseDTO;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
