package com.agromart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.agromart.model.CompanyDTO;
import com.agromart.model.Product;
import com.agromart.service.ProductService;

@Controller
public class ProductController {

	@Autowired
	private ProductService productService;

	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" }, value = "api/products")
	public ResponseEntity addNewProduct(@RequestBody Product product) {
		System.out.println("Inside createNewCompany ");
		Product retProduct = productService.addProduct(product);
		return ResponseEntity.status(HttpStatus.OK).body(retProduct);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = { "application/json" }, value = "api/products")
	public ResponseEntity updateProduct(@RequestBody Product product) {
		Product retProduct = productService.updateProduct(product);
		return ResponseEntity.status(HttpStatus.OK).body(retProduct);
	}
	
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/products/best-selling")
	public ResponseEntity<List<Product>> getProducts() {
		List<Product> retProducts = productService.getListOfProducts();
		return ResponseEntity.status(HttpStatus.OK).body(retProducts);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/products/{productId}")
	public ResponseEntity getProductById(@PathVariable String productId) {
		Product product = productService.getProductByID(productId);
		return ResponseEntity.status(HttpStatus.OK).body(product);
	}
}
