package com.agromart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agromart.model.Order;
import com.agromart.model.User;
import com.agromart.service.OrderService;

@Controller
public class OrderController {
	@Autowired
	private OrderService orderService;
	
	
	
	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" }, value = "api/order")
	public ResponseEntity creatNewOrder(@RequestBody Order order) {
		System.out.println("Inside createNewCompany ");
		Order retOrder = orderService.createOrder(order);
		return ResponseEntity.status(HttpStatus.OK).body(retOrder);
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = { "application/json" }, value = "api/order")
	public ResponseEntity updateOrder(@RequestBody Order order) {
		Order retOrder = orderService.updateOrder(order);
		return ResponseEntity.status(HttpStatus.OK).body(retOrder);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/order/{orderId}")
	public ResponseEntity getOrderDetailsById(@PathVariable String orderId) {
		Order retOrder = orderService.getOrderDetailsById(orderId);
		return ResponseEntity.status(HttpStatus.OK).body(retOrder);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/orders")
	public ResponseEntity<List<Order>> getOrders() {
		List<Order> retProducts = orderService.getListOfOrders();
		return ResponseEntity.status(HttpStatus.OK).body(retProducts);
	}
}
