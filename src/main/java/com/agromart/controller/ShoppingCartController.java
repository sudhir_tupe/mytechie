package com.agromart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.agromart.model.ShoppingCart;
import com.agromart.service.ShopingCartService;

@Controller
public class ShoppingCartController {
	@Autowired
	private ShopingCartService shopingCartService;
	
	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" }, value = "api/cart")
	public ResponseEntity addNewItemToCart(@RequestBody ShoppingCart cart) {
		System.out.println("Inside addNewItemToCart ");
		shopingCartService.addToCart(cart);
		return ResponseEntity.status(HttpStatus.OK).body(cart);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/cart/{cartId}")
	public ResponseEntity getProductById(@PathVariable String cartId) {
		ShoppingCart shoppingCart = shopingCartService.getCart(cartId);
		return ResponseEntity.status(HttpStatus.OK).body(shoppingCart);
	}
}
