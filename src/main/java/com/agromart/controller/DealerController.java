package com.agromart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.agromart.model.DealerDTO;
import com.agromart.service.DealerService;

@Controller
public class DealerController {

	@Autowired
	private DealerService dealerService;

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/dealers/{dealerId}")
	public ResponseEntity getDealerById(@PathVariable String dealerId) {
		System.out.println("Inside getDealerById ");
		DealerDTO dealerDTO = dealerService.getDealerById(dealerId);
		return ResponseEntity.status(HttpStatus.OK).body(dealerDTO);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = { "application/json" }, value = "api/dealers")
	public ResponseEntity updateDealer(@RequestBody DealerDTO dealerDTO) {
		System.out.println("Inside updateDealer ");

		DealerDTO retrived = dealerService.updateDealer(dealerDTO);
		return ResponseEntity.status(HttpStatus.OK).body(retrived);
	}

	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" }, value = "api/dealers")
	public ResponseEntity addNewDealer(@RequestBody DealerDTO dealerDTO) {
		System.out.println("Inside addNewDealer ");

		DealerDTO retrivedDealerDTO = dealerService.addNewDealer(dealerDTO);

		return ResponseEntity.status(HttpStatus.OK).body(retrivedDealerDTO);
	}

}
