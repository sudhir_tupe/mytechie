package com.agromart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.agromart.model.CompanyDealerDTO;
import com.agromart.service.CompanyDealerService;

@Controller
public class CompanyDealersController {

@Autowired
public CompanyDealerService companyDealerService;


@RequestMapping(method = RequestMethod.POST, produces = { "application/json" }, value = "api/companies/dealer")
public ResponseEntity addNewDealerInCompany(@RequestBody CompanyDealerDTO companyDealerDTO) {
	System.out.println("Inside addNewDealerInCompany ");

	CompanyDealerDTO retcompanyDealerDTO = companyDealerService.addNewDealer(companyDealerDTO);

	return ResponseEntity.status(HttpStatus.OK).body(retcompanyDealerDTO);
}

}
