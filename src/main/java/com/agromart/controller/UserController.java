package com.agromart.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agromart.config.SMSSender;
import com.agromart.data.export.CompanyDataExporter;
import com.agromart.model.Address;
import com.agromart.model.CompanyDTO;
import com.agromart.model.Item;
import com.agromart.model.Order;
import com.agromart.model.OrderStatus;
import com.agromart.model.Product;
import com.agromart.model.Receipt;
import com.agromart.model.ShoppingCart;
import com.agromart.model.User;
import com.agromart.service.NotificationService;
import com.agromart.service.OrderService;
import com.agromart.service.ProductService;
import com.agromart.service.ShopingCartService;
import com.agromart.service.UserService;
import com.agromart.util.CommonUtil;

@Controller
public class UserController {
	@Autowired
	private UserService userService;

	@Autowired
	private ProductService productService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	ShopingCartService shopingCartService;

	@RequestMapping(method = RequestMethod.GET, value = "/home-account")
	public String redirectHome(ModelMap map, Principal principal, HttpSession session) {
		User sessionUser = (User) session.getAttribute("userSession");
		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			return "index";
		} else {
			map.addAttribute("userName", sessionUser.getUsername());
		}

		User user = new User();
		map.addAttribute("user", user);

		map.addAttribute("userAccount", user);

		boolean relogin = Boolean.TRUE;
		map.addAttribute("isUser", relogin);

		List<Product> productList = productService.getListOfProducts();

		List<Product> agroChemicalList = productList.stream() // convert list to stream
				.filter(p -> "AGRO_CHEMICAL".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		List<Product> vegieCrops = productList.stream() // convert list to stream
				.filter(p -> "VEGGIE_CROPS".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		List<Product> fertilizerList = productList.stream() // convert list to stream
				.filter(p -> "FERTILIZER".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		map.addAttribute("bestSellingProducts", vegieCrops);

		map.addAttribute("fertilizerList", fertilizerList);

		map.addAttribute("agroChemicalList", agroChemicalList);

		return "index";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String agrohome(ModelMap map, HttpSession session, Principal principal) {
		System.out.println("Inside UserController.agrohome");

		User sessionUser = (User) session.getAttribute("userSession");

		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			map.addAttribute("userName", sessionUser.getUsername());
			System.out.println(sessionUser.getUsername());
			
		}
		User user = new User();

		map.addAttribute("user", user);
		map.addAttribute("userAccount", user);

		List<Product> productList = productService.getListOfProducts();

		List<Product> agroChemicalList = productList.stream() // convert list to stream
				.filter(p -> "AGRO_CHEMICAL".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		List<Product> vegieCrops = productList.stream() // convert list to stream
				.filter(p -> "VEGGIE_CROPS".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		List<Product> fertilizerList = productList.stream() // convert list to stream
				.filter(p -> "FERTILIZER".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		map.addAttribute("bestSellingProducts", vegieCrops);

		map.addAttribute("fertilizerList", fertilizerList);

		map.addAttribute("agroChemicalList", agroChemicalList);

		return "index";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/home")
	public String home(ModelMap map) {
		System.out.println("Inside UserController.agrohome");

		User user = new User();
		List<Product> productList = productService.getListOfProducts();

		map.addAttribute("user", user);
		map.addAttribute("bestSellingProducts", productList);
		return "home";
	}

	@RequestMapping(value = "/index3", method = RequestMethod.GET)
	public String index3(ModelMap map) {
		User user = new User();
		map.addAttribute("user", user);
		return "index3";
	}

	@RequestMapping(value = "/index4", method = RequestMethod.GET)
	public String index4(ModelMap map) {
		User user = new User();
		map.addAttribute("user", user);
		return "index4";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/index-bkp")
	public String index2(ModelMap map) {
		System.out.println("Inside UserController.agrohome");

		User user = new User();
		map.addAttribute("user", user);
		return "index-bkp";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/dealer")
	public String profile(ModelMap map) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null || auth instanceof AnonymousAuthenticationToken) {
			System.out.println("userName:" + auth);
		} else {
			String uName = ((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername();
			System.out.println("userName:" + uName);
			map.addAttribute("userName", uName);
		}

		System.out.println("Inside UserController.dealer");

		List<Order> orderList = orderService.getListOfOrders();
		map.addAttribute("orderList", orderList);

		List<Order> sortedList = new ArrayList<>();

		for (Order o : orderList) {
			sortedList.add(o);
		}

		Collections.sort(sortedList, new Comparator<Order>() {
			public int compare(Order o1, Order o2) {
				if (o1.getOrderDate() == null || o2.getOrderDate() == null) {
					return 0;
				} else {
					return o1.getOrderDate().compareTo(o2.getOrderDate());
				}
			}
		});

		ShoppingCart cart = shopingCartService.getCart(sortedList.get(sortedList.size() - 1).getCartId());
		map.addAttribute("cart", cart);

		map.addAttribute("orderStatus", sortedList.get(sortedList.size() - 1));
		map.addAttribute("items",
				shopingCartService.getCart(sortedList.get(sortedList.size() - 1).getCartId()).getItems());

		List<OrderStatus> orderStatusList = new ArrayList<>();

		for (int i = 0; i < sortedList.size(); i++) {
			OrderStatus orderStatus = new OrderStatus();

			Order order = sortedList.get(i);
			orderStatus.setCart(shopingCartService.getCart(order.getCartId()));
			orderStatus.setOrder(order);

			orderStatusList.add(orderStatus);
		}

		map.addAttribute("orderStatusList", orderStatusList);

		return "dealer";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/about-us")
	public String aboutus(ModelMap map) {
		System.out.println("Inside UserController.about-us");

		return "about-us";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/myhome")
	public String myHome(ModelMap map, HttpSession session, Principal principal) {
		System.out.println("Inside UserController.agrohome");

		User sessionUser = (User) session.getAttribute("userSession");

		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			map.addAttribute("userName", sessionUser.getUsername());
			System.out.println(sessionUser.getUsername());
			
		}
		User user = new User();

		map.addAttribute("user", user);
		map.addAttribute("userAccount", user);

		List<Product> productList = productService.getListOfProducts();

		List<Product> agroChemicalList = productList.stream() // convert list to stream
				.filter(p -> "AGRO_CHEMICAL".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		List<Product> vegieCrops = productList.stream() // convert list to stream
				.filter(p -> "VEGGIE_CROPS".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		List<Product> fertilizerList = productList.stream() // convert list to stream
				.filter(p -> "FERTILIZER".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

		map.addAttribute("bestSellingProducts", vegieCrops);

		map.addAttribute("fertilizerList", fertilizerList);

		map.addAttribute("agroChemicalList", agroChemicalList);
	
		return "myhome";
	}


	@RequestMapping(method = RequestMethod.GET, value = "/profile")
	public String orderprofile(ModelMap map) {
		System.out.println("Inside UserController.profile");

		List<Order> orderList = orderService.getListOfOrders();
		map.addAttribute("orderList", orderList);
		return "profile";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/product-grid")
	public String productgrid(ModelMap map, HttpSession session, Principal principal) {
		System.out.println("Inside UserController.product-grid");

		
		List<Product> productList = productService.getListOfProducts();
		List<Product> vegieCrops = productList.stream() // convert list to stream
				.filter(p -> "VEGGIE_CROPS".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List

	
		map.addAttribute("bestSellingProducts", vegieCrops);	
		
		User sessionUser = (User) session.getAttribute("userSession");

		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			map.addAttribute("userName", sessionUser.getUsername());
			System.out.println(sessionUser.getUsername());
		}

		return "product-grid";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/contact-us")
	public String contact( ModelMap map, HttpSession session,
			Principal principal) {
		System.out.println("Inside UserController.contact-us");
		
		User sessionUser = (User) session.getAttribute("userSession");
		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			map.addAttribute("userName", sessionUser.getUsername());
		}

		return "contact-us";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/terms") 
	public String termcondition(ModelMap map) {
		  return "terms";
	 }
	 
	
	@RequestMapping(method = RequestMethod.GET, value = "/seller-terms") 
	public String sellertermcondition(ModelMap map) {
		  return "seller-terms";
	 }
	
	@RequestMapping(method = RequestMethod.GET, value = "/return-policy") 
	public String returnPolicy(ModelMap map) {
		  return "return-policy";
	 }
	 

	@RequestMapping(method = RequestMethod.GET, value = "/faq")
	public String faq(ModelMap map) {
		System.out.println("Inside UserController.contact-us");

		return "faq";
	}

	@RequestMapping(value = "/viewcart", method = RequestMethod.GET)
	public String viewcart(@RequestParam(value = "id", required = true) String id, ModelMap map, HttpSession session,
			Principal principal) {
		User sessionUser = (User) session.getAttribute("userSession");

		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
			map.addAttribute("isUserLoggedIn", "false");
		} else {
			map.addAttribute("isUserLoggedIn", "true");
			map.addAttribute("userName", sessionUser.getUsername());
			map.addAttribute("userId", sessionUser.getUserId());
			
			User userDetails=userService.getUserById(sessionUser.getUserId());
			map.addAttribute("userDetails", userDetails);
		
			
			Address userAddress=userDetails.getDefaultAddress();
			
			if(userAddress!=null) {
				StringBuilder defaultAddress= new StringBuilder();
				 defaultAddress
				.append(userAddress.getStreetAddress())
				.append(", ")
				.append(userAddress.getPostName())
				.append(", ")
				.append(userAddress.getTaluka())
				.append(", ")
				.append(userAddress.getCity())
				.append(", ")
				.append(userAddress.getState())
				.append(", ")
				.append(userAddress.getCountry())
				.append(" - ")
				.append(userAddress.getZipCode());
				map.addAttribute("defaultAddress", defaultAddress.toString());
				map.addAttribute("userAddress", userAddress);
			}
		}

		ShoppingCart cart = shopingCartService.getCart(id);
		map.addAttribute("cart", cart);
		List<Item> items = cart.getItems();
		map.addAttribute("items", items);
		return "viewcart";
	}

	@RequestMapping(value = "/checkout", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	public @ResponseBody UserResponseDTO checkout(@RequestBody ShoppingCart cart) {
		System.out.println("inside POST /checkout");
		shopingCartService.addToCart(cart);
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setResponseId(cart.getCartId());
		userResponseDTO.setActionPassed(true);
		userResponseDTO.setMessage("Successfully Stored Items in the cart");
		return userResponseDTO;
	}

	@RequestMapping(value = "/placeOrder", consumes = "application/json", produces = "application/json", method = RequestMethod.POST)
	public @ResponseBody UserResponseDTO placeOrder(@RequestBody Order order,ModelMap map, HttpSession session,
			Principal principal) {
		System.out.println("inside POST /placeOrder");
		orderService.createOrder(order);

		UserResponseDTO userResponseDTO = new UserResponseDTO();
		try {
			User sessionUser = (User) session.getAttribute("userSession");
			if(sessionUser!=null) {
				User loggedInUser = userService.getUserById(sessionUser.getUserId());
				if(loggedInUser.getEmailId()!=null) {
					//notificationService.sendEmail(loggedInUser.getEmailId(), order, shopingCartService.getCart(order.getCartId()));	
				}
				//notificationService.sendEmail("agrodhan2020@gmail.com", order, shopingCartService.getCart(order.getCartId()));
				userResponseDTO.setResponseId(order.getOrderId());
				userResponseDTO.setActionPassed(true);
				userResponseDTO.setMessage("Congatulations!!! You have successfully placed your order.");
				
				
				
				ShoppingCart cart= shopingCartService.getCart(order.getCartId());
				
				Receipt.create(order,cart);
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
			userResponseDTO.setActionPassed(false);
			userResponseDTO.setMessage("Error occured while placing your order. Please contact admin@agrodhan.com");
		}
		return userResponseDTO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/thanks")
	public String thanks(@RequestParam(value = "id", required = true) String id, ModelMap map, HttpSession session,
			Principal principal) {
		System.out.println("Inside UserController.profile");
		User sessionUser = (User) session.getAttribute("userSession");
		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			map.addAttribute("userName", sessionUser.getUsername());
			
			User user = userService.getUserById(sessionUser.getUserId());
			String orderMessage="[AGRODHAN]: Thank you for shopping with us. Please note your Order Number : "+id+ " \n\n Visit http://agrodhan.com/";
			String mobNum="91"+user.getPhoneNumber();
			SMSSender.send(orderMessage,mobNum);
			
			System.out.println(sessionUser.getUsername());
		}

		Order order = orderService.getOrderDetailsById(id);
		map.addAttribute("order", order);

		return "thanks";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/user")
	public String user(ModelMap map,HttpSession session, Principal principal) {
		User sessionUser = (User) session.getAttribute("userSession");
		
		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			map.addAttribute("userName", sessionUser.getUsername());
			map.addAttribute("userId",sessionUser.getUserId());
			System.out.println(sessionUser.getUsername());
			
			
			if(sessionUser.getUserId() !=null) {
				User user= userService.getUserById(sessionUser.getUserId());
				
				Address userAddress=user.getDefaultAddress();
				if(userAddress!=null) {
					StringBuilder defaultAddress= new StringBuilder();
					 defaultAddress
					.append(userAddress.getStreetAddress())
					.append(", ")
					.append(userAddress.getPostName())
					.append(", ")
					.append(userAddress.getTaluka())
					.append(", ")
					.append(userAddress.getCity())
					.append(", ")
					.append(userAddress.getState())
					.append(", ")
					.append(userAddress.getCountry())
					.append(" - ")
					.append(userAddress.getZipCode());
					map.addAttribute("defaultAddress", defaultAddress.toString());
					map.addAttribute("userAddress", userAddress);
				
				}
					
		}
	}
		
		
		
		
		User userDTO = userService.getUserById(sessionUser.getUserId());
		List<Order> allOrderList = orderService.getListOfOrders();
		
		
		List<Order> userOrderList=allOrderList.stream()
				.filter(p -> sessionUser.getUserId().equals(p.getDealerId())) // we dont like mkyong
				.collect(Collectors.toList());
		
		
		List<Order> sorteduserOrderList = new ArrayList<>();

		for (Order o : userOrderList) {
			sorteduserOrderList.add(o);
		}

		map.addAttribute("orderList", userOrderList);
		
		Collections.sort(sorteduserOrderList, new Comparator<Order>() {
			public int compare(Order o1, Order o2) {
				if (o1.getOrderDate() == null || o2.getOrderDate() == null) {
					return 0;
				} else {
					return o1.getOrderDate().compareTo(o2.getOrderDate());
				}
			}
		});
		
		if(!userOrderList.isEmpty()) {
			ShoppingCart cart = shopingCartService.getCart(sorteduserOrderList.get(sorteduserOrderList.size() - 1).getCartId());
			map.addAttribute("cart", cart);

			map.addAttribute("orderStatus", sorteduserOrderList.get(sorteduserOrderList.size() - 1));
			map.addAttribute("items",
					shopingCartService.getCart(sorteduserOrderList.get(sorteduserOrderList.size() - 1).getCartId()).getItems());
		}
		
		map.addAttribute("userDTO", userDTO);
		
		return "user";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/product-detail")
	public String productDetail(@RequestParam(value = "id", required = true) String id, ModelMap map,
			HttpSession session, Principal principal) {

		User sessionUser = (User) session.getAttribute("userSession");

		if (sessionUser == null) {
			map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			map.addAttribute("isUser", relogin);
			map.addAttribute("userName", sessionUser.getUsername());
			System.out.println(sessionUser.getUsername());
		}

		System.out.println("Inside UserController.profile");
		Product product = productService.getProductByID(id);
		map.addAttribute("product", product);

		List<Product> productList = productService.getListOfProducts();

		List<Product> agroChemicalList = productList.stream() // convert list to stream
				.filter(p -> "AGRO_CHEMICAL".equals(p.getCategory())) // we dont like mkyong
				.collect(Collectors.toList()); // collect the output and convert streams to a List
		map.addAttribute("agroChemicalList", agroChemicalList);

		return "product-detail";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/login")
	public String login(ModelMap map) {
		User user = new User();
		map.addAttribute("user", user);
		return "login";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/downloadReceipt")
	public ResponseEntity<InputStreamResource> downloadReceipt(@RequestParam(value = "id", required = true) String id,Principal principal, HttpSession session) {
		System.out.println("Inside downloadReceipt");
		
		Order order = orderService.getOrderDetailsById(id);

		System.out.println("downloadReceipt:"+ order.getOrderId());
		
		ByteArrayInputStream bis = Receipt.create(order, shopingCartService.getCart(order.getCartId()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=Invoice_"+order.getOrderId()+".pdf");
		
		return ResponseEntity
		        .ok()
		        .headers(headers)
		        .contentType(MediaType.APPLICATION_PDF)
		        .body(new InputStreamResource(bis));
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/cancelOrder")
	public @ResponseBody UserResponseDTO cancelOrder(@RequestBody Order order,Principal principal, HttpSession session) {
		System.out.println("Inside cancelOrder");
		
		User sessionUser = (User) session.getAttribute("userSession");
		
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		if(order.getOrderId()!=null) {
			orderService.cancelOrder(order);
			notificationService.sendOrderCancellationEmail(sessionUser.getEmailId(), order);
			
			userResponseDTO.setMessage("Order Cancelled Successfully.");
			userResponseDTO.setActionPassed(true);
		}else {
			userResponseDTO.setMessage("An Error Occured while order cancellation.");
			userResponseDTO.setActionPassed(false);
		}
		return userResponseDTO;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/requestOTP")
	public @ResponseBody UserResponseDTO requestOTP(@RequestBody User user) {
		
		System.out.println("Inside requestOTP");
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		
		List<User> userList = userService.getUserList();
		try {
				User userResult = userList.stream() 
							.filter(x -> user.getPhoneNumber().equals(x.getUsername())) 
							.findAny()
							.orElse(null);
				
					if (userResult != null) {
						System.out.println("User Found:"
					+userResult.getUserId());
						String otp = String.valueOf(CommonUtil.OTP(6));
						userResult.setOneTimePassword(otp);
						userService.updateUserOTP(userResult);

						if (userResult.getEmailId() != null) {
								notificationService.sendOTP(userResult.getEmailId(), otp);
								if(userResult.getPhoneNumber()!=null) {
									String mobNum="91"+userResult.getPhoneNumber();
									SMSSender.send("[AGRODHAN] OTP to reset your acount password : "+otp, mobNum);
								}
								userResponseDTO.setMessage("Success! OTP sent to registered email: "+userResult.getEmailId()+" and mobile number : "+userResult.getPhoneNumber());
								userResponseDTO.setResponseId(userResult.getUserId());
								userResponseDTO.setActionPassed(true);
						} else {
							userResponseDTO.setMessage("An error occured while requesting OTP");
							userResponseDTO.setActionPassed(false);
						}
					}else {
						userResponseDTO.setMessage("User does not exist");
						userResponseDTO.setActionPassed(false);
					}
			}catch(Exception e) {
				e.getStackTrace();
				userResponseDTO.setMessage("An Error occurred while requesting OTP");
				userResponseDTO.setActionPassed(false);
			}
		return userResponseDTO;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/updateNewPassword")
	public @ResponseBody UserResponseDTO updateNewPassword(@RequestBody User user) {
		System.out.println("Inside updateNewPassword ");
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userService.updateUserNewPassword(user);
		userResponseDTO.setActionPassed(true);
		userResponseDTO.setMessage("New Password Updated Successfully. Use new password to login");
		return userResponseDTO;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/verifyOTP")
	public @ResponseBody UserResponseDTO verifyOTP(@RequestBody User user) {
		System.out.println("Inside verifyOTP ");
		//VALIDATE Register Mobile Number
		
		List<User> userList = userService.getUserList();
		User userResult = userList.stream() // Convert to steam
				.filter(x -> user.getPhoneNumber().equals(x.getUsername())) // we want "jack" only
				.findAny() // If 'findAny' then return found
				.orElse(null); // If not found, return null
		
		UserResponseDTO userResponseDTO = new UserResponseDTO();
			try {
				if(userResult!=null) {
					if(userResult.getOneTimePassword().equals(user.getOneTimePassword())) {
						userResponseDTO.setActionPassed(true);
						userResponseDTO.setMessage("OTP verified successfully");
						userResponseDTO.setResponseId(userResult.getUserId());
					}else {
						userResponseDTO.setActionPassed(false);
						userResponseDTO.setMessage("Incorrect OTP");
					}
				}
			}catch(Exception e) {
				userResponseDTO.setMessage("An Error Occured");
				userResponseDTO.setActionPassed(false);
			}
		return userResponseDTO;
	}
	

	@RequestMapping(method = RequestMethod.POST, value = "/validateUser")
	public @ResponseBody UserResponseDTO validate(@RequestBody User user, Principal principal, HttpSession session) {
		System.out.println("Inside validate ");
		UserResponseDTO userResponseDTO = new UserResponseDTO();

		
		System.out.println(user.getEmailId());
		List<User> userList = userService.getUserList();
		User userResult = userList.stream() // Convert to steam
				.filter(x -> user.getUsername().equals(x.getUsername())) // we want "jack" only
				.findAny() // If 'findAny' then return found
				.orElse(null); // If not found, return null
		
		System.out.println(user.getUsername());
		
		if (userResult!=null) {
			if (userResult.getPassword().equals(user.getPassword())) {
				userResponseDTO.setMessage("Login Success!");
				userResponseDTO.setActionPassed(true);
				session.setAttribute("userSession", userResult);
			} else {
				userResponseDTO.setMessage("Incorrect password");
				userResponseDTO.setActionPassed(false);
			}
		} else {
			userResponseDTO.setMessage("User does not exist");
			userResponseDTO.setActionPassed(false);
		}
		return userResponseDTO;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveAddress")
	public @ResponseBody UserResponseDTO saveAddress(@RequestBody User user, HttpSession session, Principal p) {
		
		System.out.println("Inside saveAddress" +user.getUserId());
		
		System.out.println(user.getDefaultAddress().toString());

		User sessionUser = (User) session.getAttribute("userSession");
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		if (sessionUser == null) {
		} else {
			userResponseDTO.setSuccessAction("false");
			System.out.println(sessionUser.getUsername());
		}
		try {
			user.setUserId(sessionUser.getUserId());
			
			userService.updateUserAddress(user);
			userResponseDTO.setActionPassed(true);
			userResponseDTO.setMessage("Address saved successfully");
			
		}catch (Exception e) {
			userResponseDTO.setActionPassed(false);
		}
		return userResponseDTO;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveProfile")
	public @ResponseBody UserResponseDTO saveProfile(@RequestBody User user, HttpSession session, Principal p) {
		System.out.println("Inside saveProfile"+user.getUserId());

		User sessionUser = (User) session.getAttribute("userSession");
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		if (sessionUser == null) {
		} else {
			userResponseDTO.setSuccessAction("false");
			System.out.println(sessionUser.getUsername());
		}
		try {
			userService.updateUser(user);
			userResponseDTO.setActionPassed(true);
			userResponseDTO.setMessage("Profile changes saved successfully");
			
		}catch (Exception e) {
			userResponseDTO.setActionPassed(false);
		}
		return userResponseDTO;
	}
	

	@RequestMapping(method = RequestMethod.POST, value = "/registerNewUser")
	public @ResponseBody UserResponseDTO registerNewUser(@RequestBody User user, HttpSession session, Principal p) {
		System.out.println("Inside registerNewUser");

		User sessionUser = (User) session.getAttribute("userSession");

		if (sessionUser == null) {
			// map.addAttribute("msg", "Please log in");
		} else {
			boolean relogin = Boolean.FALSE;
			// map.addAttribute("isUser",relogin);
		}

		UserResponseDTO userResponseDTO = new UserResponseDTO();

		if (userService.getUserList().contains(user)) {
			userResponseDTO.setMessage("User already Exists");
		} else {
			userService.addUser(user);
			userResponseDTO.setMessage("User Registered successfully");
			
			try {
				String mobNum="91"+user.getPhoneNumber();
				SMSSender.send("[AGRODHAN] Thank you for registering with India's No 1 Online AGRO STORE. Visit http://agrodhan.com/", mobNum);
			}
			catch (Exception e) {
			System.out.println("An Error Occurred while sending welcome message.");
			}
			
			userResponseDTO.setActionPassed(true);
			userResponseDTO.setResponseId(user.getUserId());
			session.setAttribute("userSession", user);
		}

		return userResponseDTO;
	}

	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" }, value = "api/users")
	public ResponseEntity addNewUser(@RequestBody User user) {
		// System.out.println(companyDTO.toString());
		System.out.println("Inside createNewCompany ");

		User retUser = userService.addUser(user);

		return ResponseEntity.status(HttpStatus.OK).body(retUser);
	}

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" }, value = "api/users/{userId}")
	public ResponseEntity getUserByUserId(@PathVariable String userId) {
		User retUser = userService.getUserById(userId);
		return ResponseEntity.status(HttpStatus.OK).body(retUser);
	}

	@RequestMapping(value = { "/logout" }, method = RequestMethod.POST)
	public String logoutDo(HttpServletRequest request, HttpServletResponse response) {

		System.out.println("Inside the logout");
		HttpSession session = request.getSession(false);
		SecurityContextHolder.clearContext();
		session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}
		for (Cookie cookie : request.getCookies()) {
			cookie.setMaxAge(0);
		}
		return "index";
	}

}
