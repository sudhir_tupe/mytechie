package com.agromart.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable()
         .authorizeRequests()
         .antMatchers("/admin").hasRole("ADMIN")
         .antMatchers("/dealer").hasAnyRole("USER","ADMIN")
         .antMatchers("/").permitAll()
         .and()
         .formLogin().loginPage("/login");
	}
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("dealer").password("mart").roles("USER")
                .and()
                .withUser("admin").password("mart").roles("ADMIN");
    }
}
