package com.agromart.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;

@Configuration
public class SNSClient {
	
	@Value("${aws.region}")
	private String awsRegion;
	
	@Bean
	AmazonSNS getSNSClient(){
		AmazonSNS snsClient = AmazonSNSClient
	            .builder()
	            .withRegion(Regions.US_EAST_2)
	            .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("AKIAQ3ANIG7KGOVTVUUZ", "DWCwgCE8f72IAXsfAS7GtrNLRkx5wbx6BXLvGzaQ")))
	            .build();
		return snsClient;
	}
}
