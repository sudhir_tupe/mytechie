package com.agromart.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agromart.dao.DealerDao;
import com.agromart.model.DealerDTO;
import com.agromart.service.DealerService;

@Service
public class DealerServiceImpl implements DealerService {
	@Autowired
	private DealerDao dealerDao;
	
	@Override
	public DealerDTO addNewDealer(DealerDTO dealerDTO) {
		dealerDTO.setCreationDate(Calendar.getInstance().getTime());
		dealerDTO.setLastUpdateDate(Calendar.getInstance().getTime());
		dealerDTO.setDealerStatus("ACTIVE");
		return dealerDao.addNewDealer(dealerDTO);
	}

	@Override
	public DealerDTO updateDealer(DealerDTO dealerDTO) {
		dealerDTO.setLastUpdateDate(Calendar.getInstance().getTime());
		return dealerDao.updateDealer(dealerDTO);
	}

	@Override
	public DealerDTO getDealerById(String dealerId) {
		return dealerDao.getDealerById(dealerId);
	}

	@Override
	public List<DealerDTO> getDealerList() {
		return dealerDao.getDealerList();
	}

}
