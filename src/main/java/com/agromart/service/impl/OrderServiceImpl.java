package com.agromart.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agromart.dao.OrderDao;
import com.agromart.model.Order;
import com.agromart.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{
	@Autowired
	private OrderDao orderDao;
	private DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


	
	@Override
	public Order createOrder(Order order) {
		order.setOrderDate(Calendar.getInstance().getTime());
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // Or whatever IST is supposed to be
		order.setOrderDateStr(formatter.format(Calendar.getInstance().getTime()));
		orderDao.createOrder(order);
		return order;
	}

	@Override
	public Order updateOrder(Order order) {
		orderDao.updateOrder(order);
		return order;
	}

	@Override
	public Order cancelOrder(Order order) {
		return orderDao.cancelOrder(order);
	}

	@Override
	public Order getOrderDetailsById(String orderId) {
		return orderDao.getOrderDetailsById(orderId);
	}

	@Override
	public List<Order> getListOfOrders() {
		return orderDao.getListOfOrders();
	}
}
