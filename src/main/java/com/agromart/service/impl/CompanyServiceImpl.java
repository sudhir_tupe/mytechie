package com.agromart.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agromart.dao.CompanyDao;
import com.agromart.model.CompanyDTO;
import com.agromart.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	private CompanyDao companyDao;

	@Override
	public CompanyDTO addNewCompany(CompanyDTO companyDTO) {
		companyDTO.setCreationDate(Calendar.getInstance().getTime());
		companyDTO.setLastUpdateDate(Calendar.getInstance().getTime());
		companyDTO.setCompanyStatus("ACTIVE");
		return companyDao.addNewCompany(companyDTO);
	}

	@Override
	public CompanyDTO getCompanyById(String companyId) {
		return companyDao.getCompanyById(companyId);
	}

	@Override
	public CompanyDTO updateCompany(CompanyDTO companyDTO) {
		companyDTO.setLastUpdateDate(Calendar.getInstance().getTime());
		return companyDao.updateCompany(companyDTO);
	}

	@Override
	public List<CompanyDTO> getCompanyList() {
		return companyDao.getCompanyList();
	}
	
	
}
