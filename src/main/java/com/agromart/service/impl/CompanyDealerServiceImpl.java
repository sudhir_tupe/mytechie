package com.agromart.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agromart.dao.CompanyDealerDAO;
import com.agromart.model.CompanyDealerDTO;
import com.agromart.service.CompanyDealerService;

@Service
public class CompanyDealerServiceImpl implements CompanyDealerService {
	@Autowired
	private CompanyDealerDAO companyDealerDAO;

	@Override
	public CompanyDealerDTO addNewDealer(CompanyDealerDTO companyDealerDTO) {
		companyDealerDTO.setCreationDate(Calendar.getInstance().getTime());
		companyDealerDTO.setLastUpdateDate(Calendar.getInstance().getTime());
		companyDealerDTO.setDealerStatus("ACTIVE");
		return companyDealerDAO.addNewDealer(companyDealerDTO);
	}

	@Override
	public List<CompanyDealerDTO> getCompanyDealersList() {
		return companyDealerDAO.getCompanyDealersList();
	}

}