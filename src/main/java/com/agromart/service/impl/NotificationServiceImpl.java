package com.agromart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.agromart.model.Order;
import com.agromart.model.ShoppingCart;
import com.agromart.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService{
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Override
	public boolean sendEmail(String emailId, Order order, ShoppingCart cart) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(emailId);
		mailMessage.setSubject("AGRODHAN - NEW ORDER PLACED#"+order.getOrderId());
		mailMessage.setText("Dear User, \n\nThank you for shopping on Agrodhan. Your order has been successfully placed. \n\n ORDER ID : "+order.getOrderId()+"\n\n Delivery Address: "+order.getNewAddress()+"\n\n Items Ordered:\n"+cart.getItems().toString());
		
		System.out.println(cart.getItems().toString());
		
		javaMailSender.send(mailMessage);
		
		return true;
	}

	@Override
	public boolean sendOrderCancellationEmail(String emailId, Order order) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(emailId);
		mailMessage.setSubject("AGRODHAN - ORDER CANCELLED #"+order.getOrderId());
		mailMessage.setText("Dear User, \n\nYour order "+order.getOrderId()+" has been cancelled.");
		javaMailSender.send(mailMessage);
		return true;
	}
	
	
	@Override
	public boolean sendOTP(String emailId, String otp) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(emailId);
		mailMessage.setSubject("AGRODHAN - OTP#"+otp);
		mailMessage.setText("Dear User, \n\n Here is your new OTP:"+otp);
		javaMailSender.send(mailMessage);
		
		return true;
	}

	@Override
	public boolean sendTextMsg(String MobileNumber) {
		return false;
	}
}
