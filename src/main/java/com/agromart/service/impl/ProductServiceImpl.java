package com.agromart.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agromart.dao.ProductDao;
import com.agromart.model.Product;
import com.agromart.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{
	@Autowired
	private ProductDao productDao;
	

	@Override
	public Product addProduct(Product product) {
		return productDao.addProduct(product);	
	}

	@Override
	public Product updateProduct(Product product) {
		return productDao.updateProduct(product);
	}

	@Override
	public Product getProductByID(String productId) {
		return productDao.getProductByID(productId);
	}

	@Override
	public List<Product> getListOfProducts() {
		return productDao.getListOfProducts();
	}

}
