package com.agromart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agromart.dao.ShoppingCartDao;
import com.agromart.model.ShoppingCart;
import com.agromart.service.ShopingCartService;

@Service
public class ShopingCartServiceImpl implements ShopingCartService{
	@Autowired
	private ShoppingCartDao shoppingCartDao;

	
	@Override
	public ShoppingCart addToCart(ShoppingCart cart) {
		return shoppingCartDao.addToCart(cart);
	}


	@Override
	public ShoppingCart getCart(String cartId) {
		return shoppingCartDao.getCart(cartId);
	}
}
