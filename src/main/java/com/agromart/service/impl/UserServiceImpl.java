package com.agromart.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agromart.dao.UserDao;
import com.agromart.model.User;
import com.agromart.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public User addUser(User user) {
		return userDao.addUser(user);
	}

	@Override
	public User updateUser(User user) {
		return userDao.updateUser(user);
	}

	@Override
	public User getUserById(String userId) {
		return userDao.getUserById(userId);
	}

	@Override
	public List<User> getUserList() {
		return userDao.getUserList();
	}

	@Override
	public User updateUserAddress(User user) {
		return userDao.updateUserAddress(user);
	}

	@Override
	public User updateUserOTP(User user) {
		return userDao.updateUserOTP(user);
	}

	@Override
	public User updateUserNewPassword(User user) {
		return  userDao.updateUserNewPassword(user);
	}

}
