package com.agromart.service;

import java.util.List;

import com.agromart.model.DealerDTO;

public interface DealerService {
	public DealerDTO addNewDealer(DealerDTO dealerDTO);

	public DealerDTO updateDealer(DealerDTO dealerDTO);

	public DealerDTO getDealerById(String dealerId);
	
	public List<DealerDTO> getDealerList();
}
