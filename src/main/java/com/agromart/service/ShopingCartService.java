package com.agromart.service;

import com.agromart.model.ShoppingCart;

public interface ShopingCartService {
	public ShoppingCart addToCart(ShoppingCart cart);
	public ShoppingCart getCart(String cartId);
}
