package com.agromart.service;

import java.util.List;

import com.agromart.model.CompanyDTO;

public interface CompanyService {
	public CompanyDTO addNewCompany(CompanyDTO companyDTO);
	public CompanyDTO getCompanyById(String companyId);
	public CompanyDTO updateCompany(CompanyDTO companyDTO);
	public List<CompanyDTO> getCompanyList();
}
