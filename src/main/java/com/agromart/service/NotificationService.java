package com.agromart.service;

import com.agromart.model.Order;
import com.agromart.model.ShoppingCart;

public interface NotificationService {
	public boolean sendTextMsg(String MobileNumber);
	public boolean sendEmail(String string, Order order, ShoppingCart cart);
	public boolean sendOrderCancellationEmail(String emailId, Order order);
	public boolean sendOTP(String emailId, String otp);
}
