package com.agromart.service;

import java.util.List;

import com.agromart.model.CompanyDealerDTO;

public interface CompanyDealerService {
	public CompanyDealerDTO addNewDealer(CompanyDealerDTO companyDealerDTO);
	public List<CompanyDealerDTO> getCompanyDealersList();
	
	
}
