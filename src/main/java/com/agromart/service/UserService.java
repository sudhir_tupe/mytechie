package com.agromart.service;

import java.util.List;

import com.agromart.model.User;

public interface UserService {
	public User addUser(User user);

	public User updateUser(User user);
	public User updateUserOTP(User user);
	
	public User updateUserNewPassword(User user);
	
	public User updateUserAddress(User user);

	public User getUserById(String userId);
	
	public List<User> getUserList();
}
