package com.agromart.service;

import java.util.List;

import com.agromart.model.Order;

public interface OrderService {
	public Order createOrder(Order order);
	public Order updateOrder(Order order);
	public Order cancelOrder(Order order);
	public Order getOrderDetailsById(String orderId);
	public List<Order> getListOfOrders();

}
