package com.agromart.service;

import java.util.List;

import com.agromart.model.Product;

public interface ProductService {
	public Product addProduct(Product product);
	public Product updateProduct(Product product);
	public Product getProductByID(String productId);
	public List<Product> getListOfProducts();
}
