package com.agromart.order;

public enum OrderStatus {
	  UNSHIPPED, PENDING, SHIPPED, COMPLETED, CANCELED, REFUND_APPLIED
}
