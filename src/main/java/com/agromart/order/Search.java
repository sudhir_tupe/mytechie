package com.agromart.order;

import java.util.List;

import com.agromart.model.Product;

public interface Search {
	 public List<Product> searchProductsByName(String name);
	  public List<Product> searchProductsByCategory(String category);
}
