package com.agromart.order;

public enum AccountStatus {
	ACTIVE, BLOCKED, BANNED, COMPROMISED, ARCHIVED, UNKNOWN
}
