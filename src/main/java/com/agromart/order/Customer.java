package com.agromart.order;

import com.agromart.model.Item;
import com.agromart.model.Order;
import com.agromart.model.ShoppingCart;

public class Customer {
	private ShoppingCart cart;
	private Order order;

	public ShoppingCart getShoppingCart() {
		return cart;
	}

	public boolean addItemToCart(Item item) {
		return false;
	}

	public boolean removeItemFromCart(Item item) {
		return false;
	}
}
