package com.agromart.order;

import com.agromart.model.Address;
import com.agromart.model.Product;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedJson;

public class Account {
	private String userName;
	private String password;
	private AccountStatus status;
	private String name;
	private Address shippingAddress;
	private String email;
	private String phone;

	/*
	 * private List<CreditCard> creditCards; private List<ElectronicBankTransfer>
	 * bankAccounts;
	 */

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean addProduct(Product product) {
		return false;
	}

	public boolean addProductReview(ProductReview review) {
		return false;
	}

	public boolean resetPassword() {
		return false;
	}

}
