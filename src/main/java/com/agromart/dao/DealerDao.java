package com.agromart.dao;

import java.util.List;

import com.agromart.model.DealerDTO;

public interface DealerDao {
	public DealerDTO addNewDealer(DealerDTO dealerDTO);

	public DealerDTO updateDealer(DealerDTO dealerDTO);

	public DealerDTO getDealerById(String dealerId);
	
	public List<DealerDTO> getDealerList();

}
