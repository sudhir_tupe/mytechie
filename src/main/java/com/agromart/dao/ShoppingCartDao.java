package com.agromart.dao;

import com.agromart.model.ShoppingCart;

public interface ShoppingCartDao {
	public ShoppingCart addToCart(ShoppingCart cart);
	public ShoppingCart getCart(String cartId);
}
