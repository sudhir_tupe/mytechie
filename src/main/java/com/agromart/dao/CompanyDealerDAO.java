package com.agromart.dao;

import java.util.List;

import com.agromart.model.CompanyDealerDTO;

public interface CompanyDealerDAO {

	public CompanyDealerDTO addNewDealer(CompanyDealerDTO companyDealerDTO);
	public List<CompanyDealerDTO> getCompanyDealersList();
}
