package com.agromart.dao;

import java.util.List;

import com.agromart.model.CompanyDTO;

public interface CompanyDao {
	public CompanyDTO addNewCompany(CompanyDTO companyDTO);

	public CompanyDTO updateCompany(CompanyDTO companyDTO);

	public CompanyDTO getCompanyById(String companyId);
	public List<CompanyDTO> getCompanyList();
}
