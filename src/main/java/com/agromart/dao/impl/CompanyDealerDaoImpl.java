package com.agromart.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agromart.dao.CompanyDealerDAO;
import com.agromart.model.CompanyDTO;
import com.agromart.model.CompanyDealerDTO;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

@Component
public class CompanyDealerDaoImpl implements CompanyDealerDAO {
	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	@Override
	public CompanyDealerDTO addNewDealer(CompanyDealerDTO companyDealerDTO) {
		dynamoDBMapper.save(companyDealerDTO);
		return companyDealerDTO;
	}

	@Override
	public List<CompanyDealerDTO> getCompanyDealersList() {
		List<CompanyDealerDTO> list = dynamoDBMapper.scan(CompanyDealerDTO.class, new DynamoDBScanExpression());
		return list;
	}
}
