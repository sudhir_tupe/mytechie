package com.agromart.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agromart.dao.UserDao;
import com.agromart.model.User;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

@Component
public class UserDaoImpl implements UserDao{

	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	@Override
	public User addUser(User user) {
		dynamoDBMapper.save(user);
		return user;
	}

	@Override
	public User updateUser(User user) {
		User savedUser= dynamoDBMapper.load(User.class, user.getUserId());
		savedUser.setUserLName(user.getUserLName());
		savedUser.setUserFName(user.getUserFName());
		savedUser.setPhoneNumber(user.getPhoneNumber());
		savedUser.setEmailId(user.getEmailId());
		dynamoDBMapper.save(savedUser);
		return user;
	}
	
	@Override
	public User updateUserOTP(User user) {
		User savedUser= dynamoDBMapper.load(User.class, user.getUserId());
		savedUser.setOneTimePassword(user.getOneTimePassword());
		dynamoDBMapper.save(savedUser);
		return user;
	}
	
	
	@Override
	public User updateUserAddress(User user) {
		User savedUser= dynamoDBMapper.load(User.class, user.getUserId());
		savedUser.setDefaultAddress(user.getDefaultAddress());
		dynamoDBMapper.save(savedUser);
		return user;
	}

	@Override
	public User getUserById(String userId) {
		return dynamoDBMapper.load(User.class,userId);
	}

	@Override
	public List<User> getUserList() {
		List<User> users= dynamoDBMapper.scan(User.class, new DynamoDBScanExpression());
		return users;
	}

	@Override
	public User updateUserNewPassword(User user) {
		User savedUser= dynamoDBMapper.load(User.class, user.getUserId());
		savedUser.setPassword(user.getPassword());
		dynamoDBMapper.save(savedUser);
		return user;
	}

	
	
	
}
