package com.agromart.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agromart.dao.OrderDao;
import com.agromart.model.Order;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;

@Component
public class OrderDaoImpl implements OrderDao{
	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	@Override
	public Order createOrder(Order order) {
		dynamoDBMapper.save(order);
		return order;
	}

	@Override
	public Order updateOrder(Order order) {
		System.out.println("OrderDaoImpl.updateProduct");
		Map<String, ExpectedAttributeValue> expectedAttributeValueMap = new HashMap<>();
		expectedAttributeValueMap.put("productId", new ExpectedAttributeValue(new AttributeValue().withS(order.getOrderId())));
		
		DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression().withExpected(expectedAttributeValueMap);
		dynamoDBMapper.save(order, saveExpression);
		return order;
	}

	@Override
	public Order cancelOrder(Order order) {
		 Order retrivedOrder=dynamoDBMapper.load(Order.class, order.getOrderId());
		 retrivedOrder.setOrderStatus("CANCELLED");
		 dynamoDBMapper.save(retrivedOrder);
		return retrivedOrder;
	}

	@Override
	public Order getOrderDetailsById(String orderId) {
		return dynamoDBMapper.load(Order.class, orderId);
	}

	@Override
	public List<Order> getListOfOrders() {
		List<Order> list = dynamoDBMapper.scan(Order.class, new DynamoDBScanExpression());
		return list;
	}

}
