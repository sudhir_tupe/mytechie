package com.agromart.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agromart.dao.CompanyDao;
import com.agromart.model.CompanyDTO;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

@Component
public class CompanyDaoImpl implements CompanyDao {

	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	public CompanyDaoImpl(DynamoDBMapper dynamoDBMapper) {
		this.dynamoDBMapper = dynamoDBMapper;
	}

	@Override
	public CompanyDTO addNewCompany(CompanyDTO companyDTO) {
		System.out.println(companyDTO.toString());
		dynamoDBMapper.save(companyDTO);
		return companyDTO;
	}

	@Override
	public CompanyDTO getCompanyById(String companyId) {
		return dynamoDBMapper.load(CompanyDTO.class, companyId);
	}

	@Override
	public CompanyDTO updateCompany(CompanyDTO companyDTO) {
		CompanyDTO retrievedItem = dynamoDBMapper.load(CompanyDTO.class, companyDTO.getCompanyId());

		retrievedItem.setCompanyEmailId(companyDTO.getCompanyEmailId());
		retrievedItem.setCompanyName(companyDTO.getCompanyName());

		retrievedItem.setCompanyLicenseNo(companyDTO.getCompanyLicenseNo());
		retrievedItem.setCompanyLicenseExpiryDate(companyDTO.getCompanyLicenseExpiryDate());

		retrievedItem.setCompanyGSTNumber(companyDTO.getCompanyGSTNumber());
		retrievedItem.setCompanyPanNumber(companyDTO.getCompanyPanNumber());

		retrievedItem.setCompanyAuthPersonName(companyDTO.getCompanyAuthPersonName());
		retrievedItem.setContactNumber(companyDTO.getContactNumber());

		retrievedItem.setCompanyStorageAddress(companyDTO.getCompanyStorageAddress());
		retrievedItem.setCompanyRegAddress(companyDTO.getCompanyRegAddress());

		retrievedItem.setPrincipalCertificateFileName(companyDTO.getPrincipalCertificateFileName());
		retrievedItem.setOnboardingDate(companyDTO.getOnboardingDate());

		retrievedItem.setCompanyStatus(companyDTO.getCompanyStatus());
		DynamoDBMapperConfig config = DynamoDBMapperConfig.builder()
				.withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT).build();

		CompanyDTO updatedCompanyDTO = dynamoDBMapper.load(CompanyDTO.class, companyDTO.getCompanyId(), config);

		System.out.println("Retrieved the previously updated item:");
		System.out.println(updatedCompanyDTO);

		dynamoDBMapper.save(retrievedItem);

		return retrievedItem;
	}

	@Override
	public List<CompanyDTO> getCompanyList() {
		List<CompanyDTO> list = dynamoDBMapper.scan(CompanyDTO.class, new DynamoDBScanExpression());
		return list;
	}
}
