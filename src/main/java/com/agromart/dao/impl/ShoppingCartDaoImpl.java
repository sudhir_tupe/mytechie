package com.agromart.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agromart.dao.ShoppingCartDao;
import com.agromart.model.ShoppingCart;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

@Component
public class ShoppingCartDaoImpl implements ShoppingCartDao{
	@Autowired
	private DynamoDBMapper dynamoDBMapper;
	
	
	@Override
	public ShoppingCart addToCart(ShoppingCart cart) {
		dynamoDBMapper.save(cart);
		return cart;
	}

	@Override
	public ShoppingCart getCart(String cartId) {
		return dynamoDBMapper.load(ShoppingCart.class, cartId);
	}
}
