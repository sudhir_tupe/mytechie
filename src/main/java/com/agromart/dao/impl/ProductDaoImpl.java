package com.agromart.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agromart.dao.ProductDao;
import com.agromart.model.Product;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;

@Component
public class ProductDaoImpl implements ProductDao {
	@Autowired
	private DynamoDBMapper dynamoDBMapper;
	
	@Override
	public Product addProduct(Product product) {
		System.out.println("ProductDaoImpl.addProduct");
		dynamoDBMapper.save(product);
		System.out.println("Created Product: "+product.getProductId());
		return product;
	}

	@Override
	public Product updateProduct(Product product) {
		System.out.println("ProductDaoImpl.updateProduct");
		Map<String, ExpectedAttributeValue> expectedAttributeValueMap = new HashMap<>();
		expectedAttributeValueMap.put("productId", new ExpectedAttributeValue(new AttributeValue().withS(product.getProductId())));
		
		DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression().withExpected(expectedAttributeValueMap);
		dynamoDBMapper.save(product, saveExpression);
		return product;
	}

	@Override
	public Product getProductByID(String productId) {
		System.out.println("ProductDaoImpl.getProductByID");
		return dynamoDBMapper.load(Product.class, productId);
	}

	@Override
	public List<Product> getListOfProducts() {
		System.out.println("ProductDaoImpl.getListOfProducts");
		List<Product> list = dynamoDBMapper.scan(Product.class, new DynamoDBScanExpression());
		return list;
	}
}
