package com.agromart.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agromart.dao.DealerDao;
import com.agromart.model.CompanyDTO;
import com.agromart.model.DealerDTO;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;

@Component
public class DealerDaoImpl implements DealerDao {
	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	@Override
	public DealerDTO addNewDealer(DealerDTO dealerDTO) {
		dynamoDBMapper.save(dealerDTO);
		return dealerDTO;
	}

	@Override
	public DealerDTO updateDealer(DealerDTO dealerDTO) {

		Map<String, ExpectedAttributeValue> expectedAttributeValueMap = new HashMap<>();
		expectedAttributeValueMap.put("dealerId", new ExpectedAttributeValue(new AttributeValue().withS(dealerDTO.getDealerId())));
		
		DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression().withExpected(expectedAttributeValueMap);
		dynamoDBMapper.save(dealerDTO, saveExpression);
		return dealerDTO;
	}

	@Override
	public DealerDTO getDealerById(String dealerId) {
		return dynamoDBMapper.load(DealerDTO.class, dealerId);
	}

	@Override
	public List<DealerDTO> getDealerList() {
		List<DealerDTO> list = dynamoDBMapper.scan(DealerDTO.class, new DynamoDBScanExpression());
		return list;
	}

}
