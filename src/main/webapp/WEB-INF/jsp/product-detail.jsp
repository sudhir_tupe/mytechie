<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="AGRODHAN">
	  <meta name="Keywords" content="Online Agro Shopping in India,online Agro Shopping store,Online Agro Shopping Site, Buy Online,Shop Online,Online Shopping,Agrodhan"/> 
	  <meta name="Description" content="India&#x27;s biggest online agro store for Vegetable Seeds,Field Crops Seeds,Fertilisers,Water Soluble fertilisers,Organic Products,Pesticides,Agri Equipments,Micronutrients,Insecticides,Fungicides,Herbicides,Bio Pesticides,Flower Seeds,Onion Seeds from all brands at the lowest prices in India. Payment options - COD, EMI, Credit card, Debit card &amp;amp; more."/>
      <title>Online Agro Shopping Site for Vegetable Seeds, Agro Chemicals &amp; More. Best Offers!</title>
      <!-- Favicon Icon -->	
      <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Select2 CSS -->
      <link href="vendor/select2/css/select2-bootstrap.css" />
      <link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
   </head>
   <body>
   <input type="hidden" value='${isUser}' id="isUser">
      <div class="bg-light">
         <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +91-7709462647</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold"><i class="icofont-rupee"></i>5000</span></p>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav shadow-sm">
          <jsp:include page="navbar.jsp"/>
         </div>
      </div>
      <section class="py-2  inner-header">
         <div class="container">
            <div class="row d-flex align-items-center">
               <div class="col-lg-12">
                  <div class="breadcrumbs">
                     <p class="mb-0"><a href="#"><span class="icofont icofont-ui-home"></span> Home</a> <span class="icofont icofont-thin-right"></span> <a href="#">VEGGIE</a> <span class="icofont icofont-thin-right"></span> <span>Vegetables</span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="py-5 shop-single bg-light">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <div class="shop-detail-left">
                     <div class="shop-detail-slider position-relative">
                        <div class="favourite-icon"> <a class="fav-btn" title="" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="59% OFF"><i class="icofont-ui-tag"></i></a>
                        </div>
                        <div id="sync1" class="border rounded shadow-sm bg-white mb-2 owl-carousel text-center">
                           <div class="item bg-b">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item bg-r">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item bg-b">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item bg-r">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item bg-b">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item bg-r">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                        </div>
                        <div id="sync2" class="owl-carousel">
                           <div class="item">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                           <div class="item">
                              <img alt="" src="${product.productImgPath}" class="img-fluid img-center">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="shop-detail-right">
                     <div class="border rounded shadow-sm bg-white p-4">
                        <div class="product-name">
                           <p class="badge badge-success text-uppercase mb-0"><i class="icofont-rupee"></i> 500 Off</p>
                           <h2><c:out value='${product.productName}'/></h2>
                           <span>Product code: <b>FUSIONCROP500</b> | <strong class="text-info">FREE Delivery</strong> on orders over <i class="icofont-rupee"></i>5000</span>
                        </div>
                        <div class="price-box">
                           <h5>
                              <span class="product-desc-price"><i class="icofont-rupee"></i> <c:out value='${product.price + 500}'/></span>
                              <span class="product-price text-danger"><i class="icofont-rupee"></i><c:out value='${product.price}'/></span>
                              <small class="text-success">You Save : <i class="icofont-rupee"></i>500.00</small>
                           </h5>
                        </div>
                        <div class="ratings">
                           <div class="stars-rating"> <i class="icofont icofont-star active"></i>
                              <i class="icofont icofont-star active"></i>
                              <i class="icofont icofont-star"></i>
                              <i class="icofont icofont-star"></i>
                              <i class="icofont icofont-star"></i>  <span>(613)</span>
                              <span class="rating-links float-right"><a href="#"> <font color="#000000">3 Review(s)</font></a> <span class="separator"> </span>   <a href="#det"><i class="icofont icofont-comment"></i> <font color="#000000"> Add Your Review</font></a> 
                              </span>
                           </div>
                        </div>
						<div class="clearfix"></div>
                        <div class="product-color-size-area mt-3">
                           <span class="d-inline-block pt-1">Packaging size : </span>
                           <div class="btn-group btn-group-toggle float-center" data-toggle="buttons">
                              <label class="btn btn-sm btn-outline-secondary active">
                              <input type="radio" name="options" id="option1" autocomplete="off" checked>${product.packagingGram}</label>
                           </div>
                        </div>
						<div class="clearfix"></div>
                        <div class="product-variation">
                           <form action="#" method="post">
                              <div class="mt-1 pt-2 float-left mr-2">Quantity :</div>
                              <div class="input-group quantity-input"> <span class="input-group-btn">
                                 <button type="button" class="btn btn-outline-secondary btn-number btn-lg" data-type="minus" data-field="quant[1]">
                                 <span class="fa fa-minus"></span>
                                 </button>
                                 </span>
                                 <input type="text" name="quant[1]" class="text-center form-control border-form-control form-control-sm input-number" value="1" readonly="readonly"> <span class="input-group-btn">
                                 <button type="button" class="btn btn-outline-secondary btn-number btn-lg" data-type="plus" data-field="quant[1]">
                                 <span class="fa fa-plus"></span>
                                 </button>
                                 </span>
                              </div>
                              <span class="float-right">
                              <button type="button" title="" data-placement="top" data-toggle="tooltip" data-original-title="Add to Wishlist" class="btn btn-outline-success btn-lg"><i class="icofont icofont-heart"></i></button>
                              <button type="button" class="btn btn-success btn-lg" data-toggle="modal" title="ADD TO CART"  data-target="#exampleModal" onclick="myFunction('${product.productId}','${product.productImgPath}','${product.price}','${product.productName}')">&nbsp;&nbsp;&nbsp; <i class="icofont icofont-shopping-cart"></i> Add To Cart &nbsp;&nbsp;&nbsp;</button>
                              </span>
                           </form>
                        </div>
                        <div class="short-description border-bottom">
                           <h6 class="mb-3">
                              <span class="text-dark font-weight-bold">Quick Overview</span>  
                              <small class="float-right">Availability: <strong class="badge badge-info ">In Stock : ${product.availableItemCount}</strong></small>
                           </h6>
                           <p><b>Company: </b> Fusion Crop Science Pvt Ltd</p>
                           <p><b>Product Sold By (Seller): </b> Raju Dealer</p>
                           
                           <p><a class="font-weight-bold" href="#det"><font color="#FF4500">View More[...]</font></a></p>
                        </div>
                        <div class="product-cart-option">
                           <ul class="list-inline mb-0 mt-3">
                              <li class="list-inline-item"><a href="#"><font color="#006400"><i class="icofont-share-alt"></i> <span>Share</span></font></a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="det" class="pb-5 pt-0 shop-single-detail bg-light">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="rounded shadow-sm bg-white">
                     <ul class="nav nav-pills p-3" id="pills-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">DETAILS</a>
                        </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">ADDITIONAL</a>
                        </li>
                        <li class="nav-item"> <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">REVIEWS (3)</a>
                        </li>
                     </ul>
                     <div class="tab-content p-4 border-top" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <h5 class="mt-0 mb-3">General</h5>
                            <ul class="m-0 ml-3 p-0">
                             <li><strong>Company: </strong> Fusion Crop Science Pvt Ltd</li>
                             <li><strong>Product Sold By (Seller): </strong> Raju Dealer</li>
                             <li><strong>Variety Name: </strong> ${product.productName}</li>
                              <li><strong>Product Category: </strong> ${product.description}</li>
                             <li><strong>Product Packaging: </strong>${product.packagingGram}</li>
                             
                              <li><strong>Product Price: <i class="icofont-rupee"></i></strong>${product.price}</li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                           <h5 class="mt-0 mb-3">Important Points</h5>
                           <p>
                           </p>
                            <ul class="m-0 ml-3 p-0">
                           <c:choose>
    <c:when test="${product.description == 'AGRO_CHEMICAL'}">
       <li><strong>Chemical composition: </strong></li>
                              <li><strong>Dosage: </strong>  400 ml/acre</li>
                              <li><strong>Method of application:</strong> Spray</li>
                              <li><strong> Duration of effect: </strong> 10 - 12 days</li>
                              <li><strong>Frequency of application: </strong> 2 times</li>
                              <li><strong> Applicable crops: </strong> Chilli, Rice, Cabbage, Sugarcane, Cotton</li>
                              <li><strong>Extra description:</strong> Most effective for thrips, aphid and larva</li>
    </c:when>    
    <c:otherwise>
          <li><strong>Sowing season:</strong> Kharif , Rabi</li>
                              <li><strong>Sowing method:</strong> Transplanting</li>
                              <li><strong>Sowing spacing:</strong> R-R:3.5;P-P:1 ft</li>
                              <li><strong>Please Note:</strong> The information offered here is for reference only and depends exclusively on soil type and climatic conditions. Always refer to product labels and accompanying leaflets for complete product details and directions for use.</li>
    </c:otherwise>
</c:choose>
                           
                           
                            
                           </ul>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                           <div class="card-body p-0 reviews-card">
                              <div class="media mb-4">
                                 <img class="d-flex mr-3 rounded-circle" src="img/user/1.jpg" alt="">
                                 <div class="media-body">
                                    <div class="mt-0 mb-1">
                                       <span class="h6 mr-2 font-weight-bold">Sunil Tupe</span> <span><i class="icofont-ui-calendar"></i> 	Nov 19, 2020</span>
                                       <div class="stars-rating float-right"> <i class="icofont icofont-star active"></i>
                                          <i class="icofont icofont-star active"></i>
                                          <i class="icofont icofont-star"></i>
                                          <i class="icofont icofont-star"></i>
                                          <i class="icofont icofont-star"></i>  <span class="rounded bg-warning text-dark pl-1 pr-1">5/3</span>
                                       </div>
                                    </div>
                                    <p>Go for it. Good.</p>
                                 </div>
                              </div>
                              <div class="media mt-4">
                                 <img class="d-flex mr-3 rounded-circle" src="img/user/1.jpg" alt="">
                                 <div class="media-body">
                                    <div class="mt-0 mb-1">
                                       <span class="h6 mr-2 font-weight-bold">Vikas Sheware</span> <span><i class="icofont-ui-calendar"></i> Oct 12, 2020</span>
                                       <div class="stars-rating float-right"> <i class="icofont icofont-star active"></i>
                                          <i class="icofont icofont-star active"></i>
                                          <i class="icofont icofont-star"></i>
                                          <i class="icofont icofont-star"></i>
                                          <i class="icofont icofont-star"></i>  <span class="rounded bg-warning text-dark pl-1 pr-1">5/3</span>
                                       </div>
                                    </div>
                                    <p class="mb-0">Good product. Delivery guy good humanity, very nice thanks to Agrodhan.
</p>
                                 </div>
                              </div>
                               <div class="media mt-4">
                                 <img class="d-flex mr-3 rounded-circle" src="img/user/1.jpg" alt="">
                                 <div class="media-body">
                                    <div class="mt-0 mb-1">
                                       <span class="h6 mr-2 font-weight-bold">Sunli Bhojane</span> <span><i class="icofont-ui-calendar"></i> Oct 15, 2020</span>
                                       <div class="stars-rating float-right"> <i class="icofont icofont-star active"></i>
                                          <i class="icofont icofont-star active"></i>
                                          <i class="icofont icofont-star"></i>
                                          <i class="icofont icofont-star"></i>
                                          <i class="icofont icofont-star"></i>  <span class="rounded bg-warning text-dark pl-1 pr-1">5/3</span>
                                       </div>
                                    </div>
                                    <p class="mb-0">Good product. Thanks to Agrodhan!!
</p>
                                 </div>
                              </div>
                              
                              
                           </div>
                           <div class="p-4 bg-light rounded mt-4">
                              <h5 class="card-title mb-4">Leave a Review</h5>
                              <form name="sentMessage">
                                 <div class="row">
                                    <div class="control-group form-group col-lg-4 col-md-4">
                                       <div class="controls">
                                          <label>Your Name <span class="text-danger">*</span></label>
                                          <input type="text" class="form-control">
                                       </div>
                                    </div>
                                    <div class="control-group form-group col-lg-4 col-md-4">
                                       <div class="controls">
                                          <label>Your Email <span class="text-danger">*</span></label>
                                          <input type="email" class="form-control">
                                       </div>
                                    </div>
                                    <div class="control-group form-group col-lg-4 col-md-4">
                                       <div class="controls">
                                          <label>Rating <span class="text-danger">*</span></label>
                                          <select class="form-control custom-select">
                                             <option>1 Star</option>
                                             <option>2 Star</option>
                                             <option>3 Star</option>
                                             <option>4 Star</option>
                                             <option>5 Star</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="control-group form-group">
                                    <div class="controls">
                                       <label>Review <span class="text-danger">*</span></label>
                                       <textarea rows="3" cols="100" class="form-control"></textarea>
                                    </div>
                                 </div>
                                 <div class="text-right">
                                    <button type="submit" class="btn btn-success" disabled="disabled">Send Message</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="product-list pt-5 bg-light pb-4 pbc-5 border-top">
         <div class="container">
            <h6 class="mt-1 mb-0 float-right"><a href="#">View All Items</a></h6>
            <h4 class="mt-0 mb-3 text-dark">Items You Recently Viewed</h4>
            <div class="row">
               <div class="col-md-12">
                  <div class="owl-carousel owl-carousel-category owl-theme">
                   <c:forEach items="${agroChemicalList}" var="pItem">
                     <div class="item">
                        <div class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
                           <span class="like-icon"><a href="#"> <i class="icofont icofont-heart"></i></a></span>
                           <a href="/product-detail?id=${pItem.productId}">
                           <span class="badge badge-danger">NEW</span>
                           <img src="<c:out value="${pItem.productImgPath}"></c:out>" class="card-img-top" alt="..."></a>
                           <div class="card-body">
                              <h6 class="card-title mb-1"><c:out value="${pItem.productName}"></c:out></h6>
                              <div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i> <span>613</span></div>
                              <p class="mb-0 text-dark"><i class="icofont-rupee"></i> <c:out value="${pItem.price}"></c:out> <span class="text-black-50"><del><i class="icofont-rupee"></i>${pItem.price +500} </del></span> <span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small"> 50% OFF</span></p>
                           </div>
                        </div>
                     </div>
                     </c:forEach>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Footer -->
      <jsp:include page="footer.jsp" />
      
      <%-- <form:form action="/checkout">
      <div class="cart-sidebar">
         <div class="cart-sidebar-header">
            <h5>
               My Cart <span class="text-info"></span><a data-toggle="offcanvas" class="float-right" href="#"><i class="icofont icofont-close-line"></i>
               </a>
            </h5>
         </div>
         <div class="cart-sidebar-body" id="cartBody">
         </div>
         <div class="cart-sidebar-footer">
            <div class="cart-store-details">
               <p>Delivery Charges <strong class="float-right text-success"><i class="icofont-rupee"></i>FREE</strong>
               </p>
               <h6>Your total savings <span id="totalSaving" class="float-right"><strong class="float-right text-danger"><i class="icofont-rupee"></i>0</strong></span></h6>
            </div>
            <a href="/checkout">
            <button class="btn btn-primary btn-lg btn-block text-left" id ="checkoutBtn" type="button"><span class="float-left"><i class="icofont icofont-cart"></i> Proceed to Checkout </span><span id="newTotal" class="float-right"><strong><i class="icofont-rupee"></i>0</strong> <span class="icofont icofont-bubble-right"></span></span>
            </button>
            </a>
         </div>
      </div>
    </form:form>
     --%>
     
     <!-- Shopping Cart -->
	<form:form action="/checkout">
	<div class="cart-sidebar">
		<div class="cart-sidebar-header">
			<h5>
            My Cart <font class="text-info">(</font><span id="numOfItemsId" class="text-info">0</span><font class="text-info"> item)</font><a data-toggle="offcanvas" class="float-right" href="#"><i
                  class="icofont icofont-close-line"></i>
            </a>
         </h5>
					
		</div>
		<div class="cart-sidebar-body" id="cartBody">
		<input id="cartCounter" type="hidden" value="0">
		
		</div>
		<div class="cart-sidebar-footer">
			<div class="cart-store-details">
				<p>
					Sub Total <span id="subTotal" class="float-right"><strong class="float-right text-danger"><i
						class="icofont-rupee"></i>0</strong></span>
				</p>
				<h6>
					Your total savings <span id="totalSaving" class="float-right"> <strong class="float-right text-danger"><i
						class="icofont-rupee"></i>0</strong></span>
				</h6>
			</div>
			<a href="/checkout"><button id="checkoutBtn"
					class="btn btn-success btn-lg btn-block text-left" type="button">
					<span class="float-left"><i class="fa fa-shopping-cart"></i>
						Checkout </span><span id="newTotal" class="float-right"><strong><i
							class="icofont-rupee"></i>0</strong> <span
						class="icofont icofont-bubble-right"></span></span>
				</button></a>
		</div>
	</div>
	</form:form>
     
     
     
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-shopping-cart" style="margin-right: 3px;"></i> Shopping Cart</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" data-toggle="offcanvas" class="nav-link" data-dismiss="modal">View Cart</button>
      </div>
    </div>
  </div>
</div>
	
      <!-- Bootstrap core JavaScript -->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- select2 Js -->
      <script src="vendor/select2/js/select2.min.js"></script>
      <!-- Owl Carousel -->
      <script src="vendor/owl-carousel/owl.carousel.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
      
      <script>

      $(document).ready(function() {


    	  var isUser=$("#isUser").val();

    		if(isUser){
    			$('#profileId li:contains(Login)').hide();
    			$('#profileId li:contains(My Account)').show();
    		}else{
    			$('#profileId li:contains(My Account)').hide();
    			$('#profileId li:contains(Login)').show();
    		}

    	  $('#checkoutBtn').click(function(event) {
  			event.preventDefault();
  			 var items = [];

    			$(".cart-list-product").each(function(){
  			  	var imgPath=$(this).find("img").attr('src');
  				var pNameval=$(this).find("h5").text();
  				var price=$(this).find("p").text();
  				var item= { productName: pNameval, price:price,imgPath: imgPath};
  				items.push(item);
  			});

  		
  			var cart ={'items': items};
  				$.ajax({
  					url : "/checkout",
  					contentType: 'application/json; charset=utf-8',
  				    dataType: 'json',
  				    type: 'POST',
  				    data: JSON.stringify(cart),
  					success : function(data) {
  						if (data.actionPassed == false) {
  							alert("ERROR");
  							return;
  						} else {
  							window.location.href = "/viewcart?id="+data.responseId+"";
  						}
  					}
  				});
  			});

      });
      
      /* function myFunction(id,img,price,name) {
			var mymodal = $('#exampleModal');
			 mymodal.find('.modal-body').html('Product <strong>'+name+' </strong> added to CART successfully.');
			
			
			var totalSaving =parseFloat($("#totalSaving").text());
			var currentTotatPrice  = parseFloat($("#newTotal").text());


			var newTotal = parseFloat(currentTotatPrice) + parseFloat(price);
			$("#newTotal").html('<i class="icofont-rupee"></i>'+newTotal+'<span class="icofont icofont-bubble-right"></span>');

			 
			totalSaving=totalSaving+500;
			$("#totalSaving").html('<strong><i class="icofont-rupee"></i>'+totalSaving+'</strong>');
			
			 $('#cartBody').append('<div class="cart-list-product"><a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>'+
					 '<img class="img-fluid" src='+img+'><span class="badge badge-success"><i class="icofont-rupee"></i>500 OFF</span><br><h5><a class="productName" href="/product-detail">'+name+'</a></h5>'
			 +'<div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i><span>613</span></div>'
+'<p class="f-14 mb-0 text-dark float-right"><i class="icofont-rupee"></i>'+price+'</p>'
+'<span class="count-number float-left"><button class="btn btn-outline-secondary btn-sm left dec"><i class="icofont-minus"></i></button><input class="count-number-input" type="text" value="1"><button class="btn btn-outline-secondary btn-sm right inc"><i class="icofont-plus"></i></button></span></div>');
		} */


		function myFunction(id,img,price,name) {
			var mymodal = $('#exampleModal');
			 mymodal.find('.modal-body').html('Product <strong>'+name+' </strong> added to CART successfully.');
				
			var totalSaving =parseFloat($("#totalSaving").text());
			
			var currentTotatPrice  = parseFloat($("#newTotal").text());

			var subTotal=  parseFloat(currentTotatPrice) + parseFloat(price);
			$("#subTotal").html('<strong class="float-right text-danger"><i class="icofont-rupee"></i>'+subTotal+'</strong>');
			

			var newTotal = parseFloat(currentTotatPrice) + parseFloat(price);
			$("#newTotal").html('<i class="icofont-rupee"></i>'+newTotal+'<span class="icofont icofont-bubble-right"></span>');

			
			totalSaving=totalSaving+500;
			$("#totalSaving").html('<strong><i class="icofont-rupee"></i>'+totalSaving+'</strong>');
			var cartCounter = parseInt($("#cartCounter").val()) + parseInt(1); 

			$("#cartCounter").val(cartCounter);
			var delPrice= parseInt(price) + 500;
			 $('#cartBody').append('<div class="cart-list-product" id='+cartCounter+'><a onclick="removeItemFromCart('+cartCounter+','+price+');" class="float-right remove-cart" title="REMOVE"><i class="icofont-close-squared-alt" style=""></i></a>'+
					 '<img class="img-fluid" src='+img+'><span class="badge badge-success"><i class="icofont-rupee"></i>500 OFF</span><br><h5><a class="productName" href="#">'+name+'</a></h5>'
			 +'<div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i><span>613</span></div>'
			 
			 +'<p class="f-14 mb-0 text-dark float-right"><i class="icofont-rupee"></i>'+price+'</p>'
+'<span class="count-number float-left"><button class="btn btn-outline-secondary btn-sm left dec" disabled><i class="icofont-minus"></i></button><input class="count-number-input" type="text" value="1" disabled><button class="btn btn-outline-secondary btn-sm right inc" disabled><i class="icofont-plus"></i></button></span></div>');

			 var itemCount=parseInt($("#numOfItemsId").text());	
			$("#numOfItemsId").text(itemCount + 1);
			}

      function removeItemFromCart(counter,price){
  		$('#removeCounter').val(counter);
  		$("#"+counter+"").hide();
  		var newTotal = parseFloat($("#newTotal").text()) - parseFloat(price);
  		$("#subTotal").html('<strong class="float-right text-danger"><i class="icofont-rupee"></i>'+newTotal+'</strong>');
  		$("#newTotal").html('<i class="icofont-rupee"></i>'+newTotal+'<span class="icofont icofont-bubble-right"></span>');
  		var totalSaving =parseFloat($("#totalSaving").text());
  		totalSaving=totalSaving-500;
  		$("#totalSaving").html('<strong><i class="icofont-rupee"></i>'+totalSaving+'</strong>');

  		 var itemCount=parseInt($("#numOfItemsId").text());	
  		$("#numOfItemsId").text(itemCount - 1);
  		//$('#removeItemModal').show();
  	}
      
      </script>
   </body>
</html>