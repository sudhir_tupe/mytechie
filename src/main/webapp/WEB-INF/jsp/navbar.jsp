<nav class="navbar navbar-expand-lg navbar-dark pt-0 pb-0" style="background-color:#1f7a1f">
				<div class="container">
				
					<!-- <a class="navbar-brand" href="/">  <img
						src="img/green_bag_logo.png" alt="AGROMAART">
					 -->	
				
					<a class="navbar-brand" href="/">  <img
						src="img/nlogo.png" alt="AGRODHAN">
				
						
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto main-nav-left">
							<!-- <li class="nav-item"><a class="nav-link" href="/"><font color="#ffffff">
							 <i class="icofont-ui-home"></i> 
							</font></a></li> -->
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> 
								<font color="#ffffff">SEEDS</font>
								</a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
													<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Vegetables Seeds</a>
												<a href="/product-grid" title="ONION">ONION</a> <a
													href="/product-grid" title="BHENDI">BHENDI</a> <a
													href="/product-grid" title="TOMATO">TOMATO</a> <a
													href="/product-grid" title="BITTER GOURD">BITTER GOURD</a> <a
													href="/product-grid" title="BOTTLE GOURD">BOTTLE GOURD</a> <a
													href="/product-grid" title="BRINJAL">BRINJAL</a> <a
													href="/product-grid" title="CHILLI">CHILLI</a> <a
													href="/product-grid" title="CUCUMBER">CUCUMBER</a> <a
													href="/product-grid" title="CAULIFLOWER">CAULIFLOWER</a> <a
													href="/product-grid" title="CABBAGE">CABBAGE</a>

											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Root
													Vegetables Seeds</a> <a href="/product-grid" title="POTATO"> POTATO</a> <a
													href="/product-grid" title="CARROT"> CARROT</a> <a
													href="/product-grid" title="RADISH"> RADISH </a> <a
													href="/product-grid" title="BEET ROOT"> BEET ROOT</a> <a
													href="/product-grid" title="GINGER">GINGER</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Leafy
													Vegetables Seeds</a> <a href="/product-grid" title="CORIANDER">CORIANDER</a> <a
													href="product-grid.ht/product-gridl"  title="PALAK">PALAK</a> <a
													href="/product-grid"  title="FENUGREEK">FENUGREEK</a> <a
													href="/product-grid"  title="MINT PUDINA">MINT PUDINA</a> <a
													href="/product-grid"  title="COLOCASIA LEAF">COLOCASIA LEAF</a> <a
													href="/product-grid"  title="CAPSICUM">CAPSICUM</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fibre and
													Oil Seeds</a> <a href="/product-grid" title="COTTON">COTTON</a> <a
													href="/product-grid" title="SUNFLOWER">SUNFLOWER</a> <a
													href="/product-grid" title="SOYABEAN">SOYABEAN</a> <a
													href="/product-grid" title="MUSTARD">MUSTARD</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Cereals
													and Pulses</a> <a href="/product-grid" title="BAJRA"> BAJRA</a> <a
													href="/product-grid" title="JOWAR"> JOWAR</a> <a
													href="/product-grid" title="WHEAT">WHEAT</a> <a
													href="/product-grid" title="PADDY">PADDY</a> <a
													href="/product-grid" title="MAIZE">MAIZE</a> <a
													href="/product-grid" title="GRAM">GRAM</a> <a
													href="/product-grid" title="PIGEON-PEA (TUR)">PIGEON-PEA (TUR)</a> <a
													href="/product-grid" title="GREEN GRAM">GREEN GRAM</a> <a
													href="/product-grid" title="BLACK GRAM">BLACK GRAM</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Gardening Seeds</a>
												<a href="/product-grid" title="LOTUS FLOWER"> LOTUS FLOWER</a> <a
													href="/product-grid" title="FRENCH MARIGOLD"> FRENCH MARIGOLD</a> <a
													href="/product-grid" title="AFRICON MARIGOLD">AFRICON MARIGOLD</a> <a
													href="/product-grid" title="ICE FLOWER">ICE FLOWER</a> <a
													href="/product-grid" title="CHERRY TOMATO">CHERRY TOMATO</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
											</div>
										</div>
									</div>
								</div></li>
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <font color="#ffffff">AGRO CHEMICALS</font></a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fungicides</a> 
												<a href="/product-grid" title="AMISTAR">AMISTAR</a> <a
													href="/product-grid" title="CAPTAN">CAPTAN</a> <a
													href="/product-grid" title="BAVISTIN">BAVISTIN</a>
												<a href="/product-grid" title="KAVACH">KAVACH</a> <a
													href="/product-grid" title="ACROBAT">ACROBAT</a> <a
													href="/product-grid" title="FIVE STAR">FIVE STAR</a> <a
													href="/product-grid" title="ANTRACOL">ANTRACOL</a> <a
													href="/product-grid" title="AMISTAR TOP">AMISTAR TOP</a> <a
													href="/product-grid" title="AVTAR">AVTAR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Insecticides</a> 
												<a href="/product-grid" title="ACTARA">ACTARA</a> <a
													href="/product-grid" title="AMPLIGO">AMPLIGO</a> <a
													href="/product-grid" title="CHESS">CHESS</a> <a
													href="/product-grid" title="PEGASUS">PEGASUS</a> <a
													href="/product-grid" title="ALIKA">ALIKA</a> <a
													href="/product-grid" title="METADOR">METADOR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Herbicides</a>
												<a href="/product-grid" title="RIFIT"> RIFIT</a> <a
													href="/product-grid" title="GRAMOXONE">GRAMOXONE</a> <a
													href="/product-grid" title="AXIAL">AXIAL</a> <a
													href="/product-grid" title="RIFIT PLUS">RIFIT PLUS</a> <a
													href="/product-grid" title="FUSIFLEX">FUSIFLEX</a> <a
													href="/product-grid" title="TOPIK">TOPIK</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Plant Growth Regulator(PGR)</a>
												<a href="/product-grid" title="MODDUS">MODDUS</a> <a
													href="/product-grid" title="PALISADE EC">PALISADE EC</a> <a
													href="/product-grid" title="PRIME+ EC">PRIME+ EC</a>
											</div>
										</div>
									</div>
								</div></li>
								
										<li class="nav-item dropdown"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#ffffff">FERTILIZERS</font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="#">VERMI COMPOST</a>
								</div></li>
								
								<li class="nav-item dropdown"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#ffffff">HARDWARE</font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="#" title="WATER MOTOR PUMP">WATER MOTOR PUMP</a> 
									<a class="dropdown-item" href="#" title="STARTERS">STARTERS</a>
									<a class="dropdown-item" href="#" title="SPRAYER PUMP (BATTERY)">SPRAYER PUMP (BATTERY)</a>
									<a class="dropdown-item" href="#" title="SPRINKLER">SPRINKLER</a>  
									<a class="dropdown-item" href="#" title="DRIP IRRIGATION">DRIP IRRIGATION</a> 
									<a class="dropdown-item" href="#" title="PIPE">PIPE</a>
									<a class="dropdown-item" href="#" title="MILK CANS">MILK CANS</a>
									<a class="dropdown-item" href="#" title="LED TORCH">LED TORCH</a>
								</div></li>
						</ul>
						<form class="form-inline my-2 my-lg-0 top-search">
							<button class="btn-link" type="submit">
								<i class="icofont-search"></i>
							</button>
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search for products, brands.."
								aria-label="Search">
						</form>
						
                <ul class="navbar-nav ml-auto profile-nav-right" id="profileId">
				<li class="nav-item" id="loginId"><a href="#"
					data-target="#login" data-toggle="modal" class="nav-link ml-0">
						<font color="#ffffff"><i class="icofont-ui-user"></i>&nbsp;Login</font>
				</a></li>
				<li class="nav-item dropdown" id="myAccount">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><img alt="Generic placeholder image" src="img/user/dealer2.jpg" class="nav-osahan-pic rounded-pill"><font color="#ffffff">My Account</font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="#">Hello <strong>${userName}</strong></a>
									<a class="dropdown-item" href="/user"><i class="icofont-user"></i>My Profile</a> 
									<a class="dropdown-item" href="/user"><i class="icofont-list"></i>Orders</a> 
									<a class="dropdown-item" href="/logout"><i class="icofont-logout"></i> Logout</a>
								</div></li>
                     
                     
                     <li class="nav-item cart-nav">
                        <a data-toggle="offcanvas" class="nav-link" href="#">
                         <font color="#ffffff">  <i class="fa fa-shopping-cart" style="margin-right: 3px;"></i> Cart</font>
                           <span class="badge badge-danger">0</span>
                        </a>
                     </li>
                  </ul>
					</div>
				</div>
			</nav>
