<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="AGROMAART">
      <meta name="author" content="AGROMAART">
      <title>AGROMAART</title>
      <!-- Favicon Icon -->
      <link rel="icon" type="image/png" href="img/fav-icon.png">
      <!-- Bootstrap core CSS -->
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Select2 CSS -->
      <link href="vendor/select2/css/select2-bootstrap.css" />
      <link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
      <!-- Font Awesome-->
      <link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css"/>
      <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css"/>
   </head>
   <body>
<div class="main-nav shadow-sm">
<nav class="navbar navbar-expand-lg navbar-dark pt-0 pb-0" style="background-color:#51aa1b">
				<div class="container">
					<a class="navbar-brand" href="/">  <img
						src="img/agro-logo_new.png" alt="AGROMAART">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto main-nav-left">
							<!-- <li class="nav-item"><a class="nav-link" href="/"><font color="#ffffff"><i
									class="icofont-ui-home"></i></font></a></li> -->
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> 
								<font color="#ffffff">VEGGIE CROPS </font>
								
								
								</a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
													<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Vegetables</a>
												<a href="/product-grid">ONION</a> <a
													href="/product-grid">BHENDI</a> <a
													href="/product-grid">TOMATO</a> <a
													href="/product-grid">BITTER GOURD</a> <a
													href="/product-grid">BOTTLE GOURD</a> <a
													href="/product-grid">BRINJAL</a> <a
													href="/product-grid">CHILLI</a> <a
													href="/product-grid">CUCUMBER</a> <a
													href="/product-grid">CAULIFLOWER</a> <a
													href="/product-grid">CABBAGE</a>

											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Root
													Vegetables</a> <a href="/product-grid"> POTATO</a> <a
													href="/product-grid"> CARROT</a> <a
													href="/product-grid"> RADISH </a> <a
													href="/product-grid"> BEET ROOT</a> <a
													href="/product-grid">GINGER</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Leafy
													Vegetables</a> <a href="/product-grid">CORIANDER</a> <a
													href="product-grid.ht/product-gridl">PALAK</a> <a
													href="/product-grid">FENUGREEK</a> <a
													href="/product-grid">MINT PUDINA</a> <a
													href="/product-grid">COLOCASIA LEAF</a> <a
													href="/product-grid">CAPSICUM</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">FIBRE AND
													OILS</a> <a href="/product-grid">COTTON</a> <a
													href="/product-grid">SUNFLOWER</a> <a
													href="/product-grid">SOYABEAN</a> <a
													href="/product-grid">MUSTARD</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">CEREALS
													AND PULSES</a> <a href="/product-grid"> BAJRA</a> <a
													href="/product-grid"> JOWAR</a> <a
													href="/product-grid">WHEAT</a> <a
													href="/product-grid">PADDY</a> <a
													href="/product-grid">MAIZE</a> <a
													href="/product-grid">GRAM</a> <a
													href="/product-grid">PIGEON-PEA (TUR)</a> <a
													href="/product-grid">GREEN GRAM</a> <a
													href="/product-grid">BLACK GRAM</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">GARDENING</a>
												<a href="/product-grid"> LOTUS FLOWER</a> <a
													href="/product-grid"> FRENCH MARIGOLD</a> <a
													href="/product-grid">AFRICON MARIGOLD</a> <a
													href="/product-grid">ICE FLOWER</a> <a
													href="/product-grid">CHERRY TOMATO</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
											</div>
										</div>
									</div>
								</div></li>
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <font color="#ffffff">AGRO CHEMICALS</font></a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fungicides</a> 
												<a href="/product-grid">AMISTAR</a> <a
													href="/product-grid">CAPTAN</a> <a
													href="/product-grid">BAVISTIN</a>
												<a href="/product-grid">KAVACH</a> <a
													href="/product-grid">ACROBAT</a> <a
													href="/product-grid">FIVE STAR</a> <a
													href="/product-grid">ANTRACOL</a> <a
													href="/product-grid">AMISTAR TOP</a> <a
													href="/product-grid">AVTAR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Insecticides</a> 
												<a href="/product-grid">Actara</a> <a
													href="/product-grid">Ampligo</a> <a
													href="/product-grid">Chess</a> <a
													href="/product-grid">Pegasus</a> <a
													href="/product-grid">Alika</a> <a
													href="/product-grid">Matador</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Herbicides</a>
												<a href="/product-grid"> Rifit</a> <a
													href="/product-grid">Gramoxone</a> <a
													href="/product-grid">Axial</a> <a
													href="/product-grid">Rifit Plus</a> <a
													href="/product-grid">Fusiflex</a> <a
													href="/product-grid">Topik</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Plant Growth Regulator(PGR)</a>
												<a href="/product-grid">Moddus</a> <a
													href="/product-grid">Palisade EC</a> <a
													href="/product-grid">Prime+ EC</a>
											</div>
										</div>
									</div>
								</div></li>
						</ul>
						<form class="form-inline my-2 my-lg-0 top-search">
							<button class="btn-link" type="submit">
								<i class="icofont-search"></i>
							</button>
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search for products, brands and more"
								aria-label="Search">
						</form>
						
                <ul class="navbar-nav ml-auto profile-nav-right" id="profileId">
				<li class="nav-item" id="loginId"><a href="#"
					data-target="#login" data-toggle="modal" class="nav-link ml-0">
						<font color="#ffffff"><i class="icofont-ui-user"></i>&nbsp;Login</font>
				</a></li>
                     <li class="nav-item cart-nav">
                        <a data-toggle="offcanvas" class="nav-link" href="#">
                         <font color="#ffffff">  <i class="fa fa-shopping-cart" style="margin-right: 3px;"></i> Cart</font>
                           <span class="badge badge-danger">0</span>
                        </a>
                     </li>
                  </ul>
					</div>
				</div>
			</nav>
		

</div>
		<br>
		<br>
		<br>
		
		<div align="center">
		 <form:form modelAttribute="userDTO"
									action="/requestOTP">
		<table class="text-center">
		<tr>
		<td><label>Enter Mobile No</label></td>
		<td><input type="number" id="mobileNumber" class="form-control" placeholder="Enter Mobile Number"></td></tr>
		<tr><td><label>Enter Email ID</label></td><td><input type="text" id="emailId" placeholder="Enter Email ID" class="form-control"></td></tr>
		<tr><td></td><td><button type="button" class="btn btn-info" id="btnRequestOTP" class="form-control"> Request OTP </button></td></tr>
		</table>
		<br>
		<br>
		<div id="otpDiv">
		<table class="text-center">
		<tr><td><label>Enter OTP</label></td><td><input type="number" id="textOtp" placeholder="Enter OTP" class="form-control"></td></tr>
		<tr><td></td><td> <button type="button" class="btn btn-info" id="btnVerifyOTP" class="form-control">Verify</button></td></tr>
		</table>
		</div>
		</form:form>
		 <input type="hidden" id="textUserId">
		<br>
		<br>
		<form:form modelAttribute="userDTO"
									action="/updateNewPassword">
									<div id="verifyOTPSuccessDiv" class="alert alert-success"></div>
          <div id="verifyOTPFailureDiv" class="alert alert-danger"></div>
									
									
		<div id="newPasswordDiv">
		<table><tr><td>New Password</td><td>: <input type="password" id="newPassword"></td></tr>
		<tr><td>Confirm Password</td><td>:<input type="password" id="confirmPassword"></td></tr>
		<tr><td></td><td><button type="button" class="btn btn-info" id="btnSubmitNewPassword">Submit</button></td></tr>
		</table>
		</div>
		 <div id="requestOTPSuccessDiv" class="alert alert-success"></div>
          <div id="requestOTPFailureDiv" class="alert alert-danger"></div>
          <div id="passwordUpdateSuccessDiv" class="alert alert-success"></div>
          <div id="passwordUpdateFailureDiv" class="alert alert-danger"></div>
		</form:form>
		</div>
		    <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- select2 Js -->
      <script src="vendor/select2/js/select2.min.js"></script>
      <!-- Owl Carousel -->
      <script src="vendor/owl-carousel/owl.carousel.js"></script>
      <!-- Data Tables -->
      <link href="vendor/datatables/datatables.min.css" rel="stylesheet" />
      <script src="vendor/datatables/datatables.min.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="js/custom.js"></script>
      <script>


       $(document).ready(function() {

    	   $("#newPasswordDiv").hide();
    	   $("#otpDiv").hide();
    	   
    		$("#requestOTPSuccessDiv").hide();
        	$("#requestOTPFailureDiv").hide();
        	$("#verifyOTPSuccessDiv").hide();
        	$("#verifyOTPFailureDiv").hide();

        	$("#passwordUpdateSuccessDiv").hide();
        	$("#passwordUpdateFailureDiv").hide();
        	
        	 $("#textOtp").attr("disabled", "disabled"); 
        	 $("#btnVerifyOTP").prop("disabled", true );
           
    		$('#btnRequestOTP').click(function(event) {
    			event.preventDefault();
    			  var user = {};
    			  user["phoneNumber"] = $("#mobileNumber").val();
    			  user["emailId"] = $("#emailId").val();

					if(user.phoneNumber =="" || user.emailId==""){
						$("#requestOTPFailureDiv").html("Enter Phone Number OR Email Id");
						$("#requestOTPFailureDiv").show();
						$("#requestOTPFailureDiv").fadeTo(2000, 500).slideUp(500,
								function() {
									$("#requestOTPFailureDiv").slideUp(500);
								});
							return;
						}
      			  
    			  $.ajax({
    					type : "POST",
    					url : "/requestOTP",
    					contentType : "application/json",
    					processData : false,
    					data : JSON.stringify(user),
    					success : function(data) {
    						if (data.actionPassed == false) {
    							$("#requestOTPFailureDiv").html(data.message);
    							$("#requestOTPFailureDiv").show();

    							$("#requestOTPFailureDiv").fadeTo(2000, 500).slideUp(500,
    									function() {
    										$("#requestOTPFailureDiv").slideUp(500);
    									});
    							
    							 $("#textOtp").attr("disabled", "disabled"); 
    				        	 $("#btnVerifyOTP").prop("disabled", true );
    							
    							return;
    						} else {
    								$("#requestOTPSuccessDiv").html(data.message);
    								$("#requestOTPSuccessDiv").show();	

    								$("#requestOTPSuccessDiv").fadeTo(2000, 500).slideUp(500,
        									function() {
        										$("#requestOTPSuccessDiv").slideUp(500);
        									});

    								$("#textUserId").val(data.responseId);
    								$("#otpDiv").show();
    								  $("#textOtp").removeAttr("disabled");
    								  $("#btnVerifyOTP").prop("disabled",false);
    						}
    					}
    				});
    	  	});

    		$('#btnSubmitNewPassword').click(function(event) {
    			event.preventDefault();
				var user = {};
   			 user["phoneNumber"] = $("#mobileNumber").val();
   			 user["password"] = $("#newPassword").val();
   			 user["userId"] = $("#textUserId").val();
   			 
   			  $.ajax({
   					type : "POST",
   					url : "/updateNewPassword",
   					contentType : "application/json",
   					processData : false,
   					data : JSON.stringify(user),
   					success : function(data) {
   						if (data.actionPassed == false) {
							alert("Error");
   							return;
   						} else {
   							$("#passwordUpdateSuccessDiv").html(data.message);
							$("#passwordUpdateSuccessDiv").show();
							$("#passwordUpdateSuccessDiv").fadeTo(4000, 500).slideUp(500,
									function() {
										$("#passwordUpdateSuccessDiv").slideUp(500);
									});
							window.location.href = "/";
   						}
   					}
   				});
				

            });

    		

    		$('#btnVerifyOTP').click(function(event) {
    			event.preventDefault();
    			var user = {};
    			 user["phoneNumber"] = $("#mobileNumber").val();
    			 user["oneTimePassword"] = $("#textOtp").val();
    			 
    			  $.ajax({
    					type : "POST",
    					url : "/verifyOTP",
    					contentType : "application/json",
    					processData : false,
    					data : JSON.stringify(user),
    					success : function(data) {
    						if (data.actionPassed == false) {
    							$("#verifyOTPFailureDiv").html(data.message);
    							$("#verifyOTPFailureDiv").show();

    							$("#verifyOTPFailureDiv").fadeTo(4000, 500).slideUp(500,
    									function() {
    										$("#verifyOTPFailureDiv").slideUp(500);
    									});
    							
    							return;
    						} else {
    							$("#verifyOTPSuccessDiv").html(data.message);
								$("#verifyOTPSuccessDiv").show();
								$("#verifyOTPSuccessDiv").fadeTo(4000, 500).slideUp(500,
    									function() {
    										$("#verifyOTPSuccessDiv").slideUp(500);
    									});
								 $("#newPasswordDiv").show();
    						}
    					}
    				});
    	  	});

    		

    	  	
       });
       </script>
</body>
</html>