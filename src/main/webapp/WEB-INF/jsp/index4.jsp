<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="AGROMART">
<meta name="author" content="AGROMART">
<title>AGROMART</title>
<!-- Favicon Icon -->
<link rel="icon" type="image/png" href="img/fav-icon.png">



<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Slider CSS -->
<link rel="stylesheet" href="vendor/slider/slider.css">
<!-- Select2 CSS -->
<link href="vendor/select2/css/select2-bootstrap.css" />
<link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
<!-- Font Awesome-->
<link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
<link href="vendor/icofont/icofont.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
</head>
<body>
	<div class="modal fade login-modal-main" id="login">
		<div class="modal-dialog modal-lg modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="login-modal">
						<div class="row">
							<div class="col-lg-6 d-flex align-items-center">
								<div class="login-modal-left p-4 text-center pl-5">
									<img src="img/login2.png" alt="TECHIES">
								</div>
							</div>
							<div class="col-lg-6">
								<button type="button"
									class="close close-top-right position-absolute"
									data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true"><i class="icofont-close-line"></i></span>
									<span class="sr-only">Close</span>
								</button>
								<form:form action="/validate" class="position-relative" modelAttribute="userAccount" id="frm">
									<ul
										class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute"
										role="tablist">
										<li><a class="nav-link-login active" data-toggle="tab"
											href="#login-form" role="tab"><i class="icofont-ui-lock"></i>
												LOGIN</a></li>
										<li><a class="nav-link-login" data-toggle="tab"
											href="#register" role="tab"><i
												class="icofont icofont-pencil"></i> REGISTER</a></li>
									</ul>
									<div class="login-modal-right p-4">
										<!-- Tab panes -->
										<div class="tab-content">
										
											<div class="tab-pane active" id="login-form" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">LOGIN</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label> <input type="text"
														name="username" id="username" class="form-control"
														placeholder="Email/Mob Number">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label> <input type="password"
														name="password" id="password" class="form-control"
														placeholder="********">
												</fieldset>

												<fieldset class="form-group">
													<input type="radio" id="admin" name="user" value="admin">
													<label for="Admin">ADMIN</label> &nbsp;
													<input type="radio" id="dealer" name="user" value="dealer">
													<label for="Dealer">DEALER</label>
												</fieldset>
												<fieldset class="form-group">
													<button id="enterloginBtn" class="btn btn-lg btn-primary btn-block">Enter to
														your account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck1"> <label
														class="custom-control-label" for="customCheck1">Remember
														me</label>
												</div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="register" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">REGISTER</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label> <input type="text"
														class="form-control" placeholder="+91 123 456 7890">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label> <input type="password"
														class="form-control" placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Confirm Password </label> <input
														type="password" class="form-control"
														placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<button type="submit"
														class="btn btn-lg btn-primary btn-block">Create
														Your Account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck2"> <label
														class="custom-control-label" for="customCheck2">I
														Agree with <a href="#">Term and Conditions</a>
													</label>
												</div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-white">
		<div class="main-nav shadow-sm">
			<!--  Navbar-->
			<jsp:include page="navbar.jsp" />
			<!-- End of NavBar -->
		</div>
		<div class="py-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<header>
							<div id="owl-carousel-one" class="owl-carousel">
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/banner1.jpg">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/banner2.jpg">
								</div>
								<!--                     <div class="item"><img class="img-fluid mx-auto rounded shadow-sm" src="img/banner/3.png"></div>
                        <div class="item"><img class="img-fluid mx-auto rounded shadow-sm" src="img/banner/4.png"></div> -->
							</div>
						</header>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="product-list pbc-5 pb-4 pt-5 bg-white">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark font-weight-normel">Best Selling
				Items</h4>
			<div id="product-container" class="row">
			</div>
		</div>
	</section>
	<section class="product-list pbc-5 pb-4 pt-5 bg-white">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark">Top Savers Today</h4>
					<div id="top-saver-container" class="row"> </div>
					
					</div>
	</section>
	<!-- Footer -->
	<jsp:include page="footer.jsp" />

	
	<form:form action="/checkout">
		<div class="cart-sidebar">
		<div class="cart-sidebar-header">
		<h5>
					<!--  dynamic item count-->
				My Cart <span class="text-info"></span> <a
					data-toggle="offcanvas" class="float-right" href="#"><i
					class="icofont icofont-close-line"></i> </a>
			</h5>
		
		</div>
		<div class="cart-sidebar-body" id="cartBody">
		
		</div>
		<div class="cart-sidebar-footer">
		
		<a href="/checkout">
			<button id="checkoutBtn" class="btn btn-primary btn-lg btn-block text-left" type="button">
					<span class="float-left"><i class="icofont icofont-cart"></i>
						Proceed to Checkout </span>
						
						<span id="newTotal" class="float-right"><i
							class="icofont-rupee"></i>0<span
						class="icofont icofont-bubble-right"></span>
						</span>
				</button></a>
		</div>
	</div>
	
	</form:form>
	

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Shopping Cart</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-toggle="offcanvas" class="nav-link" data-dismiss="modal">View Cart</button>
      </div>
    </div>
  </div>
</div>
	

	<script id="topSaversTemplate" type="text/x-handlebars-template">
   {{#each this}}
<div class="col-6 col-md-3">
				<div class="item">
							<div class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">

<img src='{{this.productImgPath}}' class="card-img-top" alt="...">
								
								<div class="card-body">
									<h6 class="card-title mb-1">{{this.productName}}</h6>
									
<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
<p class="mb-0 text-dark"><i class="icofont-rupee"></i> {{this.price}} <span
											class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
											50% OFF</span>
<button class="btn btn-success like-icon" data-toggle="modal" data-target="#exampleModal" style="border-radius: 6px;" onclick="myFunction('{{this.productId}}','{{this.productImgPath}}','{{this.price}}','{{this.productName}}')"><i class="fa fa-shopping-cart" style="margin-right: 3px;"></i></button>
</p>
</div>
</div>
</div>
</div>
   {{/each}}
	</script>
	
	<script id="bestProductsTemplate" type="text/x-handlebars-template">
	{{#each this}}
		<div class="col-6 col-md-3">
		<div
			class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
			<span class="like-icon"><a href="#"><i
					class="icofont-shopping-cart"></i></a></span> <a href="/product-detail">
				<span class="badge badge-danger">NEW</span>
				<img src='{{this.productImgPath}}' class="card-img-top" alt="...">
			</a>
			<div class="card-body">
				<h6 class="card-title mb-1">{{this.productName}}</h6>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="mb-0 text-dark">
					<i class="icofont-rupee"></i>{{this.price}}<span class="text-black-50"></span>
				</p>
			</div>
		</div>
	</div>
	 {{/each}}
	</script>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- select2 Js -->
	<script src="vendor/select2/js/select2.min.js"></script>
	<!-- Owl Carousel -->
	<script src="vendor/owl-carousel/owl.carousel.js"></script>
	<!-- Slider Js -->
	<script src="vendor/slider/slider.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="js/custom.js"></script>
	<script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
	<link rel="stylesheet" href="js/demo.css?ver=3.4.0">
	  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet"
		href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<script>
	

	
		$(document).ready(function() {
			bestSelling();
			topSaver();

			$('#checkoutBtn').click(function(event) {
				event.preventDefault();

				 var items = [];
				
				$(".cart-list-product").each(function(){
				  	var imgPath=$(this).find("img").attr('src');
					var pNameval=$(this).find("h5").text();
					var price=$(this).find("p").text();
					var item= { productName: pNameval, quantity: price, price:price,imgPath: imgPath};
					items.push(item);
				});

				console.log(items);
			
				var cart ={'items': items};
					$.ajax({
						url : "/checkout",
						contentType: 'application/json; charset=utf-8',
					    dataType: 'json',
					    type: 'POST',
					    data: JSON.stringify(cart),
						success : function(data) {
							if (data.actionPassed == false) {
								alert("ERROR");
								return;
							} else {
								window.location.href = "/viewcart?id="+data.responseId+"";
							}
						}
					});
				});
		});

		function myFunction(id,img,price,name) {
			var mymodal = $('#exampleModal');
			 mymodal.find('.modal-body').text('Product '+name+' added to CART successfully');
			
			var currentTotatPrice  = parseFloat($("#newTotal").text());
			var newTotal = parseFloat(currentTotatPrice) + parseFloat(price);
			$("#newTotal").html('<i class="icofont-rupee"></i>'+newTotal+'<span class="icofont icofont-bubble-right"></span>');
			
			 $('#cartBody').append('<div class="cart-list-product"><a class="float-right remove-cart" href="#"><i class="icofont icofont-close-circled"></i></a>'+
					 '<img class="img-fluid" src='+img+'><span class="badge badge-success">50% OFF</span><br><h5><a class="productName" href="/product-detail">'+name+'</a></h5>'
			 +'<div class="stars-rating"><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star active"></i><i class="icofont icofont-star"></i><span>613</span></div>'
+'<p class="f-14 mb-0 text-dark float-right"><i class="icofont-rupee"></i>'+price+'</p>'
+'<span class="count-number float-left"><button class="btn btn-outline-secondary btn-sm left dec"><i class="icofont-minus"></i></button><input class="count-number-input" type="text" value="1"><button class="btn btn-outline-secondary btn-sm right inc"><i class="icofont-plus"></i></button></span></div>');
		}


		
		
		function topSaver(){
			$.ajax({
				url : "http://localhost:8080/api/products/best-selling"
			}).then(function(data) {
				console.log("topSaver");
				createTopSaversHTML(data);
			});
		}

		function bestSelling(){
			$.ajax({
				url : "http://localhost:8080/api/products/best-selling"
			}).then(function(data) {
				console.log("bestSelling");
				createHTML(data);
			});
		}
		
		function createHTML(productData) {
			var rawTemplate = document.getElementById("bestProductsTemplate").innerHTML;
			var compiledTemplate = Handlebars.compile(rawTemplate);
			var ourGeneratedHTML = compiledTemplate(productData);

			console.log(ourGeneratedHTML);

			var productContainer = document.getElementById("product-container");
			productContainer.innerHTML = ourGeneratedHTML;

		}

		function createTopSaversHTML(topSavers) {
			var rawTemplate = document.getElementById("topSaversTemplate").innerHTML;
			var saverCompiledTemplate = Handlebars.compile(rawTemplate);
			var ourGeneratedHTML = saverCompiledTemplate(topSavers);
			console.log(ourGeneratedHTML);
			var topSaverContainer = document.getElementById("top-saver-container");
			topSaverContainer.innerHTML = ourGeneratedHTML;
		}
		
	</script>
</body>

</html>