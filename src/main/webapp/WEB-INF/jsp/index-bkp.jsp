<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="TECHIES">
<meta name="author" content="TECHIES">
<title>AgroMart</title>
<!-- Favicon Icon -->
<link rel="icon" type="image/png" href="img/leaf.png">
<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Slider CSS -->
<link rel="stylesheet" href="vendor/slider/slider.css">
<!-- Select2 CSS -->
<link href="vendor/select2/css/select2-bootstrap.css" />
<link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
<!-- Font Awesome-->
<link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
<link href="vendor/icofont/icofont.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
<style type="text/css">
#rcorners1 {
  border-radius: 25px;
  background: #73AD21;
  padding: 20px; 
  width: 200px;
  height: 150px;  
}</style>

</head>

<body>
	<div class="modal fade login-modal-main" id="login">
		<div class="modal-dialog modal-lg modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="login-modal">
						<div class="row">
							<div class="col-lg-6 d-flex align-items-center">
								<div class="login-modal-left p-4 text-center pl-5">
									<img src="img/login2.png" alt=TECHIES">
								</div>
							</div>
							<div class="col-lg-6">
								<button type="button"
									class="close close-top-right position-absolute"
									data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true"><i class="icofont-close-line"></i></span>
									<span class="sr-only">Close</span>
								</button>
								<form class="position-relative">
									<ul
										class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute"
										role="tablist">
										<li><a class="nav-link-login active" data-toggle="tab"
											href="#login-form" role="tab"><i class="icofont-ui-lock"></i>
												LOGIN</a></li>
										<li><a class="nav-link-login" data-toggle="tab"
											href="#register" role="tab"><i
												class="icofont icofont-pencil"></i> REGISTER</a></li>
									</ul>
									<div class="login-modal-right p-4">
										<!-- Tab panes -->
										<div class="tab-content">
											<div class="tab-pane active" id="login-form" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">LOGIN</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label> <input type="text"
														class="form-control" placeholder="+91 123 456 7890">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label> <input type="password"
														class="form-control" placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<button type="submit"
														class="btn btn-lg btn-primary btn-block">Enter to
														your account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck1"> <label
														class="custom-control-label" for="customCheck1">Remember
														me</label>
												</div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="register" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">REGISTER</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label> <input type="text"
														class="form-control" placeholder="+91 123 456 7890">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label> <input type="password"
														class="form-control" placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Confirm Password </label> <input
														type="password" class="form-control"
														placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<button type="submit"
														class="btn btn-lg btn-primary btn-block">Create
														Your Account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck2"> <label
														class="custom-control-label" for="customCheck2">I
														Agree with <a href="#">Term and Conditions</a>
													</label>
												</div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-light">
		<div class="header-top border-bottom bg-white">
			<div class="container">
				<div class="row">
					<div class="col-lg-12"></div>
				</div>
			</div>
		</div>
		<div class="main-nav shadow-sm">
			<!--  Navbar-->
			<jsp:include page="navbar.jsp" />
			<!-- End of NavBar -->
		</div>
		<div class="py-0">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 px-0">
						<header>
							<div id="owl-carousel-one" class="owl-carousel">
								<div class="item">
									<img class="img-fluid mx-auto" src="img/banner/1.png">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto" src="img/banner/2.png">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto" src="img/banner/3.png">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto" src="img/banner/4.png">
								</div>
							</div>
						</header>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="py-5">
		<div class="container">
			<div class="row">
				<div class="col-4">
					<div class="offers-block">
						<a href="#"> <img class="img-fluid" src="img/offer-1.png"
							alt=""></a>
					</div>
				</div>
				<div class="col-4">
					<div class="offers-block">
						<a href="#"><img class="img-fluid mb-3" src="img/offer-3.png"
							alt=""></a>
					</div>
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/offer-4.png"
							alt=""></a>
					</div>
				</div>
				<div class="col-4">
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/offer-2.png"
							alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="product-list pbc-5 pb-4 pt-5 bg-light">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark font-weight-normel">Best Selling
				Items</h4>
			<div class="row">
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-danger">NEW</span> <img src="img/item/1.jpg"
							class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/2.jpg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$ 135.00 <span class="text-black-50"><del>$500.00 </del></span>
								<span
									class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
									50% OFF</span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a class="active" href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-danger">NEW</span> <img src="img/item/3.jpg"
							class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$ 135.00 <span class="text-black-50"><del>$500.00 </del></span>
								<span class="bg-info rounded-sm pl-1 ml-1 pr-1 text-white small">
									50% OFF</span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/4.jpg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<a href="#"> <span class="badge badge-danger">NEW</span> <img
							src="img/item/5.jpg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a class="active" href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/6.jpg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-danger">NEW</span> <img src="img/item/7.jpg"
							class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/8.jpg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Fusion Crop Onion
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
	<section class="offer-product py-5">
		<div class="container">
			<div class="row">
				<div class="col-6">
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/ad/1.png" alt=""></a>
					</div>
				</div>
				<div class="col-6">
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/ad/2.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="product-list pbc-5 pb-4 pt-5 bg-light">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark">Top Savers Today</h4>
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-carousel-category owl-theme">
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-danger">NEW</span> <img src="img/item/1.jpg"
									class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Fusion Crop Onion
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$ 135.00 <span class="text-black-50"><del>$500.00
											</del></span> <span
											class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
											50% OFF</span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-success">50% OFF</span> <img
									src="img/item/2.jpg" class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Fusion Crop Onion
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a class="active" href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-danger">NEW</span> <img src="img/item/3.jpg"
									class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Fusion Crop Onion
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-success">50% OFF</span> <img
									src="img/item/4.jpg" class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Fusion Crop Onion
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-danger">NEW</span> <img src="img/item/5.jpg"
									class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Fusion Crop Onion
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-success">50% OFF</span> <img
									src="img/item/6.jpg" class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Fusion Crop Onion
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<jsp:include page="footer.jsp" />
	<div class="cart-sidebar">
		<div class="cart-sidebar-header">
			<h5>
				My Cart <span class="text-info">(5 item)</span> <a
					data-toggle="offcanvas" class="float-right" href="#"><i
					class="icofont icofont-close-line"></i> </a>
			</h5>
		</div>
		<div class="cart-sidebar-body">
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/1.jpg" alt=""> <span
					class="badge badge-success">50% OFF</span>
				<h5>
					<a href="#">Fusion Crop Onion Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$135.00
					<del class="small text-secondary">$ 500.00 </del>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/2.jpg" alt=""> <span class="badge badge-danger">55%
					OFF</span>
				<h5>
					<a href="#">Fusion Crop Onion Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$250.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-info rounded-sm pl-1 ml-1 pr-1 text-white small">NEW</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/3.jpg" alt=""> <span class="badge badge-info">NEW</span>
				<h5>
					<a href="#">Fusion Crop Onion Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$900.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
						50% OFF</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/4.jpg" alt=""> <span class="badge badge-danger">NEW</span>
				<h5>
					<a href="#">Fusion Crop Onion Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$135.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
						50% OFF</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/5.jpg" alt=""> <span class="badge badge-info">NEW</span>
				<h5>
					<a href="#">Fusion Crop Onion </a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$135.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
						50% OFF</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
		</div>
		<div class="cart-sidebar-footer">
			<div class="cart-store-details">
				<p>
					Sub Total <strong class="float-right">$900.69</strong>
				</p>
				<p>
					Delivery Charges <strong class="float-right text-danger">+
						$29.69</strong>
				</p>
				<h6>
					Your total savings <strong class="float-right text-danger">$55
						(42.31%)</strong>
				</h6>
			</div>
			<a href="checkout.html"><button
					class="btn btn-primary btn-lg btn-block text-left" type="button">
					<span class="float-left"><i class="icofont icofont-cart"></i>
						Proceed to Checkout </span><span class="float-right"><strong>$1200.69</strong>
						<span class="icofont icofont-bubble-right"></span></span>
				</button></a>
		</div>
	</div>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- select2 Js -->
	<script src="vendor/select2/js/select2.min.js"></script>
	<!-- Owl Carousel -->
	<script src="vendor/owl-carousel/owl.carousel.js"></script>
	<!-- Slider Js -->
	<script src="vendor/slider/slider.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="js/custom.js"></script>
</body>

</html>