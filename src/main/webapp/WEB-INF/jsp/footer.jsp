<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!-- Footer -->
   <footer class="bg-white border-bottom border-top">
      <div class="container">
         <div class="row no-gutters">
            <div class="col-md-4">
               <div class="border-right py-5 pr-5">
                  <h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">TOP CATEGORIES</h6>
						<div class="row no-gutters">
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">Vegetable Seeds</a></li>
									<li><a href="#"> Field Crops Seeds </a></li>
									<li><a href="#">Water Soluble fertilisers</a></li>
									<li><a href="#">Organic Products </a></li>
									<li><a href="#">Pesticides</a></li>
								</ul>
							</div>
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#"> Agri Equipments</a></li>
									<li><a href="#">Micronutrients</a></li>
									<li><a href="#">Insecticides</a></li>
									<li><a href="#">Herbicides</a></li>
									<li><a href="#"> Bio Pesticides</a></li>
								</ul>
							</div>
						</div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="border-right py-5 px-5">
                  <h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">ABOUT US</h6>
                  <div class="row no-gutters">
                     <div class="col-6">
                        <ul class="list-unstyled mb-0">
                           <li><a href="#">History</a></li>
                           <li><a href="#">Band of Trust</a></li>
                           <li><a href="#">Brand Guidelines</a></li>
                           <li><a href="#">In the News
                              </a>
                           </li>
                        </ul>
                     </div>
                     <div class="col-6">
                        <ul class="list-unstyled mb-0">
                           <li><a href="/contact-us">Contact Us</a></li>
                           <li><a href="/terms">Terms Of Use</a></li>
                           <li><a href="/seller-terms">Seller Terms & Conditions</a></li>
                           <li><a href="/return-policy">Return Policy</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="py-5 pl-5">
                  <h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">DOWNLOAD APP</h6>
                  <div class="app">
                     <a href="#">
                        <img class="img-fluid" src="img/google.png">
                     </a>
                     <a href="#">
                        <img class="img-fluid" src="img/apple.png">
                     </a>
                  </div>
                  <h6 class="mt-4 mb-4 f-14 text-dark font-weight-bold">KEEP IN TOUCH</h6>
                  <div class="footer-social">
                     <a class="btn-facebook" href="#"><i class="icofont icofont-facebook"></i></a>
                     <a class="btn-twitter" href="#"><i class="icofont icofont-twitter"></i></a>
                     <a class="btn-instagram" href="#"><i class="icofont icofont-instagram"></i></a>
                     <a class="btn-whatsapp" href="#"><i class="icofont icofont-whatsapp"></i></a>
                     <a class="btn-messenger" href="#"><i class="icofont icofont-facebook-messenger"></i></a>
                     <a class="btn-google" href="#"><i class="icofont icofont-google-plus"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /.container -->
   </footer>
   <div class="popular-tag py-5">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">POPULAR
						SEARCHES</h6>
					<p class="mb-0">
						<a href="#">Seeds </a> &nbsp; | &nbsp; 
<a href="#">Flower Seeds </a> &nbsp; | &nbsp; 
<a href="#">Vegetable Seeds </a> &nbsp; | &nbsp; 
<a href="#">Field Crops Seeds </a> &nbsp; | &nbsp; 
<a href="#">Fertilisers </a> &nbsp; | &nbsp; 
<a href="#">Water Soluble fertilisers</a> &nbsp; | &nbsp;  
<a href="#">Organic Products </a> &nbsp; | &nbsp; 
<a href="#">Pesticides </a> &nbsp; | &nbsp; 
<a href="#">Agri Equipments </a> &nbsp; | &nbsp; 
<a href="#">Micronutrients </a> &nbsp; | &nbsp; 
<a href="#">Insecticides </a> &nbsp; | &nbsp; 
<a href="#">Fungicides </a> &nbsp; | &nbsp; 
<a href="#">Herbicides </a> &nbsp; | &nbsp; 
<a href="#">Bio Pesticides </a> &nbsp; | &nbsp; 
<a href="#">Surfactants </a> &nbsp; | &nbsp; 
<a href="#">Organic Premium PGR </a> &nbsp; | 
<a href="#">Organic Coated Granules </a> &nbsp; | &nbsp; 
<a href="#">Organic PGR Technicals </a> &nbsp; | &nbsp; 
<a href="#">Micronutrients Spray </a> &nbsp; | &nbsp; 
<a href="#">Individual Micronutrient </a> &nbsp; | &nbsp; 
<a href="#">Micronutrient Mixture </a> &nbsp; | &nbsp; 
<a href="#">Organic Neem Products </a> &nbsp; | &nbsp; 
<a href="#">Organic Stimulant </a> &nbsp; | &nbsp; 
<a href="#">Starters </a> &nbsp; | &nbsp; 
<a href="#">Pumps </a> &nbsp; | &nbsp; 
<a href="#">Agro Sprayers </a> &nbsp; | &nbsp; 
<a href="#">Agro Shed Nets </a> &nbsp; | &nbsp; 
<a href="#">House Hold Insecticides </a> &nbsp; | &nbsp; 
<a href="#">Drip Irrigation </a> &nbsp; | &nbsp; 
<a href="#">Pulses & Beans </a> &nbsp; | &nbsp; 
<a href="#">Home / kitchen Garden Seeds </a> &nbsp; | &nbsp; 
<a href="#">Mulching Film </a> &nbsp; | &nbsp; 
<a href="#">Cotton Seeds</a> &nbsp;
					</p>
            </div>
         </div>
      </div>
   </div>
   <div class="copyright bg-light py-3">
      <div class="container">
         <div class="row">
            <div class="col-md-6 d-flex align-items-center">
               <p class="mb-0"><i class="icofont icofont-copyright"></i> Copyright 2020 <a href="#">AGROZEP.COM</a> All Rights Reserved
               </p>
            </div>
            <div class="col-md-6 text-right">
               <img class="img-fluid" src="img/payment_methods.png">
            </div>
         </div>
      </div>
   </div>

</body>