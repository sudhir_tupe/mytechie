<!DOCTYPE html>
<html lang="en">
  <head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="AGRODHAN">
<meta name="Keywords"
	content="Online Agro Shopping in India,online Agro Shopping store,Online Agro Shopping Site, Buy Online,Shop Online,Online Shopping,Agrodhan" />
<meta name="Description"
	content="India&#x27;s biggest online agro store for Vegetable Seeds,Field Crops Seeds,Fertilisers,Water Soluble fertilisers,Organic Products,Pesticides,Agri Equipments,Micronutrients,Insecticides,Fungicides,Herbicides,Bio Pesticides,Flower Seeds,Onion Seeds from all brands at the lowest prices in India. Payment options - COD, EMI, Credit card, Debit card &amp;amp; more." />
<meta name="google-site-verification"
	content="3RnLdERPVodJ4P2YohoQWVROxQqbmN8xzF-KRmOpc2k" />
<title>Online Agro Shopping Site for Vegetable Seeds, Agro
	Chemicals &amp; More. Best Offers!</title>


<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
          <link rel="icon" type="image/png" href="img/fav-icon.png">
    
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
      <link href="vendor/icofont/icofont.min.css" rel="stylesheet">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
      <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
    <script src="lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>
  <body class=""> 
  <!--<![endif]-->
    
    
    
    

        
        
<div class="main-nav shadow-sm">
<nav class="navbar navbar-expand-lg navbar-dark pt-0 pb-0" style="background-color:#1f7a1f">
				<div class="container">
				
					<!-- <a class="navbar-brand" href="/">  <img
						src="img/green_bag_logo.png" alt="AGROMAART">
					 -->	
				
					<a class="navbar-brand" href="/">  <img
						src="img/nlogo.png" alt="AGRODHAN">
				
						
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto main-nav-left">
							<!-- <li class="nav-item"><a class="nav-link" href="/"><font color="#ffffff">
							 <i class="icofont-ui-home"></i> 
							</font></a></li> -->
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> 
								<font color="#ffffff">SEEDS</font>
								</a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
													<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Vegetables Seeds</a>
												<a href="/product-grid" title="ONION">ONION</a> <a
													href="/product-grid" title="BHENDI">BHENDI</a> <a
													href="/product-grid" title="TOMATO">TOMATO</a> <a
													href="/product-grid" title="BITTER GOURD">BITTER GOURD</a> <a
													href="/product-grid" title="BOTTLE GOURD">BOTTLE GOURD</a> <a
													href="/product-grid" title="BRINJAL">BRINJAL</a> <a
													href="/product-grid" title="CHILLI">CHILLI</a> <a
													href="/product-grid" title="CUCUMBER">CUCUMBER</a> <a
													href="/product-grid" title="CAULIFLOWER">CAULIFLOWER</a> <a
													href="/product-grid" title="CABBAGE">CABBAGE</a>

											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Root
													Vegetables Seeds</a> <a href="/product-grid" title="POTATO"> POTATO</a> <a
													href="/product-grid" title="CARROT"> CARROT</a> <a
													href="/product-grid" title="RADISH"> RADISH </a> <a
													href="/product-grid" title="BEET ROOT"> BEET ROOT</a> <a
													href="/product-grid" title="GINGER">GINGER</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Leafy
													Vegetables Seeds</a> <a href="/product-grid" title="CORIANDER">CORIANDER</a> <a
													href="product-grid.ht/product-gridl"  title="PALAK">PALAK</a> <a
													href="/product-grid"  title="FENUGREEK">FENUGREEK</a> <a
													href="/product-grid"  title="MINT PUDINA">MINT PUDINA</a> <a
													href="/product-grid"  title="COLOCASIA LEAF">COLOCASIA LEAF</a> <a
													href="/product-grid"  title="CAPSICUM">CAPSICUM</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fibre and
													Oil Seeds</a> <a href="/product-grid" title="COTTON">COTTON</a> <a
													href="/product-grid" title="SUNFLOWER">SUNFLOWER</a> <a
													href="/product-grid" title="SOYABEAN">SOYABEAN</a> <a
													href="/product-grid" title="MUSTARD">MUSTARD</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Cereals
													and Pulses</a> <a href="/product-grid" title="BAJRA"> BAJRA</a> <a
													href="/product-grid" title="JOWAR"> JOWAR</a> <a
													href="/product-grid" title="WHEAT">WHEAT</a> <a
													href="/product-grid" title="PADDY">PADDY</a> <a
													href="/product-grid" title="MAIZE">MAIZE</a> <a
													href="/product-grid" title="GRAM">GRAM</a> <a
													href="/product-grid" title="PIGEON-PEA (TUR)">PIGEON-PEA (TUR)</a> <a
													href="/product-grid" title="GREEN GRAM">GREEN GRAM</a> <a
													href="/product-grid" title="BLACK GRAM">BLACK GRAM</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Gardening Seeds</a>
												<a href="/product-grid" title="LOTUS FLOWER"> LOTUS FLOWER</a> <a
													href="/product-grid" title="FRENCH MARIGOLD"> FRENCH MARIGOLD</a> <a
													href="/product-grid" title="AFRICON MARIGOLD">AFRICON MARIGOLD</a> <a
													href="/product-grid" title="ICE FLOWER">ICE FLOWER</a> <a
													href="/product-grid" title="CHERRY TOMATO">CHERRY TOMATO</a> <a
													href="/product-grid"></a> <a href="/product-grid"></a>
												<a href="/product-grid"></a> <a href="/product-grid"></a>
											</div>
										</div>
									</div>
								</div></li>
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <font color="#ffffff">AGRO CHEMICALS</font></a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Fungicides</a> 
												<a href="/product-grid" title="AMISTAR">AMISTAR</a> <a
													href="/product-grid" title="CAPTAN">CAPTAN</a> <a
													href="/product-grid" title="BAVISTIN">BAVISTIN</a>
												<a href="/product-grid" title="KAVACH">KAVACH</a> <a
													href="/product-grid" title="ACROBAT">ACROBAT</a> <a
													href="/product-grid" title="FIVE STAR">FIVE STAR</a> <a
													href="/product-grid" title="ANTRACOL">ANTRACOL</a> <a
													href="/product-grid" title="AMISTAR TOP">AMISTAR TOP</a> <a
													href="/product-grid" title="AVTAR">AVTAR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Insecticides</a> 
												<a href="/product-grid" title="ACTARA">ACTARA</a> <a
													href="/product-grid" title="AMPLIGO">AMPLIGO</a> <a
													href="/product-grid" title="CHESS">CHESS</a> <a
													href="/product-grid" title="PEGASUS">PEGASUS</a> <a
													href="/product-grid" title="ALIKA">ALIKA</a> <a
													href="/product-grid" title="METADOR">METADOR</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Herbicides</a>
												<a href="/product-grid" title="RIFIT"> RIFIT</a> <a
													href="/product-grid" title="GRAMOXONE">GRAMOXONE</a> <a
													href="/product-grid" title="AXIAL">AXIAL</a> <a
													href="/product-grid" title="RIFIT PLUS">RIFIT PLUS</a> <a
													href="/product-grid" title="FUSIFLEX">FUSIFLEX</a> <a
													href="/product-grid" title="TOPIK">TOPIK</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="/product-grid">Plant Growth Regulator(PGR)</a>
												<a href="/product-grid" title="MODDUS">MODDUS</a> <a
													href="/product-grid" title="PALISADE EC">PALISADE EC</a> <a
													href="/product-grid" title="PRIME+ EC">PRIME+ EC</a>
											</div>
										</div>
									</div>
								</div></li>
								
										<li class="nav-item dropdown"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#ffffff">FERTILIZERS</font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="#">VERMI COMPOST</a>
								</div></li>
								
								<li class="nav-item dropdown"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#ffffff">HARDWARE</font></a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="#" title="WATER MOTOR PUMP">WATER MOTOR PUMP</a> 
									<a class="dropdown-item" href="#" title="STARTERS">STARTERS</a>
									<a class="dropdown-item" href="#" title="SPRAYER PUMP (BATTERY)">SPRAYER PUMP (BATTERY)</a>
									<a class="dropdown-item" href="#" title="SPRINKLER">SPRINKLER</a>  
									<a class="dropdown-item" href="#" title="DRIP IRRIGATION">DRIP IRRIGATION</a> 
									<a class="dropdown-item" href="#" title="PIPE">PIPE</a>
									<a class="dropdown-item" href="#" title="MILK CANS">MILK CANS</a>
									<a class="dropdown-item" href="#" title="LED TORCH">LED TORCH</a>
								</div></li>
						</ul>
						<form class="form-inline my-2 my-lg-0 top-search">
							<button class="btn-link" type="submit">
								<i class="icofont-search"></i>
							</button>
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search for products, brands.."
								aria-label="Search">
						</form>
						
                <ul class="navbar-nav ml-auto profile-nav-right" id="profileId">
                  </ul>
					</div>
				</div>
			</nav>


			</div>
            <div class="content">
        <div class="header">
        <br>
            <h3 class="text-center">Terms and Conditions</h3>
        </div>
                <ul class="breadcrumb">
            <li>Welcome to <a href="/"> <strong>agrodhan.com</strong></a>.  Agrodhan.com is a platform, where agricultural product sellers and seed manufactures who has Government License can advertise to promote and sell their agriculture input authorized products, thus we (agrodhan.com) act as a bridge between sellers and buyers for agricultural input products. agrodhan.com's mission is to help farmers of India by providing reliable agriculture products at a reasonable rate.</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
<h5>Conditions Of Use</h5>
These Terms and Conditions constitute an agreement between you and agrodhan.com <br><br>
<h5>1. Your Account</h5>
If you use the website, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer to prevent unauthorised access to your account. You agree to accept responsibility for all activities that occur under your account or password. You should take all necessary steps to ensure that the password is kept confidential and secure and should inform us immediately if you have any reason to believe that your password has become known to anyone else, or if the password is being, or is likely to be, used in an unauthorised manner. Please ensure that the details you provide us with are correct and complete and inform us immediately of any changes to the information that you provided when registering. You can access and update much of the information you provided us with in the Your Account area of the website. You agree and acknowledge that you will use your account on the website to purchase products only for your personal use and not for business purposes. Agrodhan.com reserves the right to refuse access to the website, terminate accounts, remove or edit content at any time without notice to you.<br><br>

<h5>2. Privacy</h5>
Please review our Privacy Notice, which also governs your visit to Agrodhan.com, to understand our practices. The personal information / data provided to us by you during the course of usage of Agrodhan.com will be treated as strictly confidential and in accordance with the Privacy Notice and applicable laws and regulations. If you object to your information being transferred or used, please do not use the website. <br><br>
<h5>3. E-Platform</h5>
You agree, understand and acknowledge that the website is an online platform that enables you to purchase products listed on the website at the price indicated therein at any time from any location. You further agree and acknowledge that Agrodhan is only a facilitator and is not and cannot be a party to or control in any manner any transactions on the website. Accordingly, the contract of sale of products on the website shall be a strictly bipartite contract between you and the sellers on agrodhan.com <br><br>
<h5>4. Access to agrodhan.com</h5>
We will do our utmost to ensure that availability of the website will be uninterrupted and that transmissions will be error-free. However, due to the nature of the Internet, this cannot be guaranteed. Also, your access to the website may also be occasionally suspended or restricted to allow for repairs, maintenance, or the introduction of new facilities or services at any time without prior notice. We will attempt to limit the frequency and duration of any such suspension or restriction. <br><br>
<h5>5. Losses</h5>
We will not be responsible for any business loss (including loss of profits, revenue, contracts, anticipated savings, data, goodwill or wasted expenditure) or any other indirect or consequential loss that is not reasonably foreseeable to both you and us when you commenced using the website. <br><br>
<strong>
The seeds germination depends on the soil, atmosphere, fertilizers, watering etc. Agrodhan never claims about seeds germination and harvesting and so refund is not possible in seeds.<br><br>
</strong>

<strong>
No Warranty on Live Plants as Growing, flowering and fruiting are subject to So many climatic condition like Air, Kind of soil, water, humidity and temperature beyond our control.<br><br>
</strong>

<h5>6. Disclaimer</h5>
You acknowledge and undertake that you are accessing the services on the website and transacting at your own risk and are using your best and prudent judgment before entering into any transactions through the website. You further acknowledge and undertake that you will use the website to order products only for your personal use and not for business purposes. We shall neither be liable nor responsible for any actions or inactions of sellers nor any breach of conditions, representations or warranties by the sellers or manufacturers of the products and hereby expressly disclaim and any all responsibility and liability in that regard. We shall not mediate or resolve any dispute or disagreement between you and the sellers or manufacturers of the products. Conditions of Sale (between Sellers and the Customer) Please read these conditions carefully before placing an order for any products with the Sellers ("We" or "Our" or "Us", wherever applicable) on the Agrodhan.com (the website). These conditions signify your agreement to be bound by these conditions. <br><br>
<h5>7. Returns</h5>
Please review our Returns Policy, which applies to products sold by us. <br><br>
<h5>8. Pricing and availability</h5>
We list availability information for products sold by us on the website, including on each product information page. Beyond what we say on that page or otherwise on the website, we cannot be more specific about availability. Please note that dispatch estimates are just that. They are not guaranteed dispatch times and should not be relied upon as such. As we process your order, you will be informed by e-mail if any products you order turn out to be unavailable. For more details, please review our Pricing policy and our Availability Guide, both of which apply to products ordered from us. All prices are inclusive of VAT/CST, service tax, Goods and Services Tax ("GST"), duties and cesses as applicable - unless stated otherwise.<br><br>

<h5>9. Taxes</h5>
You shall be responsible for payment of all fees/costs/charges associated with the purchase of products from us and you agree to bear any and all applicable taxes including but not limited to VAT/CST, service tax, GST, duties and cesses etc. <br><br>
            </div>
        </div>
    </div>

<!-- Footer -->
   <footer class="bg-white border-bottom border-top">
      <div class="container">
      </div>
      <!-- /.container -->
   </footer>
   <div class="copyright bg-light py-3">
      <div class="container">
         <div class="row">
            <div class="col-md-6 d-flex align-items-center">
               <p class="mb-0"><i class="icofont icofont-copyright"></i> Copyright 2020 <a href="#">AGRODHAN.COM </a> All Rights Reserved.
               </p>
            </div>
            <div class="col-md-6 text-right">
               <img class="img-fluid" src="img/payment_methods.png">
            </div>
         </div>
      </div>
   </div>

    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>