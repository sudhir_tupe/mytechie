<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html xmlns:th="http://www.thymeleaf.org">
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="TECHIES">
<meta name="author" content="TECHIES">
<title>AGRODHAN</title>
<!-- Favicon Icon -->
<link href="img/favicon.png" type="image/png" rel="icon">

<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Select2 CSS -->
<link href="vendor/select2/css/select2-bootstrap.css" />
<link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
<!-- Font Awesome-->
<link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
<link href="vendor/icofont/icofont.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="edit-profile-modal" tabindex="-1"
		role="dialog" aria-labelledby="edit-profile" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="edit-profile">Edit profile</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-row">
							<div class="form-group col-md-12">
								<label>Phone number </label> <input type="text"
									value="+91 7709462647" class="form-control"
									placeholder="Enter Phone number">
							</div>
							<div class="form-group col-md-12">
								<label>Email id </label> <input type="text"
									value="agromart@gmail.com" class="form-control"
									placeholder="Enter Email id
                              ">
							</div>
							<div class="form-group col-md-12 mb-0">
								<label>Password </label> <input type="password"
									value="**********" class="form-control"
									placeholder="Enter password
                              ">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-outline-primary"
						data-dismiss="modal">CANCEL</button>
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-primary">UPDATE</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Add Address Modal -->
	<div class="modal fade" id="add-address-modal" tabindex="-1"
		role="dialog" aria-labelledby="add-address" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="add-address">Add Address</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="inputPassword4">Complete Address </label> <input
									type="text" class="form-control" id="addressLine1"
									placeholder="Complete Address e.g. house number, street name, landmark">
							</div>
							
							<div class="form-group col-md-6">
								<label class="control-label">Zip Code <span
									class="required">*</span></label> 
									 <div class="input-group">
									 
                              <input type="text" class="form-control" id="pincodeTextBox" placeholder="Pin Code">
                              <div class="input-group-append">
                                 <button class="btn btn-outline-secondary" type="button" id="zipCodeBtn"><i class="icofont-ui-pointer"></i></button>
                              </div>
                           </div>
							</div>

							<div class="form-group col-md-6">
								<label>Name<span class="required">*</span></label>
								<select id= "selectVillage" class="select2 form-control border-form-control">
									<option value="">Select Name</option>
									<option value="">NA</option>
								</select>
							</div>

							<div class="form-group col-md-6">
								<label>Block/Taluka <span class="required">*</span></label> <select
									class="select2 form-control border-form-control" id="selectBlock">
									<option value="">Select Block </option>
								</select>
							</div>

							<div class="form-group col-md-6">
								<label>District <span class="required">*</span></label> <select
									class="select2 form-control border-form-control" id="selectDistrict">
									<option value="">Select District</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label>State <span class="required">*</span></label> <select id="selectState"
									class="select2 form-control border-form-control">
									<option value="">Select State</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label>Country <span class="required">*</span></label> <select
									class="select2 form-control border-form-control">
									<option value="IN">INDIA</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-outline-primary"
						data-dismiss="modal">CANCEL</button>
					<button type="button" id="addAddressBtn"
						class="btn d-flex w-50 text-center justify-content-center btn-primary">Add</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- End of Add Address -->
	
	
	
	
	<!-- Modal -->
	<div class="modal fade" id="delete-address-modal" tabindex="-1"
		role="dialog" aria-labelledby="delete-address" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="delete-address">Add New Company</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="mb-0 text-black">Are you sure you want to add this
						new company?</p>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn d-flex w-50 text-center justify-content-center btn-outline-primary"
						data-dismiss="modal">CANCEL</button>
					<button type="button" id="submitBtn"
						class="btn d-flex w-50 text-center justify-content-center btn-primary">SUBMIT</button>
				</div>
				<div id="successDiv" class="alert alert-success">Company
					Registered successfully</div>
				<div id="errorDiv" class="hide"></div>

			</div>
		</div>
	</div>
	<div class="bg-light shadow-sm">
		<!-- <div class="header-top border-bottom bg-white">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item border-right border-left py-1 pr-2 mr-2 pl-2">
                           <a href=""><i class="icofont icofont-iphone"></i> +1-123-456-7890</a>
                        </li>
                        <li class="list-inline-item border-right py-1 pr-2 mr-2">
                           <a href="contact-us.html"><i class="icofont icofont-headphone-alt"></i> Contact Us</a>
                        </li>
                        <li class="list-inline-item">
                           <span>Download App</span> &nbsp;
                           <a href="#"><i class="icofont icofont-brand-windows"></i></a>
                           <a href="#"><i class="icofont icofont-brand-apple"></i></a>
                           <a href="#"><i class="icofont icofont-brand-android-robot"></i></a>
                        </li>
                     </ul>
                     <p class="mb-0 py-1">FREE CASH ON DELIVERY &amp; SHIPPING AVAILABLE OVER <span class="text-danger font-weight-bold">$499</span></p>
                  </div>
               </div>
            </div> -->
	</div>
	<div class="main-nav shadow-sm">

		<jsp:include page="navbar-admin.jsp" />


	</div>
	</div>
	<section class="py-5 account-page bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div
						class="osahan-account-page-left overflow-hidden shadow-sm rounded bg-white h-100">
						<div class="p-4">
							<div class="osahan-user text-center">
								<div class="osahan-user-media">
									<img class="mb-3 rounded-pill shadow-sm mt-1"
										src="img/user/admin.png" alt="TECHIES">
									<div class="osahan-user-media-body">
										<h6 class="mb-2 font-weight-bold">Agromart Admin</h6>
										<p class="mb-1">+91 7709462647</p>
										<p>agromart@gmail.com</p>
										<p class="mb-0 font-weight-bold">
											<a class="btn btn-outline-info btn-sm" data-toggle="modal"
												data-target="#edit-profile-modal" href="#"><i
												class="icofont-ui-edit"></i> EDIT</a>
										</p>
									</div>
								</div>
							</div>
						</div>
						<ul class="nav nav-tabs flex-column border-0" id="myTab"
							role="tablist">

							<li class="nav-item"><a class="nav-link"
								id="new-company-tab" data-toggle="tab" href="#new-company"
								role="tab" aria-controls="new-company" aria-selected="true"><i
									class="icofont-industries-4"></i>Add New Company</a></li>
							<li class="nav-item"><a class="nav-link" id="new-dealer-tab"
								data-toggle="tab" href="#new-dealer" role="tab"
								aria-controls="new-dealer" aria-selected="false"><i
									class="icofont-business-man"></i> Add New Dealer</a></li>
						
								<li class="nav-item"><a class="nav-link"
								id="add-new-product-tab" data-toggle="tab"
								href="#add-new-product" role="tab"
								aria-controls="add-new-product" aria-selected="false">
								<i class="icofont-cart"></i>
								Add New Product</a></li>			
									
							<li class="nav-item"><a class="nav-link"
								id="company-list-tab" data-toggle="tab" href="#company-list"
								role="tab" aria-controls="company-list" aria-selected="false"><i
									class="icofont-list"></i> Company List</a></li>
							<li class="nav-item"><a class="nav-link"
								id="dealer-list-tab" data-toggle="tab" href="#dealer-list"
								role="tab" aria-controls="dealer-list" aria-selected="false"><i
									class="icofont-list"></i> Dealer List</a></li>

							<li class="nav-item"><a class="nav-link"
								id="assign-dealer-tab" data-toggle="tab" href="#assign-dealer"
								role="tab" aria-controls="assign-dealer" aria-selected="false"><i
									class="icofont-users"></i>Assign Dealer</a></li>

<li class="nav-item"><a class="nav-link"
								id="product-catalog-tab" data-toggle="tab"
								href="#product-catalog" role="tab"
								aria-controls="product-catalog" aria-selected="false"><i
									class="icofont-leaf"></i>Product  Catalog</a></li>


							<li class="nav-item"><a class="nav-link Logout"><i
									class="icofont-logout"></i> Logout</a></li>

						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div
						class="osahan-account-page-right rounded shadow-sm bg-white p-4 h-100">
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade" id="new-company" role="tabpanel"
								aria-labelledby="new-company-tab">
								<h4 class="text-dark mt-0 mb-4">Add New Company</h4>
								<p></p>
								<form:form modelAttribute="companyDTO" id="frm"
									action="/newcompany" enctype="multipart/form-data">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company Name <span
													class="required">*</span></label>

												<form:input type="text" path="companyName"
													class="form-control border-form-control" id="companyName"
													placeholder="Company Name" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company Email <span
													class="required">*</span></label>

												<form:input type="email" path="companyEmailId"
													name="companyEmailId"
													class="form-control border-form-control"
													id="companyEmailId" placeholder="Company Email" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">GST Number <span
													class="required">*</span></label>
												<!-- <input
													class="form-control border-form-control" value=""
													placeholder="123 456 7890" type="number"> -->

												<form:input type="text" path="companyGSTNumber"
													class="form-control border-form-control"
													id="companyGSTNumber" placeholder="GST Number" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">PAN Number <span
													class="required">*</span></label>
												<form:input type="email" path="companyPanNumber"
													class="form-control  border-form-control"
													id="companyPanNumber" placeholder="PAN Number" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company License Number
													<span class="required">*</span>
												</label>
												<form:input type="email" path="companyLicenseNo"
													class="form-control border-form-control"
													id="companyLicenseNo" placeholder="License Number" />
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company License Valid
													Upto<span class="required">*</span>
												</label>
												<form:input type="text" id="companyLicenseExpiryDate"
													value="License Validity" path="companyLicenseExpiryDate"
													class="form-control border-form-control" />

											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Company Auth Person
													Name <span class="required">*</span>
												</label>
												<form:input type="email" path="companyAuthPersonName"
													class="form-control border-form-control"
													id="companyAuthPersonName" placeholder="Auth Person Name" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Authorised Person No<span
													class="required">*</span>
												</label>
												<form:input type="text" path="contactNumber"
													class="form-control border-form-control" id="contactNumber"
													placeholder="Authorised Person No" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Company Registered
													Address <span class="required">*</span>
												</label>
												<form:textarea path="companyRegAddress"
													class="form-control border-form-control"
													id="companyRegAddress"
													placeholder="Company Registered Address"></form:textarea>

												<a id="companyRegisteredAddressBtn" data-toggle="modal" data-target="#add-address-modal"
													href="#"><font color="#2ECC71"> <i
														class="icofont-plus-circle"></i>Add Registered Address
												</font> </a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Company Storage Address
													<span class="required">*</span>
												</label>

												<form:textarea path="companyStorageAddress"
													class="form-control border-form-control"
													id="companyStorageAddress"
													placeholder="Company Storage Address"></form:textarea>

												<a id="companyStorageAddressBtn" data-toggle="modal" data-target="#add-address-modal"
													href="#"><font color="#2ECC71"> <i
														class="icofont-plus-circle"></i>Add Storage Address
												</font> </a>



											</div>
										</div>
									</div>
									<form:hidden path="principalCertificateFileName"
										id="principalCertificateFileName" />
								</form:form>

								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label">Upload Principal
												Certificate <span class="required">*</span>
											</label>
											<form method="POST" enctype="multipart/form-data"
												id="uploadForm">

												<input type="file" name="file" />
												<button type="submit" class="btn btn-secondary"
													id="uploadBtn">Upload</button>
											</form>
										</div>
									</div>
								</div>

								<div id="uploadSuccessDiv" class="alert alert-success"></div>
								<div id="uploadErrorDiv" class="hide"></div>

								<div class="row">
									<div class="col-sm-12 text-right">
										<button type="reset" class="btn btn-secondary">
											Cancel</button>
										<button type="button" class="btn btn-primary"
											data-toggle="modal" data-target="#delete-address-modal">
											Save Changes</button>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="new-dealer" role="tabpanel"
								aria-labelledby="new-dealer-tab">
								<h4 class="text-dark mt-0 mb-4">Add New Dealer</h4>
								<p></p>
								<form:form modelAttribute="dealerDTO" id="dealerForm"
									action="/newdealer" enctype="multipart/form-data">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer Name <span
													class="required">*</span></label>

												<form:input type="text" path="dealerName"
													class="form-control border-form-control" id="dealerName"
													placeholder="Dealer Name" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer Email <span
													class="required">*</span></label>

												<form:input type="email" path="dealerEmail"
													class="form-control border-form-control"
													id="dealerEmail" placeholder="Dealer Email" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">GST Number <span
													class="required">*</span></label>
												<form:input type="text" path="dealerGSTNumber"
													class="form-control border-form-control"
													id="dealerGSTNumber" placeholder="GST Number" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">PAN Number <span
													class="required">*</span></label>
												<form:input type="email" path="dealerPanNumber"
													class="form-control  border-form-control"
													id="dealerPanNumber" placeholder="PAN Number" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer License Number <span
													class="required">*</span>
												</label>
												<form:input type="email" path="dealerLicenseNumber"
													class="form-control border-form-control"
													id="dealerLicenseNumber"
													placeholder="Dealer License Number" />
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Dealer License Valid
													Upto<span class="required">*</span>
												</label>
												<form:input type="text" id="dealerLicenseExpiryDate"
													value="License Validity" path="dealerLicenseExpiryDate"
													class="form-control border-form-control" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Proprietor Name <span
													class="required">*</span>
												</label>
												<form:input type="email" path="proprietorName"
													class="form-control border-form-control"
													id="proprietorName" placeholder="Proprietor Name" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Contact No<span
													class="required">*</span>
												</label>
												<form:input type="text" path="dealerContactNumber"
													class="form-control border-form-control"
													id="dealerContactNumber"
													placeholder="Delaer Contact Number" />

											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Dealer Registered
													Address <span class="required">*</span>
												</label>
												<form:textarea path="registerOfficeAddress"
													class="form-control border-form-control"
													id="registerOfficeAddress"
													placeholder="Dealer Registered Address"></form:textarea>

												<a id="dealerRegisteredAddressBtn" data-toggle="modal" data-target="#add-address-modal"
													href="#"><font color="#2ECC71"> <i
														class="icofont-plus-circle"></i>Add Registered Address
												</font> </a>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">Dealer Storage Address
													<span class="required">*</span>
												</label>

												<form:textarea path="storageAddress"
													class="form-control border-form-control"
													id="storageAddress" placeholder="Dealer Storage Address"></form:textarea>
												<a id="dealerStorageAddressBtn" data-toggle="modal" data-target="#add-address-modal"
													href="#"><font color="#2ECC71"> <i
														class="icofont-plus-circle"></i>Add Storage Address
												</font> </a>
											</div>
										</div>
									</div>
								</form:form>
								<div class="row">
									<div class="col-sm-12 text-right">
										<button type="button" class="btn btn-secondary">
											Cancel</button>
										<button type="button" id="dealerSubmitBtn"
											class="btn btn-primary">Save Changes</button>
										<div id="dealerSuccessDiv" class="alert alert-success">Dealer
											Registered successfully</div>
										<div id="dealerErrorDiv" class="hide"></div>
									</div>
								</div>
							</div>

							<!--  Company List -->

							<div class="tab-pane fade" id="company-list" role="tabpanel"
								aria-labelledby="company-list-tab">
								<h4 class="text-dark mt-0 mb-4">Company List</h4>

								<div align="right">
									<button id="btn-download-comapnies" class="btn btn-success">
										<i class="fa fa-download"></i> Excel
									</button>&nbsp;&nbsp;
									<br>
									<br>
								</div>
								<div class="order-list-tabel-main table-responsive">
									<table
										class="datatabel table-striped table display responsive nowrap"
										width="100%">
										<thead>
											<tr>
												<th>Company Name</th>
												<th>Company Email</th>
												<th>Company Auth Person</th>
												<th>Contact Number</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${companyDTOs}" var="cdto">
												<tr>
													<td><c:out value="${cdto.companyName}"></c:out></td>
													<td><c:out value="${cdto.companyEmailId}"></c:out></td>
													<td><c:out value="${cdto.companyAuthPersonName}"></c:out>
													</td>
													<td><c:out value="${cdto.contactNumber}"></c:out></td>
													<td><a data-toggle="tooltip" data-placement="top"
														title="" href="#" data-original-title="View Detail"
														class="btn btn-info btn-sm"><i class="icofont-eye-alt"></i></a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<!--  End Company List -->

							<!--  Dealer List -->





							<div class="tab-pane fade show active" id="dealer-list"
								role="tabpanel" aria-labelledby="dealer-list-tab">
								<div class="order-list-tabel-main table-responsive">
									<h4 class="text-dark mt-0 mb-4">Dealer List</h4>
									<div align="right">
										<button id="btn-download-dealers" class="btn btn-success">
											<i class="fa fa-download"></i> Excel 
										</button>&nbsp;&nbsp;
										<br>
										<br>
									</div>
									<table
										class="datatabel table table-striped display responsive nowrap"
										width="100%">
										<thead>
											<tr>
												<th>Dealer Name</th>
												<th>Dealer Email</th>
												<th>Contact No</th>
												<th>Proprietor Name</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${dealerDTOs}" var="ddto">
												<tr>
													<td><c:out value="${ddto.dealerName}"></c:out></td>
													<td><c:out value="${ddto.dealerEmail}"></c:out></td>
													<td><c:out value="${ddto.dealerContactNumber}"></c:out>
													</td>
													<td><c:out value="${ddto.proprietorName}"></c:out></td>
													<td><a data-toggle="tooltip" data-placement="top"
														title="" href="#" data-original-title="View Detail"
														class="btn btn-info btn-sm"><i class="icofont-eye-alt"></i></a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<!--  Dealer List -->

							<div class="tab-pane fade" id="assign-dealer" role="tabpanel"
								aria-labelledby="assign-dealer-tab">
								<h4 class="text-dark mt-0 mb-4">Assign Dealer</h4>
								<p></p>
								<form:form modelAttribute="companyDealerDTO"
									id="companyDealerForm" action="/addCompanyDealer">
									<table>
										<tr>
											<td><label class="control-label">Company Name <span
													class="required">*</span></label></td>
											<td><form:select path="companyId" class="form-control"
													id="companySelect">
													<option value="">-- Select Company --</option>
													<c:forEach items="${companyDTOs}" var="companyDTO">
														<form:option value="${companyDTO.companyId}">
															<c:out value="${companyDTO.companyName}" />
														</form:option>
													</c:forEach>
												</form:select></td>
										</tr>
										<tr>
											<td><br> <label class="control-label">Dealer
													Name <span class="required">*</span>
											</label></td>

											<td><br> <form:select path="dealerId"
													class="form-control" id="dealerSelect">
													<option value="">-- Select Dealer --</option>
													<c:forEach items="${dealerDTOs}" var="dealerDTO">
														<form:option value="${dealerDTO.dealerId}">
															<c:out value="${dealerDTO.dealerName}" />
														</form:option>
													</c:forEach>
												</form:select></td>
										</tr>

										<tr>
											<td><br>
												<button type="button" class="btn btn-outline-secondary">
													Cancel</button></td>

											<td><br>
												<button type="button" id="assignDealerSubmitBtn"
													class="btn btn-primary">Assign</button></td>
										</tr>

									</table>
								</form:form>
								<div id="assignDealerSuccessDiv" class="alert alert-success">Dealer
									Assigned successfully</div>
								<div id="assignDealerErrorDiv" class="hide"></div>
								<div align="right">
									<button id="btn-download-companyDealers"
										class="btn btn-success">
										<i class="fa fa-download"></i> Excel
									</button>&nbsp;&nbsp;
									<br>
									<br>
								</div>

								<div class="order-list-tabel-main table-responsive">
									<table
										class="datatabel table table-striped display responsive nowrap"
										width="100%">
										<thead>
											<tr>
												<th>Company Name</th>
												<th>Dealer Name</th>
												<th>Assignment Date</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${companyDealerDTOs}" var="cddto">
												<tr>
													<td><c:out value="${cddto.companyName}"></c:out></td>
													<td><c:out value="${cddto.dealerName}"></c:out></td>
													<td><c:out value="${cddto.creationDate}"></c:out></td>
													<td><c:out value="${cddto.dealerStatus}"></c:out></td>
													<td><a data-toggle="tooltip" data-placement="top"
														title="" href="#" data-original-title="View Detail"
														class="btn btn-info btn-sm"><i class="icofont-eye-alt"></i></a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							
							<!--  ADD NEW PRODUCT-->
							<div class="tab-pane fade" id="add-new-product" role="tabpanel"
								aria-labelledby="add-new-product-tab">
								<h4 class="text-dark mt-0 mb-4">Add New Product</h4>
								
								<form:form modelAttribute="productDTO" id="addNewProductForm"
									action="/addNewProduct" enctype="multipart/form-data">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Name <span
													class="required">*</span></label>

												<form:input type="text" path="productName"
													class="form-control border-form-control" id="productName"
													placeholder="Product Name" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Packaging Unit<span
													class="required">*</span></label>

												<form:input type="email" path="packagingGram"
													class="form-control border-form-control"
													placeholder="Product Packaging Unit" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Price<span
													class="required">*</span></label>
												<form:input type="text" path="price"
													class="form-control border-form-control"
													id="price" placeholder="Product Price" />

											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Avl Item Count<span
													class="required">*</span></label>
												<form:input type="number" path="availableItemCount" id="availableItemCount"
													class="form-control  border-form-control"
													placeholder="Available Count" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Category<span
													class="required">*</span>
												</label>
												
												<form:select class="form-control"
													id="addNewProductCategorySelect" path="category">
													<form:option value="">-- Select Product Category --</form:option>
													<form:option value="VEGGIE_CROPS">VEGGIE_CROPS</form:option>
													<form:option value="AGRO_CHEMICAL">AGRO_CHEMICAL</form:option>
													<form:option value="FERTILIZER">FERTILIZER</form:option>
													<form:option value="AGRO_HARDWARE">AGRO_HARDWARE</form:option>
												</form:select>
												
												<%-- <form:input type="text" path="category"
													class="form-control border-form-control"
													id="productCategory"
													placeholder="Product Category " /> --%>
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Image Path<span
													class="required">*</span>
												</label>
												<form:input type="text" path="productImgPath"
													class="form-control border-form-control"
													id="productImgPath"
													placeholder="img/item/test.png" />
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
										<%-- 	<div class="form-group">
												<label class="control-label">Product Company Id<span
													class="required">*</span>
												</label>
												<form:input type="email" path="productCompanyId"
													class="form-control border-form-control"
													id="productCompanyId" placeholder="Company Id" />
											</div> --%>
											<div class="form-group">
												<label class="control-label">Product Dealer Name<span
													class="required"></span>
												</label>
												<form:select path="productDealerId"
													class="form-control" id="productDealerSelect">
													<option value="">-- Select Dealer --</option>
													<c:forEach items="${dealerDTOs}" var="dealerDTO">
														<form:option value="${dealerDTO.dealerId}">
															<c:out value="${dealerDTO.dealerName}" />
														</form:option>
													</c:forEach>
												</form:select>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Company Name<span
													class="required">*</span>
												</label>
											<form:select path="productCompanyName" class="form-control"
													id="productCompanyNameSelect">
													<option value="">-- Select Company --</option>
													<c:forEach items="${companyDTOs}" var="companyDTO">
														<form:option value="${companyDTO.companyId}">
															<c:out value="${companyDTO.companyName}" />
														</form:option>
													</c:forEach>
												</form:select>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Tag<span
													class="required">*</span>
												</label>
												<form:input type="text" path="productTag"
													class="form-control border-form-control"
													id="productTag" placeholder="Product Tag" />
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">Product Description<span
													class="required">*</span>
												</label>
												<form:input type="text" path="description"
													class="form-control border-form-control"
													id="productDescriptionId"
													placeholder="Product Description" />
											</div>
										</div>
									</div>
								</form:form>
								
								<div class="row">
									<div class="form-group">
									<table><tr>
									<td>
										<label class="control-label">Upload Product
												Image <span class="required">*</span>
											</label>
												<form method="POST" enctype="multipart/form-data"
													id="addProductForm">
													<input type="file" name="file" />
												</form> 	
												</td>
												<td>
												<button type="submit" class="btn btn-secondary"
														id="uploadNewProductBtn">Upload</button>
														
														</td></tr></table>
										</div>
									</div>
								
								<button type="reset" class="btn btn-outline-success"
									id="addNewProductCancelBtn">CANCEL</button>

								<button type="submit" class="btn btn-success"
									id="addNewProductBtn">ADD PRODUCT</button>
								<br> <br>
								<div id="addProductSuccessDiv" class="alert alert-success hide"></div>
								<div id="addProductFailureDiv" class="alert alert-danger hide"></div>
								<br>
<br>
							</div>

							<!-- Product Catalog -->
							<div class="tab-pane fade" id="product-catalog" role="tabpanel"
								aria-labelledby="product-catalog-tab">
								<h4 class="text-dark mt-0 mb-4">Product Catalog</h4>

								<div align="left">
									<table>
										<tr>
										
										<label class="control-label">Upload Product
												CSV <span class="required">*</span>
											</label>
										
											<td>
												<form method="POST" enctype="multipart/form-data"
													id="uploadProductForm">
													<input type="file" name="file" />
												</form></td>
										</tr>
										<tr>
										<td><button type="submit" class="btn btn-primary"
														id="uploadProductBtn">Add Products</button></td>
										</tr>
									</table>
								</div>
								<div id="uploadProductSuccessDiv" class="alert alert-success">Products added successfully.</div>
								<div id="uploadProductErrorDiv" class="hide"></div>
								
								<br>
<br>
								<div class="order-list-tabel-main table-responsive">
									<table
										class="datatabel table table-striped display responsive nowrap"
										width="100%">
										<thead>
											<tr>
												<th>Product Name</th>
												<th>AVL Count</th>
												<th>Company Name</th>
												<th>Packaging/Gram</th>
												<th>Price</th>
												<th>Category</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${productList}" var="product">
												<tr>
													<td><c:out value="${product.productName}"></c:out></td>
													<td><c:out value="${product.availableItemCount}"></c:out></td>
													<td><c:out value="${product.productCompanyName}"></c:out></td>
													<td><c:out value="${product.packagingGram}"></c:out></td>
													<td><i class="icofont-rupee"></i><c:out value="${product.price}"></c:out></td>
													<td><c:out value="${product.category}"></c:out></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<!--  End <!-- Product Catalog -->


						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer -->
	<jsp:include page="footer-admin.jsp" />
	<div class="cart-sidebar">
		<div class="cart-sidebar-header">
			<h5>
				My Cart <span class="text-info">(5 item)</span> <a
					data-toggle="offcanvas" class="float-right" href="#"><i
					class="icofont icofont-close-line"></i> </a>
			</h5>
		</div>
		<div class="cart-sidebar-body">
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/1.jpg" alt=""> <span
					class="badge badge-success">50% OFF</span>
				<h5>
					<a href="#">Floret Printed Ivory Skater Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$135.00
					<del class="small text-secondary">$ 500.00 </del>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/2.jpg" alt=""> <span class="badge badge-danger">55%
					OFF</span>
				<h5>
					<a href="#">Floret Printed Ivory Skater Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$250.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-info rounded-sm pl-1 ml-1 pr-1 text-white small">NEW</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/3.jpg" alt=""> <span class="badge badge-info">NEW</span>
				<h5>
					<a href="#">Floret Printed Ivory Skater Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$900.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
						50% OFF</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/4.jpg" alt=""> <span class="badge badge-danger">NEW</span>
				<h5>
					<a href="#">Floret Printed Ivory Skater Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$135.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
						50% OFF</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
			<div class="cart-list-product">
				<a class="float-right remove-cart" href="#"><i
					class="icofont icofont-close-circled"></i></a> <img class="img-fluid"
					src="img/item/5.jpg" alt=""> <span class="badge badge-info">NEW</span>
				<h5>
					<a href="#">Floret Printed Ivory Skater Dress</a>
				</h5>
				<div class="stars-rating">
					<i class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star active"></i><i
						class="icofont icofont-star"></i> <span>613</span>
				</div>
				<p class="f-14 mb-0 text-dark float-right">
					$135.00
					<del class="small text-secondary">$ 500.00 </del>
					<span class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
						50% OFF</span>
				</p>
				<span class="count-number float-left">
					<button class="btn btn-outline-secondary  btn-sm left dec">
						<i class="icofont-minus"></i>
					</button> <input class="count-number-input" type="text" value="1"
					readonly="">
					<button class="btn btn-outline-secondary btn-sm right inc">
						<i class="icofont-plus"></i>
					</button>
				</span>
			</div>
		</div>
		<div class="cart-sidebar-footer">
			<div class="cart-store-details">
				<p>
					Sub Total <strong class="float-right">$900.69</strong>
				</p>
				<p>
					Delivery Charges <strong class="float-right text-danger">+
						$29.69</strong>
				</p>
				<h6>
					Your total savings <strong class="float-right text-danger">$55
						(42.31%)</strong>
				</h6>
			</div>
			<a href="checkout.html"><button
					class="btn btn-primary btn-lg btn-block text-left" type="button">
					<span class="float-left"><i class="icofont icofont-cart"></i>
						Proceed to Checkout </span><span class="float-right"><strong>$1200.69</strong>
						<span class="icofont icofont-bubble-right"></span></span>
				</button></a>
		</div>
	</div>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- select2 Js -->
	<script src="vendor/select2/js/select2.min.js"></script>
	<!-- Owl Carousel -->
	<script src="vendor/owl-carousel/owl.carousel.js"></script>
	<!-- Data Tables -->
	<link href="vendor/datatables/datatables.min.css" rel="stylesheet" />
	<script src="vendor/datatables/datatables.min.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="js/custom.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		(function($) {
			$("#uploadSuccessDiv").hide();
			$("#uploadErrorDiv").hide();


			$("#addProductSuccessDiv").hide();
			$("#addProductFailureDiv").hide();
			

			
			
			
			$("#uploadProductSuccessDiv").hide();
			$("#uploadProductErrorDiv").hide();

			$("#assignDealerSuccessDiv").hide();
			$("#assignDealerErrorDiv").hide();

			$("#dealerSuccessDiv").hide();
			$("#dealerErrorDiv").hide();

			$("#successDiv").hide();
			$("#errorDiv").hide();

			$("#successDiv").hide();
			$("#errorDiv").hide();

			$("#companyLicenseExpiryDate").datepicker({
				dateFormat : "yy-mm-dd"
			});

			$("#dealerLicenseExpiryDate").datepicker({
				dateFormat : "yy-mm-dd"
			});

			$('#uploadBtn').click(function(event) {
				event.preventDefault();
				uploadForm();
			});

			$('#uploadProductBtn').click(function(event) {
				event.preventDefault();
				uploadProducts();
			});

			$('#submitBtn').click(function(event) {
				event.preventDefault();
				companySubmitForm();
			});

			$('#dealerSubmitBtn').click(function(event) {
				event.preventDefault();
				dealerSubmitForm();
			});

			$('#btn-download-comapnies').click(function(event) {
				event.preventDefault();
				window.location.href = '/companies/export/excel';
			});

			$('#btn-download-dealers').click(function(event) {
				event.preventDefault();
				window.location.href = '/dealers/export/excel';
			});

			$('#companyRegisteredAddressBtn').click(function(event) {
				$("#add-address").html("Company Registered Address");
			});

			$('#companyStorageAddressBtn').click(function(event) {
				$("#add-address").html("Company Storage Address");
			});


			$('#dealerRegisteredAddressBtn').click(function(event) {
				$("#add-address").html("Dealer Registered Address");
			});


			$('#dealerStorageAddressBtn').click(function(event) {
				$("#add-address").html("Dealer Storage Address");			
			});

			$('#zipCodeBtn').click(function(event) {
				event.preventDefault();
				searchByPinCode();
			});

			$('#btn-download-companyDealers').click(function(event) {
				event.preventDefault();
				window.location.href = '/companyDealers/export/excel';
			});

			$('#assignDealerSubmitBtn').click(function(event) {
				event.preventDefault();
				companyDealerSubmitForm();
			});

			$('#addNewProductBtn').click(function(event) {
				event.preventDefault();
				addNewProductForm();
			});

			$('#addAddressBtn').click(function(event) {
				var pinCode = $("#pincodeTextBox").val();
				var addressLine1 = $("#addressLine1").val();
				var Taluka = $("#selectBlock option:selected").text().split(":");

				var address={};
				address["AddressLine"] =addressLine1;
				address["Post"]=$("#selectVillage option:selected").text();
				address["Taluka"]=Taluka[1];
				address["District"]=$("#selectDistrict option:selected").text();
				address["State"]=$("#selectState option:selected").text();
				address["Country"] = "INDIA"
				address["Pincode"] = $("#pincodeTextBox").val();

				var currentModal = $("#add-address").text();

				if(currentModal=="Dealer Registered Address"){
					$("#registerOfficeAddress").val(JSON.stringify(address));
				} else if(currentModal=="Dealer Storage Address"){
					$("#storageAddress").val(JSON.stringify(address));
				} else if(currentModal=="Company Registered Address"){
					$("#companyRegAddress").val(JSON.stringify(address));
				}else if(currentModal=="Company Storage Address"){
					$("#companyStorageAddress").val(JSON.stringify(address));
				}
				$('#add-address-modal').modal('hide');
			});

			$(".Logout")
					.click(
							function() {
								$form = $("<form>")
										.attr(
												{
													"action" : "${pageContext.request.contextPath}"
															+ "/logout",
													"method" : "post"
												}).append($("<input>").attr({
											"type" : "hidden",
											"name" : "${_csrf.parameterName}",
											"value" : "${_csrf.token}"
										}))
								$(".Logout").append($form);
								$form.submit();
							});

			function companyDealerSubmitForm() {
				var companyDealerDTO = {};
				companyDealerDTO["companyId"] = $(
						"#companySelect option:selected").val()
				companyDealerDTO["dealerId"] = $(
						"#dealerSelect option:selected").val()
				companyDealerDTO["companyName"] = $(
						"#companySelect option:selected").text();
				companyDealerDTO["dealerName"] = $(
						"#dealerSelect option:selected").text();

				$
						.ajax({
							type : "POST",
							url : "/addCompanyDealer",
							contentType : "application/json",
							processData : false,
							data : JSON.stringify(companyDealerDTO),
							success : function(data) {
								console.log(data.actionPassed)
								if (data.actionPassed == false) {
									$("#assignDealerSubmitBtn").prop(
											"disabled", false);
									return;
								} else {
									$("#assignDealerSubmitBtn").prop(
											"disabled", false);
									$("#assignDealerSuccessDiv").fadeTo(2000,
											500).slideUp(500, function() {
										$("#dealerSuccessDiv").slideUp(500);
									});
									window.location.href = "/admin";
								}
							}
						});
			}

			function dealerSubmitForm() {
				var dealerDTO = {}
				dealerDTO["dealerName"] = $("#dealerName").val();
				dealerDTO["dealerEmail"] = $("#dealerEmail").val();

				dealerDTO["dealerLicenseNumber"] = $("#dealerLicenseNumber")
						.val();
				dealerDTO["dealerLicenseExpiryDate"] = $(
						"#dealerLicenseExpiryDate").val();

				dealerDTO["dealerPanNumber"] = $("#dealerPanNumber").val();
				dealerDTO["dealerGSTNumber"] = $('#dealerGSTNumber').val();

				dealerDTO["storageAddress"] = $("#storageAddress").val();
				dealerDTO["registerOfficeAddress"] = $("#registerOfficeAddress")
						.val();

				dealerDTO["proprietorName"] = $("#proprietorName").val();

				dealerDTO["dealerContactNumber"] = $("#dealerContactNumber")
						.val();

				$("#dealerSubmitBtn").prop("disabled", true);

				$.ajax({
					type : "POST",
					url : "/newdealer",
					contentType : "application/json",
					processData : false,
					data : JSON.stringify(dealerDTO),
					success : function(data) {
						console.log(data.actionPassed)
						if (data.actionPassed == false) {
							$("#dealerSubmitBtn").prop("disabled", false);
							return;
						} else {
							$("#dealerSubmitBtn").prop("disabled", false);
							$("#dealerSuccessDiv").fadeTo(2000, 500).slideUp(
									500, function() {
										$("#dealerSuccessDiv").slideUp(500);
									});
							window.location.href = "/admin";
						}
					}
				});
			}


			

			function uploadForm() {
				var form = $('#uploadForm')[0];
				var data = new FormData(form);
				$("#uploadBtn").prop("disabled", true);

				$.ajax({
					type : "POST",
					enctype : 'multipart/form-data',
					url : "/upload",
					contentType : false,
					processData : false,
					data : data,
					cache : false,
					timeout : 600000,
					success : function(data) {
						if (data.actionPassed == false) {
							$("#uploadBtn").prop("disabled", false);
							$("#uploadErrorDiv").show();
							return;
						} else {
							$('#principalCertificateFileName')
									.val(data.message);
							$('#uploadSuccessDiv')
									.html(
											"File " + data.message
													+ " upload success.");

							$("#uploadSuccessDiv").show();
							$("#uploadBtn").prop("disabled", false);
						}
					}
				});
			}

			function searchByPinCode() {
				$("#selectVillage").html("");
				$("#selectBlock").html("");
				$("#selectDistrict").html("");
				$("#selectState").html("");
				var pinCode = $("#pincodeTextBox").val();
				var divDistrict;
				var divState;
				$
						.ajax(
								{
									url : "https://api.postalpincode.in/pincode/"
											+ pinCode
								})
						.then(
								function(data) {

									if (data[0].Status == "Error") {
										alert("Enter Data Manually");
									} else {
										var postOfcName = {};
										$
												.each(
														data[0].PostOffice,
														function(index, value) {
															$
																	.each(
																			value,
																			function(
																					ind,
																					val) {
																				postOfcName[ind] = val;
																			});
															var divName = "<option value="+postOfcName.Name+">"
																	+ postOfcName.Name
																	+ "</option>";
															var divBlock = "<option value="+postOfcName.Block+">"
																	+ postOfcName.Name
																	+ ":"
																	+ postOfcName.Block
																	+ "</option>";
															divDistrict = "<option value="+postOfcName.District+">"
																	+ postOfcName.District
																	+ "</option>";
															divState = "<option value="+postOfcName.State+">"
																	+ postOfcName.State
																	+ "</option>";
															$(divName)
																	.appendTo(
																			'#selectVillage');
															$(divBlock)
																	.appendTo(
																			'#selectBlock');
														});
										$(divDistrict).appendTo(
												'#selectDistrict');
										$(divState).appendTo('#selectState');
									}
								});
			}


			function addNewProductForm() {
				
				var productDTO = {}
				productDTO["productName"] = $("#productName").val();

				if(productDTO.productName == ""){
		  				$("#addProductFailureDiv").html("Enter Product Name");

		  				$("#addProductFailureDiv").fadeTo(2000, 500).slideUp(
								500, function() {
									$("#addProductFailureDiv").slideUp(500);
								});
		  				
		  				return;
		  		}

					productDTO["packagingGram"] = $("#packagingGram").val();
				productDTO["price"] =$("#price").val();
				productDTO["availableItemCount"] = $("#availableItemCount").val();
				productDTO["category"] = $(
				"#addNewProductCategorySelect option:selected").val().trim();

				productDTO["productDealerId"] = $(
				"#productDealerSelect option:selected").val();

				productDTO["productCompanyId"] = $(
				"#productCompanyNameSelect option:selected").val();

				productDTO["productCompanyName"] = $(
				"#productCompanyNameSelect option:selected").text().trim();

				
				
				
				productDTO["productTag"] = $("#productTag").val();
				productDTO["description"] = $("#productDescriptionId").val();

				$.ajax({
					type : "POST",
					url : "/addNewProduct",
					contentType : "application/json",
					processData : false,
					data : JSON.stringify(productDTO),
					success : function(data) {
						console.log(data.actionPassed)
						if (data.actionPassed == false) {
							$("#addNewProductBtn").prop("disabled", false);
							return;
						} else {
							$("#addNewProductBtn").prop("disabled", false);

							$("#addProductSuccessDiv").html(data.message);
							$("#addProductSuccessDiv").show();
							
							$("#addProductSuccessDiv").fadeTo(2000, 500).slideUp(
									500, function() {
										$("#addProductSuccessDiv").slideUp(500);
									});

							
							
							window.location.href = "/admin";
						}
					}
				});
				
				
			}
			
			function uploadProducts() {
				var form = $('#uploadProductForm')[0];
				var data = new FormData(form);
				$.ajax({
					type : "POST",
					enctype : 'multipart/form-data',
					url : "/addProducts",
					contentType : false,
					processData : false,
					data : data,
					cache : false,
					timeout : 600000,
					success : function(data) {
						if (data.actionPassed == false) {
							$("#uploadProductBtn").prop("disabled", false);
							$("#uploadProductErrorDiv").show();
							return;
						} else {
							$("#uploadProductBtn").prop("disabled", false);
							$("#uploadProductSuccessDiv").fadeTo(2000, 500)
									.slideUp(
											500,
											function() {
												$("#uploadProductSuccessDiv")
														.slideUp(500);
											});
						}
					}
				});
			}

			function companySubmitForm() {

				var companyDTO = {}
				companyDTO["companyName"] = $("#companyName").val();
				companyDTO["companyEmailId"] = $("#companyEmailId").val();

				companyDTO["companyGSTNumber"] = $("#companyGSTNumber").val();
				companyDTO["companyPanNumber"] = $("#companyPanNumber").val();

				companyDTO["companyLicenseNo"] = $("#companyLicenseNo").val();
				companyDTO["companyLicenseExpiryDate"] = $(
						'#companyLicenseExpiryDate').val();

				companyDTO["companyAuthPersonName"] = $(
						"#companyAuthPersonName").val();

				companyDTO["companyRegAddress"] = $("#companyRegAddress").val();
				companyDTO["companyStorageAddress"] = $(
						"#companyStorageAddress").val();

				companyDTO["companyAuthPersonName"] = $(
						"#companyAuthPersonName").val();

				companyDTO["contactNumber"] = $("#contactNumber").val();

				$("#submitBtn").prop("disabled", true);
				$.ajax({
					type : "POST",
					url : "/newcompany",
					contentType : "application/json",
					processData : false,
					data : JSON.stringify(companyDTO),
					success : function(data) {
						console.log(data.actionPassed)
						if (data.actionPassed == false) {
							$("#submitBtn").prop("disabled", false);
							return;
						} else {
							$("#submitBtn").prop("disabled", false);
							$("#successDiv").fadeTo(2000, 500).slideUp(500,
									function() {
										$("#successDiv").slideUp(500);
									});
							window.location.href = "/admin";
						}
					}
				});
			}
		})(jQuery);
	</script>
</body>
</html>