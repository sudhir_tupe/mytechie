<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<!-- https://askbootstrap.com/preview/chpoee/index.html
 -->
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="TECHIES">
<meta name="author" content="TECHIES">
<title>AGROMART</title>
<!-- Favicon Icon -->
<link rel="icon" type="image/png" href="img/fav-icon.png">
<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Slider CSS -->
<link rel="stylesheet" href="vendor/slider/slider.css">
<!-- Select2 CSS -->
<link href="vendor/select2/css/select2-bootstrap.css" />
<link href="vendor/select2/css/select2.min.css" rel="stylesheet" />
<!-- Font Awesome-->
<link href="vendor/fontawesome/css/all.min.css" rel="stylesheet">
<link href="vendor/icofont/icofont.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css">
<link href='https://fonts.googleapis.com/css?family=Bree Serif' rel='stylesheet'>
</head>

<body>
	
	<div class="modal fade login-modal-main" id="login">
		<div class="modal-dialog modal-lg modal-dialog-centered"
			role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="login-modal">
						<div class="row">
							<div class="col-lg-6 d-flex align-items-center">
								<div class="login-modal-left p-4 text-center pl-5">
									<img src="img/login2.png" alt="techies">
								</div>
							</div>
							<div class="col-lg-6">
								<button type="button"
									class="close close-top-right position-absolute"
									data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true"><i class="icofont-close-line"></i></span>
									<span class="sr-only">Close</span>
								</button>
								            
								
								
								<form:form class="position-relative" modelAttribute="user" method="post"
									action="/login">
									<ul
										class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute"
										role="tablist">
										<li><a class="nav-link-login active" data-toggle="tab"
											href="#login-form" role="tab"><i class="icofont-ui-lock"></i>
												LOGIN</a></li>
										<li><a class="nav-link-login" data-toggle="tab"
											href="#register" role="tab"><i
												class="icofont icofont-pencil"></i> REGISTER</a></li>
									</ul>
									<div class="login-modal-right p-4">
										<!-- Tab panes -->
										<div class="tab-content">
											<div class="tab-pane active" id="login-form" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">LOGIN</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label>

													<form:input type="text" path="username"
														class="form-control" id="lg_username"
														placeholder="Your username" />
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label>
													<form:input type="password" path="password"
														class="form-control" id="lg_password"
														placeholder="Your Password" />
												</fieldset>
												<fieldset class="form-group">
													<button type="submit" id="btn-login"
														class="btn btn-lg btn-primary btn-block">Enter to
														your account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck1"> <label
														class="custom-control-label" for="customCheck1">Remember
														me</label>
												</div>
												<div id="errorDiv"></div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button type="reset"
																class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button type="reset"
																class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="register" role="tabpanel">
												<h5 class="heading-design-h5 text-dark">REGISTER</h5>
												<fieldset class="form-group mt-4">
													<label>Enter Email/Mobile number</label> <input type="text"
														class="form-control" placeholder="+91 123 456 7890">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Password</label> <input type="password"
														class="form-control" placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<label>Enter Confirm Password </label> <input
														type="password" class="form-control"
														placeholder="********">
												</fieldset>
												<fieldset class="form-group">
													<button type="submit"
														class="btn btn-lg btn-primary btn-block">Create
														Your Account</button>
												</fieldset>
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input"
														id="customCheck2"> <label
														class="custom-control-label" for="customCheck2">I
														Agree with <a href="#">Term and Conditions</a>
													</label>
												</div>
												<div class="login-with-sites mt-4">
													<p class="mb-2">or Login with your social profile:</p>
													<div class="row text-center">
														<div class="col-6 pr-1">
															<button type="reset"
																class="btn-facebook btn-block login-icons btn-lg">
																<i class="icofont icofont-facebook"></i> Facebook
															</button>
														</div>
														<div class="col-6 pl-1">
															<button type="reset"
																class="btn-google btn-block login-icons btn-lg">
																<i class="icofont icofont-google-plus"></i> Google
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-light">
		<div class="header-top border-bottom bg-white">
			<div class="container">
			</div>
		</div>
		<div class="main-nav shadow-sm">
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary pt-0 pb-0">
				<div class="container">
					<a class="navbar-brand" href="index.html"> <img
						src="img/logo8.png" alt="gurdeep osahan designer">
					</a> <a class="toggle" href="#"> <span></span>
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto main-nav-left">
							<li class="nav-item"><a class="nav-link" href="/"><font color="#fff"><i
									class="icofont-ui-home"></i></font></a></li>

							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <font color="#fff">VEGGIE CROPS </font> </a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">Vegetables</a>
												<a href="product-grid.html">ONION</a> <a
													href="product-grid.html">BHENDI</a> <a
													href="product-grid.html">TOMATO</a> <a
													href="product-grid.html">BITTER GOURD</a> <a
													href="product-grid.html">BOTTLE GOURD</a> <a
													href="product-grid.html">BRINJAL</a> <a
													href="product-grid.html">CHILLI</a> <a
													href="product-grid.html">CUCUMBER</a> <a
													href="product-grid.html">CAULIFLOWER</a> <a
													href="product-grid.html">CABBAGE</a>

											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">Root
													Vegetables</a> <a href="product-grid.html"> POTATO <img
													src="img/potato.png" alt="POTATO" /></a> <a
													href="product-grid.html"> CARROT <img
													src="img/carrot.png" alt="CARROT" /></a> <a
													href="product-grid.html"> RADISH <img
													src="img/radish.png" alt="RADISH" /></a> <a
													href="product-grid.html"> BEET ROOT<img
													src="img/beetroot.png" alt="BEET" /></a> <a
													href="product-grid.html">GINGER <img
													src="img/ginger.png" alt="GINGER" /></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">Leafy
													Vegetables</a> <a href="product-grid.html">CORIANDER</a> <a
													href="product-grid.htproduct-grid.htmll">PALAK</a> <a
													href="product-grid.html">FENUGREEK</a> <a
													href="product-grid.html">MINT PUDINA</a> <a
													href="product-grid.html">COLOCASIA LEAF</a> <a
													href="product-grid.html">CAPSICUM</a> <a
													href="product-grid.html"></a> <a href="product-grid.html"></a>
												<a href="product-grid.html"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">FIBRE AND
													OILS</a> <a href="product-grid.html">COTTON</a> <a
													href="product-grid.htproduct-grid.htmll">SUNFLOWER</a> <a
													href="product-grid.html">SOYABEAN</a> <a
													href="product-grid.html">MUSTARD</a> <a
													href="product-grid.html"></a> <a href="product-grid.html"></a>
												<a href="product-grid.html"></a> <a href="product-grid.html"></a>
												<a href="product-grid.html"></a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">CEREALS
													AND PULSES</a> <a href="product-grid.html"> BAJRA</a> <a
													href="product-grid.html"> JOWAR</a> <a
													href="product-grid.html">WHEAT</a> <a
													href="product-grid.html">PADDY</a> <a
													href="product-grid.html">MAIZE</a> <a
													href="product-grid.html">GRAM</a> <a
													href="product-grid.html">PIGEON-PEA (TUR)</a> <a
													href="product-grid.html">GREEN GRAM</a> <a
													href="product-grid.html">BLACK GRAM</a>
											</div>
										</div>
										<div class="col-lg-2 col-md-2">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">GARDENING</a>
												<a href="product-grid.html"> LOTUS FLOWER</a> <a
													href="product-grid.html"> FRENCH MARIGOLD</a> <a
													href="product-grid.html">AFRICON MARIGOLD</a> <a
													href="product-grid.html">ICE FLOWER</a> <a
													href="product-grid.html">CHERRY TOMATO</a> <a
													href="product-grid.html"></a> <a href="product-grid.html"></a>
												<a href="product-grid.html"></a> <a href="product-grid.html"></a>
											</div>
										</div>

									</div>
								</div></li>
							<li class="nav-item dropdown mega-drop-main"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"><font color="#fff"> AGRO CHEMICALS</font></a>
								<div class="dropdown-menu mega-drop  shadow-sm border-0"
									aria-labelledby="navbarDropdown">
									<div class="row ml-0 mr-0">
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">PESTICIDES</a>
												<a href="product-grid.html">T-Shirts</a> <a
													href="product-grid.html">Casual Shirts</a> <a
													href="product-grid.html">Formalproduct-grid.htmlShirts</a>
												<a href="product-grid.html">Sweatshirts</a> <a
													href="product-grid.html">Sweaters</a> <a
													href="product-grid.html">Jackets</a> <a
													href="product-grid.html">Blazers & Coats</a> <a
													href="product-grid.html">Suits</a> <a
													href="product-grid.html">Rain Jackets</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">INSECTICIDES</a>
												<a href="product-grid.html">Sweaters</a> <a
													href="product-grid.html">Jackets</a> <a
													href="product-grid.html">Blazers & Coats</a> <a
													href="product-grid.html">Suits</a> <a
													href="product-grid.html">Rain Jackets</a> <a
													href="product-grid.html">T-Shirts</a> <a
													href="product-grid.html">Casual Shirts</a> <a
													href="product-grid.html">Formal Shirts</a> <a
													href="product-grid.html">Sweatshirts</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">HERBICIDES</a>
												<a href="product-grid.html"> Jeans</a> <a
													href="product-grid.html">Casual Trousers</a> <a
													href="product-grid.html">Formal Trousers</a> <a
													href="product-grid.html">Shorts</a> <a
													href="product-grid.html">Track Pants & Joggers</a> <a
													href="product-grid.html">T-Shirts</a> <a
													href="product-grid.html">Sweatshirts</a> <a
													href="product-grid.html">Casual Shirts</a> <a
													href="product-grid.html">Formal Shirts</a>
											</div>
										</div>
										<div class="col-lg-3 col-sm-3 col-xs-3 col-md-3">
											<div class="mega-list">
												<a class="mega-title" href="product-grid.html">FUNGICIDES</a>
												<a href="product-grid.html">Casual Shoes</a> <a
													href="product-grid.html">Sports Shoes</a> <a
													href="product-grid.html">Formal Shoes</a> <a
													href="product-grid.html">Sneakers</a> <a
													href="product-grid.html">Sweatshirts</a> <a
													href="product-grid.html">Sandals & Floaters</a> <a
													href="product-grid.html">Flip Flops</a> <a
													href="product-grid.html">Socks</a> <a
													href="product-grid.html">Formal Shirts</a>
											</div>
										</div>
									</div>
								</div></li>
							<!--  <li class="nav-item">
                        <a class="nav-link" href="product-grid.html">SALE</a>
                     </li> -->
							<li class="nav-item dropdown"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <font color="#fff">PRODUCTS</font> </a>
								<div
									class="dropdown-menu dropdown-menu-right shadow-sm border-0">
									<a class="dropdown-item" href="product-grid.html">
									VEGETABLE CROPS</a> <a class="dropdown-item" href="product-detail.html">FIELD
										CROPS</a> <a class="dropdown-item" href="product-detail.html">AGRO
										CHEMICALS</a> <a class="dropdown-item" href="product-detail.html">MICRONUTRIENTS</a>
									<a class="dropdown-item" href="product-detail.html">PLANTS
										GROWTH REGULATORS</a> <a class="dropdown-item"
										href="checkout.html">Checkout</a> <a class="dropdown-item"
										href="profile.html">My Account</a> <a class="dropdown-item"
										href="about-us.html">About Us</a> <a class="dropdown-item"
										href="faq.html">FAQ</a> <a class="dropdown-item"
										href="contact-us.html">Contact Us</a>
								</div></li>
						</ul>
						<form class="form-inline my-2 my-lg-0 top-search">
							<button class="btn-link" type="submit">
								<i class="icofont-search"></i>
							</button>
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search for products, brands and more"
								aria-label="Search">
						</form>
						<ul class="navbar-nav ml-auto profile-nav-right">
							<li class="nav-item"><a href="#" data-target="#login"
								data-toggle="modal" class="nav-link ml-0"><font color="#fff"> <i
									class="icofont-ui-user"></i> Login/Sign Up
							</font></a></li>
							<li class="nav-item cart-nav"><a data-toggle="offcanvas"
								class="nav-link" href="#"><font color="#fff">  <i class="icofont-basket"></i>
									Cart <span class="badge badge-danger">5</span></font>
							</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="py-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<header>
							<div id="owl-carousel-one" class="owl-carousel">
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/bg2.jpg">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/bg3.jpg">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/bg2.jpg">
								</div>
								<div class="item">
									<img class="img-fluid mx-auto rounded shadow-sm"
										src="img/banner/bg3.png">
								</div>
							</div>
						</header>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="product-list pbc-5 pb-4 pt-5 bg-light">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark font-weight-normel">Best Selling
				Items</h4>
			<div class="row">
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-danger">NEW</span> <img src="img/item/11.jpeg"
							class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/22.jpeg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$ 135.00 <span class="text-black-50"><del>$500.00 </del></span>
								<span
									class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
									50% OFF</span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a class="active" href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-danger">NEW</span> <img src="img/item/33.jpeg"
							class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$ 135.00 <span class="text-black-50"><del>$500.00 </del></span>
								<span class="bg-info rounded-sm pl-1 ml-1 pr-1 text-white small">
									50% OFF</span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/44.jpeg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<a href="#"> <span class="badge badge-danger">NEW</span> <img
							src="img/item/55.jpeg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a class="active" href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/66.jpeg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-danger">NEW</span> <img src="img/item/77.jpeg"
							class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-6 col-md-3">
					<div
						class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
						<span class="like-icon"><a href="#"> <i
								class="icofont icofont-heart"></i></a></span> <a href="#"> <span
							class="badge badge-success">50% OFF</span> <img
							src="img/item/88.jpeg" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<h6 class="card-title mb-1">Floret Printed Ivory Skater
								Dress</h6>
							<div class="stars-rating">
								<i class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star active"></i><i
									class="icofont icofont-star"></i> <span>613</span>
							</div>
							<p class="mb-0 text-dark">
								$135.00 <span class="text-black-50"><del>$500.00 </del></span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
	<section class="offer-product py-5">
		<div class="container">
			<div class="row">
				<div class="col-6">
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/ad/1.png" alt=""></a>
					</div>
				</div>
				<div class="col-6">
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/ad/2.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="product-list pbc-5 pb-4 pt-5 bg-light">
		<div class="container">
			<h6 class="mt-1 mb-0 float-right">
				<a href="#">View All Items</a>
			</h6>
			<h4 class="mt-0 mb-3 text-dark">New Arrivals Today</h4>
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-carousel-category owl-theme">
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-danger">NEW</span> <img src="img/item/111.jpg"
									class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Floret Printed Ivory Skater
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$ 135.00 <span class="text-black-50"><del>$500.00
											</del></span> <span
											class="bg-danger  rounded-sm pl-1 ml-1 pr-1 text-white small">
											50% OFF</span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-success">50% OFF</span> <img
									src="img/item/2.jpg" class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Floret Printed Ivory Skater
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a class="active" href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-danger">NEW</span> <img src="img/item/3.jpg"
									class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Floret Printed Ivory Skater
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-success">50% OFF</span> <img
									src="img/item/4.jpg" class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Floret Printed Ivory Skater
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-danger">NEW</span> <img src="img/item/5.jpg"
									class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Floret Printed Ivory Skater
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div
								class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
								<span class="like-icon"><a href="#"> <i
										class="icofont icofont-heart"></i></a></span> <a href="#"> <span
									class="badge badge-success">50% OFF</span> <img
									src="img/item/6.jpg" class="card-img-top" alt="..."></a>
								<div class="card-body">
									<h6 class="card-title mb-1">Floret Printed Ivory Skater
										Dress</h6>
									<div class="stars-rating">
										<i class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star active"></i><i
											class="icofont icofont-star"></i> <span>613</span>
									</div>
									<p class="mb-0 text-dark">
										$135.00 <span class="text-black-50"><del>$500.00
											</del></span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="py-5">
		<div class="container">
			<div class="row">
				<div class="col-4">
					<div class="offers-block">
						<a href="#"> <img class="img-fluid" src="img/offer-1.png"
							alt=""></a>
					</div>
				</div>
				<div class="col-4">
					<div class="offers-block">
						<a href="#"><img class="img-fluid mb-3" src="img/offer-3.png"
							alt=""></a>
					</div>
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/offer-4.png"
							alt=""></a>
					</div>
				</div>
				<div class="col-4">
					<div class="offers-block">
						<a href="#"><img class="img-fluid" src="img/offer-2.png"
							alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer -->
	<footer class="bg-white border-bottom border-top">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-md-4">
					<div class="border-right py-5 pr-5">
						<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">TOP
							CATEGORIES</h6>
						<div class="row no-gutters">
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">Vegetable Seeds</a></li>
									<li><a href="#"> Field Crops Seeds </a></li>
									<li><a href="#">Water Soluble fertilisers</a></li>
									<li><a href="#">Organic Products </a></li>
									<li><a href="#">Pesticides</a></li>
								</ul>
							</div>
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#"> Agri Equipments</a></li>
									<li><a href="#">Micronutrients</a></li>
									<li><a href="#">Insecticides</a></li>
									<li><a href="#">Herbicides</a></li>
									<li><a href="#"> Bio Pesticides</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="border-right py-5 px-5">
						<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">ABOUT
							US</h6>
						<div class="row no-gutters">
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">History</a></li>
									<li><a href="#">Band of Trust</a></li>
									<li><a href="#">Brand Guidelines</a></li>
									<li><a href="#">TV Commercials</a></li>
									<li><a href="#">In the News </a></li>
								</ul>
							</div>
							<div class="col-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">Awards</a></li>
									<li><a href="#">Terms & Conditions</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Careers</a></li>
									<li><a href="#">Offers</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="py-5 pl-5">
						<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">DOWNLOAD
							APP</h6>
						<div class="app">
							<a href="#"> <img class="img-fluid" src="img/google.png">
							</a> <a href="#"> <img class="img-fluid" src="img/apple.png">
							</a>
						</div>
						<h6 class="mt-4 mb-4 f-14 text-dark font-weight-bold">KEEP IN
							TOUCH</h6>
						<div class="footer-social">
							<a class="btn-facebook" href="#"><i class="icofont-facebook"></i></a>
							<a class="btn-twitter" href="#"><i class="icofont-twitter"></i></a>
							<a class="btn-instagram" href="#"><i
								class="icofont-instagram"></i></a> <a class="btn-whatsapp" href="#"><i
								class="icofont-whatsapp"></i></a> <a class="btn-messenger" href="#"><i
								class="icofont-facebook-messenger"></i></a> <a class="btn-google"
								href="#"><i class="icofont-google-plus"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</footer>
	<div class="popular-tag py-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h6 class="mt-0 mb-4 f-14 text-dark font-weight-bold">POPULAR
						SEARCHES</h6>
					<p class="mb-0">
						<a href="#">Seeds </a> &nbsp; | &nbsp; 
<a href="#">Flower Seeds </a> &nbsp; | &nbsp; 
<a href="#">Vegetable Seeds </a> &nbsp; | &nbsp; 
<a href="#">Field Crops Seeds </a> &nbsp; | &nbsp; 
<a href="#">Fertilisers </a> &nbsp; | &nbsp; 
<a href="#">Water Soluble fertilisers</a> &nbsp; | &nbsp;  
<a href="#">Organic Products </a> &nbsp; | &nbsp; 
<a href="#">Pesticides </a> &nbsp; | &nbsp; 
<a href="#">Agri Equipments </a> &nbsp; | &nbsp; 
<a href="#">Micronutrients </a> &nbsp; | &nbsp; 
<a href="#">Insecticides </a> &nbsp; | &nbsp; 
<a href="#">Fungicides </a> &nbsp; | &nbsp; 
<a href="#">Herbicides </a> &nbsp; | &nbsp; 
<a href="#">Bio Pesticides </a> &nbsp; | &nbsp; 
<a href="#">Surfactants </a> &nbsp; | &nbsp; 
<a href="#">Organic Premium PGR </a> &nbsp; | 
<a href="#">Organic Coated Granules </a> &nbsp; | &nbsp; 
<a href="#">Organic PGR Technicals </a> &nbsp; | &nbsp; 
<a href="#">Micronutrients Spray </a> &nbsp; | &nbsp; 
<a href="#">Individual Micronutrient </a> &nbsp; | &nbsp; 
<a href="#">Micronutrient Mixture </a> &nbsp; | &nbsp; 
<a href="#">Organic Neem Products </a> &nbsp; | &nbsp; 
<a href="#">Organic Stimulant </a> &nbsp; | &nbsp; 
<a href="#">Starters </a> &nbsp; | &nbsp; 
<a href="#">Pumps </a> &nbsp; | &nbsp; 
<a href="#">Agro Sprayers </a> &nbsp; | &nbsp; 
<a href="#">Agro Shed Nets </a> &nbsp; | &nbsp; 
<a href="#">House Hold Insecticides </a> &nbsp; | &nbsp; 
<a href="#">Drip Irrigation </a> &nbsp; | &nbsp; 
<a href="#">Pulses & Beans </a> &nbsp; | &nbsp; 
<a href="#">Home / kitchen Garden Seeds </a> &nbsp; | &nbsp; 
<a href="#">Mulching Film </a> &nbsp; | &nbsp; 
<a href="#">Cotton Seeds</a> &nbsp;
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright bg-light py-3">
		<div class="container">
			<div class="row">
				<div class="col-md-6 d-flex align-items-center">
					<p class="mb-0">
						� Copyright 2020 <a href="#">TECHIES</a> . All Rights Reserved
					</p>
				</div>
				<div class="col-md-6 text-right">
					<img class="img-fluid" src="img/payment_methods.png">
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1"
		role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalCenterTitle">Search...</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<input class="form-control form-control-lg mb-3" type="text"
						placeholder="Search for products, brands and more">
					<button type="button" class="btn btn-primary btn-block btn-lg">Search</button>
				</div>

			</div>
		</div>
	</div>

	<div class="footer-fix-nav shadow">
		<div class="row">
			<div class="col">
				<a href="#"><i class="icofont icofont-home"></i></a>
			</div>
			<div class="col border-0">
				<a type="button" data-toggle="modal"
					data-target="#exampleModalCenter" href="#"><i
					class="icofont icofont-search"></i></a>
			</div>
			<div class="col active">
				<a href="#"><i class="icofont icofont-heart"></i></a>
			</div>
			<div class="col">
				<a href="#"><i class="icofont icofont-tag"></i></a>
			</div>
			<div class="col">
				<a href="#"><i class="icofont icofont-bag"></i></a>
			</div>
		</div>
	</div>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- select2 Js -->
	<script src="vendor/select2/js/select2.min.js"></script>
	<!-- Owl Carousel -->
	<script src="vendor/owl-carousel/owl.carousel.js"></script>
	<!-- Slider Js -->
	<script src="vendor/slider/slider.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="js/custom.js"></script>
	<script src="js/hc-offcanvas-nav.js?ver=4.1.1"></script>
	<link rel="stylesheet" href="js/demo.css?ver=3.4.0">
	<link rel="stylesheet"
		href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<script>
      (function($) {
        var $main_nav = $('#main-nav');
        var $toggle = $('.toggle');

        var defaultOptions = {
          disableAt: false,
          customToggle: $toggle,
          levelSpacing: 40,
          navTitle: 'All Categories',
          levelTitles: true,
          levelTitleAsBack: true,
          pushContent: '#container',
          insertClose: 2
        };

        // call our plugin
        var Nav = $main_nav.hcOffcanvasNav(defaultOptions);

        // add new items to original nav
        $main_nav.find('li.add').children('a').on('click', function() {
          var $this = $(this);
          var $li = $this.parent();
          var items = eval('(' + $this.attr('data-add') + ')');

          $li.before('<li class="new"><a href="#">'+items[0]+'</a></li>');

          items.shift();

          if (!items.length) {
            $li.remove();
          }
          else {
            $this.attr('data-add', JSON.stringify(items));
          }

          Nav.update(true);
        });

        // demo settings update

        const update = (settings) => {
          if (Nav.isOpen()) {
            Nav.on('close.once', function() {
              Nav.update(settings);
              Nav.open();
            });

            Nav.close();
          }
          else {
            Nav.update(settings);
          }
        };

        $('.actions').find('a').on('click', function(e) {
          e.preventDefault();

          var $this = $(this).addClass('active');
          var $siblings = $this.parent().siblings().children('a').removeClass('active');
          var settings = eval('(' + $this.data('demo') + ')');

          update(settings);
        });

        $('.actions').find('input').on('change', function() {
          var $this = $(this);
          var settings = eval('(' + $this.data('demo') + ')');

          if ($this.is(':checked')) {
            update(settings);
          }
          else {
            var removeData = {};
            $.each(settings, function(index, value) {
              removeData[index] = false;
            });

            update(removeData);
          }
          
        });

        $('#btn-login').click(
				function(event) {

					event.preventDefault();
					var user = {};
					user["username"] = $("#lg_username").val();
					user["password"] = $("#lg_password").val();

					$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "/login",
						data : JSON.stringify(user),
						success : function(data) {
							if (data.actionPassed == false) {
								var message="Incorrect username or password";
								var errorMsg = "<div id='errorDiv' class='alert alert-danger'>"
									+ message + "</div>"
									$('#errorDiv').html(errorMsg);
								return;
							} else {
								window.location.href= data.successAction;
							}
						}
					});

				});
        
      })(jQuery);
    </script>
</body>

</html>