<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>AgroMart</title>
<meta content="TECHIES" name="descriptison">
<meta content="techies" name="keywords">

<!-- Favicons -->
<link href="assets/img/favicon.png" rel="icon">
<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">
<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
.tableDiv {
	border-collapse: collapse;
	border-spacing: 0;
}
</style>
</head>
<body>
	<!-- ======= Header ======= -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<a class="navbar-brand" href="#">AgroMart</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="#">Home
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Link</a></li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> Dropdown </a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="#">Action</a> <a
								class="dropdown-item" href="#">Another action</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">Something else here</a>
						</div></li>
					<li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0">
					<input class="form-control mr-sm-2" type="search"
						placeholder="Search" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
			</div>
		</nav>
	</header>




	<!-- 	https://freefrontend.com/bootstrap-forms/
 -->
	<div class="container" align="center">
		<br>
		<table border="0">

			<form:form modelAttribute="companyDTO" id="frm" action="/upload"
				enctype="multipart/form-data">
				<tr>
					<td colspan="1"><h5>
							<center>Register New Company</center>
						</h5></td>
				</tr>
				<tr>
					<td>
						<div class="form-row">
							<div class="col-md-6 form-group">
								<form:input type="text" path="companyName" class="form-control"
									id="companyName" placeholder="Company Name" />
							</div>
							<div class="col-md-6 form-group">
								<form:input type="email" path="companyEmailId" name="companyEmailId"
									class="form-control" id="companyEmailId"
									placeholder="Company Email" />
							</div>
						</div>


						<div class="form-row">
							<div class="col-md-6 form-group">
								<form:input type="text" path="companyGSTNumber"
									class="form-control" id="companyGSTNumber"
									placeholder="GST Number" />
							</div>
							<div class="col-md-6 form-group">
								<form:input type="email" path="companyPanNumber"
									class="form-control" id="companyPanNumber"
									placeholder="PAN Number" />
							</div>
						</div>

						<div class="form-row">

							<div class="col-md-6 form-group">
								<form:input type="email" path="companyLicenseNo"
									class="form-control" id="companyLicenseNo"
									placeholder="License Number" />
							</div>
							<div class="col-md-6 form-group">
								<form:input type="text" id="companyLicenseExpiryDate"
									value="License Validity" path="companyLicenseExpiryDate"
									class="form-control" />
							</div>
						</div>

						<div class="form-row">

							<div class="col-md-6 form-group">
								<form:input type="email" path="companyAuthPersonName"
									class="form-control" id="companyAuthPersonName"
									placeholder="Auth Person Name" />
							</div>
							<div class="col-md-6 form-group">
								<form:input type="text" path="contactNumber"
									class="form-control" id="contactNumber"
									placeholder="Authorised Person No" />
							</div>
						</div>


						<div class="form-group">
							<form:input type="email" path="companyRegAddress"
								class="form-control" id="companyRegAddress"
								placeholder="Company Registered Address" />
						</div>

						<div class="form-group">
							<form:input type="text" path="companyStorageAddress"
								class="form-control" id="companyStorageAddress"
								placeholder="Company Storage Address" />
						</div>
					</td>
				</tr>
				<form:hidden path="principalCertificateFileName" id="principalCertificateFileName"/>
			</form:form>
			<tr>
				<td><small id="emailHelp" class="form-text text-muted">Upload
						Principal Certificate</small>
					<div>

						<form method="POST" action="/upload" enctype="multipart/form-data"
							id="uploadForm">

							<input type="file" name="file" />
							<button type="submit" id="uploadBtn">Upload</button>
						</form>
					</div>
					<br>
					<div id="uploadSuccessDiv" class="alert alert-success"></div>
					<div id="uploadErrorDiv" class="hide"></div></td>
			</tr>
			<tr>
				<td><br>
					<button type="submit" id="submitBtn" class="btn btn-primary">Register</button>
					
					<div id="successDiv" class="alert alert-success">Company
						Registered successfully</div>
					<div id="errorDiv" class="hide"></div></td>
			</tr>
		</table>
	</div>


	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/counterup/counterup.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>

	<script>
		$(document).ready(function() {
			
			$("#companyLicenseExpiryDate").datepicker({
				  dateFormat: "yy-mm-dd"
			});
			
			$("#successDiv").hide();
			$("#errorDiv").hide();

			$("#uploadSuccessDiv").hide();
			$("#uploadErrorDiv").hide();

			$('#submitBtn').click(function(event) {
				event.preventDefault();
				fire_ajax_submit();
			});

			$('#uploadBtn').click(function(event) {
				event.preventDefault();
				uploadForm();
			});

		});

		function uploadForm() {

			var form = $('#uploadForm')[0];

			var data = new FormData(form);
			
			$("#uploadBtn").prop("disabled", true);

			$.ajax({
				type : "POST",
				enctype : 'multipart/form-data',
				url : "/upload",
				contentType : false,
				processData : false,
				data : data,
				cache : false,
				timeout : 600000,
				success : function(data) {
					if (data.successAction == 'false') {
						$("#uploadBtn").prop("disabled", false);
						$("#uploadErrorDiv").show();
						return;
					} else {
						$('#principalCertificateFileName').val(data.message);
						$('#uploadSuccessDiv').html("File "+data.message+" upload success.");
						
						$("#uploadSuccessDiv").show();
						$("#uploadBtn").prop("disabled", false);
					}
				}
			});
		}

		function fire_ajax_submit() {

			var companyDTO = {}
			companyDTO["companyName"] = $("#companyName").val();
			companyDTO["companyEmailId"] = $("#companyEmailId").val();

			companyDTO["companyGSTNumber"] = $("#companyGSTNumber").val();
			companyDTO["companyPanNumber"] = $("#companyPanNumber").val();

			companyDTO["companyLicenseNo"] = $("#companyLicenseNo").val();
			companyDTO["companyLicenseExpiryDate"] = $('#companyLicenseExpiryDate').val();

			companyDTO["companyAuthPersonName"] = $("#companyAuthPersonName")
					.val();

			companyDTO["companyRegAddress"] = $("#companyRegAddress").val();
			companyDTO["companyStorageAddress"] = $("#companyStorageAddress")
					.val();

			companyDTO["companyAuthPersonName"] = $("#companyAuthPersonName")
					.val();

			companyDTO["contactNumber"] = $("#contactNumber").val();
			companyDTO["principalCertificateFileName"] = $("#principalCertificateFileName").val();

			$("#submitBtn").prop("disabled", true);
			$.ajax({
				type : "POST",
				url : "/register",
				contentType : "application/json",
				processData : false,
				data : JSON.stringify(companyDTO),
				success : function(data) {
					if (data.successAction == 'false') {
						$("#submitBtn").prop("disabled", false);
						return;
					} else {
						$("#submitBtn").prop("disabled", false);
						$("#successDiv").fadeTo(2000, 500).slideUp(500,
								function() {
									$("#successDiv").slideUp(500);
								});
					}
				}
			});
		}
	</script>
</body>

</html>